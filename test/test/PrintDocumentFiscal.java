/**
 * @finalidad 
 * @author Yamel Senih
 * @date 04/05/2012
 */
package test;

import java.util.logging.Level;

import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MUser;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.sg.model.MXXDocFiscalPrinterNo;
import org.sg.model.MXXFiscalPrinterConfig;

/**
 * @author Yamel Senih 04/05/2012, 17:11:09
 *
 */
public class PrintDocumentFiscal extends SvrProcess {

	private String 			message;
	
	//private String 			trxName = null;
	private Trx 			trx = null;
	
	private int				p_C_Invoice_ID = 0;
	private int				p_AD_User_ID = 0;
	//private int				p_XX_LODocGenerated_ID = 0;
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("C_Invoice_ID"))
				p_C_Invoice_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		if(p_C_Invoice_ID == 0){
			p_C_Invoice_ID = getRecord_ID();
		}
		p_AD_User_ID = getAD_User_ID();
		//trxName = Trx.createTrxName("PF");
		trx = Trx.get(get_TrxName(), false);
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		try {
			printDocument();
			//trx.commit();
		} catch (Exception e) {
			message = e.getMessage();
			trx.rollback();
		}
		return message;
	}

	/**
	 * Imprime el Documento Fiscal
	 * @author Yamel Senih 06/04/2012, 14:31:21
	 * @throws Exception
	 * @return void
	 */
	private void printDocument() throws Exception {
		if(p_C_Invoice_ID != 0){
			MInvoice m_invoice = new MInvoice(getCtx(), p_C_Invoice_ID, get_TrxName());
			if(m_invoice.getDocStatus().equals(DocAction.STATUS_Completed)){
				if(!m_invoice.get_ValueAsBoolean("XX_PrintFiscalDocument")){
					MUser user = new MUser(getCtx(), p_AD_User_ID, get_TrxName());
					int m_XX_FiscalPrinterConfig_ID = user.get_ValueAsInt("XX_FiscalPrinterConfig_ID");
					if(m_XX_FiscalPrinterConfig_ID != 0){
						MDocType m_DocType = new MDocType(getCtx(), m_invoice.getC_DocType_ID(), get_TrxName());
						//	Fiscal Document
						int m_XX_DocFiscalPrinterNo_ID = m_DocType.get_ValueAsInt("XX_DocFiscalPrinterNo_ID");
						if(m_XX_DocFiscalPrinterNo_ID != 0){
							//	Load Fiscal No
							//MXXDocFiscalPrinterNo docNoFiscal = new MXXDocFiscalPrinterNo(getCtx(), m_XX_DocFiscalPrinterNo_ID, trxName);
							
							MXXFiscalPrinterConfig m_fpConfig = new MXXFiscalPrinterConfig(getCtx(), m_XX_FiscalPrinterConfig_ID, get_TrxName());
							/*HandlerSpooler hsp = new HandlerSpooler(
									m_fpConfig.getPuertoRS(), 
									m_fpConfig.getXX_Speed(), 
									m_fpConfig.getXX_PathSpooler(), 
									m_fpConfig.getXX_PathLog(), 
									m_fpConfig.getXX_PathTmp(), 
									m_fpConfig.getXX_OS_CMD(),
									m_fpConfig.getPrinterId(), 
									m_fpConfig.getXX_FSeparator());
							
							MBPartner cp = new MBPartner(getCtx(), m_invoice.getC_BPartner_ID(), trxName);
							
							//	Document Affected
							int m_DA_ID = m_invoice.get_ValueAsInt("XX_DocAffected");
							
							 String nroVoucher = null;
							 String codPrinter = null;
							 String dateVoucher = null;
							 String timeVoucher = null;
							 
							if(m_DA_ID != 0){
								MInvoice m_XX_DocAffected = new MInvoice(getCtx(), m_DA_ID, trxName);
								nroVoucher = m_XX_DocAffected.get_ValueAsString("XX_FiscalNo");
								codPrinter = m_XX_DocAffected.get_ValueAsString("PrinterId");
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyMMdd");
								SimpleDateFormat sdfTime = new SimpleDateFormat("hhmmss");
								Timestamp dV = m_XX_DocAffected.getDateInvoiced();//_ValueAsString("PrinterId");
								dateVoucher = sdfDate.format(dV);
								timeVoucher = sdfTime.format(dV);
								if(nroVoucher == null || nroVoucher.equals(""))
									throw new Exception(Msg.translate(getCtx(), "SGNotDocAff"));	//	No existe Codigo del Documento Afectado
								if(codPrinter == null || codPrinter.equals(""))
									throw new Exception(Msg.translate(getCtx(), "SGNotPrinterIDDocAff"));	//	No existe Codigo de Impresora en el Documento Afectado
							}
							
							hsp.printHeader(cp.getName(), cp.getValue(), nroVoucher, codPrinter, dateVoucher, timeVoucher, docNoFiscal.getXX_DocBaseType_Fiscal());
							
							MInvoiceLine [] lInvoices = m_invoice.getLines();
							for (MInvoiceLine m_InvoiceLine : lInvoices) {
								String nameItem = null;
								String valueItem = null;
								if(m_InvoiceLine.getM_Product_ID() != 0){
									MProduct product = new MProduct(getCtx(), m_InvoiceLine.getM_Product_ID(), trxName);
									nameItem = product.getName();
									valueItem = product.getValue();	
								} else if(m_InvoiceLine.getC_Charge_ID() != 0){
									MCharge charge = new MCharge(getCtx(), m_InvoiceLine.getC_Charge_ID(), trxName);
									nameItem = charge.getName();
									valueItem = charge.getName().substring(0, 3);
								} else 
									throw new Exception(Msg.translate(getCtx(), "SGNotItem"));	//	No existe Item
								
								MTax tax = new MTax(getCtx(), m_InvoiceLine.getC_Tax_ID(), trxName);
								hsp.printLine(nameItem, 
										m_InvoiceLine.getPriceEntered().doubleValue(), 
										m_InvoiceLine.getQtyInvoiced().doubleValue(), 
										tax.getRate().doubleValue(), 
										null, 
										valueItem);
							}
							hsp.printCommand("E");*/
							//	Printer ID
							m_invoice.set_ValueOfColumn("PrinterId", m_fpConfig.getPrinterId());
							MXXDocFiscalPrinterNo pdn = new MXXDocFiscalPrinterNo(getCtx(), m_XX_DocFiscalPrinterNo_ID, get_TrxName());
							
							//	Next Sequence
							m_invoice.set_ValueOfColumn("XX_FiscalNo", pdn.nextSequence());
							m_invoice.set_ValueOfColumn("XX_PrintFiscalDocument", "Y");
							m_invoice.saveEx();
							pdn.saveEx();
							message = "OK";
						} else
							throw new Exception(Msg.translate(getCtx(), "SGNotConfDocType"));	//	Secuencia Fiscal No Configurada en el Tipo de Documento
					} else
						throw new Exception(Msg.translate(getCtx(), "SGNotConfFiscalPrinter"));	//	Impresora Fiscal No Configurada
				} else
					throw new Exception(Msg.translate(getCtx(), "SGSentDoc"));	//	El Documento ya fue enviado a la Impresora Fiscal
			} else
				throw new Exception(Msg.translate(getCtx(), "SGNotCompletedDoc"));	//	Documento no Completado
		} else
			throw new Exception(Msg.translate(getCtx(), "SGDocNotFound"));	//	Documento no Encontrado
	}
	
}
