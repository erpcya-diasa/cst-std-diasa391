/**
 * @finalidad 
 * @author Yamel Senih
 * @date 01/07/2011
 */
package test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.compiere.model.MBPartner;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.eevolution.model.MHRAttribute;
import org.eevolution.model.MHREmployee;
import org.eevolution.model.MHRProcess;




/**
 * @author Yamel Senih
 *
 */
public class TestProcess {

	/**
	 * @author Yamel Senih 01/07/2011, 11:37:57
	 * @param args
	 * @return void
	 */
	public static void main(String[] args) {
		/*Calendar hoy = Calendar.getInstance();
		Calendar haceUnAno = Calendar.getInstance();
		
		System.out.println("Hoy = " + hoy.getTime());
		System.out.println("Hace Un Año = " + haceUnAno.getTime());
		
		System.out.println("Cambio");
		
		System.out.println("Hoy = " + hoy.getTime());
		haceUnAno.set(Calendar.YEAR, hoy.get(Calendar.YEAR) - 1);
		System.out.println("Hace Un Año = " + haceUnAno.getTime());
		
		Calendar fSem = Calendar.getInstance();
		Date fCal = fSem.getTime();//m_HPeriod.getEndDate();
		fSem.setTime(fCal);
		if (fSem.get(Calendar.WEEK_OF_MONTH) > 3) {
		        //'Calculo del Fondo de Incapacidad
		        //result=(getConcept ("FONINCA","Planilla Administrativa",_DateStart, _DateEnd) + 12);
		}
		System.out.println(fSem.getTime());
		
	    System.out.println("Current week of month is : " +
	                fSem.get(Calendar.WEEK_OF_MONTH));
	               
	    System.out.println("Current week of year is : " +
	                fSem.get(Calendar.WEEK_OF_YEAR));
	 
	    fSem.add(Calendar.WEEK_OF_MONTH, 1);
	    System.out.println("date after one year : " + (fSem.get(Calendar.MONTH) + 1)
	                        + "-"
	                        + fSem.get(Calendar.DATE)
	                        + "-"
	                        + fSem.get(Calendar.YEAR));
		
	    System.err.println(hoy.get(Calendar.MILLISECOND));*/
	    
	    /*for (int i = 0; i < 256; i++) {
			System.out.println("Raiz de " + i +": " + Math.sqrt(i));
			System.out.println("Round(Raiz de " + i +"): " + (int)Math.sqrt(i));
			
		}*/
		
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		Date a = new Date();*/
		/*System.out.println(String.format("%1$#" + 20 + "s", ""));
		
		JOptionPane.showMessageDialog(null, "Hola");*/
		/*
		String palabra = "EPALE QUE MAS COMO TE VA TODO POR AHI Y TU FAMILIA COMO ESTA? ESTUVE VIENDO QUE ES DIFICIL....";
		
		int breakp = 12;
		
		int top = palabra.length()/breakp;
		int begin;
		
		for(int i = 0; i <= top; i++){
			System.out.println(palabra.su);
		}
		
		Object oValue = null;
		
		String value = String.valueOf(oValue);
		
		System.err.println("--- " + (oValue!= null));
		System.err.println((value!= null && value.length() > 0));
		
		
		Calendar fSem = Calendar.getInstance();
		fSem.setTimeInMillis(System.currentTimeMillis());
		
		//System.err.println(fSem.getTime().getTime());
		Timestamp tF = new Timestamp(System.currentTimeMillis());
		Timestamp tT = new Timestamp(System.currentTimeMillis() + 2*1000*60*60);
		fSem.setTimeInMillis(tF.getTime());
		float diff = tT.getTime() - tF.getTime();
		
		//System.err.println(((diff /1000)/60)/60);
		
		int diffDays = 0;
		
		/*Calendar d_From = Calendar.getInstance();
		d_From.setTimeInMillis(1354422600000l);
		Calendar d_To = Calendar.getInstance();
		d_To.setTimeInMillis(1354336200000l);
		
		Calendar d_From = Calendar.getInstance();
		d_From.setTimeInMillis(1354336200000l);
		Calendar d_To = Calendar.getInstance();
		d_To.setTimeInMillis(1354422600000l);
		
		while (d_From.compareTo(d_To) <= 0) {
			if (d_From.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)
				diffDays++;
			else
				
		    System.out.println(" diffDays " + diffDays);
		    d_From.add(Calendar.DATE, 1);
		}*/
		
		//Login.initTest(true);
		
		/*Calendar date = Calendar.getInstance();
		date.set(Calendar.YEAR, 2011);
		date.set(Calendar.MONTH, Calendar.JANUARY);
		date.set(Calendar.DAY_OF_MONTH, 1);
		
		Timestamp validFrom = new Timestamp(date.getTimeInMillis());
		
		MHREmployee m_Employee = null;
		int m_C_BPartner_ID = 0;
		MBPartner[] linesEmployee = MHREmployee.getEmployees(new MHRProcess(Env.getCtx(), 1000461, null));
		//
		int count = 1;
		for(MBPartner bp : linesEmployee)	//=============================================================== Employee
		{
			m_C_BPartner_ID = bp.get_ID();

			m_Employee = MHREmployee.getActiveEmployee(Env.getCtx(), m_C_BPartner_ID, null);
			
			System.out.println(bp.getName());
			
			MHRAttribute att = new MHRAttribute(Env.getCtx(), 0, null);
			att.setAD_Org_ID(1000004);
			att.set_ValueOfColumn("AD_Client_ID", 1000000);
			att.setC_BPartner_ID(m_Employee.getC_BPartner_ID());
			att.setColumnType("Q");
			att.setQty(new BigDecimal(8));
			att.setHR_Concept_ID(1000224);
			att.setValidFrom(validFrom);
			att.saveEx();
			
			/*att = new MHRAttribute(Env.getCtx(), 0, null);
			att.setAD_Org_ID(1000004);
			att.set_ValueOfColumn("AD_Client_ID", 1000000);
			att.setC_BPartner_ID(m_Employee.getC_BPartner_ID());
			att.setColumnType("Q");
			att.setQty(new BigDecimal(104));
			att.setHR_Concept_ID(1000221);
			att.setValidFrom(validFrom);
			att.saveEx();*/
			
			//count++;
			//addLog ("@HR_Employee_ID@: " + bp.getName() + " @Qty@: " + double_result);
		//}*/

		/*
		System.out.println();
		
		DecimalFormat round = new DecimalFormat("###,###,###,###.###");
		round.format(1.2568);
		
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		Timestamp tm = new Timestamp(System.currentTimeMillis());
		
		System.out.println(sdf.format(new Date(tm.getTime())));
		
		BigDecimal round;
		//import java.util.Calendar;

		BigDecimal absences_Time = DB.getSQLValueBD(null, "SELECT SUM(ast.Absences_Time) Absences_Time FROM XX_HR_Absences ast WHERE ast.XX_Paid = 'N' AND ast.C_BPartner_ID = ? AND ast.DateDocument BETWEEN " + DB.TO_DATE(tm) + " AND " + DB.TO_DATE(tm), 0);
		// Si su contrato se vence en el periodo de Quincena
		if(false){
		  System.out.println("Amt 0");
		  //result = 0;
		} else if(true){ // Si su contrato inicia despues de la fecha de inicio del periodo
		  Calendar d_From = Calendar.getInstance();
		  Calendar d_To = Calendar.getInstance();
		  //d_From.setTimeInMillis(_DateStart.getTime());
		  //d_To.setTimeInMillis(_To.getTime());

		  int sundays= 0;
		  int businessDays = 0;

		  while (d_From.compareTo(d_To) <= 0) {
		    if (d_From.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
		      sundays++;
		    else
		      businessDays++;

		    //System.out.println("businessDays " + businessDays + " sundays " + sundays);
		    d_From.add(Calendar.DATE, 1);
		  }
		  double a = absences_Time.doubleValue();
		  System.out.println("Amt " + a);
		  //result = a;//(businessDays*getConcept("FHRD"))-absences_Time.doubleValue();
		} else{  //  Si no sucede nada de lo anterior
		  double a = absences_Time.doubleValue();
		  System.out.println("Amt " + a);
		  BigDecimal ma = Env.ZERO;
		  //result = a;
		}
		
		//DB.getSQLValueBD(null, "SELECT SUM(ast.Absences_Time) Absences_Time FROM XX_HR_Absences ast WHERE XX_Paid = 'N' AND C_BPartner_ID = ?", _C_BPartner_ID);
		*/
		
		
		System.out.println(getStringWithFilling("Hola Mundo", '0', 5, true));
		System.out.println(getStringWithFilling("Hola Mundo", '0', 5, false));
		System.out.println(getStringWithFilling("Hola Mundo", '-', 5, true));
		System.out.println(getStringWithFilling(null, '0', 5, true));
		System.out.println(getStringWithFilling("Hola Mundo", '*', 9, false));
	}
	
	/**
	 * get String with filling
	 * @author Yamel Senih 12/03/2013, 09:30:26
	 * @param str
	 * @param chr
	 * @param length
	 * @param isLeft
	 * @return
	 * @return String
	 */
	public static String getStringWithFilling(String str, char chr, int length, boolean isLeft){
		//	If null String str then set to ""
		if(str == null)
			str = "";
		//	Invalid arguments
		if(chr == 0
				|| length <= 0)
			return str;
		//	Do It
		StringBuffer filling = new StringBuffer();
		for(int i = 0; i < length; i++){
			filling.append(chr);
		}
		//	organize return
		if(isLeft)
			return filling.toString() + str;
		else
			return str + filling.toString();
	}

}
