/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.ghintech.agrosilos.process;

import java.util.logging.Level;

import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MRMA;
import org.compiere.model.MSysConfig;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
 
/**
 *	Create (Generate) Invoice from Shipment
 *	
 *  @author Jorg Janke
 *  @version $Id: InOutCreateInvoice.java,v 1.2 2006/07/30 00:51:02 jjanke Exp $
 */
public class InOutCreateInvoice extends SvrProcess
{
	/**	Shipment					*/
	private int 	p_M_InOut_ID = 0;
	/**	Price List Version			*/
	private int		p_M_PriceList_ID = 0;
	/* Document No					*/
	private String	p_InvoiceDocumentNo = null;
	private String TE_C_DocType_ID = "";
	private String[] ATE_C_DocType_ID ;
	private String p_docAction = DocAction.ACTION_Complete;
	private MInvoice invoice = null;
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_PriceList_ID"))
				p_M_PriceList_ID = para[i].getParameterAsInt();
			else if (name.equals("InvoiceDocumentNo"))
				p_InvoiceDocumentNo = (String)para[i].getParameter();
			else if (name.equals("DocAction"))
				p_docAction = (String)para[i].getParameter();
			else
				
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		p_M_InOut_ID = getRecord_ID();
		//		DocAction check
		if (!DocAction.ACTION_Complete.equals(p_docAction))
			p_docAction = DocAction.ACTION_Prepare;
	}	//	prepare

	/**
	 * 	Create Invoice.
	 *	@return document no
	 *	@throws Exception
	 */
	protected String doIt () throws Exception
	{
		log.info("M_InOut_ID=" + p_M_InOut_ID 
			+ ", M_PriceList_ID=" + p_M_PriceList_ID
			+ ", InvoiceDocumentNo=" + p_InvoiceDocumentNo);
		if (p_M_InOut_ID == 0)
			throw new IllegalArgumentException("No Shipment");
		//
		MInOut ship = new MInOut (getCtx(), p_M_InOut_ID, get_TrxName());
		if (ship.get_ID() == 0)
			throw new IllegalArgumentException("Shipment not found");
		if (!MInOut.DOCSTATUS_Completed.equals(ship.getDocStatus()))
			throw new IllegalArgumentException("Shipment not completed");
		
		invoice = new MInvoice (ship, null);
		TE_C_DocType_ID = MSysConfig.getValue("TE_ReturnsVsCreditNote",Env.getAD_Client_ID(Env.getCtx()));
		ATE_C_DocType_ID = TE_C_DocType_ID.split("-");
		if(get_Value_Equivalent(ATE_C_DocType_ID, ship.getC_DocType_ID())!=0)
			invoice.setC_DocTypeTarget_ID(get_Value_Equivalent(ATE_C_DocType_ID, ship.getC_DocType_ID()));
		invoice.setC_BPartner_Location_ID(ship.getC_BPartner_Location_ID());
		invoice.setSalesRep_ID(ship.getSalesRep_ID());
		
		invoice.setC_Activity_ID(MSysConfig.getIntValue("C_Activity_ID", 1000586, getAD_Client_ID()));
		// Should not override pricelist for RMA
		if (p_M_PriceList_ID != 0 && ship.getM_RMA_ID() == 0)
			invoice.setM_PriceList_ID(p_M_PriceList_ID);
		if (p_InvoiceDocumentNo != null && p_InvoiceDocumentNo.length() > 0)
			invoice.setDocumentNo(p_InvoiceDocumentNo);
		if (!invoice.save())
			throw new IllegalArgumentException("Cannot save Invoice");
		else {
			MRMA rma = (MRMA) ship.getM_RMA();
			if (rma.getM_RMA_ID()!=0) {
				rma.set_ValueOfColumn("C_Invoice_ID", invoice.getC_Invoice_ID());
				rma.save();
			}
		}
			
		MInOutLine[] shipLines = ship.getLines(false);
		for (int i = 0; i < shipLines.length; i++)
		{
			MInOutLine sLine = shipLines[i];
			MInvoiceLine line = new MInvoiceLine(invoice);
			line.setShipLine(sLine);
			if (sLine.sameOrderLineUOM())
				line.setQtyEntered(sLine.getQtyEntered());
			else
				line.setQtyEntered(sLine.getMovementQty());
			line.setQtyInvoiced(sLine.getMovementQty());
			if (!line.save())
				throw new IllegalArgumentException("Cannot save Invoice Line");
		}
		System.out.println("Entrando al proceso de creacion de la factura");
		completeInvoice();
		return invoice.getDocumentNo();
	}	//	InOutCreateInvoice

	protected int get_Value_Equivalent(String[] ATE_List, int p_Value) {
		// TODO Auto-generated method stub
		for (int i=0; i< ATE_List.length; i++ ){
			String[] sublist = ATE_List[i].split(",");
			if(Integer.valueOf(sublist[1])==p_Value)
				return Integer.valueOf(sublist[0]); 
		}
		return 0;
	}
	protected void completeInvoice()
	{
		if (invoice != null)
		{
			if (!invoice.processIt(p_docAction))
			{
				log.warning("completeInvoice - failed: " + invoice);
				addLog("completeInvoice - failed: " + invoice); // Elaine 2008/11/25
			}
			invoice.save();
			//
			addLog(invoice.getC_Invoice_ID(), invoice.getDateInvoiced(), null, invoice.getDocumentNo());
			
		}
		
		
	}	//	completeInvoice
}	//	InOutCreateInvoice
