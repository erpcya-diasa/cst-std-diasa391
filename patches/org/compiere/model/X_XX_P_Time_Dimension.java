/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_P_Time_Dimension
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_P_Time_Dimension extends PO implements I_XX_P_Time_Dimension, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120602L;

    /** Standard Constructor */
    public X_XX_P_Time_Dimension (Properties ctx, int XX_P_Time_Dimension_ID, String trxName)
    {
      super (ctx, XX_P_Time_Dimension_ID, trxName);
      /** if (XX_P_Time_Dimension_ID == 0)
        {
			setDateFrom (new Timestamp( System.currentTimeMillis() ));
			setIsFestive (false);
			setIsSummary (false);
			setName (null);
			setTimeCategory (null);
			setValue (null);
			setXX_P_Time_Dimension_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_P_Time_Dimension (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_P_Time_Dimension[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Date From.
		@param DateFrom 
		Starting date for a range
	  */
	public void setDateFrom (Timestamp DateFrom)
	{
		set_Value (COLUMNNAME_DateFrom, DateFrom);
	}

	/** Get Date From.
		@return Starting date for a range
	  */
	public Timestamp getDateFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateFrom);
	}

	/** Set Date To.
		@param DateTo 
		End date of a date range
	  */
	public void setDateTo (Timestamp DateTo)
	{
		set_Value (COLUMNNAME_DateTo, DateTo);
	}

	/** Get Date To.
		@return End date of a date range
	  */
	public Timestamp getDateTo () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTo);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Generate Sub-Periods.
		@param GenerateSubPeriods Generate Sub-Periods	  */
	public void setGenerateSubPeriods (String GenerateSubPeriods)
	{
		set_Value (COLUMNNAME_GenerateSubPeriods, GenerateSubPeriods);
	}

	/** Get Generate Sub-Periods.
		@return Generate Sub-Periods	  */
	public String getGenerateSubPeriods () 
	{
		return (String)get_Value(COLUMNNAME_GenerateSubPeriods);
	}

	/** Set Is Festive.
		@param IsFestive Is Festive	  */
	public void setIsFestive (boolean IsFestive)
	{
		set_Value (COLUMNNAME_IsFestive, Boolean.valueOf(IsFestive));
	}

	/** Get Is Festive.
		@return Is Festive	  */
	public boolean isFestive () 
	{
		Object oo = get_Value(COLUMNNAME_IsFestive);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Summary Level.
		@param IsSummary 
		This is a summary entity
	  */
	public void setIsSummary (boolean IsSummary)
	{
		set_Value (COLUMNNAME_IsSummary, Boolean.valueOf(IsSummary));
	}

	/** Get Summary Level.
		@return This is a summary entity
	  */
	public boolean isSummary () 
	{
		Object oo = get_Value(COLUMNNAME_IsSummary);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** TimeCategory AD_Reference_ID=1000065 */
	public static final int TIMECATEGORY_AD_Reference_ID=1000065;
	/** Year = Y */
	public static final String TIMECATEGORY_Year = "Y";
	/** Semester = S */
	public static final String TIMECATEGORY_Semester = "S";
	/** Quarter = Q */
	public static final String TIMECATEGORY_Quarter = "Q";
	/** Month = M */
	public static final String TIMECATEGORY_Month = "M";
	/** Week = W */
	public static final String TIMECATEGORY_Week = "W";
	/** Day = D */
	public static final String TIMECATEGORY_Day = "D";
	/** Set Time Category.
		@param TimeCategory Time Category	  */
	public void setTimeCategory (String TimeCategory)
	{

		set_Value (COLUMNNAME_TimeCategory, TimeCategory);
	}

	/** Get Time Category.
		@return Time Category	  */
	public String getTimeCategory () 
	{
		return (String)get_Value(COLUMNNAME_TimeCategory);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}

	/** Set Pentaho Time Dimension.
		@param XX_P_Time_Dimension_ID Pentaho Time Dimension	  */
	public void setXX_P_Time_Dimension_ID (int XX_P_Time_Dimension_ID)
	{
		if (XX_P_Time_Dimension_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_P_Time_Dimension_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_P_Time_Dimension_ID, Integer.valueOf(XX_P_Time_Dimension_ID));
	}

	/** Get Pentaho Time Dimension.
		@return Pentaho Time Dimension	  */
	public int getXX_P_Time_Dimension_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_P_Time_Dimension_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}