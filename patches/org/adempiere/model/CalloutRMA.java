/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/ 

package org.adempiere.model;

import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MUOM;
import org.compiere.model.MUOMConversion;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 *
 * @author  Ashley G Ramdass
 */
public class CalloutRMA extends CalloutEngine
{

    /**
    *  docType - set document properties based on document type.
    *  @param ctx
    *  @param WindowNo
    *  @param mTab
    *  @param mField
    *  @param value
    *  @return error message or ""
    */
   public String docType (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
   {
       Integer C_DocType_ID = (Integer)value;
       if (C_DocType_ID == null || C_DocType_ID.intValue() == 0)
           return "";
       
       String sql = "SELECT d.IsSoTrx "
           + "FROM C_DocType d WHERE C_DocType_ID=?";
       
       String docSOTrx = DB.getSQLValueString(null, sql, C_DocType_ID);
       
       boolean isSOTrx = "Y".equals(docSOTrx);
       
       mTab.setValue("IsSOTrx", isSOTrx);
       
       return "";
   }
   
   public String salesRep (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
   {   
		Integer m_M_InOut_ID = (Integer)value;
		if (m_M_InOut_ID == null || m_M_InOut_ID.intValue() == 0)
			return "";
		
		MInOut inOut = new MInOut(ctx, m_M_InOut_ID.intValue(), null);
		mTab.setValue("SalesRep_ID", inOut.getSalesRep_ID());
		
		return "";
	
   }
   
   /**
	 *	Order Line - Quantity.
	 *		- called from C_UOM_ID, QtyEntered, Qty
	 *  @param ctx context
	 *  @param WindowNo current Window No
	 *  @param mTab Grid Tab
	 *  @param mField Grid Field
	 *  @param value New Value
	 *  @return null or error message
	 */
	public String qty (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value) {
		if (isCalloutActive() || value == null)
			return "";
		Integer M_InOutLine_ID = (Integer) mTab.getValue("M_InOutLine_ID");
		if(M_InOutLine_ID == null || M_InOutLine_ID <= 0)
			return "";
		//	
		MInOutLine ioLine = new MInOutLine(ctx, M_InOutLine_ID, null);
		int M_Product_ID = ioLine.getM_Product_ID();
		//	
		BigDecimal Qty = Env.ZERO;
		BigDecimal QtyEntered = Env.ZERO;
		if(M_Product_ID == 0) {
			QtyEntered = ioLine.getQtyEntered();
			Qty = QtyEntered;
		}
		//		UOM Changed - convert from Entered -> Product
		else if (mField.getColumnName().equals("C_UOM_ID")) {
				int C_UOM_To_ID = ((Integer)value).intValue();
				QtyEntered = (mTab.getValue("QtyEntered") == null ? Env.ZERO : (BigDecimal)mTab.getValue("QtyEntered"));
				
				BigDecimal QtyEntered1 = QtyEntered.setScale(MUOM.getPrecision(ctx, C_UOM_To_ID), BigDecimal.ROUND_HALF_UP);
				if (QtyEntered.compareTo(QtyEntered1) != 0) {
					log.fine("Corrected QtyEntered Scale UOM=" + C_UOM_To_ID 
						+ "; QtyEntered=" + QtyEntered + "->" + QtyEntered1);  
					QtyEntered = QtyEntered1;
					mTab.setValue("QtyEntered", QtyEntered);
				}
				Qty = MUOMConversion.convertProductFrom (ctx, M_Product_ID, 
					C_UOM_To_ID, QtyEntered);
				if (Qty == null)
					Qty = QtyEntered;
				boolean conversion = QtyEntered.compareTo(Qty) != 0;
				log.fine("UOM=" + C_UOM_To_ID 
					+ " -> " + conversion 
					+ " Qty=" + Qty);
				Env.setContext(ctx, WindowNo, "UOMConversion", conversion ? "Y" : "N");
				mTab.setValue("Qty", Qty);
			}
			//	QtyEntered changed - calculate QtyOrdered
		//	QtyEntered changed - calculate QtyOrdered
		else if (mField.getColumnName().equals("Qty"))
			{
				int C_UOM_To_ID = Env.getContextAsInt(ctx, WindowNo, "C_UOM_ID");
				Qty= (BigDecimal)value;
				BigDecimal Qty1= Qty.setScale(MUOM.getPrecision(ctx, C_UOM_To_ID), BigDecimal.ROUND_HALF_UP);
				if (Qty.compareTo(Qty1) != 0)
				{
					log.fine("Corrected QtyEntered Scale UOM=" + C_UOM_To_ID 
						+ "; Qty=" + Qty + "->" + Qty1);  
					QtyEntered = Qty1;
					mTab.setValue("Qty", Qty);
				}
				QtyEntered = MUOMConversion.convertProductTo (ctx, M_Product_ID, 
					C_UOM_To_ID, Qty);
				if (QtyEntered == null)
					QtyEntered = Qty;
				boolean conversion = Qty.compareTo(QtyEntered) != 0;
				log.fine("UOM=" + C_UOM_To_ID 
					+ ", Qty=" + Qty
					+ " -> " + conversion 
					+ " QtyOrdered=" + QtyEntered);
				Env.setContext(ctx, WindowNo, "UOMConversion", conversion ? "Y" : "N");
				mTab.setValue("QtyEntered", QtyEntered);
			}
		//		QtyEntered changed - calculate QtyOrdered
		else if (mField.getColumnName().equals("QtyEntered"))
		{
			int C_UOM_To_ID = Env.getContextAsInt(ctx, WindowNo, "C_UOM_ID");
			QtyEntered = (BigDecimal)value;
			BigDecimal QtyEntered1 = QtyEntered.setScale(MUOM.getPrecision(ctx, C_UOM_To_ID), BigDecimal.ROUND_HALF_UP);
			if (QtyEntered.compareTo(QtyEntered1) != 0)
			{
				log.fine("Corrected QtyEntered Scale UOM=" + C_UOM_To_ID 
					+ "; QtyEntered=" + QtyEntered + "->" + QtyEntered1);  
				QtyEntered = QtyEntered1;
				mTab.setValue("QtyEntered", QtyEntered);
			}
			Qty = MUOMConversion.convertProductFrom (ctx, M_Product_ID, 
				C_UOM_To_ID, QtyEntered);
			if (Qty == null)
				Qty = QtyEntered;
			boolean conversion = QtyEntered.compareTo(Qty) != 0;
			log.fine("UOM=" + C_UOM_To_ID 
				+ ", QtyEntered=" + QtyEntered
				+ " -> " + conversion 
				+ " QtyOrdered=" + Qty);
			Env.setContext(ctx, WindowNo, "UOMConversion", conversion ? "Y" : "N");
			mTab.setValue("Qty", Qty);
		}
		//
		return "";
	
   }
}
