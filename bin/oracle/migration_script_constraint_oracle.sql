﻿--SF_CustomerInventory

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_CustomerInventory
Add Constraint FK_SF_CustomerInventory_C_BParnert Foreign Key (C_BPartner_ID)
References C_BPartner(C_BPartner_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_CustomerInventory
Add Constraint FK_SF_CustomerInventory_C_BPartner_Location Foreign Key (C_BPartner_Location_ID)
References C_BPartner_Location(C_BPartner_Location_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_CustomerInventory
Add Constraint FK_SF_CustomerInventory_C_Order Foreign Key (C_Order_ID)
References C_Order(C_Order_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_CustomerInventory
Add Constraint FK_SF_CustomerInventory_M_PriceList Foreign Key (M_PriceList_ID)
References M_PriceList(M_PriceList_ID)
On Update Cascade On Delete Restrict;

--SF_CustomerInventoryLine

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_CustomerInventoryLine
Add Constraint FK_SF_CustomerInventoryLine_M_Product Foreign Key (M_Product_ID)
References M_Product(M_Product_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_CustomerInventoryLine
Add Constraint FK_SF_CustomerInventoryLine_SF_CustomerInventory Foreign Key (SF_CustomerInventory_ID)
References SF_CustomerInventory(SF_CustomerInventory_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_CustomerInventoryLine
Add Constraint FK_SF_CustomerInventoryLine_SF_InventoryType Foreign Key (SF_InventoryType_ID)
References SF_InventoryType(SF_InventoryType_ID)
On Update Cascade On Delete Restrict;


--SF_DocSequence

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_DocSequence
Add Constraint FK_SF_DocSequence_AD_Sequence Foreign Key (AD_Sequence_ID)
References AD_Sequence(AD_Sequence_ID)
On Update Cascade On Delete Restrict;

--SF_InitialLoad

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_InitialLoad
Add Constraint FK_SF_InitialLoad_AD_Table Foreign Key (AD_Table_ID)
References AD_Table(AD_Table_ID)
On Update Cascade On Delete Restrict;


--SF_MenuList

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_MenuList
Add Constraint FK_SF_MenuList_AD_Form Foreign Key (AD_Form_ID)
References AD_Form(AD_Form_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_MenuList
Add Constraint FK_SF_MenuList_AD_Process Foreign Key (AD_Process_ID)
References AD_Process(AD_Process_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_MenuList
Add Constraint FK_SF_MenuList_WS_WebServiceType Foreign Key (WS_WebServiceType_ID)
References WS_WebServiceType(WS_WebServiceType_ID)
On Update Cascade On Delete Restrict;

--SF_PlanningVisit

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_PlanningVisit
Add Constraint FK_SF_PlanningVisit_C_BPartner Foreign Key (C_BPartner_ID)
References C_BPartner(C_BPartner_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_PlanningVisit
Add Constraint FK_SF_PlanningVisit_C_BPartner_Location Foreign Key (C_BPartner_Location_ID)
References C_BPartner_Location(C_BPartner_Location_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_PlanningVisit
Add Constraint FK_SF_PlanningVisit_C_SalesRegion Foreign Key (C_SalesRegion_ID)
References C_SalesRegion(C_SalesRegion_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_PlanningVisit
Add Constraint FK_SF_PlanningVisit_SF_EventType Foreign Key (SF_EventType_ID)
References SF_EventType(SF_EventType_ID)
On Update Cascade On Delete Restrict;
 
-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_PlanningVisit
Add Constraint FK_SF_PlanningVisit_SF_Frequency Foreign Key (SF_Frequency_ID)
References SF_Frequency(SF_Frequency_ID)
On Update Cascade On Delete Restrict;

--SF_ProductAccess

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_ProductAccess
Add Constraint FK_SF_ProductAccess_C_BPartner Foreign Key (C_BPartner_ID)
References C_BPartner(C_BPartner_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_ProductAccess
Add Constraint FK_SF_ProductAccess_C_BPartner_Location Foreign Key (C_BPartner_Location_ID)
References C_BPartner_Location(C_BPartner_Location_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_ProductAccess
Add Constraint FK_SF_ProductAccess_C_BP_Group Foreign Key (C_BP_Group_ID)
References C_BP_Group(C_BP_Group_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_ProductAccess
Add Constraint FK_SF_ProductAccess_M_Product_Category Foreign Key (M_Product_Category_ID)
References M_Product_Category(M_Product_Category_ID)
On Update Cascade On Delete Restrict;

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_ProductAccess
Add Constraint FK_SF_ProductAccess_M_Product Foreign Key (M_Product_ID)
References M_Product(M_Product_ID)
On Update Cascade On Delete Restrict;

--SF_SynchronizingTrace

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_SynchronizingTrace
Add Constraint FK_SF_SynchronizingTrace_SF_MenuList Foreign Key (SF_MenuList_ID)
References SF_MenuList(SF_MenuList_ID)
On Update Cascade On Delete Restrict;

--SF_Visit

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_Visit
Add Constraint FK_SF_Visit_SF_PlanningVisit Foreign Key (SF_PlanningVisit_ID)
References SF_PlanningVisit(SF_PlanningVisit_ID)
On Update Cascade On Delete Restrict;

--SF_VisitLine

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_VisitLine
Add Constraint FK_SF_VisitLine_SF_EventType Foreign Key (SF_EventType_ID)
References SF_EventType(SF_EventType_ID)
On Update Cascade On Delete Restrict;

--SF_VisitLine

-- 30/01/2013 11:18:10 AM VET
-- Sales Force Mobile with ADempiere
Alter Table SF_VisitLine
Add Constraint FK_SF_VisitLine_SF_Visit Foreign Key (SF_Visit_ID)
References SF_Visit(SF_Visit_ID)
On Update Cascade On Delete Restrict;
