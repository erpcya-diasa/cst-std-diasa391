-- 29/01/2013 02:05:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (AD_Client_ID,AD_Menu_ID,AD_Org_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES (0,2950000,0,TO_DATE('2013-01-29 14:05:47','YYYY-MM-DD HH24:MI:SS'),100,'Mobile System','SF','Y','Y','N','N','Y','Mobile System',TO_DATE('2013-01-29 14:05:47','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:05:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950000 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:05:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950000, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950000)
;

-- 29/01/2013 02:06:23 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (AD_Client_ID,AD_Menu_ID,AD_Org_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES (0,2950001,0,TO_DATE('2013-01-29 14:06:23','YYYY-MM-DD HH24:MI:SS'),100,'See Reports','SF','Y','Y','N','N','Y','Reports',TO_DATE('2013-01-29 14:06:23','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:06:23 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950001 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:06:23 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950001, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950001)
;

-- 29/01/2013 02:06:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950001
;

-- 29/01/2013 02:06:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (AD_Client_ID,AD_Menu_ID,AD_Org_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES (0,2950002,0,TO_DATE('2013-01-29 14:06:42','YYYY-MM-DD HH24:MI:SS'),100,'Sales Planning','SF','Y','Y','N','N','Y','Sales Planning',TO_DATE('2013-01-29 14:06:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:06:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950002 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:06:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950002, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950002)
;

-- 29/01/2013 02:06:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950002
;

-- 29/01/2013 02:06:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950001
;

-- 29/01/2013 02:07:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Sistema Móvil',Description='Sistema Móvil',Updated=TO_DATE('2013-01-29 14:07:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950000 AND AD_Language='es_MX'
;

-- 29/01/2013 02:07:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Informes',Description='Informes',Updated=TO_DATE('2013-01-29 14:07:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950001 AND AD_Language='es_MX'
;

-- 29/01/2013 02:07:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Planificaión de Ventas',Description='Planificaión de Ventas',Updated=TO_DATE('2013-01-29 14:07:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950002 AND AD_Language='es_MX'
;

-- 29/01/2013 02:08:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (AD_Client_ID,AD_Menu_ID,AD_Org_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES (0,2950003,0,TO_DATE('2013-01-29 14:08:07','YYYY-MM-DD HH24:MI:SS'),100,'Mobile System Admin','SF','Y','Y','N','N','Y','Mobile System Admin',TO_DATE('2013-01-29 14:08:07','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:08:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950003 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:08:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950003, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950003)
;

-- 29/01/2013 02:08:24 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Administración del Sistema Móvil',Description='Administración del Sistema Móvil',Updated=TO_DATE('2013-01-29 14:08:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950003 AND AD_Language='es_MX'
;

-- 29/01/2013 02:08:29 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950003
;

-- 29/01/2013 02:08:29 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950002
;

-- 29/01/2013 02:08:29 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950001
;

-- 29/01/2013 02:08:57 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950004,0,2950003,TO_DATE('2013-01-29 14:08:57','YYYY-MM-DD HH24:MI:SS'),100,'Event Type','SF','Y','Y','N','N','N','Event Type',TO_DATE('2013-01-29 14:08:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:08:57 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950004 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:08:57 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950004, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950004)
;

-- 29/01/2013 02:09:02 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 29/01/2013 02:09:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Tipo de Evento',Description='Tipo de Evento',Updated=TO_DATE('2013-01-29 14:09:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950004 AND AD_Language='es_MX'
;

-- 29/01/2013 02:09:33 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950005,0,2950001,TO_DATE('2013-01-29 14:09:33','YYYY-MM-DD HH24:MI:SS'),100,'Customer Inventorys','SF','Y','Y','N','N','N','Customer Inventory',TO_DATE('2013-01-29 14:09:33','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:09:33 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950005 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:09:33 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950005, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950005)
;

-- 29/01/2013 02:09:48 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Inventario (Cliente)',Description='Inventario (Cliente)',Updated=TO_DATE('2013-01-29 14:09:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950005 AND AD_Language='es_MX'
;

-- 29/01/2013 02:09:51 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 29/01/2013 02:09:51 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 29/01/2013 02:10:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950006,0,2950004,TO_DATE('2013-01-29 14:10:30','YYYY-MM-DD HH24:MI:SS'),100,'Frecuency of Visit','SF','Y','Y','N','N','N','Frecuency of Visit',TO_DATE('2013-01-29 14:10:30','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:10:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950006 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:10:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950006, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950006)
;

-- 29/01/2013 02:10:43 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Frecuencia de Visita',Description='Frecuencia de Visita',Updated=TO_DATE('2013-01-29 14:10:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950006 AND AD_Language='es_MX'
;

-- 29/01/2013 02:11:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 29/01/2013 02:11:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 29/01/2013 02:11:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 29/01/2013 02:12:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950007,0,2950007,TO_DATE('2013-01-29 14:12:29','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit','SF','Y','Y','N','N','N','Planning Visit',TO_DATE('2013-01-29 14:12:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:12:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950007 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:12:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950007, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950007)
;

-- 29/01/2013 02:12:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950007
;

-- 29/01/2013 02:12:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 29/01/2013 02:12:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 29/01/2013 02:12:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 29/01/2013 02:12:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Planificación de Visita',Description='Planificación de Visita',Updated=TO_DATE('2013-01-29 14:12:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950007 AND AD_Language='es_MX'
;

-- 29/01/2013 02:13:21 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950008,0,2950002,TO_DATE('2013-01-29 14:13:21','YYYY-MM-DD HH24:MI:SS'),100,'Visit to Customer','SF','Y','Y','N','N','N','Visit to Customer',TO_DATE('2013-01-29 14:13:21','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:13:21 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950008 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 29/01/2013 02:13:21 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950008, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950008)
;

-- 29/01/2013 02:13:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Visita a CLiente',Description='Visita a CLiente',Updated=TO_DATE('2013-01-29 14:13:32','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950008 AND AD_Language='es_MX'
;

-- 29/01/2013 02:13:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950008
;

-- 29/01/2013 02:13:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950007
;

-- 29/01/2013 02:13:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 29/01/2013 02:13:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 29/01/2013 02:13:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=4, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 30/01/2013 10:26:20 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950011,0,2950005,TO_DATE('2013-01-30 10:26:20','YYYY-MM-DD HH24:MI:SS'),100,'Initial Load for Movil System','SF','Y','Y','N','N','N','Initial Load for Movil System',TO_DATE('2013-01-30 10:26:20','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:26:20 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950011 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 30/01/2013 10:26:20 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950011, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950011)
;

-- 30/01/2013 10:27:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Carga Inicial del Sistema Móvil',Description='Carga Inicial del Sistema Móvil',Updated=TO_DATE('2013-01-30 10:27:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950011 AND AD_Language='es_MX'
;

-- 30/01/2013 10:27:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950011
;

-- 30/01/2013 10:27:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950008
;

-- 30/01/2013 10:27:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950007
;

-- 30/01/2013 10:27:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 30/01/2013 10:27:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=4, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 30/01/2013 10:27:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=5, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 30/01/2013 10:27:31 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950012,0,2950012,TO_DATE('2013-01-30 10:27:31','YYYY-MM-DD HH24:MI:SS'),100,'Inventory Type','SF','Y','Y','N','N','N','Inventory Type',TO_DATE('2013-01-30 10:27:31','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:27:31 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950012 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 30/01/2013 10:27:31 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950012, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950012)
;

-- 30/01/2013 10:27:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Tipo de Inventario',Updated=TO_DATE('2013-01-30 10:27:41','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950012 AND AD_Language='es_MX'
;

-- 30/01/2013 10:27:43 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Description='Tipo de Inventario',Updated=TO_DATE('2013-01-30 10:27:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950012 AND AD_Language='es_MX'
;

-- 30/01/2013 10:27:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950012
;

-- 30/01/2013 10:27:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950002
;

-- 30/01/2013 10:27:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950003
;

-- 30/01/2013 10:27:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950000, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950001
;

-- 30/01/2013 10:27:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950012
;

-- 30/01/2013 10:27:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950011
;

-- 30/01/2013 10:27:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950008
;

-- 30/01/2013 10:27:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950007
;

-- 30/01/2013 10:27:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=4, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 30/01/2013 10:27:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=5, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 30/01/2013 10:27:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=6, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 30/01/2013 10:28:18 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950013,0,2950006,TO_DATE('2013-01-30 10:28:18','YYYY-MM-DD HH24:MI:SS'),100,'Menu Mobile System','SF','Y','Y','N','N','N','Menu Mobile System',TO_DATE('2013-01-30 10:28:18','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:28:18 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950013 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 30/01/2013 10:28:18 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950013, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950013)
;

-- 30/01/2013 10:28:32 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Menú del Sistema Móvil',Description='Menú del Sistema Móvil',Updated=TO_DATE('2013-01-30 10:28:32','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950013 AND AD_Language='es_MX'
;

-- 30/01/2013 10:28:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950013
;

-- 30/01/2013 10:28:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950012
;

-- 30/01/2013 10:28:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950011
;

-- 30/01/2013 10:28:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950008
;

-- 30/01/2013 10:28:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=4, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950007
;

-- 30/01/2013 10:28:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=5, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 30/01/2013 10:28:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=6, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 30/01/2013 10:28:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=7, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 30/01/2013 10:29:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950014,0,2950009,TO_DATE('2013-01-30 10:29:08','YYYY-MM-DD HH24:MI:SS'),100,'Product Access','SF','Y','Y','N','N','N','Product Access',TO_DATE('2013-01-30 10:29:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:29:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950014 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 30/01/2013 10:29:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950014, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950014)
;

-- 30/01/2013 10:29:18 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Acceso a Productos',Description='Acceso a Productos',Updated=TO_DATE('2013-01-30 10:29:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950014 AND AD_Language='es_MX'
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950014
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950013
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950012
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950011
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=4, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950008
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=5, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950007
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=6, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=7, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 30/01/2013 10:29:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=8, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 30/01/2013 10:29:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950015,0,2950010,TO_DATE('2013-01-30 10:29:39','YYYY-MM-DD HH24:MI:SS'),100,'Synchronizing Trace','SF','Y','Y','N','N','N','Synchronizing Trace',TO_DATE('2013-01-30 10:29:39','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:29:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950015 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 30/01/2013 10:29:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950015, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950015)
;

-- 30/01/2013 10:29:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Traza de Sincronización',Description='Traza de Sincronización',Updated=TO_DATE('2013-01-30 10:29:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950015 AND AD_Language='es_MX'
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950015
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950014
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950013
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950012
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=4, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950011
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=5, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950008
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=6, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950007
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=7, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=8, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 30/01/2013 10:29:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=9, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;

-- 30/01/2013 10:30:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window SET Description='System Config Mobil', Name='System Config Mobil',Updated=TO_DATE('2013-01-30 10:30:23','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950011
;

-- 30/01/2013 10:30:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET IsTranslated='N' WHERE AD_Window_ID=2950011
;

-- 30/01/2013 10:30:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Configurar Sistema Móvil',Description='Configurar Sistema Móvil',Updated=TO_DATE('2013-01-30 10:30:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950011 AND AD_Language='es_MX'
;

-- 30/01/2013 10:30:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Configurar Sistema Móvil',Description='Configurar Sistema Móvil',Updated=TO_DATE('2013-01-30 10:30:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950016 AND AD_Language='es_MX'
;

-- 30/01/2013 10:30:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET Description='System Config Mobil', Name='System Config Mobil',Updated=TO_DATE('2013-01-30 10:30:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950016
;

-- 30/01/2013 10:30:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET IsTranslated='N' WHERE AD_Tab_ID=2950016
;

-- 30/01/2013 10:31:07 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu (Action,AD_Client_ID,AD_Menu_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsCentrallyMaintained,IsReadOnly,IsSOTrx,IsSummary,Name,Updated,UpdatedBy) VALUES ('W',0,2950016,0,2950011,TO_DATE('2013-01-30 10:31:07','YYYY-MM-DD HH24:MI:SS'),100,'System Config','SF','Y','Y','N','N','N','System Config',TO_DATE('2013-01-30 10:31:07','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:31:07 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Menu_Trl (AD_Language,AD_Menu_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Menu_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Menu t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Menu_ID=2950016 AND NOT EXISTS (SELECT * FROM AD_Menu_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Menu_ID=t.AD_Menu_ID)
;

-- 30/01/2013 10:31:07 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_TreeNodeMM (AD_Client_ID,AD_Org_ID, IsActive,Created,CreatedBy,Updated,UpdatedBy, AD_Tree_ID, Node_ID, Parent_ID, SeqNo) SELECT t.AD_Client_ID, 0, 'Y', SysDate, 100, SysDate, 100,t.AD_Tree_ID, 2950016, 0, 999 FROM AD_Tree t WHERE t.AD_Client_ID=0 AND t.IsActive='Y' AND t.IsAllNodes='Y' AND t.TreeType='MM' AND NOT EXISTS (SELECT * FROM AD_TreeNodeMM e WHERE e.AD_Tree_ID=t.AD_Tree_ID AND Node_ID=2950016)
;

-- 30/01/2013 10:31:20 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu SET Description='System Config Mobile', Name='System Config Mobile',Updated=TO_DATE('2013-01-30 10:31:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950016
;

-- 30/01/2013 10:31:20 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET IsTranslated='N' WHERE AD_Menu_ID=2950016
;

-- 30/01/2013 10:31:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Menu_Trl SET Name='Configurar Sistema Móvil',Description='Configurar Sistema Móvil',Updated=TO_DATE('2013-01-30 10:31:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Menu_ID=2950016 AND AD_Language='es_MX'
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950016
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950015
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950014
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950013
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=4, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950012
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=5, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950011
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=6, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950008
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=7, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950007
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=8, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950006
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=9, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950005
;

-- 30/01/2013 10:31:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950002, SeqNo=10, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950004
;
-- 30/01/2013 04:05:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950016
;

-- 30/01/2013 04:05:24 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950015
;

-- 30/01/2013 04:05:24 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950016
;

-- 30/01/2013 04:05:28 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950013
;

-- 30/01/2013 04:05:28 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950015
;

-- 30/01/2013 04:05:28 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950016
;

-- 30/01/2013 04:05:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=0, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950011
;

-- 30/01/2013 04:05:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=1, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950013
;

-- 30/01/2013 04:05:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=2, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950015
;

-- 30/01/2013 04:05:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_TreeNodeMM SET Parent_ID=2950003, SeqNo=3, Updated=SysDate WHERE AD_Tree_ID=10 AND Node_ID=2950016
;
-- 30/01/2013 04:40:57 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-30 16:40:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950066
;

-- 30/01/2013 04:41:38 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_ID=30,Updated=TO_DATE('2013-01-30 16:41:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950062
;

-- 30/01/2013 04:42:03 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-30 16:42:03','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950063
;

-- 30/01/2013 04:42:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-30 16:42:32','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950065
;

-- 30/01/2013 04:46:18 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsInsertRecord='N', IsReadOnly='Y',Updated=TO_DATE('2013-01-30 16:46:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950000
;

-- 30/01/2013 04:46:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsInsertRecord='N', IsReadOnly='Y',Updated=TO_DATE('2013-01-30 16:46:22','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950001
;

-- 30/01/2013 04:46:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-30 16:46:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950007
;

-- 30/01/2013 04:47:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-30 16:47:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950012
;

-- 30/01/2013 04:47:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET AD_Column_ID=2950092,Updated=TO_DATE('2013-01-30 16:47:54','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950001
;

-- 30/01/2013 04:48:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950016
;

-- 30/01/2013 04:48:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950020
;

-- 30/01/2013 04:48:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950021
;

-- 30/01/2013 04:48:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950017
;

-- 30/01/2013 04:48:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950019
;

-- 30/01/2013 04:48:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_ID=30,Updated=TO_DATE('2013-01-30 16:48:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950088
;

-- 30/01/2013 04:52:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Val_Rule_ID=231,Updated=TO_DATE('2013-01-30 16:52:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950088
;

-- 30/01/2013 04:52:34 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y',Updated=TO_DATE('2013-01-30 16:52:34','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950090
;

-- 30/01/2013 04:53:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-30 16:53:04','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950019
;

-- 30/01/2013 04:54:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-30 16:54:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950020
;

-- 30/01/2013 05:24:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET AD_Column_ID=2950146,Updated=TO_DATE('2013-01-30 17:24:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950010
;

-- 30/01/2013 05:26:48 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsTranslationTab='Y',Updated=TO_DATE('2013-01-30 17:26:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 05:30:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsInsertRecord='N',Updated=TO_DATE('2013-01-30 17:30:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 05:37:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950272,2950025,0,19,2950016,'SF_MenuList_ID',TO_DATE('2013-01-30 17:37:42','YYYY-MM-DD HH24:MI:SS'),100,'Menu of Mobile System ID','U',10,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Menu of Mobile System',0,TO_DATE('2013-01-30 17:37:42','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 30/01/2013 05:37:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950272 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 30/01/2013 05:37:48 PM VET
-- Sales Force Mobile with ADempiere
ALTER TABLE SF_MenuList_Trl ADD SF_MenuList_ID NUMBER(10) DEFAULT NULL 
;

-- 30/01/2013 05:40:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET EntityType='SF',Updated=TO_DATE('2013-01-30 17:40:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950272
;

-- 30/01/2013 05:40:59 PM VET
-- Sales Force Mobile with ADempiere
ALTER TABLE SF_MenuList_Trl MODIFY SF_MenuList_ID NUMBER(10) DEFAULT NULL 
;

-- 30/01/2013 05:45:13 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsParent='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 17:45:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950272
;

-- 30/01/2013 05:45:58 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950272,2950167,0,2950018,TO_DATE('2013-01-30 17:45:58','YYYY-MM-DD HH24:MI:SS'),100,'Menu of Mobile System ID',10,'SF','Y','Y','Y','N','N','N','N','N','Menu of Mobile System',TO_DATE('2013-01-30 17:45:58','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 05:45:58 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950167 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 05:51:24 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_ID=18, AD_Reference_Value_ID=106, IsParent='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 17:51:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950269
;

-- 30/01/2013 05:52:55 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 17:52:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950272
;

-- 30/01/2013 05:58:13 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO CM_Chat (AD_Client_ID,AD_Org_ID,AD_Table_ID,CM_Chat_ID,ConfidentialType,Created,CreatedBy,Description,IsActive,ModerationType,Record_ID,Updated,UpdatedBy) VALUES (0,0,106,1000000,'A',TO_DATE('2013-01-30 17:58:10','YYYY-MM-DD HH24:MI:SS'),100,'Pestaña: Menu Movil System','Y','N',2950008,TO_DATE('2013-01-30 17:58:10','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 05:58:13 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO CM_ChatEntry (AD_Client_ID,AD_Org_ID,ChatEntryType,CM_ChatEntry_ID,CM_Chat_ID,ConfidentialType,Created,CreatedBy,IsActive,Updated,UpdatedBy) VALUES (0,0,'N',1000000,1000000,'A',TO_DATE('2013-01-30 17:58:13','YYYY-MM-DD HH24:MI:SS'),100,'Y',TO_DATE('2013-01-30 17:58:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 05:58:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET WhereClause=NULL,Updated=TO_DATE('2013-01-30 17:58:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950008
;

-- 30/01/2013 05:59:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET WhereClause='MenuType = ''M''',Updated=TO_DATE('2013-01-30 17:59:04','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950008
;

-- 30/01/2013 06:00:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsTranslationTab='N',Updated=TO_DATE('2013-01-30 18:00:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 06:00:53 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsInsertRecord='Y',Updated=TO_DATE('2013-01-30 18:00:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 06:01:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950167
;

-- 30/01/2013 06:01:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950162
;

-- 30/01/2013 06:01:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950159
;

-- 30/01/2013 06:01:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950166
;

-- 30/01/2013 06:01:31 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950164
;

-- 30/01/2013 06:01:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950161
;

-- 30/01/2013 06:01:42 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-30 18:01:42','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950162
;

-- 30/01/2013 06:01:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsTranslationTab='Y',Updated=TO_DATE('2013-01-30 18:01:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 06:02:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET TabLevel=0,Updated=TO_DATE('2013-01-30 18:02:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 06:02:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET TabLevel=1,Updated=TO_DATE('2013-01-30 18:02:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 06:03:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AccessLevel='4',Updated=TO_DATE('2013-01-30 18:03:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950016
;

-- 30/01/2013 06:05:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AccessLevel='3',Updated=TO_DATE('2013-01-30 18:05:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950016
;

-- 30/01/2013 06:08:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsActive='N', IsUpdateable='N',Updated=TO_DATE('2013-01-30 18:08:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950272
;

-- 30/01/2013 06:11:12 PM VET
-- Sales Force Mobile with ADempiere
DELETE  FROM  AD_Column_Trl WHERE AD_Column_ID=2950266
;

-- 30/01/2013 06:11:12 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Column WHERE AD_Column_ID=2950266
;

-- 30/01/2013 06:11:42 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950163
;

-- 30/01/2013 06:11:48 PM VET
-- Sales Force Mobile with ADempiere
DELETE  FROM  AD_Field_Trl WHERE AD_Field_ID=2950163
;

-- 30/01/2013 06:11:48 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Field WHERE AD_Field_ID=2950163
;

-- 30/01/2013 06:11:59 PM VET
-- Sales Force Mobile with ADempiere
DELETE  FROM  AD_Column_Trl WHERE AD_Column_ID=2950266
;

-- 30/01/2013 06:11:59 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Column WHERE AD_Column_ID=2950266
;

-- 30/01/2013 06:13:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsInsertRecord='N',Updated=TO_DATE('2013-01-30 18:13:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 06:14:09 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsActive='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 18:14:09','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950272
;

-- 30/01/2013 06:19:49 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-30 18:19:49','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950160
;

-- 30/01/2013 06:19:53 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-30 18:19:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950165
;

-- 30/01/2013 06:20:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='N',Updated=TO_DATE('2013-01-30 18:20:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950165
;

-- 30/01/2013 06:20:25 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-30 18:20:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950167
;

-- 30/01/2013 06:20:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=14,Updated=TO_DATE('2013-01-30 18:20:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950162
;

-- 30/01/2013 06:21:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-30 18:21:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950159
;

-- 30/01/2013 06:21:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-30 18:21:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950165
;

-- 30/01/2013 06:22:45 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=14,Updated=TO_DATE('2013-01-30 18:22:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950167
;

-- 30/01/2013 06:27:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='N',Updated=TO_DATE('2013-01-30 18:27:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950165
;


