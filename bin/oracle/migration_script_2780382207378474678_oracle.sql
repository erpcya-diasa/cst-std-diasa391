-- 28/01/2013 10:58:31 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_EntityType (AD_Client_ID,AD_EntityType_ID,AD_Org_ID,Created,CreatedBy,EntityType,IsActive,Name,Processing,Updated,UpdatedBy) VALUES (0,2950000,0,TO_DATE('2013-01-28 10:58:30','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Android SF','N',TO_DATE('2013-01-28 10:58:30','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 10:59:06 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_EntityType SET ModelPackage='org.sf.model',Updated=TO_DATE('2013-01-28 10:59:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_EntityType_ID=2950000
;

-- 28/01/2013 10:59:54 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950002,'N',TO_DATE('2013-01-28 10:59:54','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','Y','Y','N','Y','N','N','N',0,'Customer Inventory','L','SF_CustomerInventory',TO_DATE('2013-01-28 10:59:54','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 10:59:54 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950002 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 10:59:54 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000001,TO_DATE('2013-01-28 10:59:54','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_CustomerInventory',1,'Y','N','Y','Y','SF_CustomerInventory','N',1000000,TO_DATE('2013-01-28 10:59:54','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:00:04 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Inventario de los Clientes',Updated=TO_DATE('2013-01-28 11:00:04','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950002 AND AD_Language='es_MX'
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950054,102,0,19,2950002,129,'AD_Client_ID',TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950054 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950055,113,0,19,2950002,104,'AD_Org_ID',TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950055 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950056,245,0,16,2950002,'Created',TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950056 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950057,246,0,18,110,2950002,'CreatedBy',TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950057 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950058,348,0,20,2950002,'IsActive',TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950058 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950059,607,0,16,2950002,'Updated',TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950059 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950060,608,0,18,110,2950002,'UpdatedBy',TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 11:00:19','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:00:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950060 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:01:18 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950001,0,'SF_CustomerInventory',TO_DATE('2013-01-28 11:01:18','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Customer Inventory','Customer Inventory',TO_DATE('2013-01-28 11:01:18','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:01:18 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950001 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 11:01:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Inventario de los Clientes',PrintName='Inventario de los Clientes',Updated=TO_DATE('2013-01-28 11:01:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950001 AND AD_Language='es_MX'
;

-- 28/01/2013 11:01:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET ColumnName='SF_CustomerInventory_ID',Updated=TO_DATE('2013-01-28 11:01:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950001
;

-- 28/01/2013 11:01:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_CustomerInventory_ID', Name='Customer Inventory', Description=NULL, Help=NULL WHERE AD_Element_ID=2950001
;

-- 28/01/2013 11:01:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_CustomerInventory_ID', Name='Customer Inventory', Description=NULL, Help=NULL, AD_Element_ID=2950001 WHERE UPPER(ColumnName)='SF_CUSTOMERINVENTORY_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 11:01:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_CustomerInventory_ID', Name='Customer Inventory', Description=NULL, Help=NULL WHERE AD_Element_ID=2950001 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 11:02:44 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950061,2950001,0,13,2950002,'SF_CustomerInventory_ID',TO_DATE('2013-01-28 11:02:43','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','Y','Y','N','N','N','N','N','Customer Inventory',0,TO_DATE('2013-01-28 11:02:43','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:02:44 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950061 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:03:49 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950062,187,0,13,2950002,'C_BPartner_ID',TO_DATE('2013-01-28 11:03:49','YYYY-MM-DD HH24:MI:SS'),100,'Identifies a Business Partner','SF',10,'A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Business Partner ',0,TO_DATE('2013-01-28 11:03:49','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:03:49 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950062 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:05:08 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_ID=19, AD_Val_Rule_ID=230, IsMandatory='Y',Updated=TO_DATE('2013-01-28 11:05:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950062
;

-- 28/01/2013 11:06:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950063,189,0,19,2950002,131,'C_BPartner_Location_ID',TO_DATE('2013-01-28 11:06:27','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner','SF',10,'The Partner address indicates the location of a Business Partner','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Partner Location',0,TO_DATE('2013-01-28 11:06:27','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:06:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950063 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:07:07 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950064,449,0,19,2950002,271,'M_PriceList_ID',TO_DATE('2013-01-28 11:07:07','YYYY-MM-DD HH24:MI:SS'),100,'Unique identifier of a Price List','SF',10,'Price Lists are used to determine the pricing, margin and cost of items purchased or sold.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Price List',0,TO_DATE('2013-01-28 11:07:07','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:07:07 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950064 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:07:44 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950065,558,0,19,2950002,'C_Order_ID',TO_DATE('2013-01-28 11:07:44','YYYY-MM-DD HH24:MI:SS'),100,'Order','SF',10,'The Order is a control document.  The  Order is complete when the quantity ordered is the same as the quantity shipped and invoiced.  When you close an order, unshipped (backordered) quantities are cancelled.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Order',0,TO_DATE('2013-01-28 11:07:44','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:07:44 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950065 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:08:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950066,265,0,15,2950002,'DateDoc',TO_DATE('2013-01-28 11:08:12','YYYY-MM-DD HH24:MI:SS'),100,'Date of the Document','SF',7,'The Document Date indicates the date the document was generated.  It may or may not be the same as the accounting date.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Document Date',0,TO_DATE('2013-01-28 11:08:12','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:08:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950066 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:08:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950067,275,0,14,2950002,'Description',TO_DATE('2013-01-28 11:08:43','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 11:08:43','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:08:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950067 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:11:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950068,289,0,17,131,2950002,'DocStatus',TO_DATE('2013-01-28 11:11:41','YYYY-MM-DD HH24:MI:SS'),100,'The current status of the document','SF',2,'The Document Status indicates the status of a document at this time.  If you want to change the document status, use the Document Action field','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Document Status',0,TO_DATE('2013-01-28 11:11:41','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:11:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950068 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:17:34 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Process (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Process_ID,AD_Workflow_ID,Created,CreatedBy,EntityType,IsActive,IsBetaFunctionality,IsDirectPrint,IsReport,IsServerProcess,Name,ShowHelp,Statistic_Count,Statistic_Seconds,Updated,UpdatedBy,Value) VALUES ('1',0,0,2950000,116,TO_DATE('2013-01-28 11:17:34','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N','N','N','N','Customer Inventory','Y',0,0,TO_DATE('2013-01-28 11:17:34','YYYY-MM-DD HH24:MI:SS'),100,'prc_CustomerInventory')
;

-- 28/01/2013 11:17:34 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Process_Trl (AD_Language,AD_Process_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Process_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Process t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Process_ID=2950000 AND NOT EXISTS (SELECT * FROM AD_Process_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Process_ID=t.AD_Process_ID)
;

-- 28/01/2013 11:17:54 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Trl SET Name='Inventario de los Clientes',Description='Inventario de los Clientes',Updated=TO_DATE('2013-01-28 11:17:54','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Process_ID=2950000 AND AD_Language='es_MX'
;

-- 28/01/2013 11:17:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process SET Description='Customer Inventory',Updated=TO_DATE('2013-01-28 11:17:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Process_ID=2950000
;

-- 28/01/2013 11:17:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Trl SET IsTranslated='N' WHERE AD_Process_ID=2950000
;

-- 28/01/2013 11:18:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Process_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950069,287,0,2950000,28,2950002,'DocAction',TO_DATE('2013-01-28 11:18:41','YYYY-MM-DD HH24:MI:SS'),100,'The targeted status of the document','SF',2,'You find the current status in the Document Status field. The options are listed in a popup','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Document Action',0,TO_DATE('2013-01-28 11:18:41','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:18:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950069 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:20:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950070,1047,0,20,2950002,'Processed',TO_DATE('2013-01-28 11:20:14','YYYY-MM-DD HH24:MI:SS'),100,'The document has been processed','SF',1,'The Processed checkbox indicates that a document has been processed.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Processed',0,TO_DATE('2013-01-28 11:20:14','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:20:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950070 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:35:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950003,'N',TO_DATE('2013-01-28 11:35:16','YYYY-MM-DD HH24:MI:SS'),100,'Inventory Type','SF','N','Y','Y','N','Y','N','N','N',0,'Inventory Type','L','SF_InventoryType',TO_DATE('2013-01-28 11:35:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:35:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950003 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 11:35:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000002,TO_DATE('2013-01-28 11:35:16','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_InventoryType',1,'Y','N','Y','Y','SF_InventoryType','N',1000000,TO_DATE('2013-01-28 11:35:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950071,102,0,19,2950003,129,'AD_Client_ID',TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950071 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950072,113,0,19,2950003,104,'AD_Org_ID',TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950072 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950073,245,0,16,2950003,'Created',TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950073 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950074,246,0,18,110,2950003,'CreatedBy',TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950074 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950075,348,0,20,2950003,'IsActive',TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950075 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950076,607,0,16,2950003,'Updated',TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950076 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950077,608,0,18,110,2950003,'UpdatedBy',TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 11:35:39','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:35:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950077 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:35:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Tipo de Inventario',Updated=TO_DATE('2013-01-28 11:35:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950003 AND AD_Language='es_MX'
;

-- 28/01/2013 11:36:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950078,469,0,10,2950003,'Name',TO_DATE('2013-01-28 11:36:41','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity','SF',60,'The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','N','N','N','N','N','Y','N','Y','N','N','Y','Name',0,TO_DATE('2013-01-28 11:36:41','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:36:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950078 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:37:10 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950079,275,0,14,2950003,'Description',TO_DATE('2013-01-28 11:37:10','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 11:37:10','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:37:10 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950079 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:37:32 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950080,1103,0,20,2950003,'IsDefault',TO_DATE('2013-01-28 11:37:31','YYYY-MM-DD HH24:MI:SS'),100,'Default value','SF',1,'The Default Checkbox indicates if this record will be used as a default value.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Default',0,TO_DATE('2013-01-28 11:37:31','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:37:32 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950080 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:37:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y',Updated=TO_DATE('2013-01-28 11:37:39','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950080
;

-- 28/01/2013 11:38:30 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950004,'N',TO_DATE('2013-01-28 11:38:30','YYYY-MM-DD HH24:MI:SS'),100,'Customer Inventory Line','SF','N','Y','Y','N','Y','N','N','N',0,'Customer Inventory Line','L','SF_CustomerInventoryLine',TO_DATE('2013-01-28 11:38:30','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:38:30 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950004 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 11:38:30 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000003,TO_DATE('2013-01-28 11:38:30','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_CustomerInventoryLine',1,'Y','N','Y','Y','SF_CustomerInventoryLine','N',1000000,TO_DATE('2013-01-28 11:38:30','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:38:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Linea del inventario de los clientes',Updated=TO_DATE('2013-01-28 11:38:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950004 AND AD_Language='es_MX'
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950081,102,0,19,2950004,129,'AD_Client_ID',TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950081 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950082,113,0,19,2950004,104,'AD_Org_ID',TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950082 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950083,245,0,16,2950004,'Created',TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950083 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950084,246,0,18,110,2950004,'CreatedBy',TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950084 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950085,348,0,20,2950004,'IsActive',TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950085 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950086,607,0,16,2950004,'Updated',TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950086 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950087,608,0,18,110,2950004,'UpdatedBy',TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 11:39:00','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:39:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950087 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:39:27 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950088,454,0,19,2950004,'M_Product_ID',TO_DATE('2013-01-28 11:39:27','YYYY-MM-DD HH24:MI:SS'),100,'Product, Service, Item','SF',10,'Identifies an item which is either purchased or sold in this organization.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Product',0,TO_DATE('2013-01-28 11:39:27','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:39:27 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950088 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:41:05 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950002,0,'QtyRead',TO_DATE('2013-01-28 11:41:04','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Quantity Read','Quantity Read',TO_DATE('2013-01-28 11:41:04','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:41:05 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950002 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 11:41:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Cantidad leída ',PrintName='Cantidad leída ',Updated=TO_DATE('2013-01-28 11:41:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950002 AND AD_Language='es_MX'
;

-- 28/01/2013 11:42:22 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950089,2950002,0,29,2950004,'QtyRead',TO_DATE('2013-01-28 11:42:22','YYYY-MM-DD HH24:MI:SS'),100,'SF',14,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Quantity Read',0,TO_DATE('2013-01-28 11:42:22','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:42:22 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950089 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:43:11 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950003,0,'SF_InventoryType_ID',TO_DATE('2013-01-28 11:43:11','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Inventory Type','Inventory Type',TO_DATE('2013-01-28 11:43:11','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:43:11 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950003 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 11:43:21 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Tipo de Inventario',PrintName='Tipo de Inventario',Updated=TO_DATE('2013-01-28 11:43:21','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950003 AND AD_Language='es_MX'
;

-- 28/01/2013 11:43:49 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950090,2950003,0,19,2950004,'SF_InventoryType_ID',TO_DATE('2013-01-28 11:43:49','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Inventory Type',0,TO_DATE('2013-01-28 11:43:49','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:43:49 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950090 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:44:32 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950091,1047,0,20,2950004,'Processed',TO_DATE('2013-01-28 11:44:32','YYYY-MM-DD HH24:MI:SS'),100,'The document has been processed','SF',1,'The Processed checkbox indicates that a document has been processed.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Processed',0,TO_DATE('2013-01-28 11:44:32','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:44:32 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950091 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:45:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950092,2950001,0,19,2950004,'SF_CustomerInventory_ID',TO_DATE('2013-01-28 11:45:01','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Customer Inventory',0,TO_DATE('2013-01-28 11:45:01','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:45:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950092 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:45:47 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950004,0,'SF_CustomerInventoryLine',TO_DATE('2013-01-28 11:45:47','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Customer Inventory Line','Customer Inventory Line',TO_DATE('2013-01-28 11:45:47','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:45:47 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950004 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 11:46:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Linea del inventario de los clientes',PrintName='Linea del inventario de los clientes',Updated=TO_DATE('2013-01-28 11:46:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950004 AND AD_Language='es_MX'
;

-- 28/01/2013 11:46:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET ColumnName='SF_CustomerInventoryLine_ID',Updated=TO_DATE('2013-01-28 11:46:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950004
;

-- 28/01/2013 11:46:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_CustomerInventoryLine_ID', Name='Customer Inventory Line', Description=NULL, Help=NULL WHERE AD_Element_ID=2950004
;

-- 28/01/2013 11:46:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_CustomerInventoryLine_ID', Name='Customer Inventory Line', Description=NULL, Help=NULL, AD_Element_ID=2950004 WHERE UPPER(ColumnName)='SF_CUSTOMERINVENTORYLINE_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 11:46:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_CustomerInventoryLine_ID', Name='Customer Inventory Line', Description=NULL, Help=NULL WHERE AD_Element_ID=2950004 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 11:46:53 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950093,2950004,0,13,2950004,'SF_CustomerInventoryLine_ID',TO_DATE('2013-01-28 11:46:53','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Customer Inventory Line',0,TO_DATE('2013-01-28 11:46:53','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:46:53 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950093 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:48:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950094,2950003,0,13,2950003,'SF_InventoryType_ID',TO_DATE('2013-01-28 11:48:00','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Inventory Type',0,TO_DATE('2013-01-28 11:48:00','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:48:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950094 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:50:52 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950005,'N',TO_DATE('2013-01-28 11:50:52','YYYY-MM-DD HH24:MI:SS'),100,'Event Type','SF','N','Y','Y','N','Y','N','N','N',0,'Event Type','L','SF_EventType',TO_DATE('2013-01-28 11:50:52','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:50:52 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950005 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 11:50:52 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000004,TO_DATE('2013-01-28 11:50:52','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_EventType',1,'Y','N','Y','Y','SF_EventType','N',1000000,TO_DATE('2013-01-28 11:50:52','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:50:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Tipo de Evento',Updated=TO_DATE('2013-01-28 11:50:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950005 AND AD_Language='es_MX'
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950095,102,0,19,2950005,129,'AD_Client_ID',TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950095 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950096,113,0,19,2950005,104,'AD_Org_ID',TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950096 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950097,245,0,16,2950005,'Created',TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950097 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950098,246,0,18,110,2950005,'CreatedBy',TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950098 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950099,348,0,20,2950005,'IsActive',TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950099 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950100,607,0,16,2950005,'Updated',TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950100 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950101,608,0,18,110,2950005,'UpdatedBy',TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 11:51:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 11:51:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950101 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:51:53 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950005,0,'SF_EventType_ID',TO_DATE('2013-01-28 11:51:53','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Event Type','Event Type',TO_DATE('2013-01-28 11:51:53','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:51:53 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950005 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 11:52:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Tipo de Evento',PrintName='Tipo de Evento',Updated=TO_DATE('2013-01-28 11:52:02','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950005 AND AD_Language='es_MX'
;

-- 28/01/2013 11:52:22 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950102,2950005,0,13,2950005,'SF_EventType_ID',TO_DATE('2013-01-28 11:52:22','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Event Type',0,TO_DATE('2013-01-28 11:52:22','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:52:22 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950102 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:53:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950103,1103,0,20,2950005,'IsDefault',TO_DATE('2013-01-28 11:53:13','YYYY-MM-DD HH24:MI:SS'),100,'Default value','SF',1,'The Default Checkbox indicates if this record will be used as a default value.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Default',0,TO_DATE('2013-01-28 11:53:13','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:53:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950103 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:53:47 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950104,469,0,10,2950005,'Name',TO_DATE('2013-01-28 11:53:47','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity','SF',60,'The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','N','N','N','Y','N','Y','N','Y','N','N','Y','Name',0,TO_DATE('2013-01-28 11:53:47','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:53:47 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950104 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:54:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950105,275,0,14,2950005,'Description',TO_DATE('2013-01-28 11:54:42','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 11:54:42','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:54:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950105 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:55:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950006,0,'PercentEffectivity',TO_DATE('2013-01-28 11:55:39','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Percent ffectivity','Percent Effectivity',TO_DATE('2013-01-28 11:55:39','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:55:39 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950006 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 11:55:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Porcentaje de Efectividad',PrintName='Porcentaje de Efectividad',Updated=TO_DATE('2013-01-28 11:55:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950006 AND AD_Language='es_MX'
;

-- 28/01/2013 11:56:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950106,2950006,0,22,2950005,'PercentEffectivity',TO_DATE('2013-01-28 11:56:19','YYYY-MM-DD HH24:MI:SS'),100,'SF',14,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Percent ffectivity',0,TO_DATE('2013-01-28 11:56:19','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 11:56:19 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950106 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 11:57:20 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950007,0,'BaseEventType',TO_DATE('2013-01-28 11:57:20','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Base Event Type','Base Event Type',TO_DATE('2013-01-28 11:57:20','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 11:57:20 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950007 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 11:57:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Tipo de Evento Base',PrintName='Tipo de Evento Base',Updated=TO_DATE('2013-01-28 11:57:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950007 AND AD_Language='es_MX'
;

-- 28/01/2013 12:03:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference (AD_Client_ID,AD_Org_ID,AD_Reference_ID,Created,CreatedBy,Description,EntityType,IsActive,IsOrderByValue,Name,Updated,UpdatedBy,ValidationType,VFormat) VALUES (0,0,2950000,TO_DATE('2013-01-28 12:03:02','YYYY-MM-DD HH24:MI:SS'),100,'Base Event Type','SF','Y','Y','Base Event Type',TO_DATE('2013-01-28 12:03:02','YYYY-MM-DD HH24:MI:SS'),100,'L','LLL')
;

-- 28/01/2013 12:03:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference_Trl (AD_Language,AD_Reference_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Reference_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Reference t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Reference_ID=2950000 AND NOT EXISTS (SELECT * FROM AD_Reference_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Reference_ID=t.AD_Reference_ID)
;

-- 28/01/2013 12:03:12 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET Name='Tipo de Evento Base',Description='Tipo de Evento Base',Updated=TO_DATE('2013-01-28 12:03:12','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950000 AND AD_Language='es_MX'
;

-- 28/01/2013 12:03:48 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950000,2950000,TO_DATE('2013-01-28 12:03:48','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','AR Receipt',TO_DATE('2013-01-28 12:03:48','YYYY-MM-DD HH24:MI:SS'),100,'ARR')
;

-- 28/01/2013 12:03:48 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950000 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 12:04:05 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950000,2950001,TO_DATE('2013-01-28 12:04:05','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Making Inventory',TO_DATE('2013-01-28 12:04:05','YYYY-MM-DD HH24:MI:SS'),100,'MII')
;

-- 28/01/2013 12:04:05 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950001 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 12:04:22 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950000,2950002,TO_DATE('2013-01-28 12:04:22','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','No Receipt Customer',TO_DATE('2013-01-28 12:04:22','YYYY-MM-DD HH24:MI:SS'),100,'NRC')
;

-- 28/01/2013 12:04:22 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950002 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 12:04:39 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950000,2950003,TO_DATE('2013-01-28 12:04:39','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','No Sales Order',TO_DATE('2013-01-28 12:04:39','YYYY-MM-DD HH24:MI:SS'),100,'NSO')
;

-- 28/01/2013 12:04:39 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950003 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 12:04:53 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950000,2950004,TO_DATE('2013-01-28 12:04:53','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','No Visit Customer',TO_DATE('2013-01-28 12:04:53','YYYY-MM-DD HH24:MI:SS'),100,'NVC')
;

-- 28/01/2013 12:04:53 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950004 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 12:05:12 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,Description,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950000,2950005,TO_DATE('2013-01-28 12:05:12','YYYY-MM-DD HH24:MI:SS'),100,'ACH Automatic Clearing House','U','Y','Direct Deposit',TO_DATE('2013-01-28 12:05:12','YYYY-MM-DD HH24:MI:SS'),100,'RDD')
;

-- 28/01/2013 12:05:12 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950005 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 12:05:15 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List SET EntityType='SF',Updated=TO_DATE('2013-01-28 12:05:15','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950005
;

-- 28/01/2013 12:05:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Depósito Directo',Description='O Transferencia',Updated=TO_DATE('2013-01-28 12:05:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950005 AND AD_Language='es_MX'
;

-- 28/01/2013 12:06:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950000,2950006,TO_DATE('2013-01-28 12:06:08','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Return Material',TO_DATE('2013-01-28 12:06:08','YYYY-MM-DD HH24:MI:SS'),100,'RMS')
;

-- 28/01/2013 12:06:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950006 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 12:06:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Devolución de Material',Description='Autorización de Devolución de Material',Updated=TO_DATE('2013-01-28 12:06:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950006 AND AD_Language='es_MX'
;

-- 28/01/2013 12:06:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950000,2950007,TO_DATE('2013-01-28 12:06:47','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Sales Order',TO_DATE('2013-01-28 12:06:47','YYYY-MM-DD HH24:MI:SS'),100,'SOO')
;

-- 28/01/2013 12:06:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950007 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 12:07:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Orden de Venta',Description='Toma de Pedido',Updated=TO_DATE('2013-01-28 12:07:00','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950007 AND AD_Language='es_MX'
;

-- 28/01/2013 12:07:24 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Cobranza',Description='Cobranza al Cliente',Updated=TO_DATE('2013-01-28 12:07:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950000 AND AD_Language='es_MX'
;

-- 28/01/2013 12:07:55 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Toma de Inventario',Description='Toma de Inventario a Cliente',Updated=TO_DATE('2013-01-28 12:07:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950001 AND AD_Language='es_MX'
;

-- 28/01/2013 12:08:57 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='No Cobro',Description='No se Realizó el Cobro al Cliente',Updated=TO_DATE('2013-01-28 12:08:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950002 AND AD_Language='es_MX'
;

-- 28/01/2013 12:09:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='No Venta',Description='No se tomó Pedido',Updated=TO_DATE('2013-01-28 12:09:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950003 AND AD_Language='es_MX'
;

-- 28/01/2013 12:09:37 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='No Visita',Description='No se visitó al Cliente',Updated=TO_DATE('2013-01-28 12:09:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950004 AND AD_Language='es_MX'
;

-- 28/01/2013 12:11:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950107,2950007,0,17,2950000,2950005,'BaseEventType',TO_DATE('2013-01-28 12:11:07','YYYY-MM-DD HH24:MI:SS'),100,'SF',3,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Base Event Type',0,TO_DATE('2013-01-28 12:11:07','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:11:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950107 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:15:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950006,'N',TO_DATE('2013-01-28 12:15:08','YYYY-MM-DD HH24:MI:SS'),100,'Frequency','SF','N','Y','Y','N','Y','N','N','N',0,'Frequency','L','SF_Frequency',TO_DATE('2013-01-28 12:15:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 12:15:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950006 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 12:15:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000005,TO_DATE('2013-01-28 12:15:08','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_Frequency',1,'Y','N','Y','Y','SF_Frequency','N',1000000,TO_DATE('2013-01-28 12:15:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 12:15:15 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Frecuencia',Updated=TO_DATE('2013-01-28 12:15:15','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950006 AND AD_Language='es_MX'
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950108,102,0,19,2950006,129,'AD_Client_ID',TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950108 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950109,113,0,19,2950006,104,'AD_Org_ID',TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950109 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950110,245,0,16,2950006,'Created',TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950110 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950111,246,0,18,110,2950006,'CreatedBy',TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950111 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950112,348,0,20,2950006,'IsActive',TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950112 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950113,607,0,16,2950006,'Updated',TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950113 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950114,608,0,18,110,2950006,'UpdatedBy',TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 12:15:29','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:15:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950114 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:16:19 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950008,0,'SF_Frequency_ID',TO_DATE('2013-01-28 12:16:18','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Frequency','Frequency',TO_DATE('2013-01-28 12:16:18','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 12:16:19 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950008 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 12:16:27 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Frecuencia',PrintName='Frecuencia',Updated=TO_DATE('2013-01-28 12:16:27','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950008 AND AD_Language='es_MX'
;

-- 28/01/2013 12:16:36 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950115,2950008,0,13,2950006,'SF_Frequency_ID',TO_DATE('2013-01-28 12:16:36','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Frequency',0,TO_DATE('2013-01-28 12:16:36','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:16:36 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950115 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:16:45 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y',Updated=TO_DATE('2013-01-28 12:16:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950115
;

-- 28/01/2013 12:17:12 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950116,469,0,10,2950006,'Name',TO_DATE('2013-01-28 12:17:12','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity','SF',60,'The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','N','N','N','Y','N','N','N','Y','N','N','Y','Name',0,TO_DATE('2013-01-28 12:17:12','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:17:12 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950116 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:17:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950117,275,0,14,2950006,'Description',TO_DATE('2013-01-28 12:17:34','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 12:17:34','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:17:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950117 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:18:35 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950009,0,'IntervalDays',TO_DATE('2013-01-28 12:18:35','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Interval Days','Interval Days',TO_DATE('2013-01-28 12:18:35','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 12:18:35 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950009 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 12:18:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Días de Intervalo',PrintName='Días de Intervalo',Updated=TO_DATE('2013-01-28 12:18:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950009 AND AD_Language='es_MX'
;

-- 28/01/2013 12:19:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950118,2950009,0,29,2950006,'IntervalDays',TO_DATE('2013-01-28 12:19:11','YYYY-MM-DD HH24:MI:SS'),100,'U',14,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Interval Days',0,TO_DATE('2013-01-28 12:19:11','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:19:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950118 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:19:16 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET EntityType='SF',Updated=TO_DATE('2013-01-28 12:19:16','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950118
;

-- 28/01/2013 12:20:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950007,'N',TO_DATE('2013-01-28 12:20:50','YYYY-MM-DD HH24:MI:SS'),100,'Initial Load','SF','N','Y','Y','N','Y','N','N','N',0,'Initial Load','L','SF_InitialLoad',TO_DATE('2013-01-28 12:20:50','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 12:20:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950007 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 12:20:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000006,TO_DATE('2013-01-28 12:20:50','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_InitialLoad',1,'Y','N','Y','Y','SF_InitialLoad','N',1000000,TO_DATE('2013-01-28 12:20:50','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 12:20:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Carga Inicial',Updated=TO_DATE('2013-01-28 12:20:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950007 AND AD_Language='es_MX'
;

-- 28/01/2013 12:21:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950119,102,0,19,2950007,129,'AD_Client_ID',TO_DATE('2013-01-28 12:21:07','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 12:21:07','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:21:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950119 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:21:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950120,113,0,19,2950007,104,'AD_Org_ID',TO_DATE('2013-01-28 12:21:07','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 12:21:07','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:21:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950120 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:21:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950121,245,0,16,2950007,'Created',TO_DATE('2013-01-28 12:21:07','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 12:21:07','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:21:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950121 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:21:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950122,246,0,18,110,2950007,'CreatedBy',TO_DATE('2013-01-28 12:21:07','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 12:21:07','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:21:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950122 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:21:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950123,348,0,20,2950007,'IsActive',TO_DATE('2013-01-28 12:21:08','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 12:21:08','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:21:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950123 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:21:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950124,607,0,16,2950007,'Updated',TO_DATE('2013-01-28 12:21:08','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 12:21:08','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:21:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950124 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:21:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950125,608,0,18,110,2950007,'UpdatedBy',TO_DATE('2013-01-28 12:21:08','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 12:21:08','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 12:21:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950125 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:21:51 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950126,469,0,10,2950007,'Name',TO_DATE('2013-01-28 12:21:51','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity','SF',60,'The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','N','N','N','Y','N','N','N','Y','N','N','Y','Name',0,TO_DATE('2013-01-28 12:21:51','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:21:51 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950126 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:22:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950127,275,0,14,2950007,'Description',TO_DATE('2013-01-28 12:22:11','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 12:22:11','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:22:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950127 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:22:53 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950128,566,0,22,2950007,'SeqNo',TO_DATE('2013-01-28 12:22:53','YYYY-MM-DD HH24:MI:SS'),100,'Method of ordering records; lowest number comes first','SF',14,'The Sequence indicates the order of records','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Sequence',0,TO_DATE('2013-01-28 12:22:53','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:22:53 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950128 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:23:03 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y',Updated=TO_DATE('2013-01-28 12:23:03','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950126
;

-- 28/01/2013 12:24:37 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950129,50028,0,14,2950007,'SQLStatement',TO_DATE('2013-01-28 12:24:37','YYYY-MM-DD HH24:MI:SS'),100,'SF',255,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','SQLStatement',0,TO_DATE('2013-01-28 12:24:37','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 12:24:37 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950129 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 12:25:05 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_UserQuery (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_User_ID,AD_UserQuery_ID,Created,CreatedBy,IsActive,Name,Updated,UpdatedBy) VALUES (0,0,203,276,100,1000000,TO_DATE('2013-01-28 12:25:05','YYYY-MM-DD HH24:MI:SS'),100,'Y','** Last Query **',TO_DATE('2013-01-28 12:25:05','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 12:25:22 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950010,0,'StatementType',TO_DATE('2013-01-28 12:25:22','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Statement Type','Statement Type',TO_DATE('2013-01-28 12:25:22','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 12:25:22 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950010 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 12:29:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Tipo de Instrucción',PrintName='Tipo de Instrucción',Updated=TO_DATE('2013-01-28 12:29:22','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950010 AND AD_Language='es_MX'
;

-- 28/01/2013 02:14:10 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950130,2950010,0,17,2950007,'StatementType',TO_DATE('2013-01-28 14:14:10','YYYY-MM-DD HH24:MI:SS'),100,'SF',5,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Statement Type',0,TO_DATE('2013-01-28 14:14:10','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:14:10 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950130 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:14:43 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_UserQuery (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_User_ID,AD_UserQuery_ID,Created,CreatedBy,IsActive,Name,Updated,UpdatedBy) VALUES (0,0,102,102,100,1000001,TO_DATE('2013-01-28 14:14:43','YYYY-MM-DD HH24:MI:SS'),100,'Y','** Last Query **',TO_DATE('2013-01-28 14:14:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:15:13 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference (AD_Client_ID,AD_Org_ID,AD_Reference_ID,Created,CreatedBy,Description,EntityType,IsActive,IsOrderByValue,Name,Updated,UpdatedBy,ValidationType) VALUES (0,0,2950001,TO_DATE('2013-01-28 14:15:13','YYYY-MM-DD HH24:MI:SS'),100,'Statement Type','SF','Y','N','Statement Type',TO_DATE('2013-01-28 14:15:13','YYYY-MM-DD HH24:MI:SS'),100,'L')
;

-- 28/01/2013 02:15:13 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference_Trl (AD_Language,AD_Reference_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Reference_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Reference t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Reference_ID=2950001 AND NOT EXISTS (SELECT * FROM AD_Reference_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Reference_ID=t.AD_Reference_ID)
;

-- 28/01/2013 02:15:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET Name='Tipo de Instrucción',Description='Tipo de Instrucción',Updated=TO_DATE('2013-01-28 14:15:32','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950001 AND AD_Language='es_MX'
;

-- 28/01/2013 02:19:49 PM VET
-- Sales Force Mobile with ADempiere
DELETE  FROM  AD_Reference_Trl WHERE AD_Reference_ID=2950001
;

-- 28/01/2013 02:19:49 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Reference WHERE AD_Reference_ID=2950001
;

-- 28/01/2013 02:20:04 PM VET
-- Sales Force Mobile with ADempiere
DELETE  FROM  AD_Element_Trl WHERE AD_Element_ID=2950010
;

-- 28/01/2013 02:20:04 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Element WHERE AD_Element_ID=2950010
;

-- 28/01/2013 02:20:10 PM VET
-- Sales Force Mobile with ADempiere
DELETE  FROM  AD_Column_Trl WHERE AD_Column_ID=2950130
;

-- 28/01/2013 02:20:10 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Column WHERE AD_Column_ID=2950130
;

-- 28/01/2013 02:20:13 PM VET
-- Sales Force Mobile with ADempiere
DELETE  FROM  AD_Element_Trl WHERE AD_Element_ID=2950010
;

-- 28/01/2013 02:20:13 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Element WHERE AD_Element_ID=2950010
;

-- 28/01/2013 02:21:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950131,630,0,14,2950007,'WhereClause',TO_DATE('2013-01-28 14:21:55','YYYY-MM-DD HH24:MI:SS'),100,'Fully qualified SQL WHERE clause','SF',255,'The Where Clause indicates the SQL WHERE clause to use for record selection. The WHERE clause is added to the query. Fully qualified means "tablename.columnname".','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Sql WHERE',0,TO_DATE('2013-01-28 14:21:55','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:21:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950131 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:22:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950132,126,0,19,2950007,'AD_Table_ID',TO_DATE('2013-01-28 14:22:17','YYYY-MM-DD HH24:MI:SS'),100,'Database Table information','SF',10,'The Database Table provides the information of the table definition','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Table',0,TO_DATE('2013-01-28 14:22:17','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:22:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950132 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:23:33 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950008,'N',TO_DATE('2013-01-28 14:23:33','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit','SF','N','Y','Y','N','Y','N','N','N',0,'Planning Visit','L','SF_PlanningVisit',TO_DATE('2013-01-28 14:23:33','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:23:33 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950008 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 02:23:33 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000007,TO_DATE('2013-01-28 14:23:33','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_PlanningVisit',1,'Y','N','Y','Y','SF_PlanningVisit','N',1000000,TO_DATE('2013-01-28 14:23:33','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:23:55 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Planificación de visita',Updated=TO_DATE('2013-01-28 14:23:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950008 AND AD_Language='es_MX'
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950133,102,0,19,2950008,129,'AD_Client_ID',TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950133 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950134,113,0,19,2950008,104,'AD_Org_ID',TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950134 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950135,245,0,16,2950008,'Created',TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950135 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950136,246,0,18,110,2950008,'CreatedBy',TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950136 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950137,348,0,20,2950008,'IsActive',TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950137 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950138,607,0,16,2950008,'Updated',TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950138 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950139,608,0,18,110,2950008,'UpdatedBy',TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 14:24:04','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:24:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950139 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:25:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950011,0,'SF_InitialLoad_ID',TO_DATE('2013-01-28 14:25:14','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Initial Load','Initial Load',TO_DATE('2013-01-28 14:25:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:25:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950011 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 02:25:23 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Carga Inicial',PrintName='Carga Inicial',Updated=TO_DATE('2013-01-28 14:25:23','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950011 AND AD_Language='es_MX'
;

-- 28/01/2013 02:27:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950140,2950011,0,13,2950007,'SF_InitialLoad_ID',TO_DATE('2013-01-28 14:27:47','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Initial Load',0,TO_DATE('2013-01-28 14:27:47','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:27:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950140 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:29:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950141,469,0,10,2950008,'Name',TO_DATE('2013-01-28 14:29:17','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity','SF',60,'The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','N','N','N','Y','N','N','N','Y','N','N','Y','Name',0,TO_DATE('2013-01-28 14:29:17','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:29:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950141 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:29:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y',Updated=TO_DATE('2013-01-28 14:29:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950141
;

-- 28/01/2013 02:29:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950142,275,0,14,2950008,'Description',TO_DATE('2013-01-28 14:29:50','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 14:29:50','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:29:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950142 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:30:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950143,187,0,19,2950008,'C_BPartner_ID',TO_DATE('2013-01-28 14:30:14','YYYY-MM-DD HH24:MI:SS'),100,'Identifies a Business Partner','SF',10,'A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Business Partner ',0,TO_DATE('2013-01-28 14:30:14','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:30:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950143 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:30:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950144,2040,0,18,2950008,'Bill_Location_ID',TO_DATE('2013-01-28 14:30:55','YYYY-MM-DD HH24:MI:SS'),100,'Business Partner Location for invoicing','U',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Invoice Location',0,TO_DATE('2013-01-28 14:30:55','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:30:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950144 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:31:13 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET EntityType='SF',Updated=TO_DATE('2013-01-28 14:31:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950144
;

-- 28/01/2013 02:31:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950145,189,0,19,2950008,'C_BPartner_Location_ID',TO_DATE('2013-01-28 14:31:34','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner','SF',10,'The Partner address indicates the location of a Business Partner','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Partner Location',0,TO_DATE('2013-01-28 14:31:34','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:31:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950145 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:32:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950146,210,0,19,2950008,'C_SalesRegion_ID',TO_DATE('2013-01-28 14:32:08','YYYY-MM-DD HH24:MI:SS'),100,'Sales coverage region','SF',10,'The Sales Region indicates a specific area of sales coverage.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Sales Region',0,TO_DATE('2013-01-28 14:32:08','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:32:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950146 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:34:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_Value_ID=159, AD_Val_Rule_ID=200,Updated=TO_DATE('2013-01-28 14:34:00','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950144
;

-- 28/01/2013 02:34:53 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950147,2002,0,20,2950008,'IsValid',TO_DATE('2013-01-28 14:34:53','YYYY-MM-DD HH24:MI:SS'),100,'Element is valid','SF',1,'The element passed the validation check','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Valid',0,TO_DATE('2013-01-28 14:34:53','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:34:53 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950147 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:35:35 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950148,566,0,22,2950008,'SeqNo',TO_DATE('2013-01-28 14:35:35','YYYY-MM-DD HH24:MI:SS'),100,'Method of ordering records; lowest number comes first','SF',14,'The Sequence indicates the order of records','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Sequence',0,TO_DATE('2013-01-28 14:35:35','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:35:35 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950148 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:36:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950149,617,0,15,2950008,'ValidFrom',TO_DATE('2013-01-28 14:36:02','YYYY-MM-DD HH24:MI:SS'),100,'Valid from including this date (first day)','SF',7,'The Valid From date indicates the first day of a date range','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Valid from',0,TO_DATE('2013-01-28 14:36:02','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:36:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950149 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:37:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950012,0,'VisitDay',TO_DATE('2013-01-28 14:37:06','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Visit Day ','Visit Day',TO_DATE('2013-01-28 14:37:06','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:37:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950012 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 02:37:46 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Día de Visita',PrintName='Día de Visita',Updated=TO_DATE('2013-01-28 14:37:46','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950012 AND AD_Language='es_MX'
;

-- 28/01/2013 02:38:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950150,2950012,0,15,2950008,'VisitDay',TO_DATE('2013-01-28 14:38:06','YYYY-MM-DD HH24:MI:SS'),100,'SF',7,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Visit Day ',0,TO_DATE('2013-01-28 14:38:06','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:38:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950150 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:38:31 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950151,2950008,0,19,2950008,'SF_Frequency_ID',TO_DATE('2013-01-28 14:38:31','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Frequency',0,TO_DATE('2013-01-28 14:38:31','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:38:31 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950151 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:38:45 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950152,2950005,0,19,2950008,'SF_EventType_ID',TO_DATE('2013-01-28 14:38:45','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Event Type',0,TO_DATE('2013-01-28 14:38:45','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:38:45 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950152 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:39:38 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950013,0,'SF_PlanningVisit_ID',TO_DATE('2013-01-28 14:39:38','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Planning Visit','Planning Visit',TO_DATE('2013-01-28 14:39:38','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:39:38 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950013 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 02:39:57 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Planificación de visita',PrintName='Planificación de visita',Updated=TO_DATE('2013-01-28 14:39:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950013 AND AD_Language='es_MX'
;

-- 28/01/2013 02:40:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950153,2950013,0,13,2950008,'SF_PlanningVisit_ID',TO_DATE('2013-01-28 14:40:17','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Planning Visit',0,TO_DATE('2013-01-28 14:40:17','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:40:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950153 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:41:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950009,'N',TO_DATE('2013-01-28 14:41:15','YYYY-MM-DD HH24:MI:SS'),100,'Doc Sequence','SF','N','Y','Y','N','Y','N','N','N',0,'Doc Sequence','L','SF_DocSequence',TO_DATE('2013-01-28 14:41:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:41:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950009 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 02:41:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000008,TO_DATE('2013-01-28 14:41:15','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_DocSequence',1,'Y','N','Y','Y','SF_DocSequence','N',1000000,TO_DATE('2013-01-28 14:41:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:41:27 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Secuencia del Documento',Updated=TO_DATE('2013-01-28 14:41:27','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950009 AND AD_Language='es_MX'
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950154,102,0,19,2950009,129,'AD_Client_ID',TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950154 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950155,113,0,19,2950009,104,'AD_Org_ID',TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950155 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950156,245,0,16,2950009,'Created',TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950156 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950157,246,0,18,110,2950009,'CreatedBy',TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950157 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950158,348,0,20,2950009,'IsActive',TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950158 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950159,607,0,16,2950009,'Updated',TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 14:41:46','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:41:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950159 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:41:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950160,608,0,18,110,2950009,'UpdatedBy',TO_DATE('2013-01-28 14:41:47','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 14:41:47','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 02:41:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950160 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:42:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950161,196,0,19,2950009,'C_DocType_ID',TO_DATE('2013-01-28 14:42:30','YYYY-MM-DD HH24:MI:SS'),100,'Document type or rules','SF',10,'The Document Type determines document sequence and processing rules','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Document Type',0,TO_DATE('2013-01-28 14:42:30','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:42:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950161 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:44:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950162,1063,0,18,190,2950009,'SalesRep_ID',TO_DATE('2013-01-28 14:44:17','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent','SF',10,'The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Sales Representative',0,TO_DATE('2013-01-28 14:44:17','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:44:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950162 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:44:27 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y',Updated=TO_DATE('2013-01-28 14:44:27','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950162
;

-- 28/01/2013 02:44:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950163,124,0,19,2950009,'AD_Sequence_ID',TO_DATE('2013-01-28 14:44:46','YYYY-MM-DD HH24:MI:SS'),100,'Document Sequence','SF',10,'The Sequence defines the numbering sequence to be used for documents.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Sequence',0,TO_DATE('2013-01-28 14:44:46','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:44:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950163 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:49:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950014,0,'SF_DocSequence_ID',TO_DATE('2013-01-28 14:49:18','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Doc Sequence','Doc Sequence',TO_DATE('2013-01-28 14:49:18','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:49:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950014 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 02:49:32 PM VET
-- Sales Force Mobile with ADempiere
DELETE  FROM  AD_Element_Trl WHERE AD_Element_ID=2950014
;

-- 28/01/2013 02:49:32 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Element WHERE AD_Element_ID=2950014
;

-- 28/01/2013 02:49:52 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950015,0,'SF_DocSequence_ID',TO_DATE('2013-01-28 14:49:52','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Doc Sequence','Doc Sequence',TO_DATE('2013-01-28 14:49:52','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 02:49:52 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950015 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 02:50:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Secuencia del Documento',PrintName='Secuencia del Documento',Updated=TO_DATE('2013-01-28 14:50:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950015 AND AD_Language='es_MX'
;

-- 28/01/2013 02:50:23 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950164,2950015,0,13,2950009,'SF_DocSequence_ID',TO_DATE('2013-01-28 14:50:23','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Doc Sequence',0,TO_DATE('2013-01-28 14:50:23','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 02:50:23 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950164 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 02:56:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Base Event Type for Distinct Document',Updated=TO_DATE('2013-01-28 14:56:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950007
;

-- 28/01/2013 02:56:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950007
;

-- 28/01/2013 02:56:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='BaseEventType', Name='Base Event Type', Description='Base Event Type for Distinct Document', Help=NULL WHERE AD_Element_ID=2950007
;

-- 28/01/2013 02:56:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='BaseEventType', Name='Base Event Type', Description='Base Event Type for Distinct Document', Help=NULL, AD_Element_ID=2950007 WHERE UPPER(ColumnName)='BASEEVENTTYPE' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 02:56:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='BaseEventType', Name='Base Event Type', Description='Base Event Type for Distinct Document', Help=NULL WHERE AD_Element_ID=2950007 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 02:56:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Base Event Type', Description='Base Event Type for Distinct Document', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950007) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 02:57:25 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Tipo de Evento Base para Distintos Documentos',Updated=TO_DATE('2013-01-28 14:57:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950007 AND AD_Language='es_MX'
;

-- 28/01/2013 02:59:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Interval Days until next visit ',Updated=TO_DATE('2013-01-28 14:59:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950009
;

-- 28/01/2013 02:59:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950009
;

-- 28/01/2013 02:59:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='IntervalDays', Name='Interval Days', Description='Interval Days until next visit ', Help=NULL WHERE AD_Element_ID=2950009
;

-- 28/01/2013 02:59:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='IntervalDays', Name='Interval Days', Description='Interval Days until next visit ', Help=NULL, AD_Element_ID=2950009 WHERE UPPER(ColumnName)='INTERVALDAYS' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 02:59:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='IntervalDays', Name='Interval Days', Description='Interval Days until next visit ', Help=NULL WHERE AD_Element_ID=2950009 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 02:59:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Interval Days', Description='Interval Days until next visit ', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950009) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 02:59:28 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Días de intervalo hasta la siguiente visita.',Updated=TO_DATE('2013-01-28 14:59:28','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950009 AND AD_Language='es_MX'
;

-- 28/01/2013 03:00:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Percent Effectivity to each event type',Updated=TO_DATE('2013-01-28 15:00:54','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950006
;

-- 28/01/2013 03:00:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950006
;

-- 28/01/2013 03:00:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='PercentEffectivity', Name='Percent ffectivity', Description='Percent Effectivity to each event type', Help=NULL WHERE AD_Element_ID=2950006
;

-- 28/01/2013 03:00:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='PercentEffectivity', Name='Percent ffectivity', Description='Percent Effectivity to each event type', Help=NULL, AD_Element_ID=2950006 WHERE UPPER(ColumnName)='PERCENTEFFECTIVITY' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:00:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='PercentEffectivity', Name='Percent ffectivity', Description='Percent Effectivity to each event type', Help=NULL WHERE AD_Element_ID=2950006 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:00:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Percent ffectivity', Description='Percent Effectivity to each event type', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950006) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:01:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Porcentaje de Efectividad ',Description='Porcentaje de Efectividad de cada Tipo de Evento',Updated=TO_DATE('2013-01-28 15:01:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950006 AND AD_Language='es_MX'
;

-- 28/01/2013 03:02:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Quantity Read from customer',Updated=TO_DATE('2013-01-28 15:02:30','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950002
;

-- 28/01/2013 03:02:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950002
;

-- 28/01/2013 03:02:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='QtyRead', Name='Quantity Read', Description='Quantity Read from customer', Help=NULL WHERE AD_Element_ID=2950002
;

-- 28/01/2013 03:02:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='QtyRead', Name='Quantity Read', Description='Quantity Read from customer', Help=NULL, AD_Element_ID=2950002 WHERE UPPER(ColumnName)='QTYREAD' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:02:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='QtyRead', Name='Quantity Read', Description='Quantity Read from customer', Help=NULL WHERE AD_Element_ID=2950002 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:02:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Quantity Read', Description='Quantity Read from customer', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950002) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:03:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Cantidad leída del Cliente',Updated=TO_DATE('2013-01-28 15:03:00','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950002 AND AD_Language='es_MX'
;

-- 28/01/2013 03:03:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Customer Inventory ID',Updated=TO_DATE('2013-01-28 15:03:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950001
;

-- 28/01/2013 03:03:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950001
;

-- 28/01/2013 03:03:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_CustomerInventory_ID', Name='Customer Inventory', Description='Customer Inventory ID', Help=NULL WHERE AD_Element_ID=2950001
;

-- 28/01/2013 03:03:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_CustomerInventory_ID', Name='Customer Inventory', Description='Customer Inventory ID', Help=NULL, AD_Element_ID=2950001 WHERE UPPER(ColumnName)='SF_CUSTOMERINVENTORY_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:03:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_CustomerInventory_ID', Name='Customer Inventory', Description='Customer Inventory ID', Help=NULL WHERE AD_Element_ID=2950001 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:03:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Customer Inventory', Description='Customer Inventory ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950001) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:04:07 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador del Inventario del Cliente',Updated=TO_DATE('2013-01-28 15:04:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950001 AND AD_Language='es_MX'
;

-- 28/01/2013 03:04:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Customer Inventory Line ID',Updated=TO_DATE('2013-01-28 15:04:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950004
;

-- 28/01/2013 03:04:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950004
;

-- 28/01/2013 03:04:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_CustomerInventoryLine_ID', Name='Customer Inventory Line', Description='Customer Inventory Line ID', Help=NULL WHERE AD_Element_ID=2950004
;

-- 28/01/2013 03:04:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_CustomerInventoryLine_ID', Name='Customer Inventory Line', Description='Customer Inventory Line ID', Help=NULL, AD_Element_ID=2950004 WHERE UPPER(ColumnName)='SF_CUSTOMERINVENTORYLINE_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:04:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_CustomerInventoryLine_ID', Name='Customer Inventory Line', Description='Customer Inventory Line ID', Help=NULL WHERE AD_Element_ID=2950004 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:04:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Customer Inventory Line', Description='Customer Inventory Line ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950004) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:04:53 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de la Linea del inventario de los clientes',Updated=TO_DATE('2013-01-28 15:04:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950004 AND AD_Language='es_MX'
;

-- 28/01/2013 03:05:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Doc Sequence ID',Updated=TO_DATE('2013-01-28 15:05:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950015
;

-- 28/01/2013 03:05:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950015
;

-- 28/01/2013 03:05:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_DocSequence_ID', Name='Doc Sequence', Description='Doc Sequence ID', Help=NULL WHERE AD_Element_ID=2950015
;

-- 28/01/2013 03:05:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_DocSequence_ID', Name='Doc Sequence', Description='Doc Sequence ID', Help=NULL, AD_Element_ID=2950015 WHERE UPPER(ColumnName)='SF_DOCSEQUENCE_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:05:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_DocSequence_ID', Name='Doc Sequence', Description='Doc Sequence ID', Help=NULL WHERE AD_Element_ID=2950015 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:05:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Doc Sequence', Description='Doc Sequence ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950015) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:05:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de la Secuencia del Documento',Updated=TO_DATE('2013-01-28 15:05:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950015 AND AD_Language='es_MX'
;

-- 28/01/2013 03:05:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Event Type ID',Updated=TO_DATE('2013-01-28 15:05:35','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950005
;

-- 28/01/2013 03:05:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950005
;

-- 28/01/2013 03:05:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_EventType_ID', Name='Event Type', Description='Event Type ID', Help=NULL WHERE AD_Element_ID=2950005
;

-- 28/01/2013 03:05:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_EventType_ID', Name='Event Type', Description='Event Type ID', Help=NULL, AD_Element_ID=2950005 WHERE UPPER(ColumnName)='SF_EVENTTYPE_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:05:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_EventType_ID', Name='Event Type', Description='Event Type ID', Help=NULL WHERE AD_Element_ID=2950005 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:05:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Event Type', Description='Event Type ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950005) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:05:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador del Tipo de Evento',Updated=TO_DATE('2013-01-28 15:05:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950005 AND AD_Language='es_MX'
;

-- 28/01/2013 03:05:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Frequency ID',Updated=TO_DATE('2013-01-28 15:05:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950008
;

-- 28/01/2013 03:05:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950008
;

-- 28/01/2013 03:05:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_Frequency_ID', Name='Frequency', Description='Frequency ID', Help=NULL WHERE AD_Element_ID=2950008
;

-- 28/01/2013 03:05:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_Frequency_ID', Name='Frequency', Description='Frequency ID', Help=NULL, AD_Element_ID=2950008 WHERE UPPER(ColumnName)='SF_FREQUENCY_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:05:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_Frequency_ID', Name='Frequency', Description='Frequency ID', Help=NULL WHERE AD_Element_ID=2950008 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:05:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Frequency', Description='Frequency ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950008) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:06:12 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de la Frecuencia',Updated=TO_DATE('2013-01-28 15:06:12','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950008 AND AD_Language='es_MX'
;

-- 28/01/2013 03:06:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Initial Load ID',Updated=TO_DATE('2013-01-28 15:06:22','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950011
;

-- 28/01/2013 03:06:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950011
;

-- 28/01/2013 03:06:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_InitialLoad_ID', Name='Initial Load', Description='Initial Load ID', Help=NULL WHERE AD_Element_ID=2950011
;

-- 28/01/2013 03:06:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_InitialLoad_ID', Name='Initial Load', Description='Initial Load ID', Help=NULL, AD_Element_ID=2950011 WHERE UPPER(ColumnName)='SF_INITIALLOAD_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:06:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_InitialLoad_ID', Name='Initial Load', Description='Initial Load ID', Help=NULL WHERE AD_Element_ID=2950011 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:06:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Initial Load', Description='Initial Load ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950011) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:06:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de la Carga Inicial',Updated=TO_DATE('2013-01-28 15:06:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950011 AND AD_Language='es_MX'
;

-- 28/01/2013 03:06:45 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Inventory Type ID',Updated=TO_DATE('2013-01-28 15:06:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950003
;

-- 28/01/2013 03:06:45 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950003
;

-- 28/01/2013 03:06:45 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_InventoryType_ID', Name='Inventory Type', Description='Inventory Type ID', Help=NULL WHERE AD_Element_ID=2950003
;

-- 28/01/2013 03:06:45 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_InventoryType_ID', Name='Inventory Type', Description='Inventory Type ID', Help=NULL, AD_Element_ID=2950003 WHERE UPPER(ColumnName)='SF_INVENTORYTYPE_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:06:45 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_InventoryType_ID', Name='Inventory Type', Description='Inventory Type ID', Help=NULL WHERE AD_Element_ID=2950003 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:06:45 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Inventory Type', Description='Inventory Type ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950003) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:06:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador del Tipo de Inventario',Updated=TO_DATE('2013-01-28 15:06:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950003 AND AD_Language='es_MX'
;

-- 28/01/2013 03:07:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Planning Visit ID',Updated=TO_DATE('2013-01-28 15:07:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950013
;

-- 28/01/2013 03:07:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950013
;

-- 28/01/2013 03:07:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_PlanningVisit_ID', Name='Planning Visit', Description='Planning Visit ID', Help=NULL WHERE AD_Element_ID=2950013
;

-- 28/01/2013 03:07:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_PlanningVisit_ID', Name='Planning Visit', Description='Planning Visit ID', Help=NULL, AD_Element_ID=2950013 WHERE UPPER(ColumnName)='SF_PLANNINGVISIT_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:07:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_PlanningVisit_ID', Name='Planning Visit', Description='Planning Visit ID', Help=NULL WHERE AD_Element_ID=2950013 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:07:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Planning Visit', Description='Planning Visit ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950013) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:07:25 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de la Planificación de visita',Updated=TO_DATE('2013-01-28 15:07:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950013 AND AD_Language='es_MX'
;

-- 28/01/2013 03:08:09 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Visit Day Date',Updated=TO_DATE('2013-01-28 15:08:09','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950012
;

-- 28/01/2013 03:08:09 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950012
;

-- 28/01/2013 03:08:09 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='VisitDay', Name='Visit Day ', Description='Visit Day Date', Help=NULL WHERE AD_Element_ID=2950012
;

-- 28/01/2013 03:08:09 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='VisitDay', Name='Visit Day ', Description='Visit Day Date', Help=NULL, AD_Element_ID=2950012 WHERE UPPER(ColumnName)='VISITDAY' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:08:09 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='VisitDay', Name='Visit Day ', Description='Visit Day Date', Help=NULL WHERE AD_Element_ID=2950012 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:08:09 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Visit Day ', Description='Visit Day Date', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950012) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:08:25 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Fecha del Día de Visita',Updated=TO_DATE('2013-01-28 15:08:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950012 AND AD_Language='es_MX'
;

-- 28/01/2013 03:09:52 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950010,'N',TO_DATE('2013-01-28 15:09:52','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','Y','Y','N','Y','N','N','N',0,'Visit','L','SF_Visit',TO_DATE('2013-01-28 15:09:52','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:09:52 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950010 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 03:09:52 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000009,TO_DATE('2013-01-28 15:09:52','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_Visit',1,'Y','N','Y','Y','SF_Visit','N',1000000,TO_DATE('2013-01-28 15:09:52','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950165,102,0,19,2950010,129,'AD_Client_ID',TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950165 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950166,113,0,19,2950010,104,'AD_Org_ID',TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950166 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950167,245,0,16,2950010,'Created',TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950167 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950168,246,0,18,110,2950010,'CreatedBy',TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950168 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950169,348,0,20,2950010,'IsActive',TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950169 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950170,607,0,16,2950010,'Updated',TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950170 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950171,608,0,18,110,2950010,'UpdatedBy',TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 15:10:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:10:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950171 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:11:51 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950172,275,0,14,2950010,'Description',TO_DATE('2013-01-28 15:11:51','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 15:11:51','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:11:51 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950172 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:12:26 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950173,1581,0,16,2950010,'DateFrom',TO_DATE('2013-01-28 15:12:26','YYYY-MM-DD HH24:MI:SS'),100,'Starting date for a range','SF',7,'The Date From indicates the starting date of a range.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Date From',0,TO_DATE('2013-01-28 15:12:26','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:12:26 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950173 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:12:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950174,1582,0,16,2950010,'DateTo',TO_DATE('2013-01-28 15:12:50','YYYY-MM-DD HH24:MI:SS'),100,'End date of a date range','SF',7,'The Date To indicates the end date of a range (inclusive)','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Date To',0,TO_DATE('2013-01-28 15:12:50','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:12:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950174 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:14:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950016,0,'OffCourse',TO_DATE('2013-01-28 15:14:11','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Off Course','Off Course',TO_DATE('2013-01-28 15:14:11','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:14:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950016 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 03:14:15 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Desvío de curso',PrintName='Desvío de curso',Updated=TO_DATE('2013-01-28 15:14:15','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950016 AND AD_Language='es_MX'
;

-- 28/01/2013 03:16:21 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Off Course of Fisit',Updated=TO_DATE('2013-01-28 15:16:21','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950016
;

-- 28/01/2013 03:16:21 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950016
;

-- 28/01/2013 03:16:21 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='OffCourse', Name='Off Course', Description='Off Course of Fisit', Help=NULL WHERE AD_Element_ID=2950016
;

-- 28/01/2013 03:16:21 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='OffCourse', Name='Off Course', Description='Off Course of Fisit', Help=NULL, AD_Element_ID=2950016 WHERE UPPER(ColumnName)='OFFCOURSE' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:16:21 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='OffCourse', Name='Off Course', Description='Off Course of Fisit', Help=NULL WHERE AD_Element_ID=2950016 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:16:21 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Off Course', Description='Off Course of Fisit', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950016) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:16:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Desvío de curso de la Visita',Updated=TO_DATE('2013-01-28 15:16:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950016 AND AD_Language='es_MX'
;

-- 28/01/2013 03:17:00 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950175,2950016,0,20,2950010,'OffCourse',TO_DATE('2013-01-28 15:17:00','YYYY-MM-DD HH24:MI:SS'),100,'Off Course of Fisit','SF',1,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Off Course',0,TO_DATE('2013-01-28 15:17:00','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:17:00 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950175 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:17:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Off Course of Visit',Updated=TO_DATE('2013-01-28 15:17:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950016
;

-- 28/01/2013 03:17:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950016
;

-- 28/01/2013 03:17:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='OffCourse', Name='Off Course', Description='Off Course of Visit', Help=NULL WHERE AD_Element_ID=2950016
;

-- 28/01/2013 03:17:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='OffCourse', Name='Off Course', Description='Off Course of Visit', Help=NULL, AD_Element_ID=2950016 WHERE UPPER(ColumnName)='OFFCOURSE' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:17:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='OffCourse', Name='Off Course', Description='Off Course of Visit', Help=NULL WHERE AD_Element_ID=2950016 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:17:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Off Course', Description='Off Course of Visit', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950016) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:18:57 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Fuera de Ruta',PrintName='Fuera de Ruta',Description='Fuera de Ruta de la Visita',Updated=TO_DATE('2013-01-28 15:18:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950016 AND AD_Language='es_MX'
;

-- 28/01/2013 03:19:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950176,1063,0,18,190,2950010,'SalesRep_ID',TO_DATE('2013-01-28 15:19:55','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent','SF',10,'The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Sales Representative',0,TO_DATE('2013-01-28 15:19:55','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:19:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950176 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:20:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET Name='Visit Of Client',Updated=TO_DATE('2013-01-28 15:20:04','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950010
;

-- 28/01/2013 03:20:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET IsTranslated='N' WHERE AD_Table_ID=2950010
;

-- 28/01/2013 03:20:22 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Visita a Cliente',Updated=TO_DATE('2013-01-28 15:20:22','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950010 AND AD_Language='es_MX'
;

-- 28/01/2013 03:21:20 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950017,0,'SF_Visit_ID',TO_DATE('2013-01-28 15:21:20','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Visit to Client','Visit to Client',TO_DATE('2013-01-28 15:21:20','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:21:20 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950017 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 03:22:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Name='Visit to Customer', PrintName='Visit to Customer',Updated=TO_DATE('2013-01-28 15:22:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950017
;

-- 28/01/2013 03:22:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950017
;

-- 28/01/2013 03:22:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_Visit_ID', Name='Visit to Customer', Description=NULL, Help=NULL WHERE AD_Element_ID=2950017
;

-- 28/01/2013 03:22:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_Visit_ID', Name='Visit to Customer', Description=NULL, Help=NULL, AD_Element_ID=2950017 WHERE UPPER(ColumnName)='SF_VISIT_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:22:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_Visit_ID', Name='Visit to Customer', Description=NULL, Help=NULL WHERE AD_Element_ID=2950017 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:22:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Visit to Customer', Description=NULL, Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950017) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:22:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_PrintFormatItem pi SET PrintName='Visit to Customer', Name='Visit to Customer' WHERE IsCentrallyMaintained='Y' AND EXISTS (SELECT * FROM AD_Column c WHERE c.AD_Column_ID=pi.AD_Column_ID AND c.AD_Element_ID=2950017)
;

-- 28/01/2013 03:22:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Visit to Customer ID',Updated=TO_DATE('2013-01-28 15:22:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950017
;

-- 28/01/2013 03:22:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950017
;

-- 28/01/2013 03:22:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_Visit_ID', Name='Visit to Customer', Description='Visit to Customer ID', Help=NULL WHERE AD_Element_ID=2950017
;

-- 28/01/2013 03:22:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_Visit_ID', Name='Visit to Customer', Description='Visit to Customer ID', Help=NULL, AD_Element_ID=2950017 WHERE UPPER(ColumnName)='SF_VISIT_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:22:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_Visit_ID', Name='Visit to Customer', Description='Visit to Customer ID', Help=NULL WHERE AD_Element_ID=2950017 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:22:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Visit to Customer', Description='Visit to Customer ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950017) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:22:59 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Visita a Cliente',PrintName='Visita a Cliente ',Description='Identificador de la Visita a Cliente ',Updated=TO_DATE('2013-01-28 15:22:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950017 AND AD_Language='es_MX'
;

-- 28/01/2013 03:23:22 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950177,2950017,0,13,2950010,'SF_Visit_ID',TO_DATE('2013-01-28 15:23:22','YYYY-MM-DD HH24:MI:SS'),100,'Visit to Customer ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Visit to Customer',0,TO_DATE('2013-01-28 15:23:22','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:23:22 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950177 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:23:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950178,2950013,0,19,2950010,'SF_PlanningVisit_ID',TO_DATE('2013-01-28 15:23:56','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Planning Visit',0,TO_DATE('2013-01-28 15:23:56','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:23:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950178 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:26:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,Description,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950018,0,'StatusVisit',TO_DATE('2013-01-28 15:26:06','YYYY-MM-DD HH24:MI:SS'),100,'Status Visit','SF','Y','Status Visit','Status Visit',TO_DATE('2013-01-28 15:26:06','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:26:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950018 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 03:26:19 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Estado de la Visita',PrintName='Estado de la Visita',Description='Estado de la Visita',Updated=TO_DATE('2013-01-28 15:26:19','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950018 AND AD_Language='es_MX'
;

-- 28/01/2013 03:32:00 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference (AD_Client_ID,AD_Org_ID,AD_Reference_ID,Created,CreatedBy,EntityType,IsActive,IsOrderByValue,Name,Updated,UpdatedBy,ValidationType,VFormat) VALUES (0,0,2950002,TO_DATE('2013-01-28 15:32:00','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Y','StatusVisit',TO_DATE('2013-01-28 15:32:00','YYYY-MM-DD HH24:MI:SS'),100,'L','L')
;

-- 28/01/2013 03:32:00 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference_Trl (AD_Language,AD_Reference_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Reference_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Reference t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Reference_ID=2950002 AND NOT EXISTS (SELECT * FROM AD_Reference_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Reference_ID=t.AD_Reference_ID)
;

-- 28/01/2013 03:32:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET Name='Estado de la Visita',Description='Estado de la Visita',Updated=TO_DATE('2013-01-28 15:32:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950002 AND AD_Language='es_MX'
;

-- 28/01/2013 03:32:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference SET Description='Estado de la Visita',Updated=TO_DATE('2013-01-28 15:32:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950002
;

-- 28/01/2013 03:32:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET IsTranslated='N' WHERE AD_Reference_ID=2950002
;

-- 28/01/2013 03:32:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference SET Description='Status of Visit',Updated=TO_DATE('2013-01-28 15:32:30','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950002
;

-- 28/01/2013 03:32:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET IsTranslated='N' WHERE AD_Reference_ID=2950002
;

-- 28/01/2013 03:33:00 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,Description,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950002,2950008,TO_DATE('2013-01-28 15:33:00','YYYY-MM-DD HH24:MI:SS'),100,'Visit Initiated','SF','Y','Initiated',TO_DATE('2013-01-28 15:33:00','YYYY-MM-DD HH24:MI:SS'),100,'I')
;

-- 28/01/2013 03:33:00 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950008 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 03:33:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Iniciada',Description='Vista Iniciada',Updated=TO_DATE('2013-01-28 15:33:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950008 AND AD_Language='es_MX'
;

-- 28/01/2013 03:33:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,Description,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950002,2950009,TO_DATE('2013-01-28 15:33:42','YYYY-MM-DD HH24:MI:SS'),100,'Visit Annulled','SF','Y','Annulled',TO_DATE('2013-01-28 15:33:42','YYYY-MM-DD HH24:MI:SS'),100,'A')
;

-- 28/01/2013 03:33:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950009 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 03:33:55 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Anulada',Description='Visita Anulada',Updated=TO_DATE('2013-01-28 15:33:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950009 AND AD_Language='es_MX'
;

-- 28/01/2013 03:34:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,Description,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950002,2950010,TO_DATE('2013-01-28 15:34:15','YYYY-MM-DD HH24:MI:SS'),100,'Closed Visit','SF','Y','Closed',TO_DATE('2013-01-28 15:34:15','YYYY-MM-DD HH24:MI:SS'),100,'CL')
;

-- 28/01/2013 03:34:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950010 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 03:34:24 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Cerrada',Description='Visita Cerrada',Updated=TO_DATE('2013-01-28 15:34:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950010 AND AD_Language='es_MX'
;

-- 28/01/2013 03:34:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List SET Value='AN',Updated=TO_DATE('2013-01-28 15:34:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950009
;

-- 28/01/2013 03:34:38 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List SET Value='IN',Updated=TO_DATE('2013-01-28 15:34:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950008
;

-- 28/01/2013 03:34:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference SET VFormat='LL',Updated=TO_DATE('2013-01-28 15:34:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950002
;

-- 28/01/2013 03:35:10 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950179,2950018,0,17,2950002,2950010,'StatusVisit',TO_DATE('2013-01-28 15:35:10','YYYY-MM-DD HH24:MI:SS'),100,'Status Visit','SF',2,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Status Visit',0,TO_DATE('2013-01-28 15:35:10','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:35:10 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950179 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:36:25 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950011,'N',TO_DATE('2013-01-28 15:36:24','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','Y','Y','N','Y','N','N','N',0,'Line of Visit','L','SF_VisitLine',TO_DATE('2013-01-28 15:36:24','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:36:25 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950011 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 03:36:25 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000010,TO_DATE('2013-01-28 15:36:25','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_VisitLine',1,'Y','N','Y','Y','SF_VisitLine','N',1000000,TO_DATE('2013-01-28 15:36:25','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:36:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Linea de la Visita',Updated=TO_DATE('2013-01-28 15:36:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950011 AND AD_Language='es_MX'
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950180,102,0,19,2950011,129,'AD_Client_ID',TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950180 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950181,113,0,19,2950011,104,'AD_Org_ID',TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950181 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950182,245,0,16,2950011,'Created',TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950182 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950183,246,0,18,110,2950011,'CreatedBy',TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950183 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950184,348,0,20,2950011,'IsActive',TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950184 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950185,607,0,16,2950011,'Updated',TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950185 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950186,608,0,18,110,2950011,'UpdatedBy',TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 15:36:44','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:36:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950186 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:37:43 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950019,0,'SF_VisitLine',TO_DATE('2013-01-28 15:37:43','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Visit of Line','Visit of Line',TO_DATE('2013-01-28 15:37:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:37:43 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950019 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 03:37:53 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Linea de Visita',PrintName='Linea de Visita',Updated=TO_DATE('2013-01-28 15:37:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950019 AND AD_Language='es_MX'
;

-- 28/01/2013 03:38:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Visit of Line',Updated=TO_DATE('2013-01-28 15:38:00','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line', Help=NULL WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line', Help=NULL, AD_Element_ID=2950019 WHERE UPPER(ColumnName)='SF_VISITLINE' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:38:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line', Help=NULL WHERE AD_Element_ID=2950019 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:38:00 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Visit of Line', Description='Visit of Line', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950019) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:38:17 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Visit of Line Detail',Updated=TO_DATE('2013-01-28 15:38:17','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:17 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:17 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line Detail', Help=NULL WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:17 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line Detail', Help=NULL, AD_Element_ID=2950019 WHERE UPPER(ColumnName)='SF_VISITLINE' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:38:17 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line Detail', Help=NULL WHERE AD_Element_ID=2950019 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:38:17 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Visit of Line', Description='Visit of Line Detail', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950019) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:38:37 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Linea de Visita',Updated=TO_DATE('2013-01-28 15:38:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950019 AND AD_Language='es_MX'
;

-- 28/01/2013 03:38:40 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Visit of Line',Updated=TO_DATE('2013-01-28 15:38:40','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:40 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:40 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line', Help=NULL WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:41 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line', Help=NULL, AD_Element_ID=2950019 WHERE UPPER(ColumnName)='SF_VISITLINE' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:38:41 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_VisitLine', Name='Visit of Line', Description='Visit of Line', Help=NULL WHERE AD_Element_ID=2950019 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:38:41 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Visit of Line', Description='Visit of Line', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950019) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:38:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET ColumnName='SF_VisitLine_ID', Description='Visit of Line ID', Name='Visit of Line ',Updated=TO_DATE('2013-01-28 15:38:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_VisitLine_ID', Name='Visit of Line ', Description='Visit of Line ID', Help=NULL WHERE AD_Element_ID=2950019
;

-- 28/01/2013 03:38:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_VisitLine_ID', Name='Visit of Line ', Description='Visit of Line ID', Help=NULL, AD_Element_ID=2950019 WHERE UPPER(ColumnName)='SF_VISITLINE_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 03:38:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_VisitLine_ID', Name='Visit of Line ', Description='Visit of Line ID', Help=NULL WHERE AD_Element_ID=2950019 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:38:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Visit of Line ', Description='Visit of Line ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950019) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 03:38:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_PrintFormatItem pi SET PrintName='Visit of Line', Name='Visit of Line ' WHERE IsCentrallyMaintained='Y' AND EXISTS (SELECT * FROM AD_Column c WHERE c.AD_Column_ID=pi.AD_Column_ID AND c.AD_Element_ID=2950019)
;

-- 28/01/2013 03:39:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de la Linea de Visita',Updated=TO_DATE('2013-01-28 15:39:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950019 AND AD_Language='es_MX'
;

-- 28/01/2013 03:39:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950187,2950019,0,13,2950011,'SF_VisitLine_ID',TO_DATE('2013-01-28 15:39:18','YYYY-MM-DD HH24:MI:SS'),100,'Visit of Line ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Visit of Line ',0,TO_DATE('2013-01-28 15:39:18','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:39:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950187 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:39:51 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950188,275,0,14,2950011,'Description',TO_DATE('2013-01-28 15:39:51','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 15:39:51','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:39:51 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950188 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:40:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950189,2950017,0,19,2950011,'SF_Visit_ID',TO_DATE('2013-01-28 15:40:14','YYYY-MM-DD HH24:MI:SS'),100,'Visit to Customer ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Visit to Customer',0,TO_DATE('2013-01-28 15:40:14','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:40:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950189 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:40:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950190,2950005,0,19,2950011,'SF_EventType_ID',TO_DATE('2013-01-28 15:40:30','YYYY-MM-DD HH24:MI:SS'),100,'Event Type ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Event Type',0,TO_DATE('2013-01-28 15:40:30','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:40:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950190 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:40:49 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950191,126,0,19,2950011,'AD_Table_ID',TO_DATE('2013-01-28 15:40:49','YYYY-MM-DD HH24:MI:SS'),100,'Database Table information','SF',10,'The Database Table provides the information of the table definition','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Table',0,TO_DATE('2013-01-28 15:40:49','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:40:49 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950191 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:42:33 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950192,538,0,28,2950011,'Record_ID',TO_DATE('2013-01-28 15:42:33','YYYY-MM-DD HH24:MI:SS'),100,'Direct internal record ID','SF',1,'The Record ID is the internal unique identifier of a record. Please note that zooming to the record may not be successful for Orders, Invoices and Shipment/Receipts as sometimes the Sales Order type is not known.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Record ID',0,TO_DATE('2013-01-28 15:42:33','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:42:33 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950192 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:42:39 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET FieldLength=10,Updated=TO_DATE('2013-01-28 15:42:39','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950192
;

-- 28/01/2013 03:44:36 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950012,'N',TO_DATE('2013-01-28 15:44:36','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','Y','Y','N','Y','N','N','N',0,'Product Access','L','SF_ProductAccess',TO_DATE('2013-01-28 15:44:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:44:36 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950012 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 03:44:37 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000011,TO_DATE('2013-01-28 15:44:36','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_ProductAccess',1,'Y','N','Y','Y','SF_ProductAccess','N',1000000,TO_DATE('2013-01-28 15:44:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:44:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Productos de Acceso',Updated=TO_DATE('2013-01-28 15:44:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950012 AND AD_Language='es_MX'
;

-- 28/01/2013 03:46:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List SET Name='Begining', Value='BG',Updated=TO_DATE('2013-01-28 15:46:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950008
;

-- 28/01/2013 03:46:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET IsTranslated='N' WHERE AD_Ref_List_ID=2950008
;

-- 28/01/2013 03:46:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List SET Name='Voided', Value='VO',Updated=TO_DATE('2013-01-28 15:46:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950009
;

-- 28/01/2013 03:46:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET IsTranslated='N' WHERE AD_Ref_List_ID=2950009
;

-- 28/01/2013 03:56:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950193,102,0,19,2950012,129,'AD_Client_ID',TO_DATE('2013-01-28 15:56:55','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 15:56:55','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:56:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950193 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950194,113,0,19,2950012,104,'AD_Org_ID',TO_DATE('2013-01-28 15:56:55','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 15:56:55','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950194 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950195,245,0,16,2950012,'Created',TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950195 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950196,246,0,18,110,2950012,'CreatedBy',TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950196 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950197,348,0,20,2950012,'IsActive',TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950197 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950198,607,0,16,2950012,'Updated',TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950198 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950199,608,0,18,110,2950012,'UpdatedBy',TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 15:56:56','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 03:56:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950199 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:57:35 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,Description,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950020,0,'SF_ProductAccess_ID',TO_DATE('2013-01-28 15:57:35','YYYY-MM-DD HH24:MI:SS'),100,'Product Access ID','SF','Y','Product Access','Product Accesss',TO_DATE('2013-01-28 15:57:35','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 03:57:35 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950020 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 03:58:04 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Productos de Acceso',PrintName='Productos de Acceso',Description='Identificador de Productos de Acceso',Updated=TO_DATE('2013-01-28 15:58:04','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950020 AND AD_Language='es_MX'
;

-- 28/01/2013 03:58:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950200,2950020,0,13,2950012,'SF_ProductAccess_ID',TO_DATE('2013-01-28 15:58:18','YYYY-MM-DD HH24:MI:SS'),100,'Product Access ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Product Access',0,TO_DATE('2013-01-28 15:58:18','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:58:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950200 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:58:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950201,453,0,19,2950012,'M_Product_Category_ID',TO_DATE('2013-01-28 15:58:41','YYYY-MM-DD HH24:MI:SS'),100,'Category of a Product','SF',10,'Identifies the category which this product belongs to.  Product categories are used for pricing and selection.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Product Category',0,TO_DATE('2013-01-28 15:58:41','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:58:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950201 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:58:57 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950202,454,0,19,2950012,'M_Product_ID',TO_DATE('2013-01-28 15:58:57','YYYY-MM-DD HH24:MI:SS'),100,'Product, Service, Item','SF',10,'Identifies an item which is either purchased or sold in this organization.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Product',0,TO_DATE('2013-01-28 15:58:57','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:58:58 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950202 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:59:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950203,1383,0,19,2950012,'C_BP_Group_ID',TO_DATE('2013-01-28 15:59:17','YYYY-MM-DD HH24:MI:SS'),100,'Business Partner Group','SF',10,'The Business Partner Group provides a method of defining defaults to be used for individual Business Partners.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Business Partner Group',0,TO_DATE('2013-01-28 15:59:17','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:59:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950203 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:59:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950204,187,0,19,2950012,'C_BPartner_ID',TO_DATE('2013-01-28 15:59:41','YYYY-MM-DD HH24:MI:SS'),100,'Identifies a Business Partner','SF',10,'A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Business Partner ',0,TO_DATE('2013-01-28 15:59:41','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:59:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950204 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 03:59:57 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950205,189,0,19,2950012,'C_BPartner_Location_ID',TO_DATE('2013-01-28 15:59:57','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner','SF',10,'The Partner address indicates the location of a Business Partner','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Partner Location',0,TO_DATE('2013-01-28 15:59:57','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 03:59:57 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950205 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:04:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950206,2079,0,20,2950012,'IsExclude',TO_DATE('2013-01-28 16:04:07','YYYY-MM-DD HH24:MI:SS'),100,'Exclude access to the data - if not selected Include access to the data','SF',1,'If selected (excluded), the role cannot access the data specified.  If not selected (included), the role can ONLY access the data specified. Exclude items represent a negative list (i.e. you don''t have access to the listed items). Include items represent a positive list (i.e. you only have access to the listed items).
<br>You would usually  not mix Exclude and Include. If you have one include rule in your list, you would only have access to that item anyway.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Exclude',0,TO_DATE('2013-01-28 16:04:07','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:04:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950206 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:04:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950207,1063,0,18,353,2950012,'SalesRep_ID',TO_DATE('2013-01-28 16:04:41','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent','SF',10,'The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Sales Representative',0,TO_DATE('2013-01-28 16:04:41','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:04:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950207 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:05:40 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950013,'N',TO_DATE('2013-01-28 16:05:40','YYYY-MM-DD HH24:MI:SS'),100,'Sys Config','SF','N','Y','Y','N','Y','N','N','N',0,'Sys Config','L','SF_SysConfig',TO_DATE('2013-01-28 16:05:40','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:05:40 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950013 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 04:05:40 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000012,TO_DATE('2013-01-28 16:05:40','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_SysConfig',1,'Y','N','Y','Y','SF_SysConfig','N',1000000,TO_DATE('2013-01-28 16:05:40','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:05:52 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Configurar Sistema',Updated=TO_DATE('2013-01-28 16:05:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950013 AND AD_Language='es_MX'
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950208,102,0,19,2950013,129,'AD_Client_ID',TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950208 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950209,113,0,19,2950013,104,'AD_Org_ID',TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950209 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950210,245,0,16,2950013,'Created',TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950210 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950211,246,0,18,110,2950013,'CreatedBy',TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950211 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950212,348,0,20,2950013,'IsActive',TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950212 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950213,607,0,16,2950013,'Updated',TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950213 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950214,608,0,18,110,2950013,'UpdatedBy',TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 16:05:59','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:05:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950214 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:06:24 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950021,0,'SF_SysConfig_ID',TO_DATE('2013-01-28 16:06:24','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Sys Config','Sys Config',TO_DATE('2013-01-28 16:06:24','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:06:24 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950021 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:06:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Configurar Sistema',PrintName='Configurar Sistema',Updated=TO_DATE('2013-01-28 16:06:35','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950021 AND AD_Language='es_MX'
;

-- 28/01/2013 04:07:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Sys Config ID',Updated=TO_DATE('2013-01-28 16:07:32','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950021
;

-- 28/01/2013 04:07:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950021
;

-- 28/01/2013 04:07:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_SysConfig_ID', Name='Sys Config', Description='Sys Config ID', Help=NULL WHERE AD_Element_ID=2950021
;

-- 28/01/2013 04:07:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_SysConfig_ID', Name='Sys Config', Description='Sys Config ID', Help=NULL, AD_Element_ID=2950021 WHERE UPPER(ColumnName)='SF_SYSCONFIG_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 04:07:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_SysConfig_ID', Name='Sys Config', Description='Sys Config ID', Help=NULL WHERE AD_Element_ID=2950021 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 04:07:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Sys Config', Description='Sys Config ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950021) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 04:07:46 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de Configurar Sistema',Updated=TO_DATE('2013-01-28 16:07:46','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950021 AND AD_Language='es_MX'
;

-- 28/01/2013 04:08:00 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950215,2950021,0,13,2950013,'SF_SysConfig_ID',TO_DATE('2013-01-28 16:08:00','YYYY-MM-DD HH24:MI:SS'),100,'Sys Config ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Sys Config',0,TO_DATE('2013-01-28 16:08:00','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:08:00 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950215 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:08:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950216,1063,0,18,353,2950013,'SalesRep_ID',TO_DATE('2013-01-28 16:08:34','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent','SF',10,'The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Sales Representative',0,TO_DATE('2013-01-28 16:08:34','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:08:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950216 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:10:19 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950217,620,0,10,2950013,'Value',TO_DATE('2013-01-28 16:10:19','YYYY-MM-DD HH24:MI:SS'),100,'Search key for the record in the format required - must be unique','SF',60,'A search key allows you a fast method of finding a particular record.
If you leave the search key empty, the system automatically creates a numeric number.  The document sequence used for this fallback number is defined in the "Maintain Sequence" window with the name "DocumentNo_<TableName>", where TableName is the actual name of the table (e.g. C_Order).','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Search Key',0,TO_DATE('2013-01-28 16:10:19','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:10:19 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950217 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:10:43 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950218,469,0,10,2950013,'Name',TO_DATE('2013-01-28 16:10:42','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity','SF',60,'The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','N','N','N','Y','N','Y','N','Y','N','N','Y','Name',0,TO_DATE('2013-01-28 16:10:42','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:10:43 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950218 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:10:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950219,275,0,14,2950013,'Description',TO_DATE('2013-01-28 16:10:59','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 16:10:59','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:10:59 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950219 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:11:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950014,'N',TO_DATE('2013-01-28 16:11:30','YYYY-MM-DD HH24:MI:SS'),100,'Synchronizing Trace','SF','N','Y','Y','N','Y','N','N','N',0,'Synchronizing Trace','L','SF_SynchronizingTrace',TO_DATE('2013-01-28 16:11:30','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:11:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950014 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 04:11:30 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000013,TO_DATE('2013-01-28 16:11:30','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_SynchronizingTrace',1,'Y','N','Y','Y','SF_SynchronizingTrace','N',1000000,TO_DATE('2013-01-28 16:11:30','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:11:47 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Traza de Sincronización',Updated=TO_DATE('2013-01-28 16:11:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950014 AND AD_Language='es_MX'
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950220,102,0,19,2950014,129,'AD_Client_ID',TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950220 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950221,113,0,19,2950014,104,'AD_Org_ID',TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950221 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950222,245,0,16,2950014,'Created',TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950222 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950223,246,0,18,110,2950014,'CreatedBy',TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950223 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950224,348,0,20,2950014,'IsActive',TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950224 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950225,607,0,16,2950014,'Updated',TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950225 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950226,608,0,18,110,2950014,'UpdatedBy',TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 16:12:02','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:12:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950226 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:12:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950022,0,'SF_SynchronizingTrace_ID',TO_DATE('2013-01-28 16:12:41','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Synchronizing Trace','Synchronizing Trace',TO_DATE('2013-01-28 16:12:41','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:12:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950022 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:13:37 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Traza de Sincronización',PrintName='Traza de Sincronización',Updated=TO_DATE('2013-01-28 16:13:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950022 AND AD_Language='es_MX'
;

-- 28/01/2013 04:13:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Synchronizing Trace ID',Updated=TO_DATE('2013-01-28 16:13:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950022
;

-- 28/01/2013 04:13:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950022
;

-- 28/01/2013 04:13:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_SynchronizingTrace_ID', Name='Synchronizing Trace', Description='Synchronizing Trace ID', Help=NULL WHERE AD_Element_ID=2950022
;

-- 28/01/2013 04:13:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_SynchronizingTrace_ID', Name='Synchronizing Trace', Description='Synchronizing Trace ID', Help=NULL, AD_Element_ID=2950022 WHERE UPPER(ColumnName)='SF_SYNCHRONIZINGTRACE_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 04:13:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_SynchronizingTrace_ID', Name='Synchronizing Trace', Description='Synchronizing Trace ID', Help=NULL WHERE AD_Element_ID=2950022 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 04:13:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Synchronizing Trace', Description='Synchronizing Trace ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950022) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 04:13:57 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de la Traza de Sincronización',Updated=TO_DATE('2013-01-28 16:13:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950022 AND AD_Language='es_MX'
;

-- 28/01/2013 04:14:12 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950227,2950022,0,13,2950014,'SF_SynchronizingTrace_ID',TO_DATE('2013-01-28 16:14:12','YYYY-MM-DD HH24:MI:SS'),100,'Synchronizing Trace ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Synchronizing Trace',0,TO_DATE('2013-01-28 16:14:12','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:14:12 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950227 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:16:43 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950228,2334,0,17,2950014,'EventType',TO_DATE('2013-01-28 16:16:43','YYYY-MM-DD HH24:MI:SS'),100,'Type of Event','SF',3,'Y','Y','N','N','N','N','N','N','N','N','N','Y','Y','Event Type',0,TO_DATE('2013-01-28 16:16:43','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:16:43 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950228 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:20:23 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference (AD_Client_ID,AD_Org_ID,AD_Reference_ID,Created,CreatedBy,EntityType,IsActive,IsOrderByValue,Name,Updated,UpdatedBy,ValidationType) VALUES (0,0,2950003,TO_DATE('2013-01-28 16:20:22','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N','Event Type',TO_DATE('2013-01-28 16:20:22','YYYY-MM-DD HH24:MI:SS'),100,'L')
;

-- 28/01/2013 04:20:23 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference_Trl (AD_Language,AD_Reference_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Reference_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Reference t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Reference_ID=2950003 AND NOT EXISTS (SELECT * FROM AD_Reference_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Reference_ID=t.AD_Reference_ID)
;

-- 28/01/2013 04:20:37 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET Name='Tipos de Evento',Description='Tipos de Evento',Updated=TO_DATE('2013-01-28 16:20:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950003 AND AD_Language='es_MX'
;

-- 28/01/2013 04:21:03 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950003,2950011,TO_DATE('2013-01-28 16:21:03','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Error',TO_DATE('2013-01-28 16:21:03','YYYY-MM-DD HH24:MI:SS'),100,'E')
;

-- 28/01/2013 04:21:03 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950011 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 04:21:25 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950003,2950012,TO_DATE('2013-01-28 16:21:25','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Processing',TO_DATE('2013-01-28 16:21:25','YYYY-MM-DD HH24:MI:SS'),100,'P')
;

-- 28/01/2013 04:21:25 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950012 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 04:21:41 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Procesando',Updated=TO_DATE('2013-01-28 16:21:41','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950012 AND AD_Language='es_MX'
;

-- 28/01/2013 04:22:13 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950003,2950013,TO_DATE('2013-01-28 16:22:12','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Successfully',TO_DATE('2013-01-28 16:22:12','YYYY-MM-DD HH24:MI:SS'),100,'S')
;

-- 28/01/2013 04:22:13 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950013 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 04:22:23 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Éxito',Updated=TO_DATE('2013-01-28 16:22:23','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950013 AND AD_Language='es_MX'
;

-- 28/01/2013 04:22:51 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_Value_ID=2950003,Updated=TO_DATE('2013-01-28 16:22:51','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950228
;

-- 28/01/2013 04:23:06 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y',Updated=TO_DATE('2013-01-28 16:23:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950228
;

-- 28/01/2013 04:23:45 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,Description,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950023,0,'StartSync',TO_DATE('2013-01-28 16:23:45','YYYY-MM-DD HH24:MI:SS'),100,'Start Sync Process','SF','Y','Start Sync','Start Sync',TO_DATE('2013-01-28 16:23:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:23:45 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950023 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:24:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Iniciar Sincronización',PrintName='Iniciar Sincronización',Description='Iniciar el Proceso de Sincronización',Updated=TO_DATE('2013-01-28 16:24:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950023 AND AD_Language='es_MX'
;

-- 28/01/2013 04:24:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Start Sync Date',Updated=TO_DATE('2013-01-28 16:24:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950023
;

-- 28/01/2013 04:24:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950023
;

-- 28/01/2013 04:24:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='StartSync', Name='Start Sync', Description='Start Sync Date', Help=NULL WHERE AD_Element_ID=2950023
;

-- 28/01/2013 04:24:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='StartSync', Name='Start Sync', Description='Start Sync Date', Help=NULL, AD_Element_ID=2950023 WHERE UPPER(ColumnName)='STARTSYNC' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 04:24:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='StartSync', Name='Start Sync', Description='Start Sync Date', Help=NULL WHERE AD_Element_ID=2950023 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 04:24:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Start Sync', Description='Start Sync Date', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950023) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 04:25:01 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Fecha y hora del Inicio de Sincronización',Updated=TO_DATE('2013-01-28 16:25:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950023 AND AD_Language='es_MX'
;

-- 28/01/2013 04:25:16 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950229,2950023,0,16,2950014,'StartSync',TO_DATE('2013-01-28 16:25:16','YYYY-MM-DD HH24:MI:SS'),100,'Start Sync Date','SF',7,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Start Sync',0,TO_DATE('2013-01-28 16:25:16','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:25:16 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950229 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:25:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,Description,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950024,0,'EndSync',TO_DATE('2013-01-28 16:25:44','YYYY-MM-DD HH24:MI:SS'),100,'End Sync Date','U','Y','End Sync','End Sync',TO_DATE('2013-01-28 16:25:44','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:25:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950024 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:26:07 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Inicio de la Sincronización',PrintName='Inicio de la Sincronización',Updated=TO_DATE('2013-01-28 16:26:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950023 AND AD_Language='es_MX'
;

-- 28/01/2013 04:26:41 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Fin de la Sincronización',PrintName='Fin de la Sincronización',Description='Fecha y hora del Fin de la Sincronización',Updated=TO_DATE('2013-01-28 16:26:41','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950024 AND AD_Language='es_MX'
;

-- 28/01/2013 04:27:05 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950230,2950024,0,16,2950014,'EndSync',TO_DATE('2013-01-28 16:27:05','YYYY-MM-DD HH24:MI:SS'),100,'End Sync Date','SF',7,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','End Sync',0,TO_DATE('2013-01-28 16:27:05','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:27:05 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950230 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:28:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET EntityType='SF',Updated=TO_DATE('2013-01-28 16:28:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950024
;

-- 28/01/2013 04:28:37 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,Description,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950025,0,'SF_MenuList_ID',TO_DATE('2013-01-28 16:28:37','YYYY-MM-DD HH24:MI:SS'),100,'Menu List ID','SF','Y','Menu List','Menu List',TO_DATE('2013-01-28 16:28:37','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:28:37 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950025 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:28:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Lista del Menú',PrintName='Lista del Menú',Description='Identificador de la Lista del Menú',Updated=TO_DATE('2013-01-28 16:28:54','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950025 AND AD_Language='es_MX'
;

-- 28/01/2013 04:29:13 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950231,2950025,0,19,2950014,'SF_MenuList_ID',TO_DATE('2013-01-28 16:29:13','YYYY-MM-DD HH24:MI:SS'),100,'Menu List ID','SF',10,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Menu List',0,TO_DATE('2013-01-28 16:29:13','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:29:13 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950231 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:29:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950232,275,0,14,2950014,'Description',TO_DATE('2013-01-28 16:29:29','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 16:29:29','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:29:29 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950232 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:29:49 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950015,'N',TO_DATE('2013-01-28 16:29:49','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','Y','Y','N','Y','N','N','N',0,'Menu List','L','SF_MenuList',TO_DATE('2013-01-28 16:29:49','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:29:49 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950015 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 04:29:49 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000014,TO_DATE('2013-01-28 16:29:49','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_MenuList',1,'Y','N','Y','Y','SF_MenuList','N',1000000,TO_DATE('2013-01-28 16:29:49','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:29:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Lista del Menú',Updated=TO_DATE('2013-01-28 16:29:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950015 AND AD_Language='es_MX'
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950233,102,0,19,2950015,129,'AD_Client_ID',TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950233 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950234,113,0,19,2950015,104,'AD_Org_ID',TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950234 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950235,245,0,16,2950015,'Created',TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950235 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950236,246,0,18,110,2950015,'CreatedBy',TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950236 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950237,348,0,20,2950015,'IsActive',TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950237 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950238,607,0,16,2950015,'Updated',TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950238 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950239,608,0,18,110,2950015,'UpdatedBy',TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 16:30:11','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 04:30:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950239 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:30:32 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950240,2950025,0,13,2950015,'SF_MenuList_ID',TO_DATE('2013-01-28 16:30:32','YYYY-MM-DD HH24:MI:SS'),100,'Menu List ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Menu List',0,TO_DATE('2013-01-28 16:30:32','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:30:32 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950240 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:30:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950241,1298,0,19,2950015,'AD_Form_ID',TO_DATE('2013-01-28 16:30:47','YYYY-MM-DD HH24:MI:SS'),100,'Special Form','SF',10,'The Special Form field identifies a unique Special Form in the system.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Special Form',0,TO_DATE('2013-01-28 16:30:47','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:30:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950241 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:31:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950242,117,0,19,2950015,'AD_Process_ID',TO_DATE('2013-01-28 16:31:04','YYYY-MM-DD HH24:MI:SS'),100,'Process or Report','SF',10,'The Process field identifies a unique Process or Report in the system.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Process',0,TO_DATE('2013-01-28 16:31:04','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:31:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950242 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:35:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950026,0,'AD_RuleAfter_ID',TO_DATE('2013-01-28 16:35:15','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Rule After Web Service','Rule After Web Service',TO_DATE('2013-01-28 16:35:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:35:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950026 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:35:27 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Regla Despues de Web Service',PrintName='Regla Despues de Web Service',Updated=TO_DATE('2013-01-28 16:35:27','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950026 AND AD_Language='es_MX'
;

-- 28/01/2013 04:38:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference (AD_Client_ID,AD_Org_ID,AD_Reference_ID,Created,CreatedBy,EntityType,IsActive,IsOrderByValue,Name,Updated,UpdatedBy,ValidationType) VALUES (0,0,2950004,TO_DATE('2013-01-28 16:38:07','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N','AD_RuleMenuList',TO_DATE('2013-01-28 16:38:07','YYYY-MM-DD HH24:MI:SS'),100,'T')
;

-- 28/01/2013 04:38:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference_Trl (AD_Language,AD_Reference_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Reference_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Reference t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Reference_ID=2950004 AND NOT EXISTS (SELECT * FROM AD_Reference_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Reference_ID=t.AD_Reference_ID)
;

-- 28/01/2013 04:38:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_Table (AD_Client_ID,AD_Display,AD_Key,AD_Org_ID,AD_Reference_ID,AD_Table_ID,Created,CreatedBy,EntityType,IsActive,IsValueDisplayed,Updated,UpdatedBy) VALUES (0,54249,54248,0,2950004,53058,TO_DATE('2013-01-28 16:38:42','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N',TO_DATE('2013-01-28 16:38:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:38:53 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_Table SET AD_Window_ID=53017,Updated=TO_DATE('2013-01-28 16:38:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950004
;

-- 28/01/2013 04:39:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950243,2950026,0,18,2950004,2950015,'AD_RuleAfter_ID',TO_DATE('2013-01-28 16:39:07','YYYY-MM-DD HH24:MI:SS'),100,'U',10,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Rule After Web Service',0,TO_DATE('2013-01-28 16:39:07','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:39:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950243 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:39:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950027,0,'AD_RuleBefore_ID',TO_DATE('2013-01-28 16:39:56','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Rule Before Web Service','Rule Before Web Service',TO_DATE('2013-01-28 16:39:56','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:39:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950027 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:40:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Regla Antes de Web Service',PrintName='Regla Antes de Web Service',Updated=TO_DATE('2013-01-28 16:40:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950027 AND AD_Language='es_MX'
;

-- 28/01/2013 04:40:49 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950244,2950027,0,18,2950004,2950015,'AD_RuleBefore_ID',TO_DATE('2013-01-28 16:40:49','YYYY-MM-DD HH24:MI:SS'),100,'U',10,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Rule Before Web Service',0,TO_DATE('2013-01-28 16:40:49','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:40:49 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950244 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:41:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950245,469,0,10,2950015,'Name',TO_DATE('2013-01-28 16:41:15','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity','SF',60,'The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','N','N','N','Y','N','Y','N','Y','N','N','Y','Name',0,TO_DATE('2013-01-28 16:41:15','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:41:15 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950245 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:41:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950246,275,0,14,2950015,'Description',TO_DATE('2013-01-28 16:41:34','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',255,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 16:41:34','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:41:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950246 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:42:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950028,0,'MenuType',TO_DATE('2013-01-28 16:42:44','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Menu Type','Menu Type',TO_DATE('2013-01-28 16:42:44','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:42:44 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950028 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:42:57 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Tipo de Menú',PrintName='Tipo de Menú',Updated=TO_DATE('2013-01-28 16:42:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950028 AND AD_Language='es_MX'
;

-- 28/01/2013 04:44:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference (AD_Client_ID,AD_Org_ID,AD_Reference_ID,Created,CreatedBy,EntityType,IsActive,IsOrderByValue,Name,Updated,UpdatedBy,ValidationType) VALUES (0,0,2950005,TO_DATE('2013-01-28 16:44:18','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N','SF_MenuType',TO_DATE('2013-01-28 16:44:18','YYYY-MM-DD HH24:MI:SS'),100,'L')
;

-- 28/01/2013 04:44:18 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference_Trl (AD_Language,AD_Reference_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Reference_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Reference t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Reference_ID=2950005 AND NOT EXISTS (SELECT * FROM AD_Reference_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Reference_ID=t.AD_Reference_ID)
;

-- 28/01/2013 04:44:34 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET Name='SF_Tipo de Menú',Updated=TO_DATE('2013-01-28 16:44:34','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950005 AND AD_Language='es_MX'
;

-- 28/01/2013 04:44:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950005,2950014,TO_DATE('2013-01-28 16:44:47','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Alter',TO_DATE('2013-01-28 16:44:47','YYYY-MM-DD HH24:MI:SS'),100,'A')
;

-- 28/01/2013 04:44:47 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950014 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 04:44:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Cambios',Updated=TO_DATE('2013-01-28 16:44:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950014 AND AD_Language='es_MX'
;

-- 28/01/2013 04:45:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950005,2950015,TO_DATE('2013-01-28 16:45:11','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','DownLoad',TO_DATE('2013-01-28 16:45:11','YYYY-MM-DD HH24:MI:SS'),100,'D')
;

-- 28/01/2013 04:45:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950015 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 04:45:21 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Descargar',Updated=TO_DATE('2013-01-28 16:45:21','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950015 AND AD_Language='es_MX'
;

-- 28/01/2013 04:46:54 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950005,2950016,TO_DATE('2013-01-28 16:46:54','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Menu',TO_DATE('2013-01-28 16:46:54','YYYY-MM-DD HH24:MI:SS'),100,'M')
;

-- 28/01/2013 04:46:54 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950016 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 04:46:57 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Menú',Updated=TO_DATE('2013-01-28 16:46:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950016 AND AD_Language='es_MX'
;

-- 28/01/2013 04:47:21 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950005,2950017,TO_DATE('2013-01-28 16:47:21','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Process',TO_DATE('2013-01-28 16:47:21','YYYY-MM-DD HH24:MI:SS'),100,'P')
;

-- 28/01/2013 04:47:21 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950017 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 04:47:28 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Proceso',Updated=TO_DATE('2013-01-28 16:47:28','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950017 AND AD_Language='es_MX'
;

-- 28/01/2013 04:47:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950005,2950018,TO_DATE('2013-01-28 16:47:55','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','UpLoad',TO_DATE('2013-01-28 16:47:55','YYYY-MM-DD HH24:MI:SS'),100,'U')
;

-- 28/01/2013 04:47:55 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950018 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 04:48:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Cargar',Updated=TO_DATE('2013-01-28 16:48:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950018 AND AD_Language='es_MX'
;

-- 28/01/2013 04:48:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950247,2950028,0,17,2950005,2950015,'MenuType',TO_DATE('2013-01-28 16:48:42','YYYY-MM-DD HH24:MI:SS'),100,'SF',1,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Menu Type',0,TO_DATE('2013-01-28 16:48:42','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:48:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950247 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:50:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950248,2792,0,17,319,2950015,'IsInsertRecord',TO_DATE('2013-01-28 16:50:34','YYYY-MM-DD HH24:MI:SS'),100,'The user can insert a new Record','SF',1,'If not selected, the user cannot create a new Record.  This is automatically disabled, if the Tab is Read Only.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Insert Record',0,TO_DATE('2013-01-28 16:50:34','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:50:34 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950248 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:51:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950249,406,0,17,319,2950015,'IsReadWrite',TO_DATE('2013-01-28 16:51:17','YYYY-MM-DD HH24:MI:SS'),100,'Field is read / write','SF',1,'The Read Write indicates that this field may be read and updated.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Read Write',0,TO_DATE('2013-01-28 16:51:17','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:51:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950249 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:51:36 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950250,475,0,14,2950015,'OrderByClause',TO_DATE('2013-01-28 16:51:36','YYYY-MM-DD HH24:MI:SS'),100,'Fully qualified ORDER BY clause','SF',255,'The ORDER BY Clause indicates the SQL ORDER BY clause to use for record selection','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Sql ORDER BY',0,TO_DATE('2013-01-28 16:51:36','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:51:36 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950250 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:51:51 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950251,630,0,14,2950015,'WhereClause',TO_DATE('2013-01-28 16:51:51','YYYY-MM-DD HH24:MI:SS'),100,'Fully qualified SQL WHERE clause','SF',255,'The Where Clause indicates the SQL WHERE clause to use for record selection. The WHERE clause is added to the query. Fully qualified means "tablename.columnname".','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Sql WHERE',0,TO_DATE('2013-01-28 16:51:51','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:51:51 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950251 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:53:31 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950029,0,'GroupByClause',TO_DATE('2013-01-28 16:53:31','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Group By Clause','Group By Clause',TO_DATE('2013-01-28 16:53:31','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:53:31 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950029 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:53:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Cláusula GROUP BY SQL',PrintName='Cláusula GROUP BY SQL',Updated=TO_DATE('2013-01-28 16:53:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950029 AND AD_Language='es_MX'
;

-- 28/01/2013 04:54:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950252,2950029,0,14,2950015,'GroupByClause',TO_DATE('2013-01-28 16:54:06','YYYY-MM-DD HH24:MI:SS'),100,'SF',255,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Group By Clause',0,TO_DATE('2013-01-28 16:54:06','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:54:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950252 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:54:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950030,0,'ImgName',TO_DATE('2013-01-28 16:54:50','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Image','Image',TO_DATE('2013-01-28 16:54:50','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:54:50 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950030 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:55:02 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Nombre de Imagen',PrintName='Nombre de Imagen',Description='Nombre de Imagen sin extensión de Archivo',Updated=TO_DATE('2013-01-28 16:55:02','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950030 AND AD_Language='es_MX'
;

-- 28/01/2013 04:55:32 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description=NULL,Updated=TO_DATE('2013-01-28 16:55:32','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950030 AND AD_Language='es_MX'
;

-- 28/01/2013 04:56:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950253,2950030,0,10,2950015,'ImgName',TO_DATE('2013-01-28 16:56:07','YYYY-MM-DD HH24:MI:SS'),100,'SF',60,'Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Image',0,TO_DATE('2013-01-28 16:56:07','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:56:07 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950253 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:57:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950254,1720,0,40,2950015,'ImageURL',TO_DATE('2013-01-28 16:57:17','YYYY-MM-DD HH24:MI:SS'),100,'URL of  image','SF',60,'URL of image; The image is not stored in the database, but retrieved at runtime. The image can be a gif, jpeg or png.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Image URL',0,TO_DATE('2013-01-28 16:57:17','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:57:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950254 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:57:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950031,0,'ErrImgName',TO_DATE('2013-01-28 16:57:41','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Error Imgen Name','Error Imgen Name',TO_DATE('2013-01-28 16:57:41','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 04:57:41 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950031 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 04:57:56 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Error en el Nombre de Imagen',PrintName='Error en el Nombre de Imagen',Updated=TO_DATE('2013-01-28 16:57:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950031 AND AD_Language='es_MX'
;

-- 28/01/2013 04:58:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950255,2950031,0,10,2950015,'ErrImgName',TO_DATE('2013-01-28 16:58:11','YYYY-MM-DD HH24:MI:SS'),100,'U',60,'Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Error Imgen Name',0,TO_DATE('2013-01-28 16:58:11','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 04:58:11 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950255 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 04:58:20 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET EntityType='SF',Updated=TO_DATE('2013-01-28 16:58:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950255
;

-- 28/01/2013 05:00:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950256,416,0,20,2950015,'IsSummary',TO_DATE('2013-01-28 17:00:42','YYYY-MM-DD HH24:MI:SS'),100,'This is a summary entity','SF',1,'A summary entity represents a branch in a tree rather than an end-node. Summary entities are used for reporting and do not have own values.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Summary Level',0,TO_DATE('2013-01-28 17:00:42','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:00:42 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950256 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:01:54 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950032,0,'ActivityType',TO_DATE('2013-01-28 17:01:54','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Activity Type','Activity Type',TO_DATE('2013-01-28 17:01:54','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 05:01:54 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950032 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 05:02:01 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Tipo de Actividad',PrintName='Tipo de Actividad',Updated=TO_DATE('2013-01-28 17:02:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950032 AND AD_Language='es_MX'
;

-- 28/01/2013 05:03:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference (AD_Client_ID,AD_Org_ID,AD_Reference_ID,Created,CreatedBy,EntityType,IsActive,IsOrderByValue,Name,Updated,UpdatedBy,ValidationType) VALUES (0,0,2950006,TO_DATE('2013-01-28 17:03:02','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N','ActivityType',TO_DATE('2013-01-28 17:03:02','YYYY-MM-DD HH24:MI:SS'),100,'L')
;

-- 28/01/2013 05:03:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference_Trl (AD_Language,AD_Reference_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Reference_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Reference t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Reference_ID=2950006 AND NOT EXISTS (SELECT * FROM AD_Reference_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Reference_ID=t.AD_Reference_ID)
;

-- 28/01/2013 05:03:13 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET Name='Tipo de Actividad',Updated=TO_DATE('2013-01-28 17:03:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950006 AND AD_Language='es_MX'
;

-- 28/01/2013 05:03:32 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950006,2950019,TO_DATE('2013-01-28 17:03:32','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Custom',TO_DATE('2013-01-28 17:03:32','YYYY-MM-DD HH24:MI:SS'),100,'C')
;

-- 28/01/2013 05:03:32 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950019 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 05:03:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Personalizada',Updated=TO_DATE('2013-01-28 17:03:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950019 AND AD_Language='es_MX'
;

-- 28/01/2013 05:03:53 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950006,2950020,TO_DATE('2013-01-28 17:03:53','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','List',TO_DATE('2013-01-28 17:03:53','YYYY-MM-DD HH24:MI:SS'),100,'L')
;

-- 28/01/2013 05:03:53 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950020 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 05:03:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Lista',Updated=TO_DATE('2013-01-28 17:03:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950020 AND AD_Language='es_MX'
;

-- 28/01/2013 05:04:09 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950006,2950021,TO_DATE('2013-01-28 17:04:09','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Menu',TO_DATE('2013-01-28 17:04:09','YYYY-MM-DD HH24:MI:SS'),100,'M')
;

-- 28/01/2013 05:04:09 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950021 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 05:04:14 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Menú',Updated=TO_DATE('2013-01-28 17:04:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950021 AND AD_Language='es_MX'
;

-- 28/01/2013 05:04:25 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950006,2950022,TO_DATE('2013-01-28 17:04:25','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Search',TO_DATE('2013-01-28 17:04:25','YYYY-MM-DD HH24:MI:SS'),100,'S')
;

-- 28/01/2013 05:04:25 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950022 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 05:04:40 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Búsqueda',Updated=TO_DATE('2013-01-28 17:04:40','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950022 AND AD_Language='es_MX'
;

-- 28/01/2013 05:05:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950257,2950032,0,17,2950006,2950015,'ActivityType',TO_DATE('2013-01-28 17:05:08','YYYY-MM-DD HH24:MI:SS'),100,'SF',1,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Activity Type',0,TO_DATE('2013-01-28 17:05:08','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:05:08 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950257 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:06:48 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference (AD_Client_ID,AD_Org_ID,AD_Reference_ID,Created,CreatedBy,EntityType,IsActive,IsOrderByValue,Name,Updated,UpdatedBy,ValidationType) VALUES (0,0,2950007,TO_DATE('2013-01-28 17:06:48','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N','Action Menu Movil System',TO_DATE('2013-01-28 17:06:48','YYYY-MM-DD HH24:MI:SS'),100,'L')
;

-- 28/01/2013 05:06:48 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Reference_Trl (AD_Language,AD_Reference_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Reference_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Reference t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Reference_ID=2950007 AND NOT EXISTS (SELECT * FROM AD_Reference_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Reference_ID=t.AD_Reference_ID)
;

-- 28/01/2013 05:06:51 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET Name='Acción del Menu Sistema Movil',Updated=TO_DATE('2013-01-28 17:06:51','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950007 AND AD_Language='es_MX'
;

-- 28/01/2013 05:06:59 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Reference_Trl SET Name='Acción del Menú Sistema Movil',Updated=TO_DATE('2013-01-28 17:06:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Reference_ID=2950007 AND AD_Language='es_MX'
;

-- 28/01/2013 05:07:21 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950007,2950023,TO_DATE('2013-01-28 17:07:21','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Form',TO_DATE('2013-01-28 17:07:21','YYYY-MM-DD HH24:MI:SS'),100,'F')
;

-- 28/01/2013 05:07:21 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950023 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 05:07:24 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Formulario',Updated=TO_DATE('2013-01-28 17:07:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950023 AND AD_Language='es_MX'
;

-- 28/01/2013 05:07:38 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950007,2950024,TO_DATE('2013-01-28 17:07:38','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Process',TO_DATE('2013-01-28 17:07:38','YYYY-MM-DD HH24:MI:SS'),100,'P')
;

-- 28/01/2013 05:07:38 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950024 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 05:07:43 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Proceso',Updated=TO_DATE('2013-01-28 17:07:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950024 AND AD_Language='es_MX'
;

-- 28/01/2013 05:07:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List (AD_Client_ID,AD_Org_ID,AD_Reference_ID,AD_Ref_List_ID,Created,CreatedBy,EntityType,IsActive,Name,Updated,UpdatedBy,Value) VALUES (0,0,2950007,2950025,TO_DATE('2013-01-28 17:07:56','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','Report',TO_DATE('2013-01-28 17:07:56','YYYY-MM-DD HH24:MI:SS'),100,'R')
;

-- 28/01/2013 05:07:56 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Ref_List_Trl (AD_Language,AD_Ref_List_ID, Description,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Ref_List_ID, t.Description,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Ref_List t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Ref_List_ID=2950025 AND NOT EXISTS (SELECT * FROM AD_Ref_List_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Ref_List_ID=t.AD_Ref_List_ID)
;

-- 28/01/2013 05:07:59 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Ref_List_Trl SET Name='Reporte',Updated=TO_DATE('2013-01-28 17:07:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Ref_List_ID=2950025 AND AD_Language='es_MX'
;

-- 28/01/2013 05:08:24 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950258,152,0,17,2950007,2950015,'Action',TO_DATE('2013-01-28 17:08:24','YYYY-MM-DD HH24:MI:SS'),100,'Indicates the Action to be performed','SF',1,'The Action field is a drop down list box which indicates the Action to be performed for this Item.','Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Action',0,TO_DATE('2013-01-28 17:08:24','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:08:24 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950258 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:10:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table (AccessLevel,AD_Client_ID,AD_Org_ID,AD_Table_ID,CopyColumnsFromTable,Created,CreatedBy,Description,EntityType,ImportTable,IsActive,IsCentrallyMaintained,IsChangeLog,IsDeleteable,IsHighVolume,IsSecurityEnabled,IsView,LoadSeq,Name,ReplicationType,TableName,Updated,UpdatedBy) VALUES ('3',0,0,2950016,'N',TO_DATE('2013-01-28 17:10:46','YYYY-MM-DD HH24:MI:SS'),100,'Menu List Translation','SF','N','Y','Y','N','Y','N','N','N',0,'Menu List Translation','L','SF_MenuList_Trl',TO_DATE('2013-01-28 17:10:46','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 05:10:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Table_Trl (AD_Language,AD_Table_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Table_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Table t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Table_ID=2950016 AND NOT EXISTS (SELECT * FROM AD_Table_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Table_ID=t.AD_Table_ID)
;

-- 28/01/2013 05:10:46 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Sequence (AD_Client_ID,AD_Org_ID,AD_Sequence_ID,Created,CreatedBy,CurrentNext,CurrentNextSys,Description,IncrementNo,IsActive,IsAudited,IsAutoSequence,IsTableID,Name,StartNewYear,StartNo,Updated,UpdatedBy) VALUES (0,0,1000015,TO_DATE('2013-01-28 17:10:46','YYYY-MM-DD HH24:MI:SS'),100,1000000,50000,'Table SF_MenuList_Trl',1,'Y','N','Y','Y','SF_MenuList_Trl','N',1000000,TO_DATE('2013-01-28 17:10:46','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 05:11:10 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Tradución Menú de Lista',Updated=TO_DATE('2013-01-28 17:11:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950016 AND AD_Language='es_MX'
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950259,102,0,19,2950016,129,'AD_Client_ID',TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Client_ID@','Client/Tenant for this installation.','SF',22,'A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','N','N','N','N','Y','N','N','N','N','N','Client',0,TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950259 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,AD_Val_Rule_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950260,113,0,19,2950016,104,'AD_Org_ID',TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,'@#AD_Org_ID@','Organizational entity within client','SF',22,'An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','N','N','N','N','Y','N','N','N','N','N','Organization',0,TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950260 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950261,245,0,16,2950016,'Created',TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was created','SF',7,'The Created field indicates the date that this record was created.','Y','N','N','N','N','Y','N','N','N','N','N','Created',0,TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950261 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950262,246,0,18,110,2950016,'CreatedBy',TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,'User who created this records','SF',22,'The Created By field indicates the user who created this record.','Y','N','N','N','N','Y','N','N','N','N','N','Created By',0,TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950262 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,DefaultValue,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950263,348,0,20,2950016,'IsActive',TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,'Y','The record is active in the system','SF',1,'There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','N','N','N','N','Y','N','N','N','N','Y','Active',0,TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950263 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950264,607,0,16,2950016,'Updated',TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,'Date this record was updated','SF',7,'The Updated field indicates the date that this record was updated.','Y','N','N','N','N','Y','N','N','N','N','N','Updated',0,TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950264 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAlwaysUpdateable,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950265,608,0,18,110,2950016,'UpdatedBy',TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,'User who updated this records','SF',22,'The Updated By field indicates the user who updated this record.','Y','N','N','N','N','Y','N','N','N','N','N','Updated By',0,TO_DATE('2013-01-28 17:11:17','YYYY-MM-DD HH24:MI:SS'),100,1)
;

-- 28/01/2013 05:11:17 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950265 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:12:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element (AD_Client_ID,AD_Element_ID,AD_Org_ID,ColumnName,Created,CreatedBy,Description,EntityType,IsActive,Name,PrintName,Updated,UpdatedBy) VALUES (0,2950033,0,'SF_MenuList_Trl_ID',TO_DATE('2013-01-28 17:12:06','YYYY-MM-DD HH24:MI:SS'),100,'Menu List Transate ID','SF','Y','Menu List Transate ','Menu List Transate ',TO_DATE('2013-01-28 17:12:06','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 28/01/2013 05:12:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Element_Trl (AD_Language,AD_Element_ID, Description,Help,Name,PO_Description,PO_Help,PO_Name,PO_PrintName,PrintName, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Element_ID, t.Description,t.Help,t.Name,t.PO_Description,t.PO_Help,t.PO_Name,t.PO_PrintName,t.PrintName, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Element t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Element_ID=2950033 AND NOT EXISTS (SELECT * FROM AD_Element_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Element_ID=t.AD_Element_ID)
;

-- 28/01/2013 05:12:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Menu List Translation ID', Name='Menu List Translation ', PrintName='Menu List Translation ',Updated=TO_DATE('2013-01-28 17:12:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950033
;

-- 28/01/2013 05:12:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950033
;

-- 28/01/2013 05:12:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_MenuList_Trl_ID', Name='Menu List Translation ', Description='Menu List Translation ID', Help=NULL WHERE AD_Element_ID=2950033
;

-- 28/01/2013 05:12:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_MenuList_Trl_ID', Name='Menu List Translation ', Description='Menu List Translation ID', Help=NULL, AD_Element_ID=2950033 WHERE UPPER(ColumnName)='SF_MENULIST_TRL_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 28/01/2013 05:12:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_MenuList_Trl_ID', Name='Menu List Translation ', Description='Menu List Translation ID', Help=NULL WHERE AD_Element_ID=2950033 AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 05:12:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Menu List Translation ', Description='Menu List Translation ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950033) AND IsCentrallyMaintained='Y'
;

-- 28/01/2013 05:12:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_PrintFormatItem pi SET PrintName='Menu List Translation ', Name='Menu List Translation ' WHERE IsCentrallyMaintained='Y' AND EXISTS (SELECT * FROM AD_Column c WHERE c.AD_Column_ID=pi.AD_Column_ID AND c.AD_Element_ID=2950033)
;

-- 28/01/2013 05:13:06 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Traducción Menú de Lista',PrintName='Traducción Menú de Lista',Description='Identificador de la Traducción del Menú de Lista',Updated=TO_DATE('2013-01-28 17:13:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950033 AND AD_Language='es_MX'
;

-- 28/01/2013 05:13:26 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950266,2950033,0,13,2950016,'SF_MenuList_Trl_ID',TO_DATE('2013-01-28 17:13:26','YYYY-MM-DD HH24:MI:SS'),100,'Menu List Translation ID','SF',10,'Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Menu List Translation ',0,TO_DATE('2013-01-28 17:13:26','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:13:26 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950266 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:13:48 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950267,469,0,10,2950016,'Name',TO_DATE('2013-01-28 17:13:48','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity','SF',60,'The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','N','N','N','Y','N','Y','N','Y','N','N','Y','Name',0,TO_DATE('2013-01-28 17:13:48','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:13:48 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950267 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:14:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950268,275,0,14,2950016,'Description',TO_DATE('2013-01-28 17:14:06','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record','SF',60,'A description is limited to 255 characters.','Y','Y','N','N','N','N','N','N','N','Y','N','N','Y','Description',0,TO_DATE('2013-01-28 17:14:06','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:14:06 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950268 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:15:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Reference_Value_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950269,109,0,17,311,2950016,'AD_Language',TO_DATE('2013-01-28 17:15:14','YYYY-MM-DD HH24:MI:SS'),100,'Language for this entity','SF',1,'The Language identifies the language to use for display and formatting','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Language',0,TO_DATE('2013-01-28 17:15:14','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:15:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950269 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:15:32 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950270,420,0,20,2950016,'IsTranslated',TO_DATE('2013-01-28 17:15:32','YYYY-MM-DD HH24:MI:SS'),100,'This column is translated','SF',1,'The Translated checkbox indicates if this column is translated.','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Translated',0,TO_DATE('2013-01-28 17:15:32','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:15:32 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950270 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 28/01/2013 05:47:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,EntityType,FieldLength,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950271,53768,0,19,2950015,'WS_WebServiceType_ID',TO_DATE('2013-01-28 17:47:02','YYYY-MM-DD HH24:MI:SS'),100,'SF',10,'Y','Y','N','N','N','N','N','N','N','N','N','N','Y','Web Service Type',0,TO_DATE('2013-01-28 17:47:02','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 28/01/2013 05:47:02 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950271 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;

-- 29/01/2013 08:36:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_ID=17, AD_Reference_Value_ID=167,Updated=TO_DATE('2013-01-29 08:36:30','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950150
;

-- 29/01/2013 08:44:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Inventario (Cliente)',Updated=TO_DATE('2013-01-29 08:44:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950002 AND AD_Language='es_MX'
;

-- 29/01/2013 08:46:34 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Linea de Inventario (Cliente)',Updated=TO_DATE('2013-01-29 08:46:34','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950004 AND AD_Language='es_MX'
;

-- 29/01/2013 08:56:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET Description='Frecuency of Visit',Updated=TO_DATE('2013-01-29 08:56:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950006
;

-- 29/01/2013 08:57:03 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Frecuencia de Visita',Updated=TO_DATE('2013-01-29 08:57:03','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950006 AND AD_Language='es_MX'
;

-- 29/01/2013 08:57:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Name='Frecuency of Visit', PrintName='Frecuency of Visit',Updated=TO_DATE('2013-01-29 08:57:40','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950008
;

-- 29/01/2013 08:57:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950008
;

-- 29/01/2013 08:57:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_Frequency_ID', Name='Frecuency of Visit', Description='Frequency ID', Help=NULL WHERE AD_Element_ID=2950008
;

-- 29/01/2013 08:57:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_Frequency_ID', Name='Frecuency of Visit', Description='Frequency ID', Help=NULL, AD_Element_ID=2950008 WHERE UPPER(ColumnName)='SF_FREQUENCY_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 29/01/2013 08:57:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_Frequency_ID', Name='Frecuency of Visit', Description='Frequency ID', Help=NULL WHERE AD_Element_ID=2950008 AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 08:57:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Frecuency of Visit', Description='Frequency ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950008) AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 08:57:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_PrintFormatItem pi SET PrintName='Frecuency of Visit', Name='Frecuency of Visit' WHERE IsCentrallyMaintained='Y' AND EXISTS (SELECT * FROM AD_Column c WHERE c.AD_Column_ID=pi.AD_Column_ID AND c.AD_Element_ID=2950008)
;

-- 29/01/2013 08:57:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Frecuencia de Visita',PrintName='Frecuencia de Visita',Description='Identificador de la Frecuencia de Visita',Updated=TO_DATE('2013-01-29 08:57:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950008 AND AD_Language='es_MX'
;

-- 29/01/2013 08:59:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET Description='Initial Load Movil System', Name='Initial Load Movil System',Updated=TO_DATE('2013-01-29 08:59:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950007
;

-- 29/01/2013 08:59:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET IsTranslated='N' WHERE AD_Table_ID=2950007
;

-- 29/01/2013 09:00:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Initial Load Movil System ID', Name='Initial Load Movil System', PrintName='Initial Load Movil System',Updated=TO_DATE('2013-01-29 09:00:02','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950011
;

-- 29/01/2013 09:00:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950011
;

-- 29/01/2013 09:00:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_InitialLoad_ID', Name='Initial Load Movil System', Description='Initial Load Movil System ID', Help=NULL WHERE AD_Element_ID=2950011
;

-- 29/01/2013 09:00:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_InitialLoad_ID', Name='Initial Load Movil System', Description='Initial Load Movil System ID', Help=NULL, AD_Element_ID=2950011 WHERE UPPER(ColumnName)='SF_INITIALLOAD_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 29/01/2013 09:00:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_InitialLoad_ID', Name='Initial Load Movil System', Description='Initial Load Movil System ID', Help=NULL WHERE AD_Element_ID=2950011 AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:00:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Initial Load Movil System', Description='Initial Load Movil System ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950011) AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:00:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_PrintFormatItem pi SET PrintName='Initial Load Movil System', Name='Initial Load Movil System' WHERE IsCentrallyMaintained='Y' AND EXISTS (SELECT * FROM AD_Column c WHERE c.AD_Column_ID=pi.AD_Column_ID AND c.AD_Element_ID=2950011)
;

-- 29/01/2013 09:00:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET Name='Menu of Mobile System',Updated=TO_DATE('2013-01-29 09:00:40','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950015
;

-- 29/01/2013 09:00:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET IsTranslated='N' WHERE AD_Table_ID=2950015
;

-- 29/01/2013 09:00:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Menú del Sistema Móvil',Updated=TO_DATE('2013-01-29 09:00:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950015 AND AD_Language='es_MX'
;

-- 29/01/2013 09:01:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Menu of Mobile System ID', Name='Menu of Mobile System', PrintName='Menu of Mobile System',Updated=TO_DATE('2013-01-29 09:01:35','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950025
;

-- 29/01/2013 09:01:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950025
;

-- 29/01/2013 09:01:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_MenuList_ID', Name='Menu of Mobile System', Description='Menu of Mobile System ID', Help=NULL WHERE AD_Element_ID=2950025
;

-- 29/01/2013 09:01:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_MenuList_ID', Name='Menu of Mobile System', Description='Menu of Mobile System ID', Help=NULL, AD_Element_ID=2950025 WHERE UPPER(ColumnName)='SF_MENULIST_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 29/01/2013 09:01:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_MenuList_ID', Name='Menu of Mobile System', Description='Menu of Mobile System ID', Help=NULL WHERE AD_Element_ID=2950025 AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:01:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Menu of Mobile System', Description='Menu of Mobile System ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950025) AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:01:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_PrintFormatItem pi SET PrintName='Menu of Mobile System', Name='Menu of Mobile System' WHERE IsCentrallyMaintained='Y' AND EXISTS (SELECT * FROM AD_Column c WHERE c.AD_Column_ID=pi.AD_Column_ID AND c.AD_Element_ID=2950025)
;

-- 29/01/2013 09:01:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Menú del Sistema Móvil',PrintName='Menú del Sistema Móvil',Description='Identificador del Menú del Sistema Móvil',Updated=TO_DATE('2013-01-29 09:01:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950025 AND AD_Language='es_MX'
;

-- 29/01/2013 09:03:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Name of Image Error ID', Name='Name of Image Error', PrintName='Name of Image Error',Updated=TO_DATE('2013-01-29 09:03:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950031
;

-- 29/01/2013 09:03:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950031
;

-- 29/01/2013 09:03:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='ErrImgName', Name='Name of Image Error', Description='Name of Image Error ID', Help=NULL WHERE AD_Element_ID=2950031
;

-- 29/01/2013 09:03:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='ErrImgName', Name='Name of Image Error', Description='Name of Image Error ID', Help=NULL, AD_Element_ID=2950031 WHERE UPPER(ColumnName)='ERRIMGNAME' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 29/01/2013 09:03:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='ErrImgName', Name='Name of Image Error', Description='Name of Image Error ID', Help=NULL WHERE AD_Element_ID=2950031 AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:03:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Name of Image Error', Description='Name of Image Error ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950031) AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:03:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_PrintFormatItem pi SET PrintName='Name of Image Error', Name='Name of Image Error' WHERE IsCentrallyMaintained='Y' AND EXISTS (SELECT * FROM AD_Column c WHERE c.AD_Column_ID=pi.AD_Column_ID AND c.AD_Element_ID=2950031)
;

-- 29/01/2013 09:04:13 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Nombre de la Imagen de Error',PrintName='Nombre de la Imagen de Error',Description='Identificador del Nombre de la Imagen de Error',Updated=TO_DATE('2013-01-29 09:04:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950031 AND AD_Language='es_MX'
;

-- 29/01/2013 09:04:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Group By Clause ID',Updated=TO_DATE('2013-01-29 09:04:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950029
;

-- 29/01/2013 09:04:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950029
;

-- 29/01/2013 09:04:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='GroupByClause', Name='Group By Clause', Description='Group By Clause ID', Help=NULL WHERE AD_Element_ID=2950029
;

-- 29/01/2013 09:04:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='GroupByClause', Name='Group By Clause', Description='Group By Clause ID', Help=NULL, AD_Element_ID=2950029 WHERE UPPER(ColumnName)='GROUPBYCLAUSE' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 29/01/2013 09:04:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='GroupByClause', Name='Group By Clause', Description='Group By Clause ID', Help=NULL WHERE AD_Element_ID=2950029 AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:04:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Group By Clause', Description='Group By Clause ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950029) AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:05:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Description='Identificador de la Cláusula GROUP BY SQL',Updated=TO_DATE('2013-01-29 09:05:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950029 AND AD_Language='es_MX'
;

-- 29/01/2013 09:10:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET Description='Mobile Synchronizing Trace ', Name='Mobile Synchronizing Trace ',Updated=TO_DATE('2013-01-29 09:10:49','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950014
;

-- 29/01/2013 09:10:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET IsTranslated='N' WHERE AD_Table_ID=2950014
;

-- 29/01/2013 09:11:13 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Traza de Sincronización del Móbil',Updated=TO_DATE('2013-01-29 09:11:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950014 AND AD_Language='es_MX'
;

-- 29/01/2013 09:11:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element SET Description='Mobile Synchronizing Trace  ID', Name='Mobile Synchronizing Trace ', PrintName='Mobile Synchronizing Trace ',Updated=TO_DATE('2013-01-29 09:11:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950022
;

-- 29/01/2013 09:11:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET IsTranslated='N' WHERE AD_Element_ID=2950022
;

-- 29/01/2013 09:11:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET ColumnName='SF_SynchronizingTrace_ID', Name='Mobile Synchronizing Trace ', Description='Mobile Synchronizing Trace  ID', Help=NULL WHERE AD_Element_ID=2950022
;

-- 29/01/2013 09:11:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_SynchronizingTrace_ID', Name='Mobile Synchronizing Trace ', Description='Mobile Synchronizing Trace  ID', Help=NULL, AD_Element_ID=2950022 WHERE UPPER(ColumnName)='SF_SYNCHRONIZINGTRACE_ID' AND IsCentrallyMaintained='Y' AND AD_Element_ID IS NULL
;

-- 29/01/2013 09:11:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Para SET ColumnName='SF_SynchronizingTrace_ID', Name='Mobile Synchronizing Trace ', Description='Mobile Synchronizing Trace  ID', Help=NULL WHERE AD_Element_ID=2950022 AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:11:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Name='Mobile Synchronizing Trace ', Description='Mobile Synchronizing Trace  ID', Help=NULL WHERE AD_Column_ID IN (SELECT AD_Column_ID FROM AD_Column WHERE AD_Element_ID=2950022) AND IsCentrallyMaintained='Y'
;

-- 29/01/2013 09:11:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_PrintFormatItem pi SET PrintName='Mobile Synchronizing Trace ', Name='Mobile Synchronizing Trace ' WHERE IsCentrallyMaintained='Y' AND EXISTS (SELECT * FROM AD_Column c WHERE c.AD_Column_ID=pi.AD_Column_ID AND c.AD_Element_ID=2950022)
;

-- 29/01/2013 09:12:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Traza de Sincronización del Móbil',PrintName='Traza de Sincronización del Móbil',Description='Identificador de la Traza de Sincronización del Móbil',Updated=TO_DATE('2013-01-29 09:12:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950022 AND AD_Language='es_MX'
;
-- 30/01/2013 08:46:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsMandatory='Y',Updated=TO_DATE('2013-01-30 08:46:35','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950066
;

-- 30/01/2013 08:49:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table_Trl SET Name='Linea de Inventario (Cliente)',Updated=TO_DATE('2013-01-30 08:49:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950004 AND AD_Language='es_MX'
;

-- 30/01/2013 08:50:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Element_Trl SET Name='Linea de Inventario (Cliente)',PrintName='Linea de Inventario (Cliente)',Description='Identificador de la Linea de Inventario (Cliente)',Updated=TO_DATE('2013-01-30 08:50:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Element_ID=2950004 AND AD_Language='es_MX'
;

-- 30/01/2013 08:50:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsIdentifier='Y',Updated=TO_DATE('2013-01-30 08:50:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950088
;

-- 30/01/2013 08:51:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 08:51:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950093
;

-- 30/01/2013 08:54:32 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 08:54:32','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950164
;

-- 30/01/2013 08:56:08 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET MandatoryLogic='N',Updated=TO_DATE('2013-01-30 08:56:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950103
;

-- 30/01/2013 08:57:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 08:57:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950102
;

-- 30/01/2013 08:59:06 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsIdentifier='Y', IsSelectionColumn='N', SeqNo=10,Updated=TO_DATE('2013-01-30 08:59:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950117
;

-- 30/01/2013 08:59:08 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsSelectionColumn='Y',Updated=TO_DATE('2013-01-30 08:59:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950117
;

-- 30/01/2013 08:59:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET SeqNo=2,Updated=TO_DATE('2013-01-30 08:59:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950117
;

-- 30/01/2013 08:59:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 08:59:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950115
;

-- 30/01/2013 09:00:51 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_UserQuery (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_User_ID,AD_UserQuery_ID,Created,CreatedBy,IsActive,Name,Updated,UpdatedBy) VALUES (0,0,101,101,100,1000003,TO_DATE('2013-01-30 09:00:51','YYYY-MM-DD HH24:MI:SS'),100,'Y','** Last Query **',TO_DATE('2013-01-30 09:00:51','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:00:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_UserQuery SET UpdatedBy=100,Updated=TO_DATE('2013-01-30 09:00:57','YYYY-MM-DD HH24:MI:SS') WHERE AD_UserQuery_ID=1000003
;

-- 30/01/2013 09:02:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:02:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950140
;

-- 30/01/2013 09:04:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET MandatoryLogic='N',Updated=TO_DATE('2013-01-30 09:04:29','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950080
;

-- 30/01/2013 09:04:34 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsIdentifier='Y',Updated=TO_DATE('2013-01-30 09:04:34','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950078
;

-- 30/01/2013 09:04:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:04:42','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950094
;

-- 30/01/2013 09:09:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:09:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950240
;

-- 30/01/2013 09:10:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:10:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950266
;

-- 30/01/2013 09:11:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950007,Updated=TO_DATE('2013-01-30 09:11:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950008
;

-- 30/01/2013 09:19:03 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET Callout='org.mb.model.CalloutPlanningVisit.nameCall',Updated=TO_DATE('2013-01-30 09:19:03','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950144
;

-- 30/01/2013 09:19:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET Callout='org.mb.model.CalloutPlanningVisit.bPartner;org.mb.model.CalloutPlanningVisit.nameCall',Updated=TO_DATE('2013-01-30 09:19:23','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950143
;

-- 30/01/2013 09:19:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET Callout='org.mb.model.CalloutPlanningVisit.nameCall; org.mb.model.CalloutPlanningVisit.salesRegion',Updated=TO_DATE('2013-01-30 09:19:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950145
;

-- 30/01/2013 09:20:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsIdentifier='Y', SeqNo=1,Updated=TO_DATE('2013-01-30 09:20:31','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950142
;

-- 30/01/2013 09:21:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET DefaultValue='Y',Updated=TO_DATE('2013-01-30 09:21:22','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950147
;

-- 30/01/2013 09:22:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:22:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950151
;

-- 30/01/2013 09:22:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='N',Updated=TO_DATE('2013-01-30 09:22:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950151
;

-- 30/01/2013 09:22:21 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:22:21','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950153
;

-- 30/01/2013 09:22:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET DefaultValue='@#Date@',Updated=TO_DATE('2013-01-30 09:22:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950149
;

-- 30/01/2013 09:23:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET Callout='org.mb.model.CalloutPlanningVisit.nameCall; org.mb.model.CalloutPlanningVisit.nameCall',Updated=TO_DATE('2013-01-30 09:23:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950151
;

-- 30/01/2013 09:25:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET Callout='org.mb.model.CalloutPlanningVisit.validFrom; org.mb.model.CalloutPlanningVisit.nameCall',Updated=TO_DATE('2013-01-30 09:25:12','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950149
;

-- 30/01/2013 09:26:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Val_Rule_ID=167,Updated=TO_DATE('2013-01-30 09:26:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950145
;

-- 30/01/2013 09:27:10 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Val_Rule (AD_Client_ID,AD_Org_ID,AD_Val_Rule_ID,Code,Created,CreatedBy,EntityType,IsActive,Name,Type,Updated,UpdatedBy) VALUES (0,0,2950001,'C_SalesRegion.IsActive=''Y'' AND C_SalesRegion.IsSummary=''N''',TO_DATE('2013-01-30 09:27:10','YYYY-MM-DD HH24:MI:SS'),100,'U','Y','C_SalesRegion Is Active','S',TO_DATE('2013-01-30 09:27:10','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:27:24 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Val_Rule SET Code='C_SalesRegion.IsActive=''Y'' AND AD_Client_ID = @AD_Client_ID@', EntityType='SF',Updated=TO_DATE('2013-01-30 09:27:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Val_Rule_ID=2950001
;

-- 30/01/2013 09:27:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Val_Rule_ID=2950001,Updated=TO_DATE('2013-01-30 09:27:35','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950146
;

-- 30/01/2013 09:30:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Val_Rule_ID=167,Updated=TO_DATE('2013-01-30 09:30:31','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950205
;

-- 30/01/2013 09:31:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:31:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950200
;

-- 30/01/2013 09:31:46 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AccessLevel='4',Updated=TO_DATE('2013-01-30 09:31:46','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950014
;

-- 30/01/2013 09:33:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:33:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950227
;

-- 30/01/2013 09:35:18 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:35:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950215
;

-- 30/01/2013 09:36:18 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET Description='Visit to Customer',Updated=TO_DATE('2013-01-30 09:36:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950010
;

-- 30/01/2013 09:37:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET DefaultValue='N',Updated=TO_DATE('2013-01-30 09:37:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950175
;

-- 30/01/2013 09:37:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:37:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950178
;

-- 30/01/2013 09:38:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='N',Updated=TO_DATE('2013-01-30 09:38:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950178
;

-- 30/01/2013 09:38:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:38:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950177
;

-- 30/01/2013 09:40:34 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsKey='Y', IsUpdateable='N',Updated=TO_DATE('2013-01-30 09:40:34','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950187
;
-- 30/01/2013 03:24:22 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Val_Rule (AD_Client_ID,AD_Org_ID,AD_Val_Rule_ID,Code,Created,CreatedBy,EntityType,IsActive,Name,Type,Updated,UpdatedBy) VALUES (0,0,2950003,'AD_Sequence.IsTableID = ''N''',TO_DATE('2013-01-30 15:24:22','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','AD_Sequence IsTableID','S',TO_DATE('2013-01-30 15:24:22','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 03:25:11 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Val_Rule_ID=2950003,Updated=TO_DATE('2013-01-30 15:25:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950163
;

-- 30/01/2013 04:13:23 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process SET AD_Workflow_ID=NULL, Description='Process Customer Inventory', Name='Process Customer Inventory', Value='prc_CustomerInventory Process',Updated=TO_DATE('2013-01-30 16:13:23','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Process_ID=2950000
;

-- 30/01/2013 04:13:23 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Trl SET IsTranslated='N' WHERE AD_Process_ID=2950000
;

-- 30/01/2013 04:14:01 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Process_Trl SET Name='Procesar Inventario (Clientes)',Description='Procesar Inventario (Clientes)',Updated=TO_DATE('2013-01-30 16:14:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Process_ID=2950000 AND AD_Language='es_MX'
;

-- 30/01/2013 04:14:46 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_Value_ID=135, DefaultValue='IP',Updated=TO_DATE('2013-01-30 16:14:46','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950069
;
-- 30/01/2013 04:28:46 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET DefaultValue='DR',Updated=TO_DATE('2013-01-30 16:28:46','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950068
;

-- 30/01/2013 04:30:26 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET DefaultValue='CO',Updated=TO_DATE('2013-01-30 16:30:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950069
;

-- 31/01/2013 10:35:58 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column (AD_Client_ID,AD_Column_ID,AD_Element_ID,AD_Org_ID,AD_Reference_ID,AD_Table_ID,ColumnName,Created,CreatedBy,Description,EntityType,FieldLength,Help,IsActive,IsAllowLogging,IsAlwaysUpdateable,IsAutocomplete,IsEncrypted,IsIdentifier,IsKey,IsMandatory,IsParent,IsSelectionColumn,IsSyncDatabase,IsTranslated,IsUpdateable,Name,SeqNo,Updated,UpdatedBy,Version) VALUES (0,2950273,566,0,11,2950015,'SeqNo',TO_DATE('2013-01-31 10:35:58','YYYY-MM-DD HH24:MI:SS'),100,'Method of ordering records; lowest number comes first','SF',14,'The Sequence indicates the order of records','Y','Y','N','N','N','N','N','Y','N','N','N','N','Y','Sequence',0,TO_DATE('2013-01-31 10:35:58','YYYY-MM-DD HH24:MI:SS'),100,0)
;

-- 31/01/2013 10:35:58 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Column_Trl (AD_Language,AD_Column_ID, Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Column_ID, t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Column t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Column_ID=2950273 AND NOT EXISTS (SELECT * FROM AD_Column_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Column_ID=t.AD_Column_ID)
;


