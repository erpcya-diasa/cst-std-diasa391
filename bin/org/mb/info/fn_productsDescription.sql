﻿
-- DROP FUNCTION productsDescription(numeric);

CREATE OR REPLACE FUNCTION productsDescription(M_InOut_ID numeric)
  RETURNS TEXT AS
$BODY$
DECLARE
	v_Description		TEXT;
	p_M_InOut_ID		NUMERIC := $1;
	ds			RECORD;
	
BEGIN
	FOR ds IN
	SELECT pr.Name, (del.MovementQty - COALESCE(rel.Qty, 0)) QtyAvailable
		FROM M_InOutLine del
		INNER JOIN M_Product pr ON (pr.M_Product_ID = del.M_Product_ID)
		LEFT JOIN M_RMALine rel ON (rel.M_InOutLine_ID = del.M_InOutLine_ID)
		WHERE del.M_InOut_ID = p_M_InOut_ID
	LOOP
		v_Description := COALESCE(v_Description, '') || RPAD(COALESCE(ds.Name, ''), 25, ' ') || ' <' || ROUND(ds.QtyAvailable, 2) || 
		'> 
';
	END LOOP;
	RETURN v_Description;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100