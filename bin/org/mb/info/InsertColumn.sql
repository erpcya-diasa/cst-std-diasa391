﻿SELECT AD_Table_ID, TableName, Name, IsView, IsDeleteable FROM AD_Table WHERE AD_Table_ID IN(291, 259)

SELECT AD_Table_ID, TableName, Name, IsView, IsDeleteable FROM AD_Table 
WHERE TableName IN('C_BPartner', 'C_Order', 'C_OrderLine', 'C_Activity', 'C_UOM', 'C_DocType', 'M_Product', 
'M_DiscountSchema', 'M_DiscountSchemaBreak')

SELECT AD_Column_ID, ColumnName, AD_Table_ID, ColumnSQL, AD_Reference_ID, IsMandatory, 
	DefaultValue, IsUpdateable, Name, IsTranslated, IsEncrypted, FieldLength, ValueMin, 
	ValueMax, IsActive, Callout, IsSelectionColumn, IsIdentifier 
FROM AD_Column c
WHERE ColumnName IN('C_BPartner_ID', 'Value', 'TaxID', 'Name', 'Name2', 'So_CreditLimit', 
'So_CreditUsed', 'IsActive', 'M_PriceList_ID')
AND AD_Table_ID = 291


SELECT AD_Column_ID, ColumnName, AD_Table_ID, ColumnSQL, AD_Reference_ID, IsMandatory, 
	DefaultValue, IsUpdateable, Name, IsTranslated, IsEncrypted, FieldLength, ValueMin, 
	ValueMax, IsActive, Callout, IsSelectionColumn, IsIdentifier, SeqNo 
FROM AD_Column c
WHERE ColumnName IN('C_Order_ID'
     , 'DocumentNo'
     , 'C_DocTypeTarget_ID'
     , 'C_DocType_ID'
     , 'Description'
     , 'SalesRep_ID'
     , 'DateOrdered'
     , 'DatePromised'
     , 'PaymentRule'
     , 'PaymentTerm'
     , 'TotalLines'
     , 'GrandTotal'
     , 'C_BPartner_ID'
     , 'M_PriceList_ID'
     , 'C_BPartner_Location_ID'
     , 'Bill_Location_ID'
     , 'M_Warehouse_ID'
     , 'DocStatus'
     , 'DocAction'
     , 'C_Activity_ID'
     , 'Sync_C_Order_ID')

AND AD_Table_ID = 259


SELECT AD_Column_ID, ColumnName, AD_Table_ID, ColumnSQL, AD_Reference_ID, IsMandatory, 
	DefaultValue, IsUpdateable, Name, IsTranslated, IsEncrypted, FieldLength, ValueMin, 
	ValueMax, IsActive, Callout, IsSelectionColumn, IsIdentifier, SeqNo 
FROM AD_Column c
WHERE ColumnName IN('C_DocType_ID', 'Name')
AND AD_Table_ID = 217


SELECT AD_Column_ID, ColumnName, AD_Table_ID, ColumnSQL, AD_Reference_ID, IsMandatory, 
	DefaultValue, IsUpdateable, Name, IsTranslated, IsEncrypted, FieldLength, ValueMin, 
	ValueMax, IsActive, Callout, IsSelectionColumn, IsIdentifier 
FROM AD_Column c
WHERE 
AD_Table_ID = 291


/*C_BPartner*/
2930,'M_PriceList_ID',291,'',19,'N','','Y','Price List','N','N',22,'','','Y','','N','N'
2896,'IsActive',291,'',20,'Y','Y','Y','Active','N','N',1,'','','Y','','N','N'
2893,'C_BPartner_ID',291,'',13,'Y','','N','Business Partner ','N','N',22,'','','Y','','N','N'
2902,'Name',291,'',10,'Y','','Y','Name','N','N',60,'','','Y','','Y','Y'
4216,'Name2',291,'',10,'N','','Y','Name 2','N','N',60,'','','Y','','Y','N'
2901,'Value',291,'',10,'Y','','Y','Search Key','N','N',40,'','','Y','','N','N'
2909,'TaxID',291,'',10,'N','','Y','Tax ID','N','N',20,'','','Y','org.sg.model.CalloutBPartner.taxID','N','N'


/*C_Order*/

2171,'DocAction',259,'',28,'Y','CO','Y','Document Action','N','N',2,'','','Y','','N','N',
2201,'GrandTotal',259,'',12,'Y','','N','Grand Total','N','N',22,'','','Y','','N','N',
2200,'TotalLines',259,'',12,'Y','','N','Total Lines','N','N',22,'','','Y','','N','N',
2172,'C_DocType_ID',259,'',19,'Y','0','N','Document Type','N','N',22,'','','Y','','N','N',
2170,'DocStatus',259,'',17,'Y','DR','Y','Document Status','N','N',2,'','','Y','','N','N',
2169,'DocumentNo',259,'',10,'Y','','N','Document No','N','N',30,'','','Y','','N','Y',1
2762,'C_BPartner_ID',259,'',30,'Y','','Y','Business Partner ','N','N',22,'','','Y','org.compiere.model.CalloutOrder.bPartner','N','N',
2174,'Description',259,'',14,'N','','Y','Description','N','N',255,'','','Y','','N','N',
2182,'DatePromised',259,'',15,'Y','@#Date@','Y','Date Promised','N','N',7,'','','Y','','N','N',
3403,'C_Activity_ID',259,'',19,'N','','Y','Activity','N','N',22,'','','Y','','N','N',
2204,'M_PriceList_ID',259,'',19,'Y','','Y','Price List','N','N',22,'','','Y','org.compiere.model.CalloutOrder.priceList','N','N',
4019,'PaymentRule',259,'',28,'Y','B','Y','Payment Rule','N','N',1,'','','Y','','N','N',
2161,'C_Order_ID',259,'',13,'Y','','N','Order','N','N',22,'','','Y','','N','N',
2202,'M_Warehouse_ID',259,'',19,'Y','','Y','Warehouse','N','N',22,'','','Y','','N','N',
8766,'Bill_Location_ID',259,'',18,'N','','Y','Invoice Location','N','N',22,'','','Y','','N','N',0
3400,'C_BPartner_Location_ID',259,'',19,'Y','','Y','Partner Location','N','N',22,'','','Y','','N','N',
2173,'C_DocTypeTarget_ID',259,'',18,'Y','','Y','Target Document Type','N','N',22,'','','Y','org.compiere.model.CalloutOrder.docType','N','N',
2181,'DateOrdered',259,'',15,'Y','@#Date@','Y','Date Ordered','N','N',7,'','','Y','org.compiere.model.CalloutEngine.dateAcct, org.compiere.model.CalloutOrder.priceList','Y','Y',2
2186,'SalesRep_ID',259,'',18,'Y','','Y','Sales Representative','N','N',22,'','','Y','','Y','N',

/*C_DocType*/
1509,'Name',217,'',10,'Y','','Y','Name','Y','N',60,'','','Y','','N','Y',1
1501,'C_DocType_ID',217,'',13,'Y','','N','Document Type','N','N',22,'','','Y','','N','N'





/*Tables*/
316,'C_Activity','Activity','N','Y'
146,'C_UOM','UOM','N','Y'
217,'C_DocType','Document Type','N','Y'
260,'C_OrderLine','Sales Order Line','N','Y'
291,'C_BPartner','Business Partner ','N','N'
259,'C_Order','Order','N','N'
208,'M_Product','Product','N','Y'



SELECT AD_Column_ID, ColumnName, AD_Table_ID, ColumnSQL, AD_Reference_ID, IsMandatory, 
	DefaultValue, IsUpdateable, Name, IsTranslated, IsEncrypted, FieldLength, ValueMin, 
	ValueMax, IsActive, Callout, IsSelectionColumn, IsIdentifier--, SeqNo 
FROM AD_Column c
WHERE ColumnName IN('C_Order_ID'
     , 'C_OrderLine_ID'
     , 'PriceList'
     , 'PriceActual'
     , 'PriceLimit'
     , 'LineNetAmt'
     , 'PriceEntered'
     , 'M_Product_ID'
     , 'C_Tax_ID'
     , 'C_UOM_ID'
     , 'C_Order_ID'
     , 'M_Warehouse_ID'
     , 'TaxAmt'
     , 'QtyEntered'
     , 'QtyDelivered'
     , 'QtyInvoiced'
     , 'QtyOrdered'
     , 'QtyReserved')
AND AD_Table_ID = 260

SELECT AD_Column_ID, ColumnName, AD_Table_ID, ColumnSQL, AD_Reference_ID, IsMandatory, 
	DefaultValue, IsUpdateable, Name, IsTranslated, IsEncrypted, FieldLength, ValueMin, 
	ValueMax, IsActive, Callout, IsSelectionColumn, IsIdentifier--, SeqNo 
FROM AD_Column c
WHERE ColumnName IN('M_DiscountSchema_ID'
     , 'CumulativeLevel'
     , 'DiscountType'
     , 'FlatDiscount'
     , 'IsBPartnerFlatDiscount'
     , 'IsQuantityBased'
     , 'Name'
     , 'ValidFrom'
     , 'M_DiscountSchemaBreak_ID'
     , 'BreakDiscount'
     , 'BreakValue'
     , 'IsBPartnerFlatDiscount'
     , 'SeqNo'
     , 'M_Product_ID'
     , 'M_Product_Category_ID')
AND AD_Table_ID IN(475, 476)


6595,'IsQuantityBased',475,'',20,'Y','Y','Y','Quantity based','N','N',1,'','','Y','','N','N')");
6607,'SeqNo',476,'',11,'Y','@SQL=SELECT NVL(MAX(SeqNo),0)+10 AS DefaultValue FROM M_DiscountSchemaBreak WHERE M_DiscountSchema_ID=@M_DiscountSchema_ID@','Y','Sequence','N','N',22,'','','Y','','N','Y')");
6592,'DiscountType',475,'',17,'Y','','Y','Discount Type','N','N',1,'','','Y','','N','N')");
6608,'BreakValue',476,'',22,'Y','','Y','Break Value','N','N',22,'','','Y','','N','N')");
6589,'Name',475,'',10,'Y','','Y','Name','N','N',60,'','','Y','','N','Y')");
6594,'FlatDiscount',475,'',22,'N','','Y','Flat Discount %','N','N',22,'-100','100','Y','','N','N')");
6596,'CumulativeLevel',475,'',17,'N','L','Y','Accumulation Level','N','N',1,'','','Y','','N','N')");
6598,'M_DiscountSchemaBreak_ID',476,'',13,'Y','','N','Discount Schema Break','N','N',22,'','','Y','','N','N')");
6606,'M_DiscountSchema_ID',476,'',19,'Y','','N','Discount Schema','N','N',22,'','','Y','','N','N')");
6609,'BreakDiscount',476,'',22,'Y','','Y','Break Discount %','N','N',22,'-100','100','Y','','N','N')");
6591,'ValidFrom',475,'',15,'Y','','Y','Valid from','N','N',7,'','','Y','','N','N')");
12399,'IsBPartnerFlatDiscount',476,'',20,'Y','N','Y','B.Partner Flat Discount','N','N',1,'','','Y','','N','N')");
12737,'IsBPartnerFlatDiscount',475,'',20,'Y','','Y','B.Partner Flat Discount','N','N',1,'','','Y','','N','N')");
6610,'M_Product_Category_ID',476,'',19,'N','','Y','Product Category','N','N',22,'','','Y','','N','N')");
6611,'M_Product_ID',476,'',30,'N','','Y','Product','N','N',22,'','','Y','','N','N')");
6581,'M_DiscountSchema_ID',475,'',13,'Y','','N','Discount Schema','N','N',22,'','','Y','','N','N')");