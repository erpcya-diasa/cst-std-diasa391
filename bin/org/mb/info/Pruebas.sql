--DELETE FROM C_CashLine
--DELETE FROM C_Cash
--DROP TABLE C_Cash
INSERT INTO C_Cash(C_Cash_ID, C_CashBook_ID, C_DocTypeTarget_ID, StatementDate, DocumentNo,DocAction, SalesRep_ID, BeginningBalance, EndingBalance) 
VALUES(100, 100, 1000337, Current_Timestamp, 'CDC-1', 'DR', 1000061, 0, 100);

/*INSERT INTO C_Cash(C_Cash_ID, C_CashBook_ID, C_DocType_ID, StatementDate, DocumentNo,DocAction, AD_User_ID, BeginningBalance, EndingBalance) 
VALUES(1001, 100, 1000337, Current_Timestamp, 'CDC-2', 'DR', 1000061, 0, 100);
*/
INSERT INTO C_CashLine(C_CashLine_ID, C_Cash_ID, C_BPartner_ID, CashType, TenderType, DateDoc, Amount, C_Invoice_ID)
VALUES(100, 100, 1001368, 'X', 'I', Current_Timestamp, 374, 1019728);

SELECT * FROM M_InOutLine

INSERT INTO M_InOutLine(M_InOutLine_ID, M_InOut_ID, M_Product_ID, MovementQty, QtyReturn, QtyAvailable, Amt) VALUES(1037203, 1014626, 1000095, 0, 0, 0.5, 0.5)

INSERT INTO C_CashLine(C_CashLine_ID, C_Cash_ID, C_BPartner_ID, CashType, TenderType, DateDoc, Amount, C_Invoice_ID, IsOverUnderPayment, WriteOffAmt)
VALUES(101, 100, 1001368, 'X', 'I', Current_Timestamp, 600, 1021015, 'Y', 10);

INSERT INTO C_CashLine(C_CashLine_ID, C_Cash_ID, C_BPartner_ID, CashType, TenderType, DateDoc, Amount, C_Invoice_ID, ReferenceNo, C_Bank_ID)
VALUES(102, 100, 1001368, 'K', 'I', Current_Timestamp, 467.5, 1018448, '01020304',  1000003);

--SELECT * FROM C_Invoice WHERE C_BPartner_Location_ID = 1001161

--SELECT * FROM C_Bank

--SELECT * FROM C_Order

--SELECT * FROM C_Invoice

--SELECT * FROM C_CashLine

--SELECT * FROM C_Invoice WHERE C_BPartner_ID = 1001368

SELECT ch.C_Cash_ID, ch.DocumentNo, ch.StatementDate, 
cl.C_CashLine_ID,  cl.C_Invoice_ID, inv.DocumentNo, inv.OpenAmt, cl.Amount, (inv.OpenAmt - cl.Amount) Amt, 
cl.ReferenceNo, cl.DateDoc, cl.C_Bank_ID, bn.Name, lc.Name
FROM C_Cash ch
INNER JOIN C_CashLine cl ON(cl.C_Cash_ID = ch.C_Cash_ID)
INNER JOIN C_Invoice inv ON(inv.C_Invoice_ID = cl.C_Invoice_ID)
LEFT JOIN C_Bank bn ON(bn.C_Bank_ID = cl.C_Bank_ID)
LEFT JOIN C_BPartner_Location lc ON(lc.C_BPartner_Location_ID = inv.C_BPartner_Location_ID)

/*CREATE TABLE C_Cash (
       C_Cash_ID NUMERIC(10) NOT NULL
     , C_DocTypeTarget_ID NUMERIC(10) NOT NULL
     , DocumentNo VARCHAR(30) NOT NULL
     , DocAction CHAR(2) NOT NULL
     , SalesRep_ID NUMERIC(10) NOT NULL
     , BeginningBalance NUMERIC(10) NOT NULL
     , EndingBalance NUMERIC(10) NOT NULL
     , Processed CHAR(1) NOT NULL DEFAULT 'N'
     , C_CashBook_ID NUMERIC(10) NOT NULL
     , StatementDate CHAR(10) NOT NULL
     , StatementDifference NUMERIC(10)
     , S_C_Cash_ID NUMERIC(10)
     , CONSTRAINT PK_C_CASH PRIMARY KEY (C_Cash_ID)
     , CONSTRAINT FK_C_Cash_1 FOREIGN KEY (C_DocTypeTarget_ID)
                  REFERENCES C_DocType (C_DocType_ID)
     , CONSTRAINT FK_C_Cash_2 FOREIGN KEY (SalesRep_ID)
                  REFERENCES AD_User (AD_User_ID)
     , CONSTRAINT FK_C_Cash_3 FOREIGN KEY (C_CashBook_ID)
                  REFERENCES C_CashBook (C_CashBook_ID)
);*/


/*CREATE TABLE C_Cash (
       C_Cash_ID NUMERIC(10) NOT NULL
     , C_DocTypeTarget_ID NUMERIC(10) NOT NULL
     , DocumentNo VARCHAR(30) NOT NULL
     , DocAction CHAR(2) NOT NULL
     , AD_User_ID NUMERIC(10) NOT NULL
     , BeginningBalance NUMERIC(10) NOT NULL
     , EndingBalance NUMERIC(10) NOT NULL
     , Processed CHAR(1) NOT NULL DEFAULT 'N'
     , C_CashBook_ID NUMERIC(10) NOT NULL
     , StatementDate CHAR(10) NOT NULL
     , StatementDifference NUMERIC(10)
     , S_C_Cash_ID NUMERIC(10)
     , CONSTRAINT PK_C_CASH PRIMARY KEY (C_Cash_ID)
     , CONSTRAINT FK_C_Cash_1 FOREIGN KEY (C_DocTypeTarget_ID)
                  REFERENCES C_DocType (C_DocType_ID)
     , CONSTRAINT FK_C_Cash_2 FOREIGN KEY (AD_User_ID)
                  REFERENCES AD_User (AD_User_ID)
     , CONSTRAINT FK_C_Cash_3 FOREIGN KEY (C_CashBook_ID)
                  REFERENCES C_CashBook (C_CashBook_ID)
);*/

SELECT ch.C_Cash_ID, ch.DocumentNo, ch.StatementDate, cl.C_CashLine_ID,  cl.CashType, cl.IsOverUnderPayment, cl.TenderType, cl.C_Invoice_ID, inv.DocumentNo, inv.OpenAmt, cl.Amount, cl.WriteOffAmt, cl.ReferenceNo, cl.DateDoc, cl.C_Bank_ID, bn.Name, lc.Name FROM C_Cash ch INNER JOIN C_CashLine cl ON(cl.C_Cash_ID = ch.C_Cash_ID) INNER JOIN C_Invoice inv ON(inv.C_Invoice_ID = cl.C_Invoice_ID) LEFT JOIN C_Bank bn ON(bn.C_Bank_ID = cl.C_Bank_ID) LEFT JOIN C_BPartner_Location lc ON(lc.C_BPartner_Location_ID = inv.C_BPartner_Location_ID)

SELECT ch.C_Cash_ID, ch.DocumentNo, ch.StatementDate, cl.C_CashLine_ID,  
cl.CashType, cl.IsOverUnderPayment, cl.TenderType, cl.C_Invoice_ID, 
inv.DocumentNo, inv.OpenAmt, cl.Amount, cl.WriteOffAmt, cl.ReferenceNo, 
cl.DateDoc, cl.C_Bank_ID, bn.Name, lc.Name, cl.C_BPartner_ID
FROM C_Cash ch 
INNER JOIN C_CashLine cl ON(cl.C_Cash_ID = ch.C_Cash_ID) 
INNER JOIN C_Invoice inv ON(inv.C_Invoice_ID = cl.C_Invoice_ID) 
LEFT JOIN C_Bank bn ON(bn.C_Bank_ID = cl.C_Bank_ID) 
LEFT JOIN C_BPartner_Location lc ON(lc.C_BPartner_Location_ID = inv.C_BPartner_Location_ID)

SELECT inv.C_Invoice_ID, inv.DocumentNo, inv.GrandTotal, (inv.OpenAmt - COALESCE(SUM(cl.Amount), 0)) Amt
FROM C_Invoice inv
LEFT JOIN C_CashLine cl ON(cl.C_Invoice_ID = inv.C_Invoice_ID ) 
WHERE inv.C_BPartner_ID = 1001368
GROUP BY inv.C_Invoice_ID
HAVING (inv.OpenAmt - COALESCE(SUM(cl.Amount), 0)) != 0
ORDER BY inv.DateInvoiced DESC





SELECT inv.C_Invoice_ID, inv.DocumentNo, inv.GrandTotal, (inv.OpenAmt - COALESCE(SUM(cl.Amount), 0)) Amt FROM C_Invoice inv LEFT JOIN C_CashLine cl ON(cl.C_Invoice_ID = inv.C_Invoice_ID) WHERE inv.C_BPartner_ID = 1001368 GROUP BY inv.C_Invoice_ID HAVING (inv.OpenAmt - COALESCE(SUM(cl.Amount), 0)) != 0


SELECT dt.C_DocType_ID FROM C_DocType dt WHERE dt.DocBaseType IN('SOO') AND dt.DocSubTypeSO IN('SO')