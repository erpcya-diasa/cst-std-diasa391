﻿SELECT '(', AD_Sequence_ID, Name, CurrentNext, s.IsActive, s.Prefix, s.StartNo, s.Suffix, '),' 
FROM AD_Sequence s WHERE s.IsActive = 'Y'

SELECT '(', c.AD_Table_ID, c.AD_Column_ID, c.ColumnName, 
c.ColumnSQL, c.AD_Reference_ID, c.IsMandatory, 
c.DefaultValue, c.IsUpdateable, c.IsActive,
c.IsTranslated, c.Name, c.IsEncrypted, 
c.FieldLength, c.ValueMin, c.ValueMax, c.Callout, 
c.IsSelectionColumn, c.IsIdentifier, '),'
FROM AD_Column c
WHERE AD_Table_ID 
IN(SELECT AD_Table_ID FROM AD_Table 
WHERE TableName IN('C_BPartner', 'C_BPartner_Location', 
'C_Location', 'C_Order', 
'C_OrderLine', 'C_Invoice', 
'C_InvoiceLine', 'M_InOut', 
'M_InOutLine'))

/*SELECT AD_Table_ID, TableName, Name FROM AD_Table WHERE TableName IN('C_BPartner', 'C_BPartner_Location', 
'C_Location', 'C_Order', 
'C_OrderLine', 'C_Invoice', 
'C_InvoiceLine', 'M_InOut', 
'M_InOutLine')*/

--SELECT * FROM AD_Column
--SELECT * FROM AD_Field
--SELECT * FROM AD_Reference
--SELECT * FROM C_Location
/*CASE WHEN r.Name = 'Account' THEN 'ACC'
	WHEN r.Name = 'Amount' THEN 'AMO'
	WHEN r.Name = 'Assignment' THEN 'ASS'
	WHEN r.Name = 'Binary' THEN 'BIN'
	WHEN r.Name = 'Button' THEN 'BUT'
	WHEN r.Name = 'Color' THEN 'COL'
	WHEN r.Name = 'Costs+Prices' THEN 'CSP'
	WHEN r.Name = 'Date' THEN 'DAT'
	WHEN r.Name = 'Date+Time' THEN 'DTM'
	WHEN r.Name = 'FileName' THEN 'FLN'
	WHEN r.Name = 'FilePath' THEN 'FLP'
	WHEN r.Name = 'ID' THEN 'IDR'
	WHEN r.Name = 'Image' THEN 'IMG'
	WHEN r.Name = 'Integer' THEN 'INT'
	WHEN r.Name = 'List' THEN 'LST'
	WHEN r.Name = 'Location (Address)' THEN 'LAD'
	WHEN r.Name = 'Locator (WH)' THEN 'LWH'
	WHEN r.Name = 'Memo' THEN 'MEM'
	WHEN r.Name = 'Number' THEN 'NUM'
	WHEN r.Name = 'Printer Name' THEN 'PNM'
	WHEN r.Name = 'Product Attribute' THEN 'PAT'
	WHEN r.Name = 'Quantity' THEN 'QTY'
	WHEN r.Name = 'Search' THEN 'SCH'
	WHEN r.Name = 'String' THEN 'STR'
	WHEN r.Name = 'Table' THEN 'TBL'
	WHEN r.Name = 'Table Direct' THEN 'TBD'
	WHEN r.Name = 'Text' THEN 'TXT'
	WHEN r.Name = 'Text Long' THEN 'TXL'
	WHEN r.Name = 'Time' THEN 'TIM'
	WHEN r.Name = 'URL' THEN 'URL'
	WHEN r.Name = 'Yes-No' THEN 'BYN'
	ELSE ''
END*/