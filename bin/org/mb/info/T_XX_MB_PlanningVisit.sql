﻿--DROP TABLE T_XX_MB_PlanningVisit
CREATE TABLE T_XX_MB_PlanningVisit
(
	C_SalesRegion_ID	NUMERIC(10,0),
	SalesRep_ID		NUMERIC(10,0),
	XX_MB_PlanningVisit_ID	NUMERIC(10,0),
	XX_MB_Day_ID		NUMERIC(10,0),
	XX_MB_Frequency_ID	NUMERIC(10,0),
	Period			VARCHAR(10),
	DateVisit		TIMESTAMP,
	XX_MB_Mo_C_BPartner_ID	NUMERIC(10,0),
	XX_MB_Tu_C_BPartner_ID	NUMERIC(10,0),
	XX_MB_We_C_BPartner_ID	NUMERIC(10,0),
	XX_MB_Th_C_BPartner_ID	NUMERIC(10,0),
	XX_MB_Fr_C_BPartner_ID	NUMERIC(10,0),
	XX_MB_Sa_C_BPartner_ID	NUMERIC(10,0),
	XX_MB_Week		NUMERIC(10,0),
	SeqNo			NUMERIC,
	AD_Client_ID		NUMERIC(10,0), 
	AD_Org_ID		NUMERIC(10,0), 
	AD_PInstance_ID		NUMERIC(10,0)
);