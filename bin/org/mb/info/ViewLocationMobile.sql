﻿
--CREATE OR REPLACE VIEW XX_RV_MB_C_Location AS
SELECT DISTINCT lo.C_Location_ID, lo.C_Region_ID, lo.C_City_ID, lo.Address1, lo.Address2, lo.Address3, lo.Address4, 
lo.RegionName, sr.SalesRep_ID
FROM C_SalesRegion sr
INNER JOIN XX_MB_PlanningVisit pv ON(pv.C_SalesRegion_ID = sr.C_SalesRegion_ID)
INNER JOIN C_BPartner_Location bpl ON(bpl.C_BPartner_Location_ID = pv.C_BPartner_Location_ID OR bpl.C_BPartner_Location_ID = pv.Bill_Location_ID)
INNER JOIN C_Location lo ON(lo.C_Location_ID = bpl.C_Location_ID)
WHERE bpl.IsActive = 'Y' AND sr.IsActive = 'Y' AND pv.IsActive = 'Y'

--CREATE OR REPLACE VIEW XX_RV_MB_C_Region AS
SELECT DISTINCT re.C_Region_ID, re.Name, sr.SalesRep_ID
FROM C_SalesRegion sr
INNER JOIN XX_MB_PlanningVisit pv ON(pv.C_SalesRegion_ID = sr.C_SalesRegion_ID)
INNER JOIN C_BPartner_Location bpl ON(bpl.C_BPartner_Location_ID = pv.C_BPartner_Location_ID OR bpl.C_BPartner_Location_ID = pv.Bill_Location_ID)
INNER JOIN C_Location lo ON(lo.C_Location_ID = bpl.C_Location_ID)
INNER JOIN C_Region re ON(re.C_Region_ID = lo.C_Region_ID)
WHERE bpl.IsActive = 'Y' AND sr.IsActive = 'Y' AND pv.IsActive = 'Y'

--CREATE OR REPLACE VIEW XX_RV_MB_C_City AS
SELECT DISTINCT ci.C_City_ID, ci.Name, sr.SalesRep_ID
FROM C_SalesRegion sr
INNER JOIN XX_MB_PlanningVisit pv ON(pv.C_SalesRegion_ID = sr.C_SalesRegion_ID)
INNER JOIN C_BPartner_Location bpl ON(bpl.C_BPartner_Location_ID = pv.C_BPartner_Location_ID OR bpl.C_BPartner_Location_ID = pv.Bill_Location_ID)
INNER JOIN C_Location lo ON(lo.C_Location_ID = bpl.C_Location_ID)
INNER JOIN C_City ci ON(ci.C_City_ID = lo.C_City_ID)
WHERE bpl.IsActive = 'Y' AND sr.IsActive = 'Y' AND pv.IsActive = 'Y'



CREATE OR REPLACE VIEW xx_rv_mb_c_bpartner_location AS 
 SELECT DISTINCT bpl.c_bpartner_location_id, bpl.name, bpl.isbillto, bpl.isshipto, bpl.ispayfrom, 
 bpl.phone, bpl.phone2, bpl.c_bpartner_id, bpl.c_salesregion_id, bpl.c_bpartner_location_id AS s_c_bpartner_location_id, 
 bpl.isactive, sr.salesrep_id, bpl.ad_client_id, bpl.ad_org_id, bpl.Fax, bpl.C_Location_ID, bpl.Latitude, bpl.Longitude
   FROM c_bpartner_location bpl
   JOIN xx_mb_planningvisit pv ON bpl.c_bpartner_location_id = pv.c_bpartner_location_id OR bpl.c_bpartner_location_id = pv.bill_location_id
   JOIN c_salesregion sr ON pv.c_salesregion_id = sr.c_salesregion_id
  WHERE bpl.isactive = 'Y'::bpchar AND sr.isactive = 'Y'::bpchar AND pv.isactive = 'Y'::bpchar;
