INSERT INTO AD_Table(AD_Table_ID, TableName)
VALUES(1000000, 'C_BPartner');
INSERT INTO AD_Table(AD_Table_ID, TableName)
VALUES(1000001, 'C_BPartnerLocation');

INSERT INTO AD_Column(AD_Column_ID
     ,ColumnName
     , AD_Table_ID
     , DisplayType
     , IsMandatory
     , IsUpdateable
     , ColumnLabel
     , IsTranslated
     , IsEncrypted
     , FieldLength)
     VALUES(1000000, 'Name', 1000000, 1, 'Y', 'N', 'Nombre', 'Y', 'N', 60)

SELECT * FROM AD_Table
SELECT * FROM AD_Column
SELECT t.AD_Table_ID, t.TableName, c.AD_Column_ID, c.ColumnName 
FROM AD_Table t INNER JOIN AD_Column c ON(c.AD_table_ID = t.AD_table_ID)