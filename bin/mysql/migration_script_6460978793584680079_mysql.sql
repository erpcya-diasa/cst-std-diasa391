-- 29/01/2013 09:20:47 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950001,TO_DATE('2013-01-29 09:20:47','YYYY-MM-DD HH24:MI:SS'),100,'U','Y','N','N','Y','Customer Inventory','N',TO_DATE('2013-01-29 09:20:47','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 29/01/2013 09:20:47 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950001 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 29/01/2013 09:20:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Inventario (Cliente)',Updated=TO_DATE('2013-01-29 09:20:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950001 AND AD_Language='es_MX'
;

-- 29/01/2013 09:21:18 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window SET EntityType='SF',Updated=TO_DATE('2013-01-29 09:21:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950001
;

-- 29/01/2013 09:22:32 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950000,2950002,2950001,TO_DATE('2013-01-29 09:22:32','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','N','Y','N','N','N','Y','Y','N','N','Customer Inventory','N',10,0,TO_DATE('2013-01-29 09:22:32','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:22:32 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950000 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 09:22:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Inventario (Cliente)',Updated=TO_DATE('2013-01-29 09:22:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950000 AND AD_Language='es_MX'
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950058,2950000,0,2950000,TO_DATE('2013-01-29 09:23:07','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 09:23:07','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950000 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950062,2950001,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Identifies a Business Partner',10,'SF','A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson','Y','Y','Y','N','N','N','N','N','Business Partner ',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950001 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950054,2950002,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950002 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950061,2950003,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Customer Inventory ID',10,'SF','Y','Y','N','N','N','N','N','N','Customer Inventory',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950003 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950067,2950004,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950004 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950069,2950005,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'The targeted status of the document',2,'SF','You find the current status in the Document Status field. The options are listed in a popup','Y','Y','Y','N','N','N','N','N','Document Action',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950005 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950066,2950006,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Date of the Document',7,'SF','The Document Date indicates the date the document was generated.  It may or may not be the same as the accounting date.','Y','Y','Y','N','N','N','N','N','Document Date',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950006 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950068,2950007,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'The current status of the document',2,'SF','The Document Status indicates the status of a document at this time.  If you want to change the document status, use the Document Action field','Y','Y','Y','N','N','N','N','N','Document Status',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950007 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950065,2950008,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Order',10,'SF','The Order is a control document.  The  Order is complete when the quantity ordered is the same as the quantity shipped and invoiced.  When you close an order, unshipped (backordered) quantities are cancelled.','Y','Y','Y','N','N','N','N','N','Order',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950008 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950055,2950009,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950009 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950063,2950010,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner',10,'SF','The Partner address indicates the location of a Business Partner','Y','Y','Y','N','N','N','N','N','Partner Location',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950010 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950064,2950011,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'Unique identifier of a Price List',10,'SF','Price Lists are used to determine the pricing, margin and cost of items purchased or sold.','Y','Y','Y','N','N','N','N','N','Price List',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950011 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950070,2950012,0,2950000,TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100,'The document has been processed',1,'SF','The Processed checkbox indicates that a document has been processed.','Y','Y','Y','N','N','N','N','N','Processed',TO_DATE('2013-01-29 09:23:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:23:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950012 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950002
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950009
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950000
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950006
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950001
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950010
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950008
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950011
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950004
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950005
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950007
;

-- 29/01/2013 09:24:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950012
;

-- 29/01/2013 09:24:43 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:24:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950009
;

-- 29/01/2013 09:24:56 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950006
;

-- 29/01/2013 09:24:56 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950000
;

-- 29/01/2013 09:25:03 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:25:03','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950000
;

-- 29/01/2013 09:25:10 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:25:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950010
;

-- 29/01/2013 09:25:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:25:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950011
;

-- 29/01/2013 09:25:20 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:25:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950007
;

-- 29/01/2013 09:27:17 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLogic='@C_Order_ID@!0',Updated=TO_DATE('2013-01-29 09:27:17','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950008
;

-- 29/01/2013 09:28:28 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950001,Updated=TO_DATE('2013-01-29 09:28:28','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950002
;

-- 29/01/2013 09:29:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950001,2950004,2950001,TO_DATE('2013-01-29 09:29:35','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','N','Y','N','N','N','Y','Y','N','N','Customer Inventory Line','N',20,0,TO_DATE('2013-01-29 09:29:35','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:29:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950001 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 09:29:58 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Linea de Inventario (Cliente)',Updated=TO_DATE('2013-01-29 09:29:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950001 AND AD_Language='es_MX'
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950085,2950013,0,2950001,TO_DATE('2013-01-29 09:30:02','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 09:30:02','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950013 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950081,2950014,0,2950001,TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950014 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950092,2950015,0,2950001,TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100,'Customer Inventory ID',10,'SF','Y','Y','Y','N','N','N','N','N','Customer Inventory',TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950015 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950093,2950016,0,2950001,TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100,'Customer Inventory Line ID',10,'SF','Y','Y','Y','N','N','N','N','N','Customer Inventory Line',TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950016 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950090,2950017,0,2950001,TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100,'Inventory Type ID',10,'SF','Y','Y','Y','N','N','N','N','N','Inventory Type',TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950017 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950082,2950018,0,2950001,TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950018 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950091,2950019,0,2950001,TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100,'The document has been processed',1,'SF','The Processed checkbox indicates that a document has been processed.','Y','Y','Y','N','N','N','N','N','Processed',TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950019 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950088,2950020,0,2950001,TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100,'Product, Service, Item',10,'SF','Identifies an item which is either purchased or sold in this organization.','Y','Y','Y','N','N','N','N','N','Product',TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950020 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950089,2950021,0,2950001,TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100,'Quantity Read from customer',14,'SF','Y','Y','Y','N','N','N','N','N','Quantity Read',TO_DATE('2013-01-29 09:30:03','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:30:03 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950021 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950014
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950018
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950015
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950013
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950016
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950017
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950019
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950020
;

-- 29/01/2013 09:31:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950021
;

-- 29/01/2013 09:31:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:31:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950018
;

-- 29/01/2013 09:32:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:32:00','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950013
;

-- 29/01/2013 09:32:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950020
;

-- 29/01/2013 09:32:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950017
;

-- 29/01/2013 09:32:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950019
;

-- 29/01/2013 09:33:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:33:41','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950017
;

-- 29/01/2013 09:33:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950021
;

-- 29/01/2013 09:33:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950017
;

-- 29/01/2013 09:33:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950019
;

-- 29/01/2013 09:34:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:34:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950020
;

-- 29/01/2013 09:37:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950001,Updated=TO_DATE('2013-01-29 09:37:29','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950004
;

-- 29/01/2013 09:38:47 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950002,TO_DATE('2013-01-29 09:38:47','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N','N','Y','Visit to Customer','N',TO_DATE('2013-01-29 09:38:47','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 29/01/2013 09:38:47 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950002 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 29/01/2013 09:38:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Visita a Cliente',Updated=TO_DATE('2013-01-29 09:38:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950002 AND AD_Language='es_MX'
;

-- 29/01/2013 09:41:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950002,2950010,2950002,TO_DATE('2013-01-29 09:41:36','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','N','Y','N','N','N','Y','Y','N','N','Visit','N',10,0,TO_DATE('2013-01-29 09:41:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950002 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 09:41:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Visita',Updated=TO_DATE('2013-01-29 09:41:39','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950002 AND AD_Language='es_MX'
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950169,2950022,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950022 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950165,2950023,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950023 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950173,2950024,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Starting date for a range',7,'SF','The Date From indicates the starting date of a range.','Y','Y','Y','N','N','N','N','N','Date From',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950024 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950174,2950025,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'End date of a date range',7,'SF','The Date To indicates the end date of a range (inclusive)','Y','Y','Y','N','N','N','N','N','Date To',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950025 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950172,2950026,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950026 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950175,2950027,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Off Course of Visit',1,'SF','Y','Y','Y','N','N','N','N','N','Off Course',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950027 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950166,2950028,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950028 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950178,2950029,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit ID',10,'SF','Y','Y','Y','N','N','N','N','N','Planning Visit',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950029 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950176,2950030,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent',10,'SF','The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','Y','N','N','N','N','N','Sales Representative',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950030 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950179,2950031,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Status Visit',2,'SF','Y','Y','Y','N','N','N','N','N','Status Visit',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950031 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950177,2950032,0,2950002,TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100,'Visit to Customer ID',10,'SF','Y','Y','Y','N','N','N','N','N','Visit to Customer',TO_DATE('2013-01-29 09:41:43','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:41:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950032 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950026
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950032
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950023
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950028
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950022
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950029
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950027
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950024
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950025
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950030
;

-- 29/01/2013 09:43:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950031
;

-- 29/01/2013 09:43:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:43:49','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950028
;

-- 29/01/2013 09:43:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950029
;

-- 29/01/2013 09:43:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950022
;

-- 29/01/2013 09:45:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:45:23','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950022
;

-- 29/01/2013 09:46:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:46:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950025
;

-- 29/01/2013 09:47:03 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:47:03','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950031
;

-- 29/01/2013 09:47:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950003,2950011,2950002,TO_DATE('2013-01-29 09:47:42','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','N','Y','N','N','N','Y','N','N','N','Visit Line','N',20,1,TO_DATE('2013-01-29 09:47:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950003 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 09:47:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Línea de Visita',Updated=TO_DATE('2013-01-29 09:47:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950003 AND AD_Language='es_MX'
;

-- 29/01/2013 09:47:56 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950184,2950033,0,2950003,TO_DATE('2013-01-29 09:47:56','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 09:47:56','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:56 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950033 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:47:56 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950180,2950034,0,2950003,TO_DATE('2013-01-29 09:47:56','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 09:47:56','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:56 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950034 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950188,2950035,0,2950003,TO_DATE('2013-01-29 09:47:56','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 09:47:56','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950035 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950190,2950036,0,2950003,TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100,'Event Type ID',10,'SF','Y','Y','Y','N','N','N','N','N','Event Type',TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950036 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950181,2950037,0,2950003,TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950037 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950192,2950038,0,2950003,TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100,'Direct internal record ID',10,'SF','The Record ID is the internal unique identifier of a record. Please note that zooming to the record may not be successful for Orders, Invoices and Shipment/Receipts as sometimes the Sales Order type is not known.','Y','Y','Y','N','N','N','N','N','Record ID',TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950038 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950191,2950039,0,2950003,TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100,'Database Table information',10,'SF','The Database Table provides the information of the table definition','Y','Y','Y','N','N','N','N','N','Table',TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950039 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950187,2950040,0,2950003,TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100,'Visit of Line ID',10,'SF','Y','Y','Y','N','N','N','N','N','Visit of Line ',TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950040 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950189,2950041,0,2950003,TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100,'Visit to Customer ID',10,'SF','Y','Y','Y','N','N','N','N','N','Visit to Customer',TO_DATE('2013-01-29 09:47:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 09:47:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950041 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950040
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950034
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950037
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950033
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950035
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950036
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950038
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950039
;

-- 29/01/2013 09:48:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950041
;

-- 29/01/2013 09:49:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950041
;

-- 29/01/2013 09:49:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950033
;

-- 29/01/2013 09:49:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950035
;

-- 29/01/2013 09:49:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950036
;

-- 29/01/2013 09:49:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950038
;

-- 29/01/2013 09:49:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950039
;

-- 29/01/2013 09:49:16 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:49:16','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950037
;

-- 29/01/2013 09:50:24 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950036
;

-- 29/01/2013 09:50:24 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950035
;

-- 29/01/2013 09:50:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950035
;

-- 29/01/2013 09:50:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950036
;

-- 29/01/2013 09:51:10 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 09:51:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950039
;

-- 29/01/2013 09:52:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950040
;

-- 29/01/2013 09:52:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950040
;

-- 29/01/2013 09:53:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950002,Updated=TO_DATE('2013-01-29 09:53:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950010
;

-- 29/01/2013 09:53:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950002,Updated=TO_DATE('2013-01-29 09:53:40','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950011
;

-- 29/01/2013 09:59:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET TabLevel=1,Updated=TO_DATE('2013-01-29 09:59:30','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950001
;

-- 29/01/2013 10:01:33 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950003,TO_DATE('2013-01-29 10:01:33','YYYY-MM-DD HH24:MI:SS'),100,'Event Type','SF','Y','N','N','Y','Event Type','N',TO_DATE('2013-01-29 10:01:33','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 29/01/2013 10:01:33 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950003 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 29/01/2013 10:01:43 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Tipo de Evento',Description='Tipo de Evento',Updated=TO_DATE('2013-01-29 10:01:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950003 AND AD_Language='es_MX'
;

-- 29/01/2013 10:02:34 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950004,2950005,2950003,TO_DATE('2013-01-29 10:02:34','YYYY-MM-DD HH24:MI:SS'),100,NULL,'SF','N','N','Y','N','N','Y','N','Y','N','N','Event Type','N',10,0,TO_DATE('2013-01-29 10:02:34','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:02:34 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950004 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 10:02:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Tipo de Evento',Description='Tipo de Evento',Updated=TO_DATE('2013-01-29 10:02:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950004 AND AD_Language='es_MX'
;

-- 29/01/2013 10:02:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET Description='Event Type',Updated=TO_DATE('2013-01-29 10:02:55','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950004
;

-- 29/01/2013 10:02:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET IsTranslated='N' WHERE AD_Tab_ID=2950004
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950099,2950042,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950042 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950107,2950043,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'Base Event Type for Distinct Document',3,'SF','Y','Y','Y','N','N','N','N','N','Base Event Type',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950043 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950095,2950044,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950044 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950103,2950045,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'Default value',1,'SF','The Default Checkbox indicates if this record will be used as a default value.','Y','Y','Y','N','N','N','N','N','Default',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950045 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950105,2950046,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950046 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950102,2950047,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'Event Type ID',10,'SF','Y','Y','Y','N','N','N','N','N','Event Type',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950047 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950104,2950048,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950048 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950096,2950049,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950049 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950106,2950050,0,2950004,TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100,'Percent Effectivity to each event type',14,'SF','Y','Y','Y','N','N','N','N','N','Percent ffectivity',TO_DATE('2013-01-29 10:03:12','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:03:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950050 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950047
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950044
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950049
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950048
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950046
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950043
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950045
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950050
;

-- 29/01/2013 10:04:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950042
;

-- 29/01/2013 10:04:06 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:04:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950049
;

-- 29/01/2013 10:04:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:04:30','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950045
;

-- 29/01/2013 10:04:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:04:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950042
;

-- 29/01/2013 10:05:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950003,Updated=TO_DATE('2013-01-29 10:05:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950005
;

-- 29/01/2013 10:05:37 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950004,TO_DATE('2013-01-29 10:05:37','YYYY-MM-DD HH24:MI:SS'),100,'Frecuency of Visit','SF','Y','N','N','Y','Frecuency of Visit','N',TO_DATE('2013-01-29 10:05:37','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 29/01/2013 10:05:37 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950004 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 29/01/2013 10:05:46 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Frecuencia de Visita',Description='Frecuencia de Visita',Updated=TO_DATE('2013-01-29 10:05:46','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950004 AND AD_Language='es_MX'
;

-- 29/01/2013 10:07:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950005,2950006,2950004,TO_DATE('2013-01-29 10:07:29','YYYY-MM-DD HH24:MI:SS'),100,'Frecuency Of Visit','SF','N','N','Y','N','N','Y','N','Y','N','N','Frecuency Of Visit','N',10,0,TO_DATE('2013-01-29 10:07:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:07:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950005 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 10:07:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Frecuencia de Visita',Description='Frecuencia de Visita',Updated=TO_DATE('2013-01-29 10:07:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950005 AND AD_Language='es_MX'
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950112,2950051,0,2950005,TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950051 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950108,2950052,0,2950005,TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950052 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950117,2950053,0,2950005,TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950053 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950115,2950054,0,2950005,TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Frequency ID',10,'SF','Y','Y','Y','N','N','N','N','N','Frecuency of Visit',TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950054 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950118,2950055,0,2950005,TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Interval Days until next visit ',14,'SF','Y','Y','Y','N','N','N','N','N','Interval Days',TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950055 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950116,2950056,0,2950005,TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950056 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950109,2950057,0,2950005,TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950057 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:08:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950054
;

-- 29/01/2013 10:08:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950052
;

-- 29/01/2013 10:08:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950057
;

-- 29/01/2013 10:08:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950056
;

-- 29/01/2013 10:08:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950055
;

-- 29/01/2013 10:08:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950053
;

-- 29/01/2013 10:08:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950051
;

-- 29/01/2013 10:08:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:08:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950057
;

-- 29/01/2013 10:08:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:08:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950055
;

-- 29/01/2013 10:14:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950005,TO_DATE('2013-01-29 10:14:12','YYYY-MM-DD HH24:MI:SS'),100,'Initial Load for Movil System','SF','Y','N','N','Y','Initial Load for Movil System','N',TO_DATE('2013-01-29 10:14:12','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 29/01/2013 10:14:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950005 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 29/01/2013 10:14:24 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Carga Inicial Del Sistema Movil',Description='Carga Inicial Del Sistema Movil',Updated=TO_DATE('2013-01-29 10:14:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950005 AND AD_Language='es_MX'
;

-- 29/01/2013 10:14:53 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950006,2950007,2950005,TO_DATE('2013-01-29 10:14:53','YYYY-MM-DD HH24:MI:SS'),100,'Initial Load','SF','N','N','Y','N','N','Y','N','N','N','N','Initial Load','N',10,0,TO_DATE('2013-01-29 10:14:53','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:14:53 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950006 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 10:17:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Carga Inicial',Description='Carga Inicial',Updated=TO_DATE('2013-01-29 10:17:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950006 AND AD_Language='es_MX'
;

-- 29/01/2013 10:17:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsSingleRow='Y',Updated=TO_DATE('2013-01-29 10:17:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950006
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950123,2950058,0,2950006,TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950058 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950119,2950059,0,2950006,TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950059 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950127,2950060,0,2950006,TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950060 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950140,2950061,0,2950006,TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100,'Initial Load Movil System ID',10,'SF','Y','Y','Y','N','N','N','N','N','Initial Load Movil System',TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950061 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950126,2950062,0,2950006,TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950062 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950120,2950063,0,2950006,TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950063 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950128,2950064,0,2950006,TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100,'Method of ordering records; lowest number comes first',14,'SF','The Sequence indicates the order of records','Y','Y','Y','N','N','N','N','N','Sequence',TO_DATE('2013-01-29 10:17:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950064 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950129,2950065,0,2950006,TO_DATE('2013-01-29 10:17:09','YYYY-MM-DD HH24:MI:SS'),100,255,'SF','Y','Y','Y','N','N','N','N','N','SQLStatement',TO_DATE('2013-01-29 10:17:09','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950065 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950131,2950066,0,2950006,TO_DATE('2013-01-29 10:17:09','YYYY-MM-DD HH24:MI:SS'),100,'Fully qualified SQL WHERE clause',255,'SF','The Where Clause indicates the SQL WHERE clause to use for record selection. The WHERE clause is added to the query. Fully qualified means "tablename.columnname".','Y','Y','Y','N','N','N','N','N','Sql WHERE',TO_DATE('2013-01-29 10:17:09','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950066 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:17:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950132,2950067,0,2950006,TO_DATE('2013-01-29 10:17:09','YYYY-MM-DD HH24:MI:SS'),100,'Database Table information',10,'SF','The Database Table provides the information of the table definition','Y','Y','Y','N','N','N','N','N','Table',TO_DATE('2013-01-29 10:17:09','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:17:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950067 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950061
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950059
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950063
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950062
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950064
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950060
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950065
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950067
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950066
;

-- 29/01/2013 10:18:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950058
;

-- 29/01/2013 10:18:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:18:41','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950063
;

-- 29/01/2013 10:18:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:18:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950064
;

-- 29/01/2013 10:18:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:18:49','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950067
;

-- 29/01/2013 10:19:27 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:19:27','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950058
;

-- 29/01/2013 10:20:04 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-29 10:20:04','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950064
;

-- 29/01/2013 10:23:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_ID=11, DefaultValue='@SQL=Select case when exists(Select 1From SF_InitialLoad) then  (Select Max(SeqNo)+10From SF_InitialLoad) else 10 end',Updated=TO_DATE('2013-01-29 10:23:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950128
;

-- 29/01/2013 10:25:06 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_ColumnSortOrder_ID,AD_ColumnSortYesNo_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,2950128,2950123,0,2950007,2950007,2950005,TO_DATE('2013-01-29 10:25:06','YYYY-MM-DD HH24:MI:SS'),100,'Sequence','SF','N','N','Y','N','N','Y','N','N','Y','N','Sequence','N',20,0,TO_DATE('2013-01-29 10:25:06','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:25:06 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950007 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 10:25:32 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Secuencia',Description='Secuencia',Updated=TO_DATE('2013-01-29 10:25:32','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950007 AND AD_Language='es_MX'
;

-- 29/01/2013 10:26:03 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950005,Updated=TO_DATE('2013-01-29 10:26:03','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950007
;

-- 29/01/2013 10:26:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950004,Updated=TO_DATE('2013-01-29 10:26:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950006
;

-- 29/01/2013 10:30:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950006,TO_DATE('2013-01-29 10:30:43','YYYY-MM-DD HH24:MI:SS'),100,'Menu Mobile System','SF','Y','N','N','Y','Menu Mobile System','N',TO_DATE('2013-01-29 10:30:43','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 29/01/2013 10:30:43 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950006 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 29/01/2013 10:30:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Menú del Sistema Móvil',Description='Menú del Sistema Móvil',Updated=TO_DATE('2013-01-29 10:30:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950006 AND AD_Language='es_MX'
;

-- 29/01/2013 10:32:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,OrderByClause,Processing,SeqNo,TabLevel,Updated,UpdatedBy,WhereClause) VALUES (0,0,2950008,2950015,2950006,TO_DATE('2013-01-29 10:32:12','YYYY-MM-DD HH24:MI:SS'),100,'Menu Movil System','SF','N','N','Y','N','N','Y','N','N','N','N','Menu Movil System','SeqNo','N',10,0,TO_DATE('2013-01-29 10:32:12','YYYY-MM-DD HH24:MI:SS'),100,'MenuType = ''M''')
;

-- 29/01/2013 10:32:12 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950008 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 10:32:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Menú Sistema Movil',Description='Menú Sistema Movil',Updated=TO_DATE('2013-01-29 10:32:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950008 AND AD_Language='es_MX'
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950258,2950068,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'Indicates the Action to be performed',1,'SF','The Action field is a drop down list box which indicates the Action to be performed for this Item.','Y','Y','Y','N','N','N','N','N','Action',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950068 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950237,2950069,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950069 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950257,2950070,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,1,'SF','Y','Y','Y','N','N','N','N','N','Activity Type',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950070 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950233,2950071,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950071 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950246,2950072,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950072 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950252,2950073,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'Group By Clause ID',255,'SF','Y','Y','Y','N','N','N','N','N','Group By Clause',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950073 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950253,2950074,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,60,'SF','Y','Y','Y','N','N','N','N','N','Image',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950074 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950254,2950075,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'URL of  image',60,'SF','URL of image; The image is not stored in the database, but retrieved at runtime. The image can be a gif, jpeg or png.','Y','Y','Y','N','N','N','N','N','Image URL',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950075 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950248,2950076,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'The user can insert a new Record',1,'SF','If not selected, the user cannot create a new Record.  This is automatically disabled, if the Tab is Read Only.','Y','Y','Y','N','N','N','N','N','Insert Record',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950076 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950240,2950077,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'Menu of Mobile System ID',10,'SF','Y','Y','Y','N','N','N','N','N','Menu of Mobile System',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950077 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950247,2950078,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,1,'SF','Y','Y','Y','N','N','N','N','N','Menu Type',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950078 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950245,2950079,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:16 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950079 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950255,2950080,0,2950008,TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100,'Name of Image Error ID',60,'SF','Y','Y','Y','N','N','N','N','N','Name of Image Error',TO_DATE('2013-01-29 10:34:16','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950080 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950234,2950081,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950081 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950242,2950082,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,'Process or Report',10,'SF','The Process field identifies a unique Process or Report in the system.','Y','Y','Y','N','N','N','N','N','Process',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950082 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950249,2950083,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,'Field is read / write',1,'SF','The Read Write indicates that this field may be read and updated.','Y','Y','Y','N','N','N','N','N','Read Write',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950083 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950243,2950084,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,10,'U','Y','Y','Y','N','N','N','N','N','Rule After Web Service',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950084 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950244,2950085,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,10,'U','Y','Y','Y','N','N','N','N','N','Rule Before Web Service',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950085 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950241,2950086,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,'Special Form',10,'SF','The Special Form field identifies a unique Special Form in the system.','Y','Y','Y','N','N','N','N','N','Special Form',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950086 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950250,2950087,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,'Fully qualified ORDER BY clause',255,'SF','The ORDER BY Clause indicates the SQL ORDER BY clause to use for record selection','Y','Y','Y','N','N','N','N','N','Sql ORDER BY',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950087 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950251,2950088,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,'Fully qualified SQL WHERE clause',255,'SF','The Where Clause indicates the SQL WHERE clause to use for record selection. The WHERE clause is added to the query. Fully qualified means "tablename.columnname".','Y','Y','Y','N','N','N','N','N','Sql WHERE',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950088 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950256,2950089,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,'This is a summary entity',1,'SF','A summary entity represents a branch in a tree rather than an end-node. Summary entities are used for reporting and do not have own values.','Y','Y','Y','N','N','N','N','N','Summary Level',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950089 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950271,2950090,0,2950008,TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100,10,'SF','Y','Y','Y','N','N','N','N','N','Web Service Type',TO_DATE('2013-01-29 10:34:17','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:34:17 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950090 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950077
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950090
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950071
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950081
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950079
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950072
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950068
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950086
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950082
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950080
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950088
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950073
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950087
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950070
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=130,IsDisplayed='Y' WHERE AD_Field_ID=2950078
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=140,IsDisplayed='Y' WHERE AD_Field_ID=2950083
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=150,IsDisplayed='Y' WHERE AD_Field_ID=2950074
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=160,IsDisplayed='Y' WHERE AD_Field_ID=2950075
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=170,IsDisplayed='Y' WHERE AD_Field_ID=2950076
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=180,IsDisplayed='Y' WHERE AD_Field_ID=2950084
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=190,IsDisplayed='Y' WHERE AD_Field_ID=2950085
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=200,IsDisplayed='Y' WHERE AD_Field_ID=2950089
;

-- 29/01/2013 10:37:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=210,IsDisplayed='Y' WHERE AD_Field_ID=2950069
;

-- 29/01/2013 10:37:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:37:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950081
;

-- 29/01/2013 10:37:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:37:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950086
;

-- 29/01/2013 10:38:06 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:38:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950080
;

-- 29/01/2013 10:38:10 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:38:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950073
;

-- 29/01/2013 10:38:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:38:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950070
;

-- 29/01/2013 10:38:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:38:19','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950083
;

-- 29/01/2013 10:38:24 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:38:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950075
;

-- 29/01/2013 10:38:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:38:29','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950084
;

-- 29/01/2013 10:38:34 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:38:34','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950089
;

-- 29/01/2013 10:38:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950006,Updated=TO_DATE('2013-01-29 10:38:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950015
;

-- 29/01/2013 10:40:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLogic='@Action@ = ''R'' | @Action@ = ''P''',Updated=TO_DATE('2013-01-29 10:40:00','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950082
;

-- 29/01/2013 10:40:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950074
;

-- 29/01/2013 10:40:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=150,IsDisplayed='Y' WHERE AD_Field_ID=2950075
;

-- 29/01/2013 10:40:55 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=160,IsDisplayed='Y' WHERE AD_Field_ID=2950080
;

-- 29/01/2013 10:41:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLogic='@Action@ ! ''R'' & @Action@ ! ''P''
',Updated=TO_DATE('2013-01-29 10:41:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950070
;

-- 29/01/2013 10:43:09 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Val_Rule (AD_Client_ID,AD_Org_ID,AD_Val_Rule_ID,Code,Created,CreatedBy,EntityType,IsActive,Name,Type,Updated,UpdatedBy) VALUES (0,0,2950000,'AD_Ref_List.Value = ''M''',TO_DATE('2013-01-29 10:43:08','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','MenuType Menu','S',TO_DATE('2013-01-29 10:43:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:43:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET AD_Reference_ID=17, AD_Val_Rule_ID=2950000,Updated=TO_DATE('2013-01-29 10:43:19','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950078
;

-- 29/01/2013 10:44:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950007,TO_DATE('2013-01-29 10:44:15','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit','SF','Y','N','N','Y','Planning Visit','N',TO_DATE('2013-01-29 10:44:15','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 29/01/2013 10:44:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950007 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 29/01/2013 10:44:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Planificación de Visita',Description='Planificación de Visita',Updated=TO_DATE('2013-01-29 10:44:29','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950007 AND AD_Language='es_MX'
;

-- 29/01/2013 10:45:26 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950009,230,2950007,TO_DATE('2013-01-29 10:45:26','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','N','Y','N','N','N','Y','Y','N','N','Sales Region','N',10,0,TO_DATE('2013-01-29 10:45:26','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:26 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950009 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 10:45:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Región de Ventas',Updated=TO_DATE('2013-01-29 10:45:30','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950009 AND AD_Language='es_MX'
;

-- 29/01/2013 10:45:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,1826,2950091,0,2950009,TO_DATE('2013-01-29 10:45:41','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 10:45:41','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950091 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,1824,2950092,0,2950009,TO_DATE('2013-01-29 10:45:41','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 10:45:41','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:41 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950092 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,4252,2950093,0,2950009,TO_DATE('2013-01-29 10:45:41','YYYY-MM-DD HH24:MI:SS'),100,'Default value',1,'SF','The Default Checkbox indicates if this record will be used as a default value.','Y','Y','Y','N','N','N','N','N','Default',TO_DATE('2013-01-29 10:45:41','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950093 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,1832,2950094,0,2950009,TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950094 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,1831,2950095,0,2950009,TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950095 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,1825,2950096,0,2950009,TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950096 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,1823,2950097,0,2950009,TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100,'Sales coverage region',22,'SF','The Sales Region indicates a specific area of sales coverage.','Y','Y','N','N','N','N','N','N','Sales Region',TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950097 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,3395,2950098,0,2950009,TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent',22,'SF','The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','Y','N','N','N','N','N','Sales Representative',TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950098 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2023,2950099,0,2950009,TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100,'Search key for the record in the format required - must be unique',40,'SF','A search key allows you a fast method of finding a particular record.
If you leave the search key empty, the system automatically creates a numeric number.  The document sequence used for this fallback number is defined in the "Maintain Sequence" window with the name "DocumentNo_<TableName>", where TableName is the actual name of the table (e.g. C_Order).','Y','Y','Y','N','N','N','N','N','Search Key',TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950099 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,1834,2950100,0,2950009,TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100,'This is a summary entity',1,'SF','A summary entity represents a branch in a tree rather than an end-node. Summary entities are used for reporting and do not have own values.','Y','Y','Y','N','N','N','N','N','Summary Level',TO_DATE('2013-01-29 10:45:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:45:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950100 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:46:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950092
;

-- 29/01/2013 10:46:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950096
;

-- 29/01/2013 10:46:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950099
;

-- 29/01/2013 10:46:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950095
;

-- 29/01/2013 10:46:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950094
;

-- 29/01/2013 10:46:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950093
;

-- 29/01/2013 10:46:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950091
;

-- 29/01/2013 10:46:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950098
;

-- 29/01/2013 10:46:24 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950100
;

-- 29/01/2013 10:46:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:46:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950096
;

-- 29/01/2013 10:47:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:47:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950093
;

-- 29/01/2013 10:47:46 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-29 10:47:46','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950093
;

-- 29/01/2013 10:47:56 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:47:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950091
;

-- 29/01/2013 10:48:10 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:48:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950100
;

-- 29/01/2013 10:52:58 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950010,2950008,2950007,TO_DATE('2013-01-29 10:52:58','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit','SF','N','N','Y','N','N','N','Y','Y','N','N','Planning Visit','N',20,1,TO_DATE('2013-01-29 10:52:58','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:52:58 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950010 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 10:53:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950137,2950101,0,2950010,TO_DATE('2013-01-29 10:53:00','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 10:53:00','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950101 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950143,2950102,0,2950010,TO_DATE('2013-01-29 10:53:00','YYYY-MM-DD HH24:MI:SS'),100,'Identifies a Business Partner',10,'SF','A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson','Y','Y','Y','N','N','N','N','N','Business Partner ',TO_DATE('2013-01-29 10:53:00','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950102 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950133,2950103,0,2950010,TO_DATE('2013-01-29 10:53:00','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 10:53:00','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950103 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950142,2950104,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950104 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950152,2950105,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Event Type ID',10,'SF','Y','Y','Y','N','N','N','N','N','Event Type',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950105 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950151,2950106,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Frequency ID',10,'SF','Y','Y','Y','N','N','N','N','N','Frecuency of Visit',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950106 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950144,2950107,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Business Partner Location for invoicing',10,'SF','Y','Y','Y','N','N','N','N','N','Invoice Location',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950107 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950141,2950108,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950108 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950134,2950109,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950109 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950145,2950110,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner',10,'SF','The Partner address indicates the location of a Business Partner','Y','Y','Y','N','N','N','N','N','Partner Location',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950110 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950153,2950111,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit ID',10,'SF','Y','Y','Y','N','N','N','N','N','Planning Visit',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950111 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950146,2950112,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Sales coverage region',10,'SF','The Sales Region indicates a specific area of sales coverage.','Y','Y','Y','N','N','N','N','N','Sales Region',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950112 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950148,2950113,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Method of ordering records; lowest number comes first',14,'SF','The Sequence indicates the order of records','Y','Y','Y','N','N','N','N','N','Sequence',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950113 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950147,2950114,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Element is valid',1,'SF','The element passed the validation check','Y','Y','Y','N','N','N','N','N','Valid',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950114 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950149,2950115,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Valid from including this date (first day)',7,'SF','The Valid From date indicates the first day of a date range','Y','Y','Y','N','N','N','N','N','Valid from',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950115 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950150,2950116,0,2950010,TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100,'Visit Day Date',7,'SF','Y','Y','Y','N','N','N','N','N','Visit Day ',TO_DATE('2013-01-29 10:53:01','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 10:53:01 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950116 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 10:53:15 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Planificacion de la Visita',Description='Planificacion de la Visita',Updated=TO_DATE('2013-01-29 10:53:15','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950010 AND AD_Language='es_MX'
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950111
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950103
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950109
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950108
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950112
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950104
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950116
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950113
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950106
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950115
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950102
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950110
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950101
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=130,IsDisplayed='Y' WHERE AD_Field_ID=2950114
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=140,IsDisplayed='Y' WHERE AD_Field_ID=2950107
;

-- 29/01/2013 10:54:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=150,IsDisplayed='Y' WHERE AD_Field_ID=2950105
;

-- 29/01/2013 10:54:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:54:49','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950109
;

-- 29/01/2013 10:54:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:54:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950112
;

-- 29/01/2013 10:55:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:55:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950113
;

-- 29/01/2013 10:56:28 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:56:28','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950115
;

-- 29/01/2013 10:56:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:56:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950110
;

-- 29/01/2013 10:57:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 10:57:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950114
;

-- 29/01/2013 10:59:20 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET AD_Reference_ID=11,Updated=TO_DATE('2013-01-29 10:59:20','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950148
;

-- 29/01/2013 11:00:02 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_ColumnSortYesNo_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,2950137,0,2950011,2950008,2950007,TO_DATE('2013-01-29 11:00:02','YYYY-MM-DD HH24:MI:SS'),100,'SF','N','N','Y','N','N','Y','N','N','N','N','Sequence','N',30,0,TO_DATE('2013-01-29 11:00:02','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 11:00:02 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950011 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 01:42:39 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET AD_ColumnSortOrder_ID=2950148, IsSortTab='Y', TabLevel=1,Updated=TO_DATE('2013-01-29 13:42:39','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950011
;

-- 29/01/2013 01:44:30 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Secuencia',Description='Secuencia',Updated=TO_DATE('2013-01-29 13:44:30','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950011 AND AD_Language='es_MX'
;

-- 29/01/2013 01:44:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET Description='Sequence',Updated=TO_DATE('2013-01-29 13:44:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950011
;

-- 29/01/2013 01:44:36 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET IsTranslated='N' WHERE AD_Tab_ID=2950011
;

-- 29/01/2013 01:46:32 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950008,TO_DATE('2013-01-29 13:46:32','YYYY-MM-DD HH24:MI:SS'),100,'System Sequence','SF','Y','N','N','Y','System Sequence','N',TO_DATE('2013-01-29 13:46:32','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 29/01/2013 01:46:32 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950008 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 29/01/2013 01:46:40 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Secuencias del Sistema',Description='Secuencias del Sistema',Updated=TO_DATE('2013-01-29 13:46:40','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950008 AND AD_Language='es_MX'
;

-- 29/01/2013 01:50:09 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Window_Trl WHERE AD_Window_ID=2950008
;

-- 29/01/2013 01:50:09 PM VET
-- Sales Force Mobile with ADempiere
DELETE FROM AD_Window WHERE AD_Window_ID=2950008
;

-- 29/01/2013 01:59:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window SET Description='Visit to Customer',Updated=TO_DATE('2013-01-29 13:59:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950002
;

-- 29/01/2013 01:59:44 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET IsTranslated='N' WHERE AD_Window_ID=2950002
;

-- 29/01/2013 01:59:48 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Description='Visita a Cliente',Updated=TO_DATE('2013-01-29 13:59:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950002 AND AD_Language='es_MX'
;

-- 29/01/2013 01:59:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window SET Description='Customer Inventory',Updated=TO_DATE('2013-01-29 13:59:54','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950001
;

-- 29/01/2013 01:59:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET IsTranslated='N' WHERE AD_Window_ID=2950001
;

-- 29/01/2013 01:59:58 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Description='Inventario (Cliente)',Updated=TO_DATE('2013-01-29 13:59:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950001 AND AD_Language='es_MX'
;

-- 29/01/2013 02:19:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Column_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,2950161,0,2950012,2950009,135,TO_DATE('2013-01-29 14:19:04','YYYY-MM-DD HH24:MI:SS'),100,'Doc Secuence','SF','N','N','Y','N','N','Y','N','Y','N','N','Doc Secuence','N',30,1,TO_DATE('2013-01-29 14:19:04','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:19:04 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950012 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950158,2950117,0,2950012,TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950117 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950154,2950118,0,2950012,TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950118 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950164,2950119,0,2950012,TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100,'Doc Sequence ID',10,'SF','Y','Y','Y','N','N','N','N','N','Doc Sequence',TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950119 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950161,2950120,0,2950012,TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100,'Document type or rules',10,'SF','The Document Type determines document sequence and processing rules','Y','Y','Y','N','N','N','N','N','Document Type',TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950120 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950155,2950121,0,2950012,TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950121 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950162,2950122,0,2950012,TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent',10,'SF','The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','Y','N','N','N','N','N','Sales Representative',TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950122 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950163,2950123,0,2950012,TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100,'Document Sequence',10,'SF','The Sequence defines the numbering sequence to be used for documents.','Y','Y','Y','N','N','N','N','N','Sequence',TO_DATE('2013-01-29 14:19:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 29/01/2013 02:19:14 PM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950123 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 29/01/2013 02:19:37 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Secuence del Documento',Description='Secuence del Documento',Updated=TO_DATE('2013-01-29 14:19:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950012 AND AD_Language='es_MX'
;

-- 29/01/2013 02:21:33 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Secuencia del Documento',Description='Secuencia del Documento',Updated=TO_DATE('2013-01-29 14:21:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950012 AND AD_Language='es_MX'
;

-- 29/01/2013 02:21:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950123
;

-- 29/01/2013 02:21:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950118
;

-- 29/01/2013 02:21:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950121
;

-- 29/01/2013 02:21:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950119
;

-- 29/01/2013 02:21:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950120
;

-- 29/01/2013 02:21:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950122
;

-- 29/01/2013 02:21:50 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950117
;

-- 29/01/2013 02:21:57 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 14:21:57','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950121
;

-- 29/01/2013 02:22:08 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-29 14:22:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950119
;

-- 29/01/2013 02:22:23 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 14:22:23','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950120
;

-- 29/01/2013 02:22:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950119
;

-- 29/01/2013 02:22:35 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950123
;

-- 29/01/2013 02:23:05 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-29 14:23:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950117
;
-- 30/01/2013 09:49:34 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950009,TO_DATE('2013-01-30 09:49:34','YYYY-MM-DD HH24:MI:SS'),100,'Product Access','SF','Y','N','N','Y','Product Access','N',TO_DATE('2013-01-30 09:49:34','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 30/01/2013 09:49:34 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950009 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 30/01/2013 09:49:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Acceso a Productos',Description='Acceso a Productos',Updated=TO_DATE('2013-01-30 09:49:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950009 AND AD_Language='es_MX'
;

-- 30/01/2013 09:50:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950014,2950012,2950009,TO_DATE('2013-01-30 09:50:42','YYYY-MM-DD HH24:MI:SS'),100,'Product Access','SF','N','N','Y','N','N','Y','N','Y','N','N','Product Access','N',10,0,TO_DATE('2013-01-30 09:50:42','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950014 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950197,2950124,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950124 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950204,2950125,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Identifies a Business Partner',10,'SF','A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson','Y','Y','Y','N','N','N','N','N','Business Partner ',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950125 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950203,2950126,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Business Partner Group',10,'SF','The Business Partner Group provides a method of defining defaults to be used for individual Business Partners.','Y','Y','Y','N','N','N','N','N','Business Partner Group',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950126 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950193,2950127,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950127 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950206,2950128,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Exclude access to the data - if not selected Include access to the data',1,'SF','If selected (excluded), the role cannot access the data specified.  If not selected (included), the role can ONLY access the data specified. Exclude items represent a negative list (i.e. you don''t have access to the listed items). Include items represent a positive list (i.e. you only have access to the listed items).
<br>You would usually  not mix Exclude and Include. If you have one include rule in your list, you would only have access to that item anyway.','Y','Y','Y','N','N','N','N','N','Exclude',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950128 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950194,2950129,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950129 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950205,2950130,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner',10,'SF','The Partner address indicates the location of a Business Partner','Y','Y','Y','N','N','N','N','N','Partner Location',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950130 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950202,2950131,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Product, Service, Item',10,'SF','Identifies an item which is either purchased or sold in this organization.','Y','Y','Y','N','N','N','N','N','Product',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950131 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950200,2950132,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Product Access ID',10,'SF','Y','Y','N','N','N','N','N','N','Product Access',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950132 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950201,2950133,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Category of a Product',10,'SF','Identifies the category which this product belongs to.  Product categories are used for pricing and selection.','Y','Y','Y','N','N','N','N','N','Product Category',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950133 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950207,2950134,0,2950014,TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent',10,'SF','The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','Y','N','N','N','N','N','Sales Representative',TO_DATE('2013-01-30 09:50:45','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:50:45 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950134 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:50:54 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Acceso a Productos',Description='Acceso a Productos',Updated=TO_DATE('2013-01-30 09:50:54','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950014 AND AD_Language='es_MX'
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950127
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950129
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950134
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950124
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950131
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950133
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950125
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950126
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950130
;

-- 30/01/2013 09:51:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950128
;

-- 30/01/2013 09:51:34 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 09:51:34','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950129
;

-- 30/01/2013 09:51:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 09:51:41','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950124
;

-- 30/01/2013 09:51:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 09:51:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950133
;

-- 30/01/2013 09:51:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 09:51:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950126
;

-- 30/01/2013 09:51:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 09:51:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950128
;

-- 30/01/2013 09:52:18 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsMandatory='Y',Updated=TO_DATE('2013-01-30 09:52:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950133
;

-- 30/01/2013 09:52:27 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsMandatory='Y',Updated=TO_DATE('2013-01-30 09:52:27','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950131
;

-- 30/01/2013 09:52:41 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsMandatory='Y',Updated=TO_DATE('2013-01-30 09:52:41','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950126
;

-- 30/01/2013 09:52:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsMandatory='Y',Updated=TO_DATE('2013-01-30 09:52:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950125
;

-- 30/01/2013 09:56:27 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsMandatory='Y',Updated=TO_DATE('2013-01-30 09:56:27','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950068
;

-- 30/01/2013 09:57:05 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950010,TO_DATE('2013-01-30 09:57:04','YYYY-MM-DD HH24:MI:SS'),100,'SF','Y','N','N','Y','Synchronizing Trace','N',TO_DATE('2013-01-30 09:57:04','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 30/01/2013 09:57:05 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950010 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 30/01/2013 09:57:27 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950015,2950014,2950010,TO_DATE('2013-01-30 09:57:27','YYYY-MM-DD HH24:MI:SS'),100,'Synchronizing Trace','SF','N','N','Y','N','N','Y','N','Y','N','N','Synchronizing Trace','N',10,0,TO_DATE('2013-01-30 09:57:27','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:27 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950015 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950224,2950135,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950135 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950220,2950136,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950136 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950232,2950137,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950137 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950230,2950138,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'End Sync Date',7,'SF','Y','Y','Y','N','N','N','N','N','End Sync',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950138 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950228,2950139,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'Type of Event',3,'SF','Y','Y','Y','N','N','N','N','N','Event Type',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950139 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950231,2950140,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'Menu of Mobile System ID',10,'SF','Y','Y','Y','N','N','N','N','N','Menu of Mobile System',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950140 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950227,2950141,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'Mobile Synchronizing Trace  ID',10,'SF','Y','Y','N','N','N','N','N','N','Mobile Synchronizing Trace ',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950141 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950221,2950142,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950142 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950229,2950143,0,2950015,TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100,'Start Sync Date',7,'SF','Y','Y','Y','N','N','N','N','N','Start Sync',TO_DATE('2013-01-30 09:57:29','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 09:57:29 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950143 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 09:57:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Traza de Sincronización',Description='Traza de Sincronización',Updated=TO_DATE('2013-01-30 09:57:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950010 AND AD_Language='es_MX'
;

-- 30/01/2013 09:57:51 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Traza de Sincronización',Description='Traza de Sincronización',Updated=TO_DATE('2013-01-30 09:57:51','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950015 AND AD_Language='es_MX'
;

-- 30/01/2013 09:58:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window SET Description='Synchronizing Trace',Updated=TO_DATE('2013-01-30 09:58:00','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950010
;

-- 30/01/2013 09:58:00 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET IsTranslated='N' WHERE AD_Window_ID=2950010
;

-- 30/01/2013 09:58:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950136
;

-- 30/01/2013 09:58:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950142
;

-- 30/01/2013 09:58:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950139
;

-- 30/01/2013 09:58:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950140
;

-- 30/01/2013 09:58:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950137
;

-- 30/01/2013 09:58:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950143
;

-- 30/01/2013 09:58:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950138
;

-- 30/01/2013 09:58:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950135
;

-- 30/01/2013 09:59:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 09:59:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950142
;

-- 30/01/2013 09:59:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 09:59:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950140
;

-- 30/01/2013 09:59:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 09:59:19','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950138
;

-- 30/01/2013 10:00:07 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950011,TO_DATE('2013-01-30 10:00:07','YYYY-MM-DD HH24:MI:SS'),100,'System Config','SF','Y','N','N','Y','System Config','N',TO_DATE('2013-01-30 10:00:07','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 30/01/2013 10:00:07 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950011 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 30/01/2013 10:00:18 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950016,2950013,2950011,TO_DATE('2013-01-30 10:00:18','YYYY-MM-DD HH24:MI:SS'),100,'System Config','U','N','N','Y','N','N','Y','N','N','N','N','System Config','N',10,0,TO_DATE('2013-01-30 10:00:18','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:18 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950016 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 30/01/2013 10:00:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Configurar Sistema',Description='Configurar Sistema',Updated=TO_DATE('2013-01-30 10:00:31','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950011 AND AD_Language='es_MX'
;

-- 30/01/2013 10:00:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Configurar Sistema',Description='Configurar Sistema',Updated=TO_DATE('2013-01-30 10:00:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950016 AND AD_Language='es_MX'
;

-- 30/01/2013 10:00:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET EntityType='SF', IsSingleRow='Y',Updated=TO_DATE('2013-01-30 10:00:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950016
;

-- 30/01/2013 10:00:56 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950212,2950144,0,2950016,TO_DATE('2013-01-30 10:00:56','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-30 10:00:56','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:56 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950144 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950208,2950145,0,2950016,TO_DATE('2013-01-30 10:00:56','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-30 10:00:56','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950145 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950219,2950146,0,2950016,TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950146 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950218,2950147,0,2950016,TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950147 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950209,2950148,0,2950016,TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950148 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950216,2950149,0,2950016,TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100,'Sales Representative or Company Agent',10,'SF','The Sales Representative indicates the Sales Rep for this Region.  Any Sales Rep must be a valid internal user.','Y','Y','Y','N','N','N','N','N','Sales Representative',TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950149 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950217,2950150,0,2950016,TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100,'Search key for the record in the format required - must be unique',60,'SF','A search key allows you a fast method of finding a particular record.
If you leave the search key empty, the system automatically creates a numeric number.  The document sequence used for this fallback number is defined in the "Maintain Sequence" window with the name "DocumentNo_<TableName>", where TableName is the actual name of the table (e.g. C_Order).','Y','Y','Y','N','N','N','N','N','Search Key',TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950150 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950215,2950151,0,2950016,TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100,'Sys Config ID',10,'SF','Y','Y','N','N','N','N','N','N','Sys Config',TO_DATE('2013-01-30 10:00:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:00:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950151 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:01:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950145
;

-- 30/01/2013 10:01:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950148
;

-- 30/01/2013 10:01:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950150
;

-- 30/01/2013 10:01:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950147
;

-- 30/01/2013 10:01:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950146
;

-- 30/01/2013 10:01:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950149
;

-- 30/01/2013 10:01:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950144
;

-- 30/01/2013 10:01:35 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 10:01:35','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950148
;

-- 30/01/2013 10:01:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950144
;

-- 30/01/2013 10:01:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950146
;

-- 30/01/2013 10:01:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950149
;

-- 30/01/2013 10:01:56 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 10:01:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950144
;

-- 30/01/2013 10:02:13 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950011,Updated=TO_DATE('2013-01-30 10:02:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950013
;

-- 30/01/2013 10:02:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950009,Updated=TO_DATE('2013-01-30 10:02:40','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950012
;

-- 30/01/2013 10:03:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=135,Updated=TO_DATE('2013-01-30 10:03:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950009
;

-- 30/01/2013 10:03:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window (AD_Client_ID,AD_Org_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,IsActive,IsBetaFunctionality,IsDefault,IsSOTrx,Name,Processing,Updated,UpdatedBy,WindowType,WinHeight,WinWidth) VALUES (0,0,2950012,TO_DATE('2013-01-30 10:03:42','YYYY-MM-DD HH24:MI:SS'),100,'Inventory Type','SF','Y','N','N','Y','Inventory Type','N',TO_DATE('2013-01-30 10:03:42','YYYY-MM-DD HH24:MI:SS'),100,'M',0,0)
;

-- 30/01/2013 10:03:42 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Window_Trl (AD_Language,AD_Window_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Window_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Window t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Window_ID=2950012 AND NOT EXISTS (SELECT * FROM AD_Window_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Window_ID=t.AD_Window_ID)
;

-- 30/01/2013 10:03:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950017,2950003,2950012,TO_DATE('2013-01-30 10:03:57','YYYY-MM-DD HH24:MI:SS'),100,'Inventory Type','SF','N','N','Y','N','N','Y','N','Y','N','N','Inventory Type','N',10,0,TO_DATE('2013-01-30 10:03:57','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:03:57 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950017 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 30/01/2013 10:04:06 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Window_Trl SET Name='Tipo de Inventario',Description='Tipo de Inventario',Updated=TO_DATE('2013-01-30 10:04:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Window_ID=2950012 AND AD_Language='es_MX'
;

-- 30/01/2013 10:04:09 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Tipo de Inventario',Description='Tipo de Inventario',Updated=TO_DATE('2013-01-30 10:04:09','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950017 AND AD_Language='es_MX'
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950075,2950152,0,2950017,TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950152 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950071,2950153,0,2950017,TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950153 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950080,2950154,0,2950017,TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100,'Default value',1,'SF','The Default Checkbox indicates if this record will be used as a default value.','Y','Y','Y','N','N','N','N','N','Default',TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950154 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950079,2950155,0,2950017,TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950155 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950094,2950156,0,2950017,TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100,'Inventory Type ID',10,'SF','Y','Y','N','N','N','N','N','N','Inventory Type',TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950156 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950078,2950157,0,2950017,TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950157 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950072,2950158,0,2950017,TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-30 10:04:15','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950158 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:04:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950153
;

-- 30/01/2013 10:04:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950158
;

-- 30/01/2013 10:04:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950157
;

-- 30/01/2013 10:04:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950152
;

-- 30/01/2013 10:04:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950155
;

-- 30/01/2013 10:04:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950154
;

-- 30/01/2013 10:04:49 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 10:04:49','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950158
;

-- 30/01/2013 10:04:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 10:04:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950152
;

-- 30/01/2013 10:05:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950012,Updated=TO_DATE('2013-01-30 10:05:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950003
;

-- 30/01/2013 10:05:52 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy) VALUES (0,0,2950018,2950016,2950006,TO_DATE('2013-01-30 10:05:52','YYYY-MM-DD HH24:MI:SS'),100,'U','N','N','Y','N','N','Y','N','Y','N','N','Translate Menu Movil System ','N',20,0,TO_DATE('2013-01-30 10:05:52','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:05:52 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950018 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 30/01/2013 10:07:18 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET EntityType='SF', TabLevel=1,Updated=TO_DATE('2013-01-30 10:07:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 10:07:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET Description='Translate Menu Movil System ',Updated=TO_DATE('2013-01-30 10:07:22','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 10:07:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET IsTranslated='N' WHERE AD_Tab_ID=2950018
;

-- 30/01/2013 10:07:51 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET IsSingleRow='Y',Updated=TO_DATE('2013-01-30 10:07:51','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950008
;

-- 30/01/2013 10:08:10 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Traducción del Menú Sistema Movil',Description='Traducción del Menú Sistema Movil',Updated=TO_DATE('2013-01-30 10:08:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950018 AND AD_Language='es_MX'
;

-- 30/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950263,2950159,0,2950018,TO_DATE('2013-01-30 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-30 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950159 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950259,2950160,0,2950018,TO_DATE('2013-01-30 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-30 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950160 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950268,2950161,0,2950018,TO_DATE('2013-01-30 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',60,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-30 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:08:13 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950161 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950269,2950162,0,2950018,TO_DATE('2013-01-30 10:08:13','YYYY-MM-DD HH24:MI:SS'),100,'Language for this entity',1,'SF','The Language identifies the language to use for display and formatting','Y','Y','Y','N','N','N','N','N','Language',TO_DATE('2013-01-30 10:08:13','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950162 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950266,2950163,0,2950018,TO_DATE('2013-01-30 10:08:14','YYYY-MM-DD HH24:MI:SS'),100,'Menu List Translation ID',10,'SF','Y','Y','N','N','N','N','N','N','Menu List Translation ',TO_DATE('2013-01-30 10:08:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950163 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950267,2950164,0,2950018,TO_DATE('2013-01-30 10:08:14','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-30 10:08:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950164 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950260,2950165,0,2950018,TO_DATE('2013-01-30 10:08:14','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-30 10:08:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950165 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950270,2950166,0,2950018,TO_DATE('2013-01-30 10:08:14','YYYY-MM-DD HH24:MI:SS'),100,'This column is translated',1,'SF','The Translated checkbox indicates if this column is translated.','Y','Y','Y','N','N','N','N','N','Translated',TO_DATE('2013-01-30 10:08:14','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 30/01/2013 10:08:14 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950166 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 30/01/2013 10:08:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950160
;

-- 30/01/2013 10:08:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950165
;

-- 30/01/2013 10:08:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950164
;

-- 30/01/2013 10:08:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950159
;

-- 30/01/2013 10:08:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950161
;

-- 30/01/2013 10:08:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950162
;

-- 30/01/2013 10:08:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950166
;

-- 30/01/2013 10:08:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 10:08:39','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950165
;

-- 30/01/2013 10:08:43 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 10:08:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950159
;

-- 30/01/2013 10:08:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950162
;

-- 30/01/2013 10:08:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950164
;

-- 30/01/2013 10:08:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950159
;

-- 30/01/2013 10:08:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950161
;

-- 30/01/2013 10:09:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950166
;

-- 30/01/2013 10:09:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950164
;

-- 30/01/2013 10:09:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950159
;

-- 30/01/2013 10:09:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950161
;

-- 30/01/2013 10:09:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950159
;

-- 30/01/2013 10:09:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950166
;

-- 30/01/2013 10:09:57 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950164
;

-- 30/01/2013 10:10:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET Description='Menu of Mobile System',Updated=TO_DATE('2013-01-30 10:10:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950016
;

-- 30/01/2013 10:10:37 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950006,Updated=TO_DATE('2013-01-30 10:10:37','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950016
;

-- 30/01/2013 10:11:18 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Table SET AD_Window_ID=2950010,Updated=TO_DATE('2013-01-30 10:11:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Table_ID=2950014
;
-- 30/01/2013 12:08:47 PM VET
-- SF Android
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-30 12:08:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950073
;

-- 30/01/2013 12:09:04 PM VET
-- SF Android
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-30 12:09:04','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950070
;

-- 30/01/2013 12:09:15 PM VET
-- SF Android
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-30 12:09:15','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950075
;

-- 30/01/2013 12:09:36 PM VET
-- SF Android
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-30 12:09:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950084
;

-- 30/01/2013 12:09:39 PM VET
-- SF Android
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-30 12:09:39','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950085
;

-- 30/01/2013 12:09:43 PM VET
-- SF Android
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-30 12:09:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950089
;
-- 30/01/2013 12:15:59 PM VET
-- SF Android
UPDATE AD_Tab SET OrderByClause=NULL,Updated=TO_DATE('2013-01-30 12:15:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950008
;
-- 30/01/2013 03:10:49 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET SeqNo=30,Updated=TO_DATE('2013-01-30 15:10:49','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=230
;

-- 30/01/2013 03:10:54 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET SeqNo=20,Updated=TO_DATE('2013-01-30 15:10:54','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950012
;

-- 30/01/2013 03:11:18 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET Description='Doc Secuence for Sales Rep.', Name='Doc Secuence for Sales Rep.',Updated=TO_DATE('2013-01-30 15:11:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950012
;

-- 30/01/2013 03:11:18 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET IsTranslated='N' WHERE AD_Tab_ID=2950012
;

-- 30/01/2013 03:11:38 PM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Secuencia del Documento por Rep. Ventas',Description='Secuencia del Documento por Rep. Ventas',Updated=TO_DATE('2013-01-30 15:11:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950012 AND AD_Language='es_MX'
;
-- 31/01/2013 08:15:06 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy,WhereClause) VALUES (0,0,2950019,2950008,123,TO_DATE('2013-01-31 08:15:06','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit','SF','N','N','Y','N','N','Y','N','Y','N','N','Planning Visit','N',120,1,TO_DATE('2013-01-31 08:15:06','YYYY-MM-DD HH24:MI:SS'),100,'C_BPartner_ID = @C_BPartner_ID@')
;

-- 31/01/2013 08:15:06 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950019 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 31/01/2013 08:15:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET SeqNo=100,Updated=TO_DATE('2013-01-31 08:15:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019
;

-- 31/01/2013 08:16:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET SeqNo=110,Updated=TO_DATE('2013-01-31 08:16:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=496
;

-- 31/01/2013 08:16:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET SeqNo=120,Updated=TO_DATE('2013-01-31 08:16:30','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=756
;

-- 31/01/2013 08:16:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET SeqNo=130,Updated=TO_DATE('2013-01-31 08:16:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=439
;

-- 31/01/2013 08:17:27 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950137,2950168,0,2950019,TO_DATE('2013-01-31 08:17:27','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','Y','Y','Y','N','N','N','N','N','Active',TO_DATE('2013-01-31 08:17:27','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950168 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950143,2950169,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Identifies a Business Partner',10,'SF','A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson','Y','Y','Y','N','N','N','N','N','Business Partner ',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950169 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950133,2950170,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','Y','Y','Y','N','N','N','N','N','Client',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950170 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950142,2950171,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','Y','Y','Y','N','N','N','N','N','Description',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950171 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950152,2950172,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Event Type ID',10,'SF','Y','Y','Y','N','N','N','N','N','Event Type',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950172 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950151,2950173,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Frequency ID',10,'SF','Y','Y','Y','N','N','N','N','N','Frecuency of Visit',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950173 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950144,2950174,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Business Partner Location for invoicing',10,'SF','Y','Y','Y','N','N','N','N','N','Invoice Location',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950174 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950141,2950175,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','Y','Y','Y','N','N','N','N','N','Name',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950175 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950134,2950176,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','Y','Y','Y','N','N','N','N','N','Organization',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950176 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950145,2950177,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner',10,'SF','The Partner address indicates the location of a Business Partner','Y','Y','Y','N','N','N','N','N','Partner Location',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950177 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950153,2950178,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit ID',10,'SF','Y','Y','N','N','N','N','N','N','Planning Visit',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950178 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950146,2950179,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Sales coverage region',10,'SF','The Sales Region indicates a specific area of sales coverage.','Y','Y','Y','N','N','N','N','N','Sales Region',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950179 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950148,2950180,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Method of ordering records; lowest number comes first',14,'SF','The Sequence indicates the order of records','Y','Y','Y','N','N','N','N','N','Sequence',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950180 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950147,2950181,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Element is valid',1,'SF','The element passed the validation check','Y','Y','Y','N','N','N','N','N','Valid',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950181 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950149,2950182,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Valid from including this date (first day)',7,'SF','The Valid From date indicates the first day of a date range','Y','Y','Y','N','N','N','N','N','Valid from',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950182 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950150,2950183,0,2950019,TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100,'Visit Day Date',7,'SF','Y','Y','Y','N','N','N','N','N','Visit Day ',TO_DATE('2013-01-31 08:17:28','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 08:17:28 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950183 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 08:17:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Planificación de Visita',Description='Planificación de Visita',Updated=TO_DATE('2013-01-31 08:17:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019 AND AD_Language='es_MX'
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2950170
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2950176
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950175
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950179
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950171
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950183
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950180
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950173
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950182
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950169
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950177
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950168
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=130,IsDisplayed='Y' WHERE AD_Field_ID=2950181
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=140,IsDisplayed='Y' WHERE AD_Field_ID=2950172
;

-- 31/01/2013 08:20:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=150,IsDisplayed='Y' WHERE AD_Field_ID=2950174
;

-- 31/01/2013 08:21:43 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 08:21:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950176
;

-- 31/01/2013 08:22:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-31 08:22:12','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950175
;

-- 31/01/2013 08:22:58 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 08:22:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950179
;

-- 31/01/2013 08:26:56 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-31 08:26:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950183
;

-- 31/01/2013 08:27:16 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y', IsSameLine='Y',Updated=TO_DATE('2013-01-31 08:27:15','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950180
;

-- 31/01/2013 08:28:24 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 08:28:24','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950182
;

-- 31/01/2013 08:29:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DefaultValue='@C_BPartner_ID@', IsReadOnly='Y',Updated=TO_DATE('2013-01-31 08:29:02','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950169
;

-- 31/01/2013 08:41:26 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DefaultValue=NULL,Updated=TO_DATE('2013-01-31 08:41:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950169
;

-- 31/01/2013 08:42:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET AD_Column_ID=2950145,Updated=TO_DATE('2013-01-31 08:42:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019
;

-- 31/01/2013 08:44:25 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET TabLevel=2,Updated=TO_DATE('2013-01-31 08:44:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2950169
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950177
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950179
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950174
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950175
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950171
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950183
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950173
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950172
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=130,IsDisplayed='Y' WHERE AD_Field_ID=2950168
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=140,IsDisplayed='Y' WHERE AD_Field_ID=2950180
;

-- 31/01/2013 08:54:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=150,IsDisplayed='Y' WHERE AD_Field_ID=2950181
;

-- 31/01/2013 08:54:10 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-31 08:54:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950170
;

-- 31/01/2013 08:54:13 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-31 08:54:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950176
;

-- 31/01/2013 08:56:26 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-31 08:56:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950145
;

-- 31/01/2013 08:57:12 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-31 08:57:12','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950146
;

-- 31/01/2013 08:59:15 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-31 08:59:15','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950141
;

-- 31/01/2013 08:59:31 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-31 08:59:31','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950149
;

-- 31/01/2013 08:59:46 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-31 08:59:46','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950150
;

-- 31/01/2013 09:00:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-31 09:00:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950152
;

-- 31/01/2013 09:02:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 09:02:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950183
;

-- 31/01/2013 09:02:06 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 09:02:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950172
;

-- 31/01/2013 09:03:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 09:03:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950177
;

-- 31/01/2013 09:04:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-31 09:04:02','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950179
;

-- 31/01/2013 09:04:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 09:04:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950174
;

-- 31/01/2013 09:04:18 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-31 09:04:18','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950182
;

-- 31/01/2013 09:07:13 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsReadOnly='Y',Updated=TO_DATE('2013-01-31 09:07:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950179
;

-- 31/01/2013 09:07:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DefaultValue='-1',Updated=TO_DATE('2013-01-31 09:07:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950172
;

-- 31/01/2013 09:10:09 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Column SET IsUpdateable='N',Updated=TO_DATE('2013-01-31 09:10:09','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Column_ID=2950144
;

-- 31/01/2013 09:20:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET AD_Column_ID=2950143, TabLevel=1,Updated=TO_DATE('2013-01-31 09:20:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019
;

-- 31/01/2013 09:24:32 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,Included_Tab_ID,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,PreferredWidth,SeqNo,SortNo,Updated,UpdatedBy) VALUES (0,2959,2950184,0,222,TO_DATE('2013-01-31 09:24:32','YYYY-MM-DD HH24:MI:SS'),100,'Location or Address',0,'SF','The Location / Address field defines the location of an entity.','N',2950019,'Y','Y','Y','N','N','N','N','N','Address',0,170,0,TO_DATE('2013-01-31 09:24:32','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:24:38 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,Included_Tab_ID,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,PreferredWidth,SeqNo,SortNo,Updated,UpdatedBy) VALUES (0,2959,2950185,0,222,TO_DATE('2013-01-31 09:24:38','YYYY-MM-DD HH24:MI:SS'),100,'Location or Address',0,'SF','The Location / Address field defines the location of an entity.','N',2950019,'Y','Y','Y','N','N','N','N','N','Address',0,170,0,TO_DATE('2013-01-31 09:24:38','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:24:59 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,Included_Tab_ID,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,PreferredWidth,SeqNo,SortNo,Updated,UpdatedBy) VALUES (0,2959,2950186,0,222,TO_DATE('2013-01-31 09:24:59','YYYY-MM-DD HH24:MI:SS'),100,'Location or Address',0,'SF','The Location / Address field defines the location of an entity.','N',2950019,'Y','Y','Y','N','N','N','N','N','Address',0,170,0,TO_DATE('2013-01-31 09:24:59','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:26:08 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,Included_Tab_ID,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,PreferredWidth,SeqNo,SortNo,Updated,UpdatedBy) VALUES (0,3434,2950187,0,222,TO_DATE('2013-01-31 09:26:08','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner',0,'SF','The Partner address indicates the location of a Business Partner','N',2950019,'Y','Y','Y','N','N','N','N','N','Partner Location',0,170,0,TO_DATE('2013-01-31 09:26:08','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:29:00 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,Included_Tab_ID,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,PreferredWidth,SeqNo,SortNo,Updated,UpdatedBy) VALUES (0,3434,2950188,0,222,TO_DATE('2013-01-31 09:29:00','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner',0,'SF','The Partner address indicates the location of a Business Partner','N',2950019,'Y','Y','Y','N','N','N','N','N','Partner Location',0,170,0,TO_DATE('2013-01-31 09:29:00','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:30:33 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,Included_Tab_ID,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,PreferredWidth,SeqNo,SortNo,Updated,UpdatedBy) VALUES (0,3434,2950189,0,222,TO_DATE('2013-01-31 09:30:33','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner',0,'SF','The Partner address indicates the location of a Business Partner','N',2950019,'Y','Y','Y','N','N','N','N','N','Partner Location',0,170,0,TO_DATE('2013-01-31 09:30:33','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=10,IsDisplayed='Y' WHERE AD_Field_ID=2181
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=20,IsDisplayed='Y' WHERE AD_Field_ID=2182
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=30,IsDisplayed='Y' WHERE AD_Field_ID=2183
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2190
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2189
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2185
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2191
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2192
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2187
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2188
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2196
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2193
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=130,IsDisplayed='Y' WHERE AD_Field_ID=2194
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=140,IsDisplayed='Y' WHERE AD_Field_ID=2195
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=150,IsDisplayed='Y' WHERE AD_Field_ID=2186
;

-- 31/01/2013 09:33:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=160,IsDisplayed='Y' WHERE AD_Field_ID=2613
;

-- 31/01/2013 09:35:39 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET Included_Tab_ID=2950019,Updated=TO_DATE('2013-01-31 09:35:39','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2613
;

-- 31/01/2013 09:37:27 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET AD_Column_ID=2950145,Updated=TO_DATE('2013-01-31 09:37:27','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019
;

-- 31/01/2013 09:37:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET Description='Planning Visit for Location', Name='Planning Visit for Location',Updated=TO_DATE('2013-01-31 09:37:53','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019
;

-- 31/01/2013 09:37:53 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET IsTranslated='N' WHERE AD_Tab_ID=2950019
;

-- 31/01/2013 09:38:05 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Planificación de Visita para la Localización',Updated=TO_DATE('2013-01-31 09:38:05','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019 AND AD_Language='es_MX'
;

-- 31/01/2013 09:38:06 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Description='Planificación de Visita para la Localización',Updated=TO_DATE('2013-01-31 09:38:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019 AND AD_Language='es_MX'
;

-- 31/01/2013 09:38:30 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_Column_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy,WhereClause) VALUES (0,2950143,0,2950020,2950008,123,TO_DATE('2013-01-31 09:38:30','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit','SF','N','N','Y','N','N','Y','N','Y','N','N','Planning Visit','N',100,1,TO_DATE('2013-01-31 09:38:30','YYYY-MM-DD HH24:MI:SS'),100,NULL)
;

-- 31/01/2013 09:38:30 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950020 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950133,2950190,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Client/Tenant for this installation.',22,'SF','A Client is a company or a legal entity. You cannot share data between Clients. Tenant is a synonym for Client.','N','Y','Y','Y','N','N','N','Y','N','Client',10,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950190 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950134,2950191,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Organizational entity within client',22,'SF','An organization is a unit of your client or legal entity - examples are store, department. You can share data between organizations.','N','Y','Y','Y','N','N','N','Y','Y','Organization',20,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950191 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950143,2950192,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Identifies a Business Partner',10,'SF','A Business Partner is anyone with whom you transact.  This can include Vendor, Customer, Employee or Salesperson','N','Y','Y','Y','N','N','N','Y','N','Business Partner ',30,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950192 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950145,2950193,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Identifies the (ship to) address for this Business Partner',10,'SF','The Partner address indicates the location of a Business Partner','N','Y','Y','Y','N','N','N','N','Y','Partner Location',40,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950193 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950146,2950194,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Sales coverage region',10,'SF','The Sales Region indicates a specific area of sales coverage.','N','Y','Y','Y','N','N','N','Y','N','Sales Region',50,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950194 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950144,2950195,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Business Partner Location for invoicing',10,'SF','N','Y','Y','Y','N','N','N','N','Y','Invoice Location',60,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950195 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950141,2950196,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Alphanumeric identifier of the entity',60,'SF','The name of an entity (record) is used as an default search option in addition to the search key. The name is up to 60 characters in length.','N','Y','Y','Y','N','N','N','Y','N','Name',70,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950196 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950142,2950197,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Optional short description of the record',255,'SF','A description is limited to 255 characters.','N','Y','Y','Y','N','N','N','N','N','Description',80,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950197 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950149,2950198,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Valid from including this date (first day)',7,'SF','The Valid From date indicates the first day of a date range','N','Y','Y','Y','N','N','N','N','N','Valid from',90,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950198 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950150,2950199,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Visit Day Date',7,'SF','N','Y','Y','Y','N','N','N','Y','Y','Visit Day ',100,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950199 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950151,2950200,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Frequency ID',10,'SF','N','Y','Y','Y','N','N','N','N','N','Frecuency of Visit',110,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950200 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,DefaultValue,Description,DisplayLength,EntityType,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950152,2950201,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'-1','Event Type ID',10,'SF','N','Y','Y','Y','N','N','N','N','Y','Event Type',120,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950201 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950137,2950202,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'The record is active in the system',1,'SF','There are two methods of making records unavailable in the system: One is to delete the record, the other is to de-activate the record. A de-activated record is not available for selection, but available for reports.
There are two reasons for de-activating and not deleting records:
(1) The system requires the record for audit purposes.
(2) The record is referenced by other records. E.g., you cannot delete a Business Partner, if there are invoices for this partner record existing. You de-activate the Business Partner and prevent that this record is used for future entries.','N','Y','Y','Y','N','N','N','N','N','Active',130,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950202 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950148,2950203,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Method of ordering records; lowest number comes first',14,'SF','The Sequence indicates the order of records','N','Y','Y','Y','N','N','N','Y','Y','Sequence',140,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950203 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,Help,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,SeqNo,Updated,UpdatedBy) VALUES (0,2950147,2950204,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Element is valid',1,'SF','The element passed the validation check','N','Y','Y','Y','N','N','N','N','N','Valid',150,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950204 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field (AD_Client_ID,AD_Column_ID,AD_Field_ID,AD_Org_ID,AD_Tab_ID,Created,CreatedBy,Description,DisplayLength,EntityType,HideInListView,IsActive,IsCentrallyMaintained,IsDisplayed,IsEncrypted,IsFieldOnly,IsHeading,IsReadOnly,IsSameLine,Name,Updated,UpdatedBy) VALUES (0,2950153,2950205,0,2950020,TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100,'Planning Visit ID',10,'SF','N','Y','Y','N','N','N','N','N','N','Planning Visit',TO_DATE('2013-01-31 09:38:36','YYYY-MM-DD HH24:MI:SS'),100)
;

-- 31/01/2013 09:38:36 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Field_Trl (AD_Language,AD_Field_ID, Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Field_ID, t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Field t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Field_ID=2950205 AND NOT EXISTS (SELECT * FROM AD_Field_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Field_ID=t.AD_Field_ID)
;

-- 31/01/2013 09:38:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET WhereClause=NULL,Updated=TO_DATE('2013-01-31 09:38:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950019
;

-- 31/01/2013 09:39:10 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab_Trl SET Name='Planificación de Visita',Description='Planificación de Visita',Updated=TO_DATE('2013-01-31 09:39:10','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950020 AND AD_Language='es_MX'
;

-- 31/01/2013 09:47:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Tab SET OrderByClause='SeqNo',Updated=TO_DATE('2013-01-31 09:47:59','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Tab_ID=2950008
;

-- 31/01/2013 09:53:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLogic='@Action@! ''R'' & @Action@ !''P''',Updated=TO_DATE('2013-01-31 09:53:29','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950086
;

-- 31/01/2013 09:53:58 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 09:53:58','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950082
;

-- 31/01/2013 10:02:17 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950080
;

-- 31/01/2013 10:02:17 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950085
;

-- 31/01/2013 10:02:17 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=0,IsDisplayed='N' WHERE AD_Field_ID=2950084
;

-- 31/01/2013 10:02:17 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=160,IsDisplayed='Y' WHERE AD_Field_ID=2950076
;

-- 31/01/2013 10:02:17 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=170,IsDisplayed='Y' WHERE AD_Field_ID=2950089
;

-- 31/01/2013 10:02:17 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=180,IsDisplayed='Y' WHERE AD_Field_ID=2950069
;

-- 31/01/2013 10:04:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950078
;

-- 31/01/2013 10:04:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950070
;

-- 31/01/2013 10:04:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950074
;

-- 31/01/2013 10:04:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950088
;

-- 31/01/2013 10:04:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950073
;

-- 31/01/2013 10:04:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=130,IsDisplayed='Y' WHERE AD_Field_ID=2950087
;

-- 31/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=150,IsDisplayed='Y' WHERE AD_Field_ID=2950076
;

-- 31/01/2013 10:04:15 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=160,IsDisplayed='Y' WHERE AD_Field_ID=2950075
;

-- 31/01/2013 10:04:44 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:04:44','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950070
;

-- 31/01/2013 10:05:36 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:05:36','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950076
;

-- 31/01/2013 10:06:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950075
;

-- 31/01/2013 10:06:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950088
;

-- 31/01/2013 10:06:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=130,IsDisplayed='Y' WHERE AD_Field_ID=2950073
;

-- 31/01/2013 10:06:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=140,IsDisplayed='Y' WHERE AD_Field_ID=2950087
;

-- 31/01/2013 10:06:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=150,IsDisplayed='Y' WHERE AD_Field_ID=2950083
;

-- 31/01/2013 10:06:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=160,IsDisplayed='Y' WHERE AD_Field_ID=2950076
;

-- 31/01/2013 10:06:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:06:19','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950075
;

-- 31/01/2013 10:06:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:06:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950069
;

-- 31/01/2013 10:07:43 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=10,Updated=TO_DATE('2013-01-31 10:07:43','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950075
;

-- 31/01/2013 10:07:47 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=10,Updated=TO_DATE('2013-01-31 10:07:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950074
;

-- 31/01/2013 10:09:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950058
;

-- 31/01/2013 10:09:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950060
;

-- 31/01/2013 10:09:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950065
;

-- 31/01/2013 10:09:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950067
;

-- 31/01/2013 10:09:19 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950066
;

-- 31/01/2013 10:09:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950058
;

-- 31/01/2013 10:09:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950064
;

-- 31/01/2013 10:10:08 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=10,Updated=TO_DATE('2013-01-31 10:10:08','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950062
;

-- 31/01/2013 10:10:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-31 10:10:22','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950064
;

-- 31/01/2013 10:10:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950067
;

-- 31/01/2013 10:10:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950064
;

-- 31/01/2013 10:10:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950060
;

-- 31/01/2013 10:10:40 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950065
;

-- 31/01/2013 10:10:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-31 10:10:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950067
;

-- 31/01/2013 10:11:02 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:11:02','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950064
;

-- 31/01/2013 10:13:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=10,Updated=TO_DATE('2013-01-31 10:13:01','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950147
;

-- 31/01/2013 10:13:16 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-31 10:13:16','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950144
;

-- 31/01/2013 10:13:20 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950149
;

-- 31/01/2013 10:13:20 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950144
;

-- 31/01/2013 10:13:20 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950146
;

-- 31/01/2013 10:13:28 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:13:28','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950149
;

-- 31/01/2013 10:13:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950146
;

-- 31/01/2013 10:13:38 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950144
;

-- 31/01/2013 10:14:04 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950144
;

-- 31/01/2013 10:14:04 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950147
;

-- 31/01/2013 10:14:04 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950149
;

-- 31/01/2013 10:14:04 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950146
;

-- 31/01/2013 10:14:11 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=10,Updated=TO_DATE('2013-01-31 10:14:11','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950150
;

-- 31/01/2013 10:14:13 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:14:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950144
;

-- 31/01/2013 10:15:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=10,Updated=TO_DATE('2013-01-31 10:15:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950157
;

-- 31/01/2013 10:17:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950024
;

-- 31/01/2013 10:17:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950025
;

-- 31/01/2013 10:17:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950022
;

-- 31/01/2013 10:17:59 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950027
;

-- 31/01/2013 10:18:08 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950030
;

-- 31/01/2013 10:18:08 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950031
;

-- 31/01/2013 10:18:09 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950027
;

-- 31/01/2013 10:18:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950022
;

-- 31/01/2013 10:18:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950024
;

-- 31/01/2013 10:18:22 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950025
;

-- 31/01/2013 10:19:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950036
;

-- 31/01/2013 10:19:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950039
;

-- 31/01/2013 10:19:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950035
;

-- 31/01/2013 10:19:23 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950038
;

-- 31/01/2013 10:19:33 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:19:33','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950033
;

-- 31/01/2013 10:21:07 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=10,Updated=TO_DATE('2013-01-31 10:21:07','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950108
;

-- 31/01/2013 10:21:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:21:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950105
;

-- 31/01/2013 10:22:48 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET DisplayLength=10,Updated=TO_DATE('2013-01-31 10:22:48','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950056
;

-- 31/01/2013 10:24:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950001
;

-- 31/01/2013 10:24:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950011
;

-- 31/01/2013 10:24:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950004
;

-- 31/01/2013 10:24:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950005
;

-- 31/01/2013 10:24:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950007
;

-- 31/01/2013 10:24:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950012
;

-- 31/01/2013 10:24:42 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950000
;

-- 31/01/2013 10:24:50 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:24:50','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950001
;

-- 31/01/2013 10:24:52 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='N',Updated=TO_DATE('2013-01-31 10:24:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950011
;

-- 31/01/2013 10:25:15 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950012
;

-- 31/01/2013 10:25:15 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950004
;

-- 31/01/2013 10:25:15 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950005
;

-- 31/01/2013 10:25:15 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950007
;

-- 31/01/2013 10:25:29 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:25:29','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950012
;

-- 31/01/2013 10:26:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950000
;

-- 31/01/2013 10:26:01 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950012
;

-- 31/01/2013 10:26:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=90,IsDisplayed='Y' WHERE AD_Field_ID=2950012
;

-- 31/01/2013 10:26:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=100,IsDisplayed='Y' WHERE AD_Field_ID=2950004
;

-- 31/01/2013 10:26:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=110,IsDisplayed='Y' WHERE AD_Field_ID=2950005
;

-- 31/01/2013 10:26:30 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=120,IsDisplayed='Y' WHERE AD_Field_ID=2950007
;

-- 31/01/2013 10:27:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=40,IsDisplayed='Y' WHERE AD_Field_ID=2950017
;

-- 31/01/2013 10:27:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950013
;

-- 31/01/2013 10:27:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950020
;

-- 31/01/2013 10:27:14 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950021
;

-- 31/01/2013 10:27:28 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=50,IsDisplayed='Y' WHERE AD_Field_ID=2950020
;

-- 31/01/2013 10:27:28 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=60,IsDisplayed='Y' WHERE AD_Field_ID=2950021
;

-- 31/01/2013 10:27:28 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=70,IsDisplayed='Y' WHERE AD_Field_ID=2950019
;

-- 31/01/2013 10:27:28 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET SeqNo=80,IsDisplayed='Y' WHERE AD_Field_ID=2950013
;

-- 31/01/2013 10:27:45 AM VET
-- Sales Force Mobile with ADempiere
UPDATE AD_Field SET IsSameLine='Y',Updated=TO_DATE('2013-01-31 10:27:45','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=2950021
;

-- 31/01/2013 10:53:11 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab (AD_Client_ID,AD_ColumnSortOrder_ID,AD_ColumnSortYesNo_ID,AD_Org_ID,AD_Tab_ID,AD_Table_ID,AD_Window_ID,Created,CreatedBy,Description,EntityType,HasTree,ImportFields,IsActive,IsAdvancedTab,IsInfoTab,IsInsertRecord,IsReadOnly,IsSingleRow,IsSortTab,IsTranslationTab,Name,Processing,SeqNo,TabLevel,Updated,UpdatedBy,WhereClause) VALUES (0,2950273,2950237,0,2950021,2950015,2950006,TO_DATE('2013-01-31 10:53:11','YYYY-MM-DD HH24:MI:SS'),100,'Sequence','SF','N','N','Y','N','N','Y','N','N','Y','N','Sequence','N',30,0,TO_DATE('2013-01-31 10:53:11','YYYY-MM-DD HH24:MI:SS'),100,'MenuType =''M''')
;

-- 31/01/2013 10:53:11 AM VET
-- Sales Force Mobile with ADempiere
INSERT INTO AD_Tab_Trl (AD_Language,AD_Tab_ID, CommitWarning,Description,Help,Name, IsTranslated,AD_Client_ID,AD_Org_ID,Created,Createdby,Updated,UpdatedBy) SELECT l.AD_Language,t.AD_Tab_ID, t.CommitWarning,t.Description,t.Help,t.Name, 'N',t.AD_Client_ID,t.AD_Org_ID,t.Created,t.Createdby,t.Updated,t.UpdatedBy FROM AD_Language l, AD_Tab t WHERE l.IsActive='Y' AND l.IsSystemLanguage='Y' AND l.IsBaseLanguage='N' AND t.AD_Tab_ID=2950021 AND NOT EXISTS (SELECT * FROM AD_Tab_Trl tt WHERE tt.AD_Language=l.AD_Language AND tt.AD_Tab_ID=t.AD_Tab_ID)
;

