package org.ag.process;

import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.sg.model.MXXLoadOrder;

public class DriverLoarOrder extends SvrProcess{

	/**	Load Order ID		*/
	private int 			p_XX_LoadOrder_ID = 0;
	/**	Load M_Shipper_ID		*/
	private int 			p_M_Shipper_ID = 0;
	//**XX_Conductor_ID    */
	private int 			p_XX_Conductor_ID = 0;
	//**XX_Vehiculo_ID   */
	private int 			p_XX_Vehiculo_ID = 0;
	/**	Description			*/
	private String 			p_DriverDescription = null;
	/**	Description			*/
	private String 			p_VehicleDescription = null;
	/**	Message				*/
	private String 			message;
	private String 			trxName = null;
	private Trx 			trx = null;
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		//Para tomar los parametros del Proceso
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("XX_LoadOrder_ID"))
				p_XX_LoadOrder_ID = para.getParameterAsInt();
			else if (name.equals("M_Shipper_ID"))
				p_M_Shipper_ID = para.getParameterAsInt();
			else if (name.equals("XX_Conductor_ID"))
				p_XX_Conductor_ID = para.getParameterAsInt();
			else if (name.equals("XX_Vehiculo_ID"))
				p_XX_Vehiculo_ID = para.getParameterAsInt();
			else if (name.equals("DriverDescription"))
				p_DriverDescription = (String) para.getParameter();
			else if (name.equals("VehicleDescription"))
				p_VehicleDescription = (String) para.getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		trxName = Trx.createTrxName("DR");
		trx = Trx.get(trxName, true);
		
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		//Para correr el Proceso
		if(p_XX_LoadOrder_ID != 0){
			if(p_M_Shipper_ID != 0){
				if(p_XX_Conductor_ID != 0){
					if(p_XX_Vehiculo_ID != 0){
						try {
							// Instancia de la Clase MXXLoadOrder y le paso de parametro la variable con la OCD p_XX_LoadOrder_ID
							//trxName nombre de la transaccion.
							MXXLoadOrder m_XX_LoadOrder = new MXXLoadOrder(getCtx(), p_XX_LoadOrder_ID, trxName);
							
							//Asigno Valores
							m_XX_LoadOrder.setM_Shipper_ID(p_M_Shipper_ID);
							m_XX_LoadOrder.setXX_Conductor_ID(p_XX_Conductor_ID);
							m_XX_LoadOrder.setXX_Vehiculo_ID(p_XX_Vehiculo_ID);
						
							if(m_XX_LoadOrder.getDriverDescription() != null)
								m_XX_LoadOrder.setDriverDescription(m_XX_LoadOrder.getDriverDescription() + " [" + p_DriverDescription + "]");
							else
								m_XX_LoadOrder.setDriverDescription("[" + p_DriverDescription + "]");
							
							if(m_XX_LoadOrder.getVehicleDescription() != null)
								m_XX_LoadOrder.setDriverDescription(m_XX_LoadOrder.getVehicleDescription() + " [" + p_VehicleDescription + "]");
							else
								m_XX_LoadOrder.setVehicleDescription("[" + p_VehicleDescription + "]");
							
							
							m_XX_LoadOrder.saveEx();
							trx.commit();
							message = "OK";
						} catch (Exception e) {
							message = Msg.translate(getCtx(), "SaveError") + ":" + e.getMessage();
							log.log(Level.SEVERE, message);
							trx.rollback();
						}//try
					} else {
						message = Msg.translate(getCtx(), "LSShipperCero"); //es el Value de los Message
					}
				}else {
					message = Msg.translate(getCtx(), "LSConductorCero");
				}
			}else {
				message = Msg.translate(getCtx(), "LSVehiculoCero");
			}
		}
		
		
		return message;
	}
				
	}


