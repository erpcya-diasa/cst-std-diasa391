package org.ag.model;


import java.sql.Timestamp;

import java.util.Calendar;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;


public class CalloutDependents extends CalloutEngine{
	
	
	public String dependestsAge (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive() || value == null)
			return "";
		
		Timestamp m_Birth_Date = (Timestamp)value;
		
		int dependentsAge = calcularEdad(m_Birth_Date);
		System.out.println("CalloutDependents.dependestsAge()"+ dependentsAge);
	    mTab.setValue("xx_kinship_age", dependentsAge);
	    return "";
	}

	public int calcularEdad(Timestamp fecha){
            Calendar fechaNac=Calendar.getInstance();
            fechaNac.setTime(fecha);
            System.out.println("CalloutDependents.calcularEdad()" + fechaNac);
            Calendar fechaActual = Calendar.getInstance();
            int diferAno = fechaActual.get(Calendar.YEAR)-fechaNac.get(Calendar.YEAR);
            int diferMes = fechaActual.get(Calendar.MONTH)-fechaNac.get(Calendar.MONTH);
            int diferDia = fechaActual.get(Calendar.DAY_OF_MONTH)-fechaNac.get(Calendar.DAY_OF_MONTH);
            if (diferMes<0||(diferMes==0&&diferDia<0)){
                diferAno -= 1;
            }
            return diferAno;
    }

}
