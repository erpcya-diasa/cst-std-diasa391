/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.ag.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_DriverEvaluation
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_DriverEvaluation extends PO implements I_XX_DriverEvaluation, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120612L;

    /** Standard Constructor */
    public X_XX_DriverEvaluation (Properties ctx, int XX_DriverEvaluation_ID, String trxName)
    {
      super (ctx, XX_DriverEvaluation_ID, trxName);
      /** if (XX_DriverEvaluation_ID == 0)
        {
			setDocumentNo (null);
			setM_Shipper_ID (0);
			setNombre (null);
			setXX_Conductor_ID (0);
			setXX_DriverEvaluation_ID (0);
			setXX_DriverEvaluationType (null);
			setXX_Vehiculo_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_DriverEvaluation (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_DriverEvaluation[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_Value (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	public I_M_Shipper getM_Shipper() throws RuntimeException
    {
		return (I_M_Shipper)MTable.get(getCtx(), I_M_Shipper.Table_Name)
			.getPO(getM_Shipper_ID(), get_TrxName());	}

	/** Set Shipper.
		@param M_Shipper_ID 
		Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID)
	{
		if (M_Shipper_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Shipper_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Shipper_ID, Integer.valueOf(M_Shipper_ID));
	}

	/** Get Shipper.
		@return Method or manner of product delivery
	  */
	public int getM_Shipper_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Shipper_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Nombre.
		@param Nombre Nombre	  */
	public void setNombre (String Nombre)
	{
		set_Value (COLUMNNAME_Nombre, Nombre);
	}

	/** Get Nombre.
		@return Nombre	  */
	public String getNombre () 
	{
		return (String)get_Value(COLUMNNAME_Nombre);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Conductor.
		@param XX_Conductor_ID Conductor	  */
	public void setXX_Conductor_ID (int XX_Conductor_ID)
	{
		if (XX_Conductor_ID < 1) 
			set_Value (COLUMNNAME_XX_Conductor_ID, null);
		else 
			set_Value (COLUMNNAME_XX_Conductor_ID, Integer.valueOf(XX_Conductor_ID));
	}

	/** Get Conductor.
		@return Conductor	  */
	public int getXX_Conductor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Conductor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Driver Evaluation.
		@param XX_DriverEvaluation_ID Driver Evaluation	  */
	public void setXX_DriverEvaluation_ID (int XX_DriverEvaluation_ID)
	{
		if (XX_DriverEvaluation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_DriverEvaluation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_DriverEvaluation_ID, Integer.valueOf(XX_DriverEvaluation_ID));
	}

	/** Get Driver Evaluation.
		@return Driver Evaluation	  */
	public int getXX_DriverEvaluation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_DriverEvaluation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** XX_DriverEvaluationType AD_Reference_ID=1000062 */
	public static final int XX_DRIVEREVALUATIONTYPE_AD_Reference_ID=1000062;
	/** Consumo de Combustible = G */
	public static final String XX_DRIVEREVALUATIONTYPE_ConsumoDeCombustible = "G";
	/** Mantenimiento del Vehículo = V */
	public static final String XX_DRIVEREVALUATIONTYPE_MantenimientoDelVehículo = "V";
	/** Colisiones = C */
	public static final String XX_DRIVEREVALUATIONTYPE_Colisiones = "C";
	/** Set XX_DriverEvaluationType.
		@param XX_DriverEvaluationType XX_DriverEvaluationType	  */
	public void setXX_DriverEvaluationType (String XX_DriverEvaluationType)
	{

		set_Value (COLUMNNAME_XX_DriverEvaluationType, XX_DriverEvaluationType);
	}

	/** Get XX_DriverEvaluationType.
		@return XX_DriverEvaluationType	  */
	public String getXX_DriverEvaluationType () 
	{
		return (String)get_Value(COLUMNNAME_XX_DriverEvaluationType);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getXX_DriverEvaluationType()));
    }

	/** Set Car.
		@param XX_Vehiculo_ID Car	  */
	public void setXX_Vehiculo_ID (int XX_Vehiculo_ID)
	{
		if (XX_Vehiculo_ID < 1) 
			set_Value (COLUMNNAME_XX_Vehiculo_ID, null);
		else 
			set_Value (COLUMNNAME_XX_Vehiculo_ID, Integer.valueOf(XX_Vehiculo_ID));
	}

	/** Get Car.
		@return Car	  */
	public int getXX_Vehiculo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Vehiculo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}