package org.ag.model;

import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInOut;
import org.compiere.model.MUOMConversion;
import org.compiere.util.Env;
import org.sg.model.MXXVehiculo;

public class CalloutDriverEvaluations extends CalloutEngine{
	
	public String mileageTravel (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		
		/**
		 *Miguel Rondon - 11-Junio-2012
		 *Calculo de Recorrido Total Kilometraje Inicial - Kilometraje Final
		
		*/
		
		if (isCalloutActive() || value == null)
			return "";
		
		//Obtengo ID del Driver Evaluation
		//Integer m_XX_DriverEvaluation_ID = (Integer)mTab.getValue("XX_DriverEvaluation_ID");
		Integer m_XX_Vehiculo_ID = Env.getContextAsInt(ctx, WindowNo, 0, "XX_Vehiculo_ID");
		//System.out.println("Resultado resta: " + m_XX_Conductor_ID); 
		
		//m_XX_Vehiculo Variable del Tipo Objeto que estoy instanciando
		
		if (m_XX_Vehiculo_ID == null || m_XX_Vehiculo_ID.intValue() == 0)
			return "";
		MXXVehiculo m_XX_Vehiculo = new MXXVehiculo(ctx, m_XX_Vehiculo_ID.intValue(), null);
		
		//C_UOM_ID Variable que viene de la tabla Vehiculo
		//m_XX_Vehiculo es la variable instanciada
		//getXX_DistUOM_ID() get seguido de nombre de variable de la tabla.. lo que indique que de Vehiculo me trae el ID de UOM de Distancia
		//mTab.setValue establece valor en ventana
		//Unidade de Distancia
		mTab.setValue("C_UOM_ID", m_XX_Vehiculo.getXX_DistUOM_ID());
		//Unidad de Volumen
		mTab.setValue("XX_G_UOM_ID", m_XX_Vehiculo.getXX_VolUOM_ID());
		
		BigDecimal m_XX_MileageIni = (BigDecimal)mTab.getValue("XX_MileageIni");
		
		BigDecimal m_XX_MileageFin = (BigDecimal)mTab.getValue("XX_MileageFin");
		
		BigDecimal diferMileage; 
		
		
		diferMileage = m_XX_MileageFin.subtract(m_XX_MileageIni);
		//System.out.println("Resultado resta: " + diferMileage); 
		 mTab.setValue("XX_MileageTravel", diferMileage);
	    return "";
	}
	
	
	public String fuelTotal (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive() || value == null)
			return "";
		
		BigDecimal m_XX_QtyFuel = (BigDecimal)mTab.getValue("QtyFuel");
		
		BigDecimal m_PriceFuel = (BigDecimal)mTab.getValue("PriceFuel");
		
		BigDecimal totalInvoice; 
		
		totalInvoice = m_XX_QtyFuel.multiply(m_PriceFuel);
       
		mTab.setValue("TotalFuel", totalInvoice);
		
		
		
		
		return "";
	}
	
	public String mileageGallons (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive() || value == null)
			return "";
		//Galones Entre Kilometraje Recorrido
		
		
		BigDecimal m_MileageTravel = (BigDecimal)mTab.getValue("XX_MileageTravel");
		
		BigDecimal m_XX_QtyFuel = (BigDecimal)mTab.getValue("QtyFuel");
		//Se inicializa variable
		BigDecimal gallonsMileage = Env.ZERO; 
		
		if (m_XX_QtyFuel != null && m_XX_QtyFuel.signum() > 0){
			gallonsMileage = m_MileageTravel.divide(m_XX_QtyFuel,BigDecimal.ROUND_HALF_DOWN);
			mTab.setValue("XX_MileageGallon", gallonsMileage.setScale(2,BigDecimal.ROUND_HALF_DOWN)); 
		}
		//**Evaluación de Consumo**
		//Obtengo del Contexto ID del Vehiculo
	   Integer m_XX_Vehiculo_ID = Env.getContextAsInt(ctx, WindowNo, 0, "XX_Vehiculo_ID");
	   if (m_XX_Vehiculo_ID == null || m_XX_Vehiculo_ID.intValue() == 0)
			return "";
	   //Instancio Clase Vehiculo
	   MXXVehiculo m_XX_Vehiculo = new MXXVehiculo(ctx, m_XX_Vehiculo_ID.intValue(), null);
	   
	   //System.out.println("Eval Ini: " + m_XX_Vehiculo.getPerformanceFuelI()); 
	   //System.out.println("Eval Fin: " + m_XX_Vehiculo.getPerformanceFuelF()); 
	
	   
	   BigDecimal m_PerformanceFuelI = m_XX_Vehiculo.getPerformanceFuelI();
	   
	   BigDecimal m_PerformanceFuelF = m_XX_Vehiculo.getPerformanceFuelF();
	   
	   if (gallonsMileage.compareTo(m_PerformanceFuelI) >= 0 && gallonsMileage.compareTo(m_PerformanceFuelF) <= 0 ){
		   mTab.setValue("isoptimal", true);
		   //System.out.println("es optimo"); 
		}else{
			mTab.setValue("isoptimal", false);	
		}
		
		return "";
	}
	
	
	public String mileageLitres (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive() || value == null)
			return "";
		
		BigDecimal m_MileageGallon = (BigDecimal)mTab.getValue("XX_MileageGallon");
		
		if (m_MileageGallon == null || m_MileageGallon.signum() <= 0){
			return "";
		}
		
			
		Integer m_XX_G_UOM_ID = (Integer)mTab.getValue("XX_G_UOM_ID");
		
		Integer m_XX_L_UOM_ID = (Integer)mTab.getValue("XX_L_UOM_ID");
		
		
		if (m_XX_L_UOM_ID != null && m_XX_L_UOM_ID.intValue() != 0
				&& m_XX_G_UOM_ID != null && m_XX_G_UOM_ID.intValue() != 0) {
			//BigDecimal rate = MUOMConversion.getRate(m_XX_G_UOM_ID, m_XX_L_UOM_ID);
			
			BigDecimal rate = MUOMConversion.getRate(m_XX_L_UOM_ID, m_XX_G_UOM_ID);
			
			System.out.println("Rate: " + rate); 
			
			BigDecimal mileageLitre; 
			
			mileageLitre = m_MileageGallon.multiply(rate);
			
			System.out.println("Litros: " + mileageLitre); 
			
			mTab.setValue("XX_MileageLitres", mileageLitre);
			
		}
		 
	    return "";
	}
	
	
	
}
