/**
 * 
 */
package org.ag.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Miguel Rondon 12/06/2012
 *
 */
public class MXXDriverEvaluation extends X_XX_DriverEvaluation{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7194108480610070579L;
	
	public MXXDriverEvaluation(Properties ctx, int XX_DriverEvaluation_ID, String trxName) {
		super(ctx, XX_DriverEvaluation_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MXXDriverEvaluation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}	
}
