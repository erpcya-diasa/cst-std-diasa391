/*************************************************************************************
 * Product: SFAndroid (Sales Force Mobile)                       		             *
 * This program is free software; you can redistribute it and/or modify it    		 *
 * under the terms version 2 of the GNU General Public License as published   		 *
 * by the Free Software Foundation. This program is distributed in the hope   		 *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 		 *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           		 *
 * See the GNU General Public License for more details.                       		 *
 * You should have received a copy of the GNU General Public License along    		 *
 * with this program; if not, write to the Free Software Foundation, Inc.,    		 *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     		 *
 * For the text or an alternative of this public license, you may reach us    		 *
 * Copyright (C) 2012-2012 E.R.P. Consultores y Asociados, S.A. All Rights Reserved. *
 * Contributor(s): Yamel Senih www.erpconsultoresyasociados.com				  		 *
 *************************************************************************************/
package org.mb.process;

import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MColumn;
import org.compiere.model.MTable;
import org.compiere.model.MWebServiceType;
import org.compiere.model.X_WS_WebServiceFieldInput;
import org.compiere.model.X_WS_WebServiceFieldOutput;
import org.compiere.model.X_WS_WebService_Para;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Trx;

public class GenerateWebService extends SvrProcess {
	private final String CONSTANTVALUE_Read = "Read";
	private final String CONSTANTVALUE_Create = "Create";
	private final String CONSTANTVALUE_Update = "Update";
	private final String CONSTANTVALUE_Delete = "Delete";
	/**	Table Name as Parameter	*/
	private final String PARAMETERNAME_TableName= "TableName";
	private final String PARAMETERNAME_RecordID = "RecordID";
	private final String PARAMETERNAME_Action = "Action";
	/** Upload = U */
	private final String SYNCHRONIZETYPE_Upload = "U";
	/** Download = D */
	private final String SYNCHRONIZETYPE_Download = "D";
	/** Alter = A */
	private final String SYNCHRONIZETYPE_Alter = "A";
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			if (para[i].getParameterName().equals("Value"))
				m_Value = (para[i].getParameter()!=null?para[i].getParameter().toString():"");
			else if (para[i].getParameterName().equals("Name"))
				m_Name = (para[i].getParameter()!=null?para[i].getParameter().toString():"");
			else if (para[i].getParameterName().equals("WS_WebService_ID"))
				m_WebService = (para[i].getParameter()!=null?Integer.parseInt(para[i].getParameter().toString()):0);
			else if (para[i].getParameterName().equals("WS_WebServiceMethod_ID"))
				m_WebServiceMethod = (para[i].getParameter()!=null?Integer.parseInt(para[i].getParameter().toString()):0);
			else if (para[i].getParameterName().equals("AD_Table_ID"))
				m_Table = (para[i].getParameter()!=null?Integer.parseInt(para[i].getParameter().toString()):0);
			else if (para[i].getParameterName().equals("XX_MB_Table_ID"))
				m_TableMobile = (para[i].getParameter()!=null?Integer.parseInt(para[i].getParameter().toString()):0);
			else if (para[i].getParameterName().equals("SynchronizeType"))
				m_SynchronizedType= (para[i].getParameter()!=null?para[i].getParameter().toString():"");
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + para[i].getParameterName());
		}
		
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		ctx = getCtx();
		trxName= Trx.createTrxName();
		trx = Trx.get(trxName, true);
		
		return GenerateWebServiceType();
	}
	@SuppressWarnings("static-access")
	private String GenerateWebServiceType()
	{
		
		String nameTable="";
		String returnMsg = "@Created@ = ";
		try
		{
			if (m_WebService!=0 && m_WebServiceMethod!=0 && m_Table!=0)
			{
				returnMsg +=m_Name;
				//Record WS_WebServiceType
				MWebServiceType wst = new MWebServiceType(ctx, 0, trxName);
				wst.setValue(m_Value);
				wst.setName(m_Name);
				wst.setWS_WebService_ID(m_WebService);
				wst.setWS_WebServiceMethod_ID(m_WebServiceMethod);
				wst.setDescription(m_Name);
				wst.setAD_Table_ID(m_Table);
				wst.set_ValueOfColumn("IsSynchronized", true);
				wst.set_ValueOfColumn("SynchronizeType", m_SynchronizedType);
				wst.setIsActive(true);
				wst.set_ValueOfColumn("XX_MB_Table_ID", m_TableMobile);
				wst.saveEx(trxName);
			
				//look Table
				MTable table = new MTable(ctx, m_Table, trxName);
				nameTable = table.getTableName();
								
				//Record WS_WebService_Para (TableName)
				X_WS_WebService_Para wsp = new X_WS_WebService_Para(ctx, 0, trxName);
				wsp.setWS_WebServiceType_ID(wst.getWS_WebServiceType_ID());
				wsp.setParameterName(PARAMETERNAME_TableName);
				wsp.setParameterType(wsp.PARAMETERTYPE_Constant);
				wsp.setConstantValue(nameTable);
				wsp.saveEx(trxName);
				
				//Record WS_WebService_Para (RecordID)
				wsp = new X_WS_WebService_Para(ctx, 0, trxName);
				wsp.setWS_WebServiceType_ID(wst.getWS_WebServiceType_ID());
				wsp.setParameterName(PARAMETERNAME_RecordID);
				wsp.setParameterType(wsp.PARAMETERTYPE_Free);
				wsp.setConstantValue(null);
				wsp.saveEx(trxName);
				
				//Record WS_WebService_Para (Action)
				wsp = new X_WS_WebService_Para(ctx, 0, trxName);
				wsp.setWS_WebServiceType_ID(wst.getWS_WebServiceType_ID());
				wsp.setParameterName(PARAMETERNAME_Action);
				wsp.setParameterType(wsp.PARAMETERTYPE_Constant);
				wsp.setConstantValue((wst.get_ValueAsString("SynchronizeType").equals(SYNCHRONIZETYPE_Upload)? CONSTANTVALUE_Create: CONSTANTVALUE_Read));
				wsp.saveEx(trxName);
				
				//Upload Create record in WS_WebServiceField_Input
				if (wst.get_ValueAsString("SynchronizeType").equals(SYNCHRONIZETYPE_Upload))
				{
					MColumn[] columns = table.getColumns(false);
					X_WS_WebServiceFieldInput wsfi ;
					for (int i=0;i<columns.length;i++)
					{
						wsfi= new X_WS_WebServiceFieldInput(ctx, 0, trxName);
						wsfi.setWS_WebServiceType_ID(wst.getWS_WebServiceType_ID());
						wsfi.setAD_Column_ID(columns[i].getAD_Column_ID());
						wsfi.saveEx(trxName);
					}
					
				}
				//Upload Create record in WS_WebServiceField_Input
				else if (!wst.get_ValueAsString("SynchronizeType").equals(SYNCHRONIZETYPE_Upload))
				{
					MColumn[] columns = table.getColumns(false);
					X_WS_WebServiceFieldOutput wsfo ;
					for (int i=0;i<columns.length;i++)
					{
						wsfo= new X_WS_WebServiceFieldOutput(ctx, 0, trxName);
						wsfo.setWS_WebServiceType_ID(wst.getWS_WebServiceType_ID());
						wsfo.setAD_Column_ID(columns[i].getAD_Column_ID());
						wsfo.saveEx(trxName);
					}
				}

			}
			trx.commit(true);
		}
		catch(AdempiereException e)
		{
			trx.rollback();
			returnMsg = e.getMessage();
			log.log(Level.SEVERE, e.getMessage());
		}
		catch(SQLException e)
		{
			trx.rollback();
			returnMsg = e.getMessage();
			log.log(Level.SEVERE, e.getMessage());
		}
		
		return returnMsg;
	}
	private Trx trx;
	private String trxName;
	private String m_Value;
	private String m_Name;
	private int m_WebService;
	private int m_WebServiceMethod;
	private int m_Table;
	private int m_TableMobile;
	private String m_SynchronizedType;
	private Properties ctx; 
}
