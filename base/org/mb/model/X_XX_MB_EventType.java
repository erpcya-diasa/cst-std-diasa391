/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.mb.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_MB_EventType
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_MB_EventType extends PO implements I_XX_MB_EventType, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20121123L;

    /** Standard Constructor */
    public X_XX_MB_EventType (Properties ctx, int XX_MB_EventType_ID, String trxName)
    {
      super (ctx, XX_MB_EventType_ID, trxName);
      /** if (XX_MB_EventType_ID == 0)
        {
			setName (null);
			setXX_MB_EventType_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_MB_EventType (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_MB_EventType[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Default.
		@param IsDefault 
		Default value
	  */
	public void setIsDefault (boolean IsDefault)
	{
		set_Value (COLUMNNAME_IsDefault, Boolean.valueOf(IsDefault));
	}

	/** Get Default.
		@return Default value
	  */
	public boolean isDefault () 
	{
		Object oo = get_Value(COLUMNNAME_IsDefault);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set % Effectivity.
		@param PercentEffectivity % Effectivity	  */
	public void setPercentEffectivity (BigDecimal PercentEffectivity)
	{
		set_Value (COLUMNNAME_PercentEffectivity, PercentEffectivity);
	}

	/** Get % Effectivity.
		@return % Effectivity	  */
	public BigDecimal getPercentEffectivity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PercentEffectivity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** XX_BaseEventType AD_Reference_ID=1000078 */
	public static final int XX_BASEEVENTTYPE_AD_Reference_ID=1000078;
	/** Making Inventory = MII */
	public static final String XX_BASEEVENTTYPE_MakingInventory = "MII";
	/** Sales Order = SOO */
	public static final String XX_BASEEVENTTYPE_SalesOrder = "SOO";
	/** AR Receipt = ARR */
	public static final String XX_BASEEVENTTYPE_ARReceipt = "ARR";
	/** No Sales Order = NSO */
	public static final String XX_BASEEVENTTYPE_NoSalesOrder = "NSO";
	/** Return Material = RMS */
	public static final String XX_BASEEVENTTYPE_ReturnMaterial = "RMS";
	/** No Visit Customer = NVC */
	public static final String XX_BASEEVENTTYPE_NoVisitCustomer = "NVC";
	/** No Receipt Customer = NRC */
	public static final String XX_BASEEVENTTYPE_NoReceiptCustomer = "NRC";
	/** Direct Deposit = RDD */
	public static final String XX_BASEEVENTTYPE_DirectDeposit = "RDD";
	/** Set Base Event Type.
		@param XX_BaseEventType Base Event Type	  */
	public void setXX_BaseEventType (String XX_BaseEventType)
	{

		set_Value (COLUMNNAME_XX_BaseEventType, XX_BaseEventType);
	}

	/** Get Base Event Type.
		@return Base Event Type	  */
	public String getXX_BaseEventType () 
	{
		return (String)get_Value(COLUMNNAME_XX_BaseEventType);
	}

	/** Set Envent Type.
		@param XX_MB_EventType_ID Envent Type	  */
	public void setXX_MB_EventType_ID (int XX_MB_EventType_ID)
	{
		if (XX_MB_EventType_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_MB_EventType_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_MB_EventType_ID, Integer.valueOf(XX_MB_EventType_ID));
	}

	/** Get Envent Type.
		@return Envent Type	  */
	public int getXX_MB_EventType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_EventType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}