/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.mb.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_MB_CustomerInventoryLine
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_MB_CustomerInventoryLine extends PO implements I_XX_MB_CustomerInventoryLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120722L;

    /** Standard Constructor */
    public X_XX_MB_CustomerInventoryLine (Properties ctx, int XX_MB_CustomerInventoryLine_ID, String trxName)
    {
      super (ctx, XX_MB_CustomerInventoryLine_ID, trxName);
      /** if (XX_MB_CustomerInventoryLine_ID == 0)
        {
			setLine (0);
			setM_Product_ID (0);
			setXX_MB_CustomerInventory_ID (0);
			setXX_MB_CustomerInventoryLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_MB_CustomerInventoryLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_MB_CustomerInventoryLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_OrderLine getC_OrderLine() throws RuntimeException
    {
		return (I_C_OrderLine)MTable.get(getCtx(), I_C_OrderLine.Table_Name)
			.getPO(getC_OrderLine_ID(), get_TrxName());	}

	/** Set Sales Order Line.
		@param C_OrderLine_ID 
		Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID)
	{
		if (C_OrderLine_ID < 1) 
			set_Value (COLUMNNAME_C_OrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_C_OrderLine_ID, Integer.valueOf(C_OrderLine_ID));
	}

	/** Get Sales Order Line.
		@return Sales Order Line
	  */
	public int getC_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_Value (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_XX_MB_CustomerInventory getXX_MB_CustomerInventory() throws RuntimeException
    {
		return (I_XX_MB_CustomerInventory)MTable.get(getCtx(), I_XX_MB_CustomerInventory.Table_Name)
			.getPO(getXX_MB_CustomerInventory_ID(), get_TrxName());	}

	/** Set Customer Inventory.
		@param XX_MB_CustomerInventory_ID Customer Inventory	  */
	public void setXX_MB_CustomerInventory_ID (int XX_MB_CustomerInventory_ID)
	{
		if (XX_MB_CustomerInventory_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_MB_CustomerInventory_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_MB_CustomerInventory_ID, Integer.valueOf(XX_MB_CustomerInventory_ID));
	}

	/** Get Customer Inventory.
		@return Customer Inventory	  */
	public int getXX_MB_CustomerInventory_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_CustomerInventory_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Customer Inventory Line.
		@param XX_MB_CustomerInventoryLine_ID Customer Inventory Line	  */
	public void setXX_MB_CustomerInventoryLine_ID (int XX_MB_CustomerInventoryLine_ID)
	{
		if (XX_MB_CustomerInventoryLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_MB_CustomerInventoryLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_MB_CustomerInventoryLine_ID, Integer.valueOf(XX_MB_CustomerInventoryLine_ID));
	}

	/** Get Customer Inventory Line.
		@return Customer Inventory Line	  */
	public int getXX_MB_CustomerInventoryLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_CustomerInventoryLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity In Stock.
		@param XX_QtyInStock Quantity In Stock	  */
	public void setXX_QtyInStock (BigDecimal XX_QtyInStock)
	{
		set_Value (COLUMNNAME_XX_QtyInStock, XX_QtyInStock);
	}

	/** Get Quantity In Stock.
		@return Quantity In Stock	  */
	public BigDecimal getXX_QtyInStock () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_XX_QtyInStock);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity On Rack.
		@param XX_QtyOnRack Quantity On Rack	  */
	public void setXX_QtyOnRack (BigDecimal XX_QtyOnRack)
	{
		set_Value (COLUMNNAME_XX_QtyOnRack, XX_QtyOnRack);
	}

	/** Get Quantity On Rack.
		@return Quantity On Rack	  */
	public BigDecimal getXX_QtyOnRack () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_XX_QtyOnRack);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getXX_QtyOnRack()));
    }
}