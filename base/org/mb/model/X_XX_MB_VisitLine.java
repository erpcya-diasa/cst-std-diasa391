/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.mb.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for XX_MB_VisitLine
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_MB_VisitLine extends PO implements I_XX_MB_VisitLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120722L;

    /** Standard Constructor */
    public X_XX_MB_VisitLine (Properties ctx, int XX_MB_VisitLine_ID, String trxName)
    {
      super (ctx, XX_MB_VisitLine_ID, trxName);
      /** if (XX_MB_VisitLine_ID == 0)
        {
			setC_Order_ID (0);
			setC_Payment_ID (0);
			setM_Product_ID (0);
			setXX_MB_EventType_ID (0);
			setXX_MB_Visit_ID (0);
			setXX_MB_VisitLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_MB_VisitLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_MB_VisitLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_Order getC_Order() throws RuntimeException
    {
		return (I_C_Order)MTable.get(getCtx(), I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_Value (COLUMNNAME_C_Order_ID, null);
		else 
			set_Value (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Payment getC_Payment() throws RuntimeException
    {
		return (I_C_Payment)MTable.get(getCtx(), I_C_Payment.Table_Name)
			.getPO(getC_Payment_ID(), get_TrxName());	}

	/** Set Payment.
		@param C_Payment_ID 
		Payment identifier
	  */
	public void setC_Payment_ID (int C_Payment_ID)
	{
		if (C_Payment_ID < 1) 
			set_Value (COLUMNNAME_C_Payment_ID, null);
		else 
			set_Value (COLUMNNAME_C_Payment_ID, Integer.valueOf(C_Payment_ID));
	}

	/** Get Payment.
		@return Payment identifier
	  */
	public int getC_Payment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Payment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_Value (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set On Hand Quantity.
		@param QtyOnHand 
		On Hand Quantity
	  */
	public void setQtyOnHand (BigDecimal QtyOnHand)
	{
		set_Value (COLUMNNAME_QtyOnHand, QtyOnHand);
	}

	/** Get On Hand Quantity.
		@return On Hand Quantity
	  */
	public BigDecimal getQtyOnHand () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyOnHand);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_XX_MB_CustomerInventory getXX_MB_CustomerInventory() throws RuntimeException
    {
		return (I_XX_MB_CustomerInventory)MTable.get(getCtx(), I_XX_MB_CustomerInventory.Table_Name)
			.getPO(getXX_MB_CustomerInventory_ID(), get_TrxName());	}

	/** Set Customer Inventory.
		@param XX_MB_CustomerInventory_ID Customer Inventory	  */
	public void setXX_MB_CustomerInventory_ID (int XX_MB_CustomerInventory_ID)
	{
		if (XX_MB_CustomerInventory_ID < 1) 
			set_Value (COLUMNNAME_XX_MB_CustomerInventory_ID, null);
		else 
			set_Value (COLUMNNAME_XX_MB_CustomerInventory_ID, Integer.valueOf(XX_MB_CustomerInventory_ID));
	}

	/** Get Customer Inventory.
		@return Customer Inventory	  */
	public int getXX_MB_CustomerInventory_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_CustomerInventory_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_XX_MB_EventType getXX_MB_EventType() throws RuntimeException
    {
		return (I_XX_MB_EventType)MTable.get(getCtx(), I_XX_MB_EventType.Table_Name)
			.getPO(getXX_MB_EventType_ID(), get_TrxName());	}

	/** Set Envent Type.
		@param XX_MB_EventType_ID Envent Type	  */
	public void setXX_MB_EventType_ID (int XX_MB_EventType_ID)
	{
		if (XX_MB_EventType_ID < 1) 
			set_Value (COLUMNNAME_XX_MB_EventType_ID, null);
		else 
			set_Value (COLUMNNAME_XX_MB_EventType_ID, Integer.valueOf(XX_MB_EventType_ID));
	}

	/** Get Envent Type.
		@return Envent Type	  */
	public int getXX_MB_EventType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_EventType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_XX_MB_Visit getXX_MB_Visit() throws RuntimeException
    {
		return (I_XX_MB_Visit)MTable.get(getCtx(), I_XX_MB_Visit.Table_Name)
			.getPO(getXX_MB_Visit_ID(), get_TrxName());	}

	/** Set Visit to Customer.
		@param XX_MB_Visit_ID Visit to Customer	  */
	public void setXX_MB_Visit_ID (int XX_MB_Visit_ID)
	{
		if (XX_MB_Visit_ID < 1) 
			set_Value (COLUMNNAME_XX_MB_Visit_ID, null);
		else 
			set_Value (COLUMNNAME_XX_MB_Visit_ID, Integer.valueOf(XX_MB_Visit_ID));
	}

	/** Get Visit to Customer.
		@return Visit to Customer	  */
	public int getXX_MB_Visit_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_Visit_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line of Visit.
		@param XX_MB_VisitLine_ID Line of Visit	  */
	public void setXX_MB_VisitLine_ID (int XX_MB_VisitLine_ID)
	{
		if (XX_MB_VisitLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_MB_VisitLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_MB_VisitLine_ID, Integer.valueOf(XX_MB_VisitLine_ID));
	}

	/** Get Line of Visit.
		@return Line of Visit	  */
	public int getXX_MB_VisitLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_VisitLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}