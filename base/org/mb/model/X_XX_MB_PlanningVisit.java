/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.mb.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_MB_PlanningVisit
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_MB_PlanningVisit extends PO implements I_XX_MB_PlanningVisit, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120722L;

    /** Standard Constructor */
    public X_XX_MB_PlanningVisit (Properties ctx, int XX_MB_PlanningVisit_ID, String trxName)
    {
      super (ctx, XX_MB_PlanningVisit_ID, trxName);
      /** if (XX_MB_PlanningVisit_ID == 0)
        {
			setBill_Location_ID (0);
			setC_BPartner_ID (0);
			setC_BPartner_Location_ID (0);
			setC_SalesRegion_ID (0);
			setName (null);
			setSeqNo (0);
			setValidFrom (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setXX_MB_Day_ID (0);
			setXX_MB_Frequency_ID (0);
			setXX_MB_PlanningVisit_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_MB_PlanningVisit (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_MB_PlanningVisit[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner_Location getBill_Location() throws RuntimeException
    {
		return (I_C_BPartner_Location)MTable.get(getCtx(), I_C_BPartner_Location.Table_Name)
			.getPO(getBill_Location_ID(), get_TrxName());	}

	/** Set Invoice Location.
		@param Bill_Location_ID 
		Business Partner Location for invoicing
	  */
	public void setBill_Location_ID (int Bill_Location_ID)
	{
		if (Bill_Location_ID < 1) 
			set_Value (COLUMNNAME_Bill_Location_ID, null);
		else 
			set_Value (COLUMNNAME_Bill_Location_ID, Integer.valueOf(Bill_Location_ID));
	}

	/** Get Invoice Location.
		@return Business Partner Location for invoicing
	  */
	public int getBill_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Bill_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException
    {
		return (I_C_BPartner_Location)MTable.get(getCtx(), I_C_BPartner_Location.Table_Name)
			.getPO(getC_BPartner_Location_ID(), get_TrxName());	}

	/** Set Partner Location.
		@param C_BPartner_Location_ID 
		Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID)
	{
		if (C_BPartner_Location_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, Integer.valueOf(C_BPartner_Location_ID));
	}

	/** Get Partner Location.
		@return Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_SalesRegion getC_SalesRegion() throws RuntimeException
    {
		return (I_C_SalesRegion)MTable.get(getCtx(), I_C_SalesRegion.Table_Name)
			.getPO(getC_SalesRegion_ID(), get_TrxName());	}

	/** Set Sales Region.
		@param C_SalesRegion_ID 
		Sales coverage region
	  */
	public void setC_SalesRegion_ID (int C_SalesRegion_ID)
	{
		if (C_SalesRegion_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_SalesRegion_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_SalesRegion_ID, Integer.valueOf(C_SalesRegion_ID));
	}

	/** Get Sales Region.
		@return Sales coverage region
	  */
	public int getC_SalesRegion_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_SalesRegion_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Sequence.
		@param SeqNo 
		Method of ordering records; lowest number comes first
	  */
	public void setSeqNo (int SeqNo)
	{
		set_Value (COLUMNNAME_SeqNo, Integer.valueOf(SeqNo));
	}

	/** Get Sequence.
		@return Method of ordering records; lowest number comes first
	  */
	public int getSeqNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_SeqNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Valid from.
		@param ValidFrom 
		Valid from including this date (first day)
	  */
	public void setValidFrom (Timestamp ValidFrom)
	{
		set_Value (COLUMNNAME_ValidFrom, ValidFrom);
	}

	/** Get Valid from.
		@return Valid from including this date (first day)
	  */
	public Timestamp getValidFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ValidFrom);
	}

	public I_XX_MB_Day getXX_MB_Day() throws RuntimeException
    {
		return (I_XX_MB_Day)MTable.get(getCtx(), I_XX_MB_Day.Table_Name)
			.getPO(getXX_MB_Day_ID(), get_TrxName());	}

	/** Set Visiting Day.
		@param XX_MB_Day_ID Visiting Day	  */
	public void setXX_MB_Day_ID (int XX_MB_Day_ID)
	{
		if (XX_MB_Day_ID < 1) 
			set_Value (COLUMNNAME_XX_MB_Day_ID, null);
		else 
			set_Value (COLUMNNAME_XX_MB_Day_ID, Integer.valueOf(XX_MB_Day_ID));
	}

	/** Get Visiting Day.
		@return Visiting Day	  */
	public int getXX_MB_Day_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_Day_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_XX_MB_Frequency getXX_MB_Frequency() throws RuntimeException
    {
		return (I_XX_MB_Frequency)MTable.get(getCtx(), I_XX_MB_Frequency.Table_Name)
			.getPO(getXX_MB_Frequency_ID(), get_TrxName());	}

	/** Set Frecuency of Visit.
		@param XX_MB_Frequency_ID Frecuency of Visit	  */
	public void setXX_MB_Frequency_ID (int XX_MB_Frequency_ID)
	{
		if (XX_MB_Frequency_ID < 1) 
			set_Value (COLUMNNAME_XX_MB_Frequency_ID, null);
		else 
			set_Value (COLUMNNAME_XX_MB_Frequency_ID, Integer.valueOf(XX_MB_Frequency_ID));
	}

	/** Get Frecuency of Visit.
		@return Frecuency of Visit	  */
	public int getXX_MB_Frequency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_Frequency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Planning Visit.
		@param XX_MB_PlanningVisit_ID Planning Visit	  */
	public void setXX_MB_PlanningVisit_ID (int XX_MB_PlanningVisit_ID)
	{
		if (XX_MB_PlanningVisit_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_MB_PlanningVisit_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_MB_PlanningVisit_ID, Integer.valueOf(XX_MB_PlanningVisit_ID));
	}

	/** Get Planning Visit.
		@return Planning Visit	  */
	public int getXX_MB_PlanningVisit_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_PlanningVisit_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}