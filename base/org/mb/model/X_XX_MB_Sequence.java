/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.mb.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for XX_MB_Sequence
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_MB_Sequence extends PO implements I_XX_MB_Sequence, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120702L;

    /** Standard Constructor */
    public X_XX_MB_Sequence (Properties ctx, int XX_MB_Sequence_ID, String trxName)
    {
      super (ctx, XX_MB_Sequence_ID, trxName);
      /** if (XX_MB_Sequence_ID == 0)
        {
			setCurrentNext (0);
			setNameSequence (null);
			setStartNo (0);
			setXX_MB_Sequence_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_MB_Sequence (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_MB_Sequence[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_AD_Table getAD_Table() throws RuntimeException
    {
		return (I_AD_Table)MTable.get(getCtx(), I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_Value (COLUMNNAME_AD_Table_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_AD_User getAD_User() throws RuntimeException
    {
		return (I_AD_User)MTable.get(getCtx(), I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_Value (COLUMNNAME_AD_User_ID, null);
		else 
			set_Value (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_DocType getC_DocType() throws RuntimeException
    {
		return (I_C_DocType)MTable.get(getCtx(), I_C_DocType.Table_Name)
			.getPO(getC_DocType_ID(), get_TrxName());	}

	/** Set Document Type.
		@param C_DocType_ID 
		Document type or rules
	  */
	public void setC_DocType_ID (int C_DocType_ID)
	{
		if (C_DocType_ID < 0) 
			set_Value (COLUMNNAME_C_DocType_ID, null);
		else 
			set_Value (COLUMNNAME_C_DocType_ID, Integer.valueOf(C_DocType_ID));
	}

	/** Get Document Type.
		@return Document type or rules
	  */
	public int getC_DocType_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_DocType_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Current Next.
		@param CurrentNext 
		The next number to be used
	  */
	public void setCurrentNext (int CurrentNext)
	{
		set_Value (COLUMNNAME_CurrentNext, Integer.valueOf(CurrentNext));
	}

	/** Get Current Next.
		@return The next number to be used
	  */
	public int getCurrentNext () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CurrentNext);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name Sequence.
		@param NameSequence Name Sequence	  */
	public void setNameSequence (String NameSequence)
	{
		set_Value (COLUMNNAME_NameSequence, NameSequence);
	}

	/** Get Name Sequence.
		@return Name Sequence	  */
	public String getNameSequence () 
	{
		return (String)get_Value(COLUMNNAME_NameSequence);
	}

	/** Set Prefix Value.
		@param PrefixValue Prefix Value	  */
	public void setPrefixValue (String PrefixValue)
	{
		set_Value (COLUMNNAME_PrefixValue, PrefixValue);
	}

	/** Get Prefix Value.
		@return Prefix Value	  */
	public String getPrefixValue () 
	{
		return (String)get_Value(COLUMNNAME_PrefixValue);
	}

	/** Set Start No.
		@param StartNo 
		Starting number/position
	  */
	public void setStartNo (int StartNo)
	{
		set_Value (COLUMNNAME_StartNo, Integer.valueOf(StartNo));
	}

	/** Get Start No.
		@return Starting number/position
	  */
	public int getStartNo () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_StartNo);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Suffix Value.
		@param SuffixValue Suffix Value	  */
	public void setSuffixValue (String SuffixValue)
	{
		set_Value (COLUMNNAME_SuffixValue, SuffixValue);
	}

	/** Get Suffix Value.
		@return Suffix Value	  */
	public String getSuffixValue () 
	{
		return (String)get_Value(COLUMNNAME_SuffixValue);
	}

	/** Set XX_MB_Sequence (Sequence).
		@param XX_MB_Sequence_ID XX_MB_Sequence (Sequence)	  */
	public void setXX_MB_Sequence_ID (int XX_MB_Sequence_ID)
	{
		if (XX_MB_Sequence_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_MB_Sequence_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_MB_Sequence_ID, Integer.valueOf(XX_MB_Sequence_ID));
	}

	/** Get XX_MB_Sequence (Sequence).
		@return XX_MB_Sequence (Sequence)	  */
	public int getXX_MB_Sequence_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_Sequence_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}