/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.mb.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for XX_MB_Visit
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_MB_Visit extends PO implements I_XX_MB_Visit, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120722L;

    /** Standard Constructor */
    public X_XX_MB_Visit (Properties ctx, int XX_MB_Visit_ID, String trxName)
    {
      super (ctx, XX_MB_Visit_ID, trxName);
      /** if (XX_MB_Visit_ID == 0)
        {
			setXX_MB_PlanningVisit_ID (0);
			setXX_MB_Visit_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_MB_Visit (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_MB_Visit[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Date From.
		@param DateFrom 
		Starting date for a range
	  */
	public void setDateFrom (Timestamp DateFrom)
	{
		set_Value (COLUMNNAME_DateFrom, DateFrom);
	}

	/** Get Date From.
		@return Starting date for a range
	  */
	public Timestamp getDateFrom () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateFrom);
	}

	/** Set Date To.
		@param DateTo 
		End date of a date range
	  */
	public void setDateTo (Timestamp DateTo)
	{
		set_Value (COLUMNNAME_DateTo, DateTo);
	}

	/** Get Date To.
		@return End date of a date range
	  */
	public Timestamp getDateTo () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateTo);
	}

	/** Set DateVisit.
		@param DateVisit DateVisit	  */
	public void setDateVisit (Timestamp DateVisit)
	{
		set_Value (COLUMNNAME_DateVisit, DateVisit);
	}

	/** Get DateVisit.
		@return DateVisit	  */
	public Timestamp getDateVisit () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateVisit);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public I_XX_MB_PlanningVisit getXX_MB_PlanningVisit() throws RuntimeException
    {
		return (I_XX_MB_PlanningVisit)MTable.get(getCtx(), I_XX_MB_PlanningVisit.Table_Name)
			.getPO(getXX_MB_PlanningVisit_ID(), get_TrxName());	}

	/** Set Planning Visit.
		@param XX_MB_PlanningVisit_ID Planning Visit	  */
	public void setXX_MB_PlanningVisit_ID (int XX_MB_PlanningVisit_ID)
	{
		if (XX_MB_PlanningVisit_ID < 1) 
			set_Value (COLUMNNAME_XX_MB_PlanningVisit_ID, null);
		else 
			set_Value (COLUMNNAME_XX_MB_PlanningVisit_ID, Integer.valueOf(XX_MB_PlanningVisit_ID));
	}

	/** Get Planning Visit.
		@return Planning Visit	  */
	public int getXX_MB_PlanningVisit_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_PlanningVisit_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Visit to Customer.
		@param XX_MB_Visit_ID Visit to Customer	  */
	public void setXX_MB_Visit_ID (int XX_MB_Visit_ID)
	{
		if (XX_MB_Visit_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_MB_Visit_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_MB_Visit_ID, Integer.valueOf(XX_MB_Visit_ID));
	}

	/** Get Visit to Customer.
		@return Visit to Customer	  */
	public int getXX_MB_Visit_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_MB_Visit_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}