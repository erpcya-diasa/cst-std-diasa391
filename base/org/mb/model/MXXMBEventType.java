/*************************************************************************************
 * Product: SFAndroid (Sales Force Mobile)                       		             *
 * This program is free software; you can redistribute it and/or modify it    		 *
 * under the terms version 2 of the GNU General Public License as published   		 *
 * by the Free Software Foundation. This program is distributed in the hope   		 *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 		 *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           		 *
 * See the GNU General Public License for more details.                       		 *
 * You should have received a copy of the GNU General Public License along    		 *
 * with this program; if not, write to the Free Software Foundation, Inc.,    		 *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     		 *
 * For the text or an alternative of this public license, you may reach us    		 *
 * Copyright (C) 2012-2012 E.R.P. Consultores y Asociados, S.A. All Rights Reserved. *
 * Contributor(s): Yamel Senih www.erpconsultoresyasociados.com				  		 *
 *************************************************************************************/
package org.mb.model;

import java.math.BigDecimal;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * 
 * @author Yamel Senih
 *
 */
public class MXXMBEventType extends X_XX_MB_EventType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4104201663837868624L;

	public MXXMBEventType(Properties ctx, int XX_MB_EventType_ID, String trxName) {
		super(ctx, XX_MB_EventType_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verifica la ponderacion del tipo de evento
	 */
	public boolean beforeSave(boolean newRecord){
		super.beforeSave(newRecord);
		String whereAdd = new String();
		if(!newRecord)
			whereAdd = "AND XX_MB_EventType_ID <> " + getXX_MB_EventType_ID();
		BigDecimal total = DB.getSQLValueBD(get_TrxName(), "SELECT SUM(PercentEffectivity) " +
				"FROM XX_MB_EventType " +
				"WHERE IsActive = 'Y' " +
				"AND AD_Client_ID = ? " +
				whereAdd, Env.getAD_Client_ID(getCtx()));
		
		BigDecimal m_PercentEffectivity = getPercentEffectivity();
		if(m_PercentEffectivity != null)
			total = total.add(m_PercentEffectivity);
		
		if(total != null
				&& !total.equals(Env.ZERO)){
			double maxValue = 100;
			double diff = maxValue - total.doubleValue();
			if(diff < 0)
				throw new AdempiereException("@Above_Percent_100@ " + diff);
			if(diff > 0)
				throw new AdempiereException("@Below_Percent_100@ " + diff);
		} else
			throw new AdempiereException("@Zero@");
			
		return true;
	}
	
}
