/*************************************************************************************
 * Product: SFAndroid (Sales Force Mobile)                       		             *
 * This program is free software; you can redistribute it and/or modify it    		 *
 * under the terms version 2 of the GNU General Public License as published   		 *
 * by the Free Software Foundation. This program is distributed in the hope   		 *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 		 *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           		 *
 * See the GNU General Public License for more details.                       		 *
 * You should have received a copy of the GNU General Public License along    		 *
 * with this program; if not, write to the Free Software Foundation, Inc.,    		 *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     		 *
 * For the text or an alternative of this public license, you may reach us    		 *
 * Copyright (C) 2012-2012 E.R.P. Consultores y Asociados, S.A. All Rights Reserved. *
 * Contributor(s): Yamel Senih www.erpconsultoresyasociados.com				  		 *
 *************************************************************************************/
package org.mb.model;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.util.DB;

/**
 * @author yamel
 *
 */
public class CalloutPlanningVisit extends CalloutEngine {

	/**
	 * Establece el Nombre del Socio de Negocio en el campo Name de la Visita
	 * @author Yamel Senih 13/06/2012, 16:12:15
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 * @return String
	 */
	public String bPartner (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive() || value == null)
			return "";
		
		Integer m_C_BPartner_ID = (Integer) value;
		if(m_C_BPartner_ID.intValue() != 0){
			MBPartner bPartner = new MBPartner(ctx, m_C_BPartner_ID, null);
			mTab.setValue("Name", bPartner.getName());	
		}
		return "";
	}
	
	/**
	 * Establece el ID del dia a partir de la fecha Valido Desde
	 * @author Yamel Senih 22/07/2012, 03:20:15
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 * @return String
	 */
	public String validFrom (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive() || value == null)
			return "";
		//	Se Obtiene la fecha del Value
		Timestamp m_ValidFrom = (Timestamp) value;
		//	Se instancia la clase Calendar con la fecha actual
		Calendar m_CalW = Calendar.getInstance();
		//	Se establece la fecha en el objeto Calendar a partir de los milisegundos
		m_CalW.setTimeInMillis(m_ValidFrom.getTime());
		//	Se obtiene el dia de la semana
		int dayOfWeek = m_CalW.get(Calendar.DAY_OF_WEEK);
		//	Se arma la consulta a partir de la secuencia que equivale al orden de los dias en la tabla XX_MB_Day
		String sql = new String("SELECT XX_MB_Day_ID FROM XX_MB_Day WHERE SeqNo = ?");
		//	Se realiza la consulta
		int m_XX_MB_Day_ID = DB.getSQLValue(null, sql, dayOfWeek * 10);
		//	Se establece el valor del dia en la pestaña
		mTab.setValue("XX_MB_Day_ID", m_XX_MB_Day_ID);
		
		return "";
	}
	
	/**
	 * Establece el Nombre de la Planificacion de Visita
	 * @author Yamel Senih 22/07/2012, 21:15:56
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 * @return String
	 */
	public String nameCall (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive() || value == null)
			return "";
		
		//	Declaracion de Objetos
		MXXMBDay m_Day = null;
		MBPartner m_BPartner = null;
		MBPartnerLocation m_BPartnerLocation = null;
		StringBuffer name = new StringBuffer();
		
		Timestamp m_ValidFrom = (Timestamp) mTab.getValue("ValidFrom");
		//	Se establece el Dia en el Nombre
		if(m_ValidFrom != null){
			//	Se instancia la clase Calendar con la fecha actual
			Calendar m_CalW = Calendar.getInstance();
			//	Se establece la fecha en el objeto Calendar a partir de los milisegundos
			m_CalW.setTimeInMillis(m_ValidFrom.getTime());
			//	Se obtiene el dia de la semana
			int dayOfWeekMonth = m_CalW.get(Calendar.DAY_OF_WEEK_IN_MONTH);

			name.append((dayOfWeekMonth < 3? 1: 2));
			name.append(" | ");
		}
		
		//	Se Obtiene el ID del Dia
		Integer m_XX_MB_Day_ID = (Integer) mTab.getValue("XX_MB_Day_ID");
		//	Se establece el Dia en el Nombre
		if(m_XX_MB_Day_ID != null && m_XX_MB_Day_ID.intValue() != 0){
			m_Day = new MXXMBDay(ctx, m_XX_MB_Day_ID, null);
			name.append(m_Day.getName());
			name.append(" | ");
		}
		
		Integer m_C_BPartner_ID = (Integer) mTab.getValue("C_BPartner_ID");
		
		if(m_C_BPartner_ID != null && m_C_BPartner_ID.intValue() != 0){
			m_BPartner = new MBPartner(ctx, m_C_BPartner_ID, null);
			name.append(m_BPartner.getName());
			name.append(" | ");
		}
		
		Integer m_C_BPartner_Location_ID = (Integer) mTab.getValue("C_BPartner_Location_ID");		

		if(m_C_BPartner_Location_ID != null && m_C_BPartner_Location_ID.intValue() != 0){
			m_BPartnerLocation = new MBPartnerLocation(ctx, m_C_BPartner_Location_ID, null);
			name.append(m_BPartnerLocation.getName());
		}
		
		if(name.length() != 0)
			mTab.setValue("Name", name.toString());
		
		return "";
	}
	
	/**
	 * Establece el ID de la Region de Ventas 
	 * en base al ID de la direccion de entrega
	 * @author Yamel Senih 23/07/2012, 08:48:59
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 * @return String
	 */
	public String salesRegion (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive() || value == null)
			return "";
		
		MBPartnerLocation m_BPartnerLocation = null;
		Integer m_C_BPartner_Location_ID = (Integer) mTab.getValue("C_BPartner_Location_ID");		

		if(m_C_BPartner_Location_ID != null && m_C_BPartner_Location_ID.intValue() != 0){
			m_BPartnerLocation = new MBPartnerLocation(ctx, m_C_BPartner_Location_ID, null);
			mTab.setValue("C_SalesRegion_ID", m_BPartnerLocation.getC_SalesRegion_ID());
		}
		
		return "";
	}
	
}
