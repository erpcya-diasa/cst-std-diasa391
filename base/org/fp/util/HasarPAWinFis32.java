
package org.fp.util;

/**
 * 
 * @author Yamel Senih
 */
public class HasarPAWinFis32
{
	
	/*' Funciones disponibles en winfis32.dll
	'--------------------------------------
	Declare Sub Abort Lib "winfis32" (ByVal handler As Long)
	Declare Sub SetKeepAliveHandlerStdCall Lib "winfis32" (ByVal Ptr As Long)
	//Declare Function VersionDLLFiscal Lib "winfis32" () As Long
	//Declare Function OpenComFiscal Lib "winfis32" (ByVal puerto As Long, ByVal mode As Long) As Long
	//Declare Function ReOpenComFiscal Lib "winfis32" (ByVal puerto As Long) As Long
	//Declare Function OpenTcpFiscal Lib "winfis32" (ByVal hostname As String, ByVal socket As Long, _
	                                               ByVal miliseg As Double, ByVal mode As Long) As Long
	Declare Sub CloseComFiscal Lib "winfis32" (ByVal handler As Long)
	Declare Function InitFiscal Lib "winfis32" (ByVal handler As Long) As Long
	Declare Function CambiarVelocidad Lib "winfis32" (ByVal handler As Long, ByVal NewSpeed As Long) As Long
	Declare Sub BusyWaitingMode Lib "winfis32" (ByVal mode As Long)
	Declare Sub ProtocolMode Lib "winfis32" (ByVal mode As Long)
	Declare Function SetModoEpson Lib "winfis32" (ByVal epson As Boolean) As Long
	Declare Function SearchPrn Lib "winfis32" (ByVal handler As Long) As Long
	Declare Function MandaPaqueteFiscal Lib "winfis32" (ByVal handler As Long, ByVal Buffer As String) As Long
	Declare Function UltimaRespuesta Lib "winfis32" (ByVal handler As Long, ByVal Buffer As String) As Long
	Declare Function UltimoStatus Lib "winfis32" (ByVal handler As Long, ByRef FiscalStatus As Integer, _
	                                              ByRef PrinterStatus As Integer) As Long
	Declare Function SetCmdRetries Lib "winfis32" (ByVal cat As Long) As Long
	Declare Function SetSndRetries Lib "winfis32" (ByVal cat As Long) As Long
	Declare Function SetRcvRetries Lib "winfis32" (ByVal cat As Long) As Long
	Declare Function ObtenerNumeroDePaquetes Lib "winfis32" (ByVal handler As Long, ByRef Paqsend As Long, _
	                                                         ByRef Paqrec As Long, ByRef Idcmd As String) As Long

	' Constantes para manejarse con la DLL
	'-------------------------------------
	Public Const MODE_ANSI = 1                  '// Usar caracteres ANSI
	Public Const MODE_ASCII = 0                 '// Usar caracteres ASCII

	Public Const BUSYWAITING_OFF = 0            '// Control en la DLL
	Public Const BUSYWAITING_ON = 1             '// Control en el WinPRUF

	Public Const OLD_PROTOCOL = 0               '// Pasar a protocolo viejo
	Public Const NEW_PROTOCOL = 1               '// Pasar a protocolo nuevo

	' Errores devueltos por las funciones de la DLL
	'----------------------------------------------
	Public Const ERROR = -1                      '// La transmisi�n no se pudo completar
	Public Const ERR_HANDLER = -2                '// Handler inv�lido
	Public Const ERR_ATOMIC = -3                 '// Intento de enviar un comando cuando se estaba procesando el anterior
	Public Const ERR_TIMEOUT = -4                '// Error de comunicaciones (time-out)
	Public Const ERR_ALREADYOPEN = -5            '// El puerto indicado ya estaba abierto
	Public Const ERR_NOMEM = -6                  '// Memoria host insuficiente
	Public Const ERR_NOTOPENYET = -7             '// Aun no se han inicializado las comunicaciones
	Public Const ERR_INVALIDPTR = -8             '// La direcci�n del buffer de respuesta es inv�lida
	Public Const ERR_STATPRN = -9                '// El comando no finaliz�, sino que lleg� una respuesta tipo STAT_PRN
	Public Const ERR_ABORT = -10                 '// El proceso en curso fue abortado por el usuario
	Public Const ERR_NOT_PORT = -11              '// No hay m�s puertos disponibles
	Public Const ERR_TCPIP = -12                 '// Error estableciendo una comunicaci�n TCP/IP
	Public Const ERR_HOST_NOT_FOUND = -13        '// No existe el nodo en la red (conversor LAN / serie)
	Public Const ERR_HOST_TIMEOUT = -14          '// Error conectando con el host(conversor LAN / Serie)
	Public Const ERR_NAK = -15                   '// Se ha recibido NAK como respuesta
	*/
	
	static {
		System.loadLibrary("WINFIS32");
	}
	
	/**
	 * Obtiene la version del DLL
	 * @author Yamel Senih 03/24/2012, 10:24:41
	 * @return
	 * @return int
	 */
	public native int VersionDLLFiscal();
	
	/**
	 * Abre el Puuerto de la Impresora Fiscal
	 * @author Yamel Senih 03/24/2012, 10:25:35
	 * @param puerto
	 * @param mode
	 * @return
	 * @return int
	 */
	public native int OpenComFiscal(int puerto, int mode);
	
	/**
	 * Vuelve a abrir el puerto
	 * @author Yamel Senih 03/24/2012, 10:27:58
	 * @param port
	 * @return
	 * @return int
	 */
	public native int ReOpenComFiscal(int port);
	
	/**
	 * Abre el puerto via TCP/IP
	 * @author Yamel Senih 03/24/2012, 10:30:24
	 * @param hostname
	 * @param socket
	 * @param miliseg
	 * @param mode
	 * @return
	 * @return int
	 */
	public native int OpenTcpFiscal(String hostname,int socket,double miliseg, int mode);
	
	/**
	 * Cierra e Puerto COM
	 * @author Yamel Senih 03/24/2012, 10:32:59
	 * @param handler
	 * @return
	 * @return int
	 */
	public native int CloseComFiscal(int handler);
}
