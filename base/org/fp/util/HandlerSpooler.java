/**
 * @finalidad 
 * @author Yamel Senih
 * @date 03/31/2012
 */
package org.fp.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.sg.model.MXXFiscalPrinterConfig;
import org.spin.util.SendReportToFiscalPrintQueue;

/**
 * @author Yamel Senih
 *
 */
public class HandlerSpooler implements I_PrinterFiscalHasar{

	Runtime 				runT					= 	null;
	/**	Puerto							*/
	private String			port					=	null;
	/**	Velocidad						*/
	private int				speed					=	0;
	/**	Ruta del Archivo de Registro	*/
	private String 			pathFileLog				= 	null;
	/**	Nombre del directorio donde va 
	 * a guardar los archivos temporales
	 */
	private String 			pathNameTmp				= 	null;
	/**	Path Spooler					*/
	private String 			pathSpooler				= 	null;
	/**	Arguments						*/
	private StringBuffer 	args 					= 	null;
	/**	Message							*/
	private StringBuffer 	msg 					=	null;
	/**	OS CMD							*/
	private String 			os_Cmd					=	"";
	/**	Separator						*/
	String 					cSeparator				=	null;
	/**	Code Prionter					*/
	String 					cPrinter				=	null;
	/**	Constador de lineas del Encabezdo	*/
	private int				lineHeader				=	0;
	/**	Contador de lineas del Pie de Pagina*/
	private int				lineTrailer				=	0;
	/**	Cmd Port				*/
	private final String CMD_M_PORT__P				=	"-p";
	/**	Cmd Speed				*/
	private final String CMD_O_SPEED__V				=	"-v";
	/**	Cmd	Search Speed		*/
	private final String CMD_O_SEARCH_SPEED__T		= 	"-t";
	/**	Cmd Protocolo Log File	*/
	private final String CMD_O_PROT_LOG__L			=	"-l";
	/**	Cmd	Path Name File Log	*/
	private final String CMD_O_PATH_LOG__D			=	"-d";
	/**	Cmd	Retrieved File		*/
	private final String CMD_O_ID_FILE__N			=	"-n";
	/**	Cmd	Not Message			*/
	private final String CMD_O_N_MSG__M				=	"-m";
	/**	Cmd Path File Tmp		*/
	private final String CMD_O_PH_TMP__B			=	"-b";
	/**	Not Cancel Journal		*/
	private final String CMD_O_NC_JOURNAL__B		=	"-b";
	/**	Cmd Command				*/
	private final String CMD_M_CMD__C				=	"-c";
	
	/**	Param P					*/
	private final String P__V_Z						=	"z";
	
	/**	Space					*/
	private final String C_SPACE					=	" ";
	/**	Quotes					*/
	private final String C_QUOTE					=	"\"";
	/**	X Report				*/
	private final String CMD_S_X					=	"9X";
	/**	Z Report				*/
	private final String CMD_S_Z					=	"9Z";
	
	/**Encabezado de Comprobante*/
	//"@Yamel SenihV-20237661120401093310A"
	//"BCoca-Cola25.10.7.M123456"
	private static final String RESOURCE = "org.fp.util.HandlerSpoolerLan";
	/** Resources							*/
	private static ResourceBundle res = ResourceBundle.getBundle(RESOURCE);
	
	/**	Last Spooler result	*/
	private StringBuffer lastSpoolerLog = new StringBuffer();
	/**	File Writer	*/
	private FileWriter fileWriter = null;
	/**	File	*/
	private File file = null;
	/**	Error Message	*/
	private StringBuffer errorMessage = new StringBuffer();
	/**	First Command	*/
	private boolean isFirst = true;
	/**	Default fiscal printer config	*/
	private MXXFiscalPrinterConfig fpConfig = null;
	
	/**
	 * Standard constructor
	 * @param documentNo
	 * @param fpConfig
	 */
	public HandlerSpooler(String documentNo, MXXFiscalPrinterConfig fpConfig){
		runT = Runtime.getRuntime();
		this.fpConfig = fpConfig;
		this.port = fpConfig.getPuertoRS();
		this.speed = fpConfig.getXX_Speed();
		this.pathSpooler = fpConfig.getXX_PathSpooler(); 
		this.pathFileLog = fpConfig.getXX_PathLog();
		this.pathNameTmp = fpConfig.getXX_PathTmp(); 
		if(!Util.isEmpty(fpConfig.getXX_OS_CMD())) {
			this.os_Cmd = fpConfig.getXX_OS_CMD();
		}
		this.cPrinter = fpConfig.getPrinterId(); 
		this.cSeparator = Character.toString((char)fpConfig.getXX_FSeparator());
		
		//	Arguments
		args = new StringBuffer();
		//	Path Spooler
		args.append(pathSpooler);
		//	Space
		args.append(C_SPACE);
		//	Param
		args.append(CMD_M_PORT__P);
		//	Space
		args.append(C_SPACE);
		//	Port
		args.append(port);
		//	Speed
		if(speed != 0){
			args.append(C_SPACE);
			args.append(CMD_O_SPEED__V + speed);
		}
		//	Parth Log File
		if(pathFileLog != null 
				&& pathFileLog.length() != 0){
			args.append(C_SPACE);
			args.append(CMD_O_PATH_LOG__D + 
					C_SPACE + 
					pathFileLog);
		}
		//	Path Tmp Files
		if(pathNameTmp != null 
				&& pathNameTmp.length() != 0){
			args.append(C_SPACE);
			args.append(CMD_O_PH_TMP__B + 
					C_SPACE + 
					pathNameTmp);
		}
		//	TODO: set real name
		try {
			file = File.createTempFile(documentNo + "_" + System.currentTimeMillis(), ".txt");
			deleteIfExist(file);
			openFileWriter(file);
		} catch (IOException e) {
			throw new AdempiereException(e);
		}
	}
	
	@Override
	public void printCommand(String cmd) throws Exception {
		writeLine(cmd);
	}
	
	@Override
	public void printCommand(String[] cmd) throws Exception{
		for (int i = 0; i < cmd.length; i++) {
			printCommand(cmd[i]);
		}
	}
	
	@Override
	public void printLine(String product, double price, double units,
			double taxRate, String facProd, String codProduct) throws Exception{
		printCommand("B" + 
			cSeparator + 
			product + 
			cSeparator + 
			price + 
			cSeparator + 
			units + 
			cSeparator + 
			taxRate + 
			cSeparator + 
			(facProd != null? facProd: "M") + 
			cSeparator + 
			codProduct
			);
	}
	@Override
	public void printTextHeader(String sheader) throws Exception {
		if(lineHeader == 0)
			printCommand("]" + cSeparator + lineHeader++ + cSeparator + sheader);
		if(lineHeader <= 4)
			printCommand("]" + cSeparator + lineHeader++ + cSeparator + sheader);
	}
	@Override
	public void printMessage(String smessage) throws Exception{
		printCommand("A" + cSeparator + smessage + cSeparator);
	}
	@Override
	public void printTextTrailer(String strailer) throws Exception {
		if(lineTrailer == 0)
			printCommand("^" + cSeparator + lineTrailer++ + cSeparator + strailer);
		if(lineTrailer <= 4)
			printCommand("^" + cSeparator + lineTrailer++ + cSeparator + strailer);
	}
	@Override
	public void printTotal(String sPayment, double dpaid) throws Exception{
		
	}
	@Override
	public void printZReport() throws Exception{
		printCommand("9" + cSeparator + "Z" + cSeparator);
	}
	@Override
	public void printXReport() throws Exception{
		printCommand("9" + cSeparator + "X" + cSeparator);
	}

	@Override
	public void printHeader(String nameBP, String rfc, String nroVoucher,
			String codPrinter, String dateVoucher, String timeVoucher, String docType)
			throws Exception {
		StringBuffer cmd = new StringBuffer();
		cmd.append("@");
		cmd.append(cSeparator);
		//	Name Business Partner
		if(nameBP != null)
			cmd.append(nameBP);
		cmd.append(cSeparator);
		//	RFC
		if(rfc != null)
			cmd.append(rfc);
		cmd.append(cSeparator);
		//	Nro Voucher
		if(nroVoucher != null)
			cmd.append(nroVoucher);
		cmd.append(cSeparator);
		//	Code Printer
		if(codPrinter != null)
			cmd.append(codPrinter);
		cmd.append(cSeparator);
		//	date Voucher
		if(dateVoucher != null)
			cmd.append(dateVoucher);
		cmd.append(cSeparator);
		//	time Voucher
		if(timeVoucher != null)
			cmd.append(timeVoucher);
		cmd.append(cSeparator);
		//	docType
		if(docType != null)
			cmd.append(docType);
		cmd.append(cSeparator);
		cmd.append(cSeparator);
		
		printCommand(cmd.toString());
		
	}

	@Override
	public void printFiscalMemReport(Timestamp dateFrom, Timestamp dateTo,
			String type) throws Exception {
		if(type == null)
			throw new AdempiereException("@Type@ = Null");
		//	
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		//	String
		String strFrom = sdf.format(new Date(dateFrom.getTime()));
		String strTo = sdf.format(new Date(dateTo.getTime()));
		
		printCommand(":" + cSeparator + strFrom + cSeparator + strTo + cSeparator + type);
	}
	
	@Override
	public void printFiscalMemReport(int zFrom, int zTo, String type)
			throws Exception {
		if(type == null)
			throw new AdempiereException("@Type@ = Null");
		//	
		printCommand(";" + cSeparator + zFrom + cSeparator + zTo + cSeparator + type);
	}

	@Override
	public void printStackCmd() throws Exception {
		closeFileWriter();
		sendFileToPrinter();
	}
	
	/**
	 * Send file to printer
	 */
	private void sendFileToPrinter() {
		SendReportToFiscalPrintQueue.sendFile(fpConfig.get_ValueAsInt("AD_AppRegistration_ID"), file);
	}
	
	/**
	 * Delete File
	 * @param file
	 */
	private void deleteIfExist(File file) {
		if(file == null) {
			return;
		}
		//	Delete it
		if (file.exists()) {
			file.delete();
		}
	}
	
	/**
	 * Open File Writer
	 * @param file
	 * @throws IOException
	 */
	public void openFileWriter(File file) {
		if(fileWriter != null) {
			return;
		}
		//	Open it
		try {
			fileWriter = new FileWriter(file);
		} catch (IOException e) {
			addError(e.getLocalizedMessage());
		}
	}
	
	/**
	 * Close File Writer and set to null
	 * @throws IOException
	 */
	public void closeFileWriter() {
		if(fileWriter == null) {
			return;
		}
		//	Close it
		try {
			fileWriter.flush();
			fileWriter.close();
			//	Set to null it
			fileWriter = null;
		} catch (IOException e) {
			addError(e.getLocalizedMessage());
		}
	}
	
	/**
	 * Write a line to file
	 * @param line
	 * @throws IOException 
	 */
	public boolean writeLine(String line) {
		if(fileWriter == null) {
			return false;
		}
		//	Write line
		boolean ok = false;
		try {
			if(!isFirst) {
				line = Env.NL + line;
			}
			fileWriter.write(line);
			if(isFirst) {
				isFirst = false;
			}
			ok = true;
		} catch (IOException e) {
			addError(e.getLocalizedMessage());
		}
		return ok;
	}
	
	/**
	 * Add error to buffer
	 * @param error
	 */
	public void addError(String error) {
		if(errorMessage.length() > 0) {
			errorMessage.append(Env.NL);
		}
		//	Add error
		errorMessage.append(error);
	}
	
	/**
	 * Get Error Message
	 * @return
	 */
	public String getError() {
		return errorMessage.toString();
	}
	
	// Discount (Custom DIASA)
	public void printLineDisc(String product, double price, double units,
            double taxRate, String facProd, String codProduct) throws Exception{
        printCommand("B" +
            cSeparator +
            product +
            cSeparator +
            price +
            cSeparator +
            units +
            cSeparator +
            taxRate +
            cSeparator +
            (facProd != null? facProd: "m") +
            cSeparator +
            codProduct
            );
    }
}
