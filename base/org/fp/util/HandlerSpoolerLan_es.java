/**
 * @finalidad Manejar la traduccion de la ventana de login
 * @author Yamel Senih
 * @date 15/01/2012
 */
package org.fp.util;

import java.util.ListResourceBundle;

/**
 * @author Yamel Senih
 *
 */
public class HandlerSpoolerLan_es extends ListResourceBundle {

	@Override
	protected Object[][] getContents() {
		return contents;
	}
	
	static final Object[][] contents = {
        {"1", "Error en la sintaxis de invocación del (w)spooler"},
        {"2", "Error de comunicaciones con la impresora fiscal hasar"},
        {"3", "Error tratando de abrir el archivo de respuestas"},
        {"4", "Error tratando de abrir el archivo de comandos"},
        {"5", "Error tratando de abrir el puerto serie"},
        {"6", "Error fatal provocado por el comando enviado"},
    };
	
}
