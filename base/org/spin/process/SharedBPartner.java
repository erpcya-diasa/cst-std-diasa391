package org.spin.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.compiere.model.I_I_BPartner;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;

public class SharedBPartner extends SvrProcess{

	private String p_URL = null;
	private String p_NameSpace = null;
	private String p_CreateServiceTypeName = null;
	private String p_ProcessServiceTypeName = null;
	private String p_UserName = null;
	private String p_Password = null;
	private int p_ClientID = 0;
	private int p_AD_Role_ID = 0;
	private int p_Org_ID = 0;
	private int m_C_BPartner_ID = 0;
	private int p_WarehouseID = 0;
	
	
	private String m_TableName = I_I_BPartner.Table_Name;
	
	private static String AD_Process_ID = "124";
	private static String MESSAGETYPE_Create = "C";
	private static String MESSAGETYPE_Process = "P";
	private static String STRING_NameSpace = "adin";
	private static String ELEMENT_CreateData = "createData";
	private static String ELEMENT_ModelCRUDRequest = "ModelCRUDRequest";
	private static String ELEMENT_ModelCRUD= "ModelCRUD";
	private static String ELEMENT_ServiceType= "serviceType";
	private static String ELEMENT_DataRow= "DataRow";
	private static String ELEMENT_Field= "field";
	private static String ELEMENT_Value= "val";
	
	
	private static String ELEMENT_RunProcess= "runProcess";
	private static String ELEMENT_ModelRunProcessRequest= "ModelRunProcessRequest";
	private static String ELEMENT_ModelRunProcess= "ModelRunProcess";
	
	
	private static String ELEMENT_ParamValues= "ParamValues";
	
	
	private static String ELEMENT_ADLoginRequest= "ADLoginRequest";
	private static String ELEMENT_User= "user";
	private static String ELEMENT_Password= "pass";
	private static String ELEMENT_ClientID= "ClientID";
	private static String ELEMENT_RoleID= "RoleID";
	private static String ELEMENT_OrgID= "OrgID";
	private static String ELEMENT_WarehouseID= "WarehouseID";
	
	
	private static String ATTRIBUTE_Column = "column";
	private static String ATTRIBUTE_Menu = "AD_Menu_ID";
	private static String ATTRIBUTE_Process = "AD_Process_ID";
	
	private ArrayList<ProcessColumnInfo> m_Columns = new ArrayList<ProcessColumnInfo>();
	private ArrayList<ArrayList<ProcessColumnInfo>> m_Values = new ArrayList<ArrayList<ProcessColumnInfo>>();
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		
		for (ProcessInfoParameter para : params) {
			if (para.getParameterName().equals("URL"))
				p_URL = para.getParameter().toString();
			else if (para.getParameterName().equals("NameSpace"))
				p_NameSpace = para.getParameter().toString();
			else if (para.getParameterName().equals("CreateServiceTypeName"))
				p_CreateServiceTypeName = para.getParameter().toString();
			else if (para.getParameterName().equals("ProcessServiceTypeName"))
				p_ProcessServiceTypeName = para.getParameter().toString();
			else if (para.getParameterName().equals("UserName"))
				p_UserName = para.getParameter().toString();
			else if (para.getParameterName().equals("Password"))
				p_Password = para.getParameter().toString();
			else if (para.getParameterName().equals("Remote_Client_ID"))
				p_ClientID = para.getParameterAsInt();
			else if (para.getParameterName().equals("AD_Role_ID"))
				p_AD_Role_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("Org_ID"))
				p_Org_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("WarehouseID"))
				p_WarehouseID = para.getParameterAsInt();
			
		}
		
		if (getRecord_ID()!=0)
			m_C_BPartner_ID = getRecord_ID();
		//Set data to List
		setData();
	}

	@Override
	protected String doIt() throws Exception {
		String result = "";
		for (ArrayList<ProcessColumnInfo> columns : m_Values) 
			result += callWebService(getRequestMessage(MESSAGETYPE_Create,columns));
		
		callWebService(getRequestMessage(MESSAGETYPE_Process,null));
		m_Columns.clear();
		m_Values.clear();
		return result;
		
	}
	
	private String callWebService(SOAPMessage message) {
		String result = "";
		
		try {
			SOAPConnectionFactory soapConFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConFactory.createConnection();
			SOAPMessage soapResponse = soapConnection.call(message, p_URL);
			
			if (soapResponse.getSOAPBody().hasChildNodes()) {
				SOAPElement resp = (SOAPElement) soapResponse.getSOAPBody().getChildElements().next();
				while (resp!=null && resp.hasChildNodes())
				{
					
					if (resp.getNodeName()!=null
							&& resp.getNodeName().equals("Error")
							) {
						result = resp.getValue();
						addLog(resp.getValue());
						break;
					}else if(resp.getNodeName()!=null
							&& resp.getNodeName().equals("soap:Fault")) {
						if (resp.getChildNodes().getLength() > 1) {
							result = resp.getChildNodes().item(0).getTextContent();
							result += " : " + resp.getChildNodes().item(1).getTextContent();
							addLog(result);
							break;
							
						}
					}
					
					if (resp.getChildElements().hasNext()
							&& resp.getChildElements().next() instanceof SOAPElement)
						resp = (SOAPElement) resp.getChildElements().next();
					else 
						resp = null;
				}
			}
			soapConnection.close();
			
		}catch(Exception e){
			result = e.getMessage();
			addLog(result);
		}
		
		return result;
	}
	
	private SOAPMessage getRequestMessage(String messageType,ArrayList<ProcessColumnInfo> columns) throws Exception{
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage requestMessage = messageFactory.createMessage();
		
		SOAPPart soapPart = requestMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		soapEnvelope.addNamespaceDeclaration(STRING_NameSpace, p_NameSpace);
		SOAPBody soapBody = soapEnvelope.getBody();
		if (messageType.equals(MESSAGETYPE_Create)) {
			SOAPElement createDataElement = soapBody.addChildElement(ELEMENT_CreateData,STRING_NameSpace);
				SOAPElement modelCRUDRequestElement = createDataElement.addChildElement(ELEMENT_ModelCRUDRequest,STRING_NameSpace);
					SOAPElement modelCRUDElement = modelCRUDRequestElement.addChildElement(ELEMENT_ModelCRUD,STRING_NameSpace);
						SOAPElement serviceTypeElement = modelCRUDElement.addChildElement(ELEMENT_ServiceType,STRING_NameSpace);
						serviceTypeElement.addTextNode(p_CreateServiceTypeName);
						SOAPElement tableNameElement = modelCRUDElement.addChildElement("TableName",STRING_NameSpace);
						tableNameElement.addTextNode(m_TableName);
						SOAPElement actionElement = modelCRUDElement.addChildElement("Action",STRING_NameSpace);
						actionElement.addTextNode("Create");
						
						SOAPElement dataRowElement = modelCRUDElement.addChildElement(ELEMENT_DataRow,STRING_NameSpace);
							AddRecordRequest(dataRowElement,columns);
							
					addLoginRequest(modelCRUDRequestElement);
		}else if (messageType.equals(MESSAGETYPE_Process)) {
			SOAPElement runProcessElement = soapBody.addChildElement(ELEMENT_RunProcess,STRING_NameSpace);
			
				SOAPElement ModelRunProcessRequestElement = runProcessElement.addChildElement(ELEMENT_ModelRunProcessRequest,STRING_NameSpace);
			
					SOAPElement ModelRunProcessElement = ModelRunProcessRequestElement.addChildElement(ELEMENT_ModelRunProcess,STRING_NameSpace);
					ModelRunProcessElement.setAttribute(ATTRIBUTE_Menu, "0");
					ModelRunProcessElement.setAttribute(ATTRIBUTE_Process, AD_Process_ID);
						SOAPElement serviceTypeElement = ModelRunProcessElement.addChildElement(ELEMENT_ServiceType,STRING_NameSpace);
						serviceTypeElement.addTextNode(p_ProcessServiceTypeName);
						SOAPElement ParamValuesElement = ModelRunProcessElement.addChildElement(ELEMENT_ParamValues,STRING_NameSpace);
						
							SOAPElement fieldElement = ParamValuesElement.addChildElement(ELEMENT_Field,STRING_NameSpace);
							fieldElement.setAttribute(ATTRIBUTE_Column, I_I_BPartner.COLUMNNAME_AD_Client_ID);
							SOAPElement Value_ValueElement = fieldElement.addChildElement(ELEMENT_Value,STRING_NameSpace);
							Value_ValueElement.addTextNode(((Integer)p_ClientID).toString());
						
					addLoginRequest(ModelRunProcessRequestElement);	
				
			
		}
		return requestMessage;
	}
	
	/**
	 * Add Login Request  
	 * @param parent
	 * @throws Exception
	 * @return void
	 */
	private void addLoginRequest(SOAPElement parent) throws Exception {
		SOAPElement ADLoginRequest_Element = parent.addChildElement(ELEMENT_ADLoginRequest,STRING_NameSpace);
		SOAPElement User_Element = ADLoginRequest_Element.addChildElement(ELEMENT_User,STRING_NameSpace);
		User_Element.addTextNode(p_UserName);
		SOAPElement Password_Element = ADLoginRequest_Element.addChildElement(ELEMENT_Password,STRING_NameSpace);
		Password_Element.addTextNode(p_Password);
		SOAPElement ClientID_Element = ADLoginRequest_Element.addChildElement(ELEMENT_ClientID,STRING_NameSpace);
		ClientID_Element.addTextNode(((Integer) p_ClientID).toString());
		SOAPElement RoleID_Element = ADLoginRequest_Element.addChildElement(ELEMENT_RoleID,STRING_NameSpace);
		RoleID_Element.addTextNode(((Integer) p_AD_Role_ID).toString());
		SOAPElement OrgID_Element = ADLoginRequest_Element.addChildElement(ELEMENT_OrgID,STRING_NameSpace);
		OrgID_Element.addTextNode(((Integer) p_Org_ID).toString());
		SOAPElement WarehouseID_Element = ADLoginRequest_Element.addChildElement(ELEMENT_WarehouseID,STRING_NameSpace);
		WarehouseID_Element.addTextNode(((Integer) p_WarehouseID).toString());
		
	}
	
	private String setData() {
		String result = "";
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT ");
		
		//Basic Data
		m_Columns.add(new ProcessColumnInfo("bp.Value", "Value", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.IsProspect", "IsProspect", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.Name", "Name", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.Name2", "Name2", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.Description", "Description", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.Rating", "Rating", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.XX_TipoPersona", "XX_TipoPersona", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.XX_APC_TBP", "XX_APC_TBP", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.TaxID", "TaxID", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.DvID", "DvID", "S"));
		m_Columns.add(new ProcessColumnInfo("ctg.Value", "TaxGroupValue", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.IsTaxExempt", "IsTaxExempt", "S"));
		m_Columns.add(new ProcessColumnInfo("bpg.Value", "GroupValue", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.XX_BP_SubGroup", "XX_BP_SubGroup", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.AcqusitionCost", "AcqusitionCost", "B"));
		m_Columns.add(new ProcessColumnInfo("bp.SalesVolume", "SalesVolume", "I"));
		m_Columns.add(new ProcessColumnInfo("bp.SOCreditStatus", "SOCreditStatus", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.FirstSale", "FirstSale", "D"));
		m_Columns.add(new ProcessColumnInfo("bp.IsPop", "IsPop", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.HightVolumen", "HightVolumen", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.Scope", "Scope", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.PollProductCategory1", "PollProductCategory1", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.PollProductCategory2", "PollProductCategory2", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.PollProductCategory3", "PollProductCategory3", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.ShareMarket", "ShareMarket", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.QtyProvider", "QtyProvider", "I"));
		m_Columns.add(new ProcessColumnInfo("bp.IsPrePaid", "IsPrePaid", "S"));
		m_Columns.add(new ProcessColumnInfo("usr.Name", "SalesRep_Name", "S"));
		m_Columns.add(new ProcessColumnInfo("bpp.Value", "BPValue", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.NumberEmployees", "NumberEmployees", "I"));
		m_Columns.add(new ProcessColumnInfo("bp.A1_CodigoTarjeta", "A1_CodigoTarjeta", "S"));
		
		//Comun Data
		m_Columns.add(new ProcessColumnInfo("bp.InvoiceRule", "InvoiceRule", "S"));
		m_Columns.add(new ProcessColumnInfo("cis.Name", "InvoiceScheduleName", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.DeliveryRule", "DeliveryRule", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.DeliveryViaRule", "DeliveryViaRule", "S"));
		m_Columns.add(new ProcessColumnInfo("pl.Name", "PriceListName", "S"));
		m_Columns.add(new ProcessColumnInfo("dsch.Name", "DiscountSchemaName", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.FlatDiscount", "FlatDiscount", "B"));
		m_Columns.add(new ProcessColumnInfo("bp.PaymentRule", "PaymentRule", "S"));
		m_Columns.add(new ProcessColumnInfo("cpt.Value", "PaymentTermValue", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.IsDiscountPrinted", "IsDiscountPrinted", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.PollProduct1", "PollProduct1", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.XX_First_Name", "XX_First_Name", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.XX_SurName", "XX_SurName", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.XX_Last_Name", "XX_Last_Name", "S"));
		m_Columns.add(new ProcessColumnInfo("bp.XX_Mother_Last_Name", "XX_Mother_Last_Name", "S"));
		
		//Contact User
		m_Columns.add(new ProcessColumnInfo("ucont.Name", "ContactName", "S"));
		m_Columns.add(new ProcessColumnInfo("ucont.Description", "ContactDescription", "S"));
		m_Columns.add(new ProcessColumnInfo("ucont.Comments", "Comments", "S"));
		m_Columns.add(new ProcessColumnInfo("ucont.EMail", "EMail", "S"));
		m_Columns.add(new ProcessColumnInfo("cg.Name", "BPContactGreeting", "S"));
		m_Columns.add(new ProcessColumnInfo("ucont.Title", "Title", "S"));
		m_Columns.add(new ProcessColumnInfo("ucont.Birthday", "Birthday", "D"));
		m_Columns.add(new ProcessColumnInfo("ucont.Phone", "Phone", "S"));
		m_Columns.add(new ProcessColumnInfo("ucont.Phone2", "Phone2", "S"));
		m_Columns.add(new ProcessColumnInfo("ucont.Fax", "Fax", "S"));
		m_Columns.add(new ProcessColumnInfo("ucont.NotificationType", "NotificationType", "S"));
		
		//Location
		m_Columns.add(new ProcessColumnInfo("bpl.Name", "LocationName", "S")); 
		m_Columns.add(new ProcessColumnInfo("cc.CountryCode", "CountryCode", "S"));
		m_Columns.add(new ProcessColumnInfo("cr.Name", "RegionName", "S"));
		m_Columns.add(new ProcessColumnInfo("COALESCE(cct.Name,cl.City)", "City", "S"));
		m_Columns.add(new ProcessColumnInfo("cl.Address1", "Address1", "S"));
		m_Columns.add(new ProcessColumnInfo("cl.Address2", "Address2", "S"));
		m_Columns.add(new ProcessColumnInfo("cl.Address3", "Address3", "S"));
		m_Columns.add(new ProcessColumnInfo("cl.Address4", "Address4", "S"));
		m_Columns.add(new ProcessColumnInfo("cl.Postal", "Postal", "S"));
		m_Columns.add(new ProcessColumnInfo("cl.Postal_Add", "Postal_Add", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.ISDN", "ISDN", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.IsShipTo", "IsShipTo", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.IsBillTo", "IsBillTo", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.IsPayFrom", "IsPayFrom", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.IsRemitTo", "IsRemitTo", "S"));
		m_Columns.add(new ProcessColumnInfo("csr.Value", "SalesRegionValue", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.InSideLocation", "InSideLocation", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.XX_VisitFrequency", "XX_VisitFrequency", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.XX_VisitMonday", "XX_VisitMonday", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.XX_VisitTuesday", "XX_VisitTuesday", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.XX_VisitWednesday", "XX_VisitWednesday", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.XX_VisitThursday", "XX_VisitThursday", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.XX_VisitFriday", "XX_VisitFriday", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.XX_VisitSaturday", "XX_VisitSaturday", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.XX_VisitSunday", "XX_VisitSunday", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.Latitude", "Latitude", "S"));
		m_Columns.add(new ProcessColumnInfo("bpl.Longitude", "Longitude", "S"));
		
		//Visit Planning
		m_Columns.add(new ProcessColumnInfo("mpv.Name", "PlanningVisitName", "S"));
		m_Columns.add(new ProcessColumnInfo("csrv.Value", "PlanningVisitSalesRegionValue", "S"));
		m_Columns.add(new ProcessColumnInfo("mpv.Description", "PlanningVisitDescription", "S"));
		m_Columns.add(new ProcessColumnInfo("mbd.Name", "DayName", "S"));
		m_Columns.add(new ProcessColumnInfo("mpv.SeqNo", "SeqNo", "I"));
		m_Columns.add(new ProcessColumnInfo("mpv.ValidFrom", "ValidFrom", "D"));
		m_Columns.add(new ProcessColumnInfo("mpv.IsValid", "IsValid", "S"));
		m_Columns.add(new ProcessColumnInfo("mbf.Name", "FrequencyName", "S"));
		m_Columns.add(new ProcessColumnInfo("sbpl.Name", "PlaningVisitShipAddressName", "S"));
		m_Columns.add(new ProcessColumnInfo("bbpl.Name", "PlaningVisitBillAddressName", "S"));
		m_Columns.add(new ProcessColumnInfo("orgbp.Value", "OrgValue", "S"));
		
		
		int i = 0;
		for (ProcessColumnInfo column : m_Columns) {
			i+=1;
			sql.append(column.getColumnName()).append(" AS ").append(column.getColumnAlias()).append(" ");
			if (i != m_Columns.size())
				sql.append(", ");
		}

		sql.append("FROM C_BPartner bp ");
		sql.append("INNER JOIN C_BP_Group bpg ON (bp.C_BP_Group_ID = bpg.C_BP_Group_ID) ");
		sql.append("LEFT JOIN AD_User usr ON (bp.SalesRep_ID = usr.AD_User_ID) ");
		sql.append("LEFT JOIN C_BPartner bpp ON (bp.C_BPartner_Parent_ID = bpp.C_BPartner_ID) ");
		sql.append("LEFT JOIN C_InvoiceSchedule cis ON (bp.C_InvoiceSchedule_ID = cis.C_InvoiceSchedule_ID) ");
		sql.append("LEFT JOIN M_PriceList pl ON (bp.M_PriceList_ID = pl.M_PriceList_ID) ");
		sql.append("LEFT JOIN M_DiscountSchema dsch ON (bp.M_DiscountSchema_ID = dsch.M_DiscountSchema_ID) ");
		sql.append("LEFT JOIN C_PaymentTerm cpt ON (bp.C_PaymentTerm_ID = cpt.C_PaymentTerm_ID) ");
		sql.append("LEFT JOIN C_TaxGroup ctg ON (bp.C_TaxGroup_ID = ctg.C_TaxGroup_ID) ");
		sql.append("LEFT JOIN AD_User ucont ON (bp.C_BPartner_ID = ucont.C_BPartner_ID AND ucont.IsActive = 'Y') ");
		sql.append("LEFT JOIN C_Greeting cg ON (ucont.C_Greeting_ID = cg.C_Greeting_ID) ");
		sql.append("LEFT JOIN C_BPartner_Location bpl ON (bp.C_BPartner_ID = bpl.C_BPartner_ID AND bpl.IsActive = 'Y') ");
		sql.append("LEFT JOIN C_Location cl ON (bpl.C_Location_ID = cl.C_Location_ID) ");
		sql.append("LEFT JOIN C_Country cc ON (cl.C_Country_ID = cc.C_Country_ID) ");
		sql.append("LEFT JOIN C_Region cr ON (cl.C_Region_ID = cr.C_Region_ID) ");
		sql.append("LEFT JOIN C_City cct ON (cl.C_City_ID = cct.C_City_ID) ");
		sql.append("LEFT JOIN C_SalesRegion csr ON (bpl.C_SalesRegion_ID = csr.C_SalesRegion_ID) ");
		sql.append("LEFT JOIN XX_MB_PlanningVisit mpv ON (bp.C_BPartner_ID = mpv.C_BPartner_ID AND mpv.IsActive = 'Y') ");
		sql.append("LEFT JOIN C_SalesRegion csrv ON (mpv.C_SalesRegion_ID = csrv.C_SalesRegion_ID) ");
		sql.append("LEFT JOIN XX_MB_Day mbd ON (mpv.XX_MB_Day_ID = mbd.XX_MB_Day_ID) ");
		sql.append("LEFT JOIN XX_MB_Frequency mbf ON (mpv.XX_MB_Frequency_ID = mbf.XX_MB_Frequency_ID) ");
		sql.append("LEFT JOIN C_BPartner_Location sbpl ON (mpv.C_BPartner_Location_ID = sbpl.C_BPartner_Location_ID) ");
		sql.append("LEFT JOIN C_BPartner_Location bbpl ON (mpv.Bill_Location_ID = bbpl.C_BPartner_Location_ID) ");
		sql.append("LEFT JOIN AD_Org orgbp ON (bp.AD_Org_ID = orgbp.AD_Org_ID) ");
		
		
		
		sql.append("WHERE ");
		sql.append("bp.C_BPartner_ID = ? AND bp.IsActive='Y'");
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = DB.prepareStatement(sql.toString(),get_TrxName());
			stmt.setInt(1, m_C_BPartner_ID);
			rs = stmt.executeQuery();
			i = 0;
			while (rs.next()) {
				ArrayList<ProcessColumnInfo> columnValues = new ArrayList<ProcessColumnInfo>();
				for (ProcessColumnInfo column : m_Columns) {
					String value = "";
					if (column.getColumnType().equals("S")) {
						String valueString = rs.getString(column.getColumnAlias());
						if (valueString!=null)
							value = valueString;
					}else if (column.getColumnType().equals("I")) { 
						Integer valueInt = rs.getInt(column.getColumnAlias());
						value = valueInt.toString();
					}else if (column.getColumnType().equals("B")) { 
						BigDecimal valueBD = rs.getBigDecimal(column.getColumnAlias());
						if (valueBD!=null)
							value = valueBD.toString();
					}else if (column.getColumnType().equals("D")) { 
						Timestamp valueTS = rs.getTimestamp(column.getColumnAlias());
						if (valueTS!=null)
							value = valueTS.toString();
					}
					column.setValue(value);
					columnValues.add(new ProcessColumnInfo(column.getColumnName(), column.getColumnAlias(), column.getColumnType(), column.getValue()));
					
				}
				m_Values.add(columnValues);
				
			}
		}
		catch (Exception e) {
			result = e.getMessage();
		}
		finally {
			DB.close(rs, stmt);
			rs=null;
			stmt = null;
		}
		return result;
	}
	
	private void AddRecordRequest(SOAPElement parent,ArrayList<ProcessColumnInfo> columns) throws Exception{
		for (ProcessColumnInfo column : columns) {		
			SOAPElement fieldElement = parent.addChildElement(ELEMENT_Field,STRING_NameSpace);
			fieldElement.setAttribute(ATTRIBUTE_Column, column.getColumnAlias());
			SOAPElement Value_ValueElement = fieldElement.addChildElement(ELEMENT_Value,STRING_NameSpace);
			Value_ValueElement.addTextNode(column.getValue());
		}
	}
}

class ProcessColumnInfo{
	private String ColumnName = null;
	private String ColumnAlias = null;
	private String ColumnType = null;
	private String Value = "";
	
	public ProcessColumnInfo() {
		ColumnName = "";
		ColumnAlias = "";
	}
	
	public ProcessColumnInfo(String ColumnName, String ColumnAlias,String ColumnType) {
		this.ColumnName = ColumnName;
		this.ColumnAlias = ColumnAlias;
		this.ColumnType = ColumnType;
	}
	
	public ProcessColumnInfo(String ColumnName, String ColumnAlias,String ColumnType,String Value) {
		this.ColumnName = ColumnName;
		this.ColumnAlias = ColumnAlias;
		this.ColumnType = ColumnType;
		this.Value = Value;
	}
	
	public String getColumnName() {
		return ColumnName;
	}
	
	public String getColumnAlias() {
		return ColumnAlias;
	}
	
	public String getColumnType() {
		return ColumnType;
	}
	
	public void setValue(String value) {
		Value = value;
	}
	
	public String getValue() {
		return Value;
	}
	@Override
	public String toString() {
		return "ColumnName = " + ColumnName + " | Value = " + Value;
	}
}
