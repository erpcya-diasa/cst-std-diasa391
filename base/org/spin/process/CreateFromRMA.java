/******************************************************************************
 * Product: ADempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 2006-2017 ADempiere Foundation, All Rights Reserved.         *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * or (at your option) any later version.										*
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * or via info@adempiere.net or http://www.adempiere.net/license.html         *
 *****************************************************************************/

package org.spin.process;


import java.math.BigDecimal;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MRMA;
import org.compiere.model.MRMALine;
import org.compiere.util.Env;

/** Generated Process for (Create From RMA)
 *  @author ADempiere (generated) 
 *  @version Release 3.9.1
 */
public class CreateFromRMA extends CreateFromRMAAbstract
{
	@Override
	protected void prepare()
	{
		super.prepare();
	}

	@Override
	protected String doIt() throws Exception
	{
		int bPartnerID = 0;
		int bPartnerLocationId = 0;
		int rmaId = getRMAId();
		MRMA rma = new MRMA(Env.getCtx(), rmaId, get_TrxName());
		MInOut io = null;

		for(int key : getSelectionKeys()) {
			int inOutLineId = getSelectionAsInt(key,"RMA_M_InOutLine_ID");
			BigDecimal movementQty = getSelectionAsBigDecimal(key, "RMA_MovementQty");
			MInOutLine iol = new MInOutLine(Env.getCtx(), inOutLineId , get_TrxName());

			MRMALine rmaLine = new MRMALine(rma.getCtx(), 0, rma.get_TrxName());
			rmaLine.setM_RMA_ID(rmaId);
			rmaLine.setM_InOutLine_ID(rma.getInOut_ID());
			rmaLine.setM_InOutLine_ID(getSelectionAsInt(key,"RMA_M_InOutLine_ID"));
			rmaLine.setM_InOutLine_ID(inOutLineId);
			rmaLine.set_ValueOfColumn("M_RMAType_ID", rma.get_Value("M_RMAType_ID"));
			rmaLine.set_ValueOfColumn("M_Locator_ID", rma.get_Value("M_Locator_ID"));
			rmaLine.set_ValueOfColumn("C_UOM_ID", iol.getC_UOM_ID());
			if (movementQty.compareTo(iol.getQtyEntered()) <= 0) 	
				rmaLine.setQty(movementQty);
			else 
				throw new AdempiereException("@QtyEntered@ < @MovementQty@");
					
			rmaLine.set_ValueOfColumn("QtyEntered", iol.getQtyEntered());
			rmaLine.setAD_Org_ID(rma.getAD_Org_ID());
			rmaLine.saveEx();
			
			if (io==null)
				io = (MInOut) iol.getM_InOut();
			
			if (io!=null
					&& io.getM_InOut_ID() > 0) { 
				bPartnerID = io.getC_BPartner_ID();
				bPartnerLocationId = io.getC_BPartner_Location_ID();
			}
		}
		if (rma.getC_BPartner_ID()==0
				&& bPartnerID != 0)
			rma.setC_BPartner_ID(bPartnerID);

		if (rma.get_ValueAsInt("C_BPartner_Location_ID")==0 
				&& bPartnerLocationId!=0)
			rma.set_ValueOfColumn("C_BPartner_Location_ID", bPartnerLocationId);

		if (rma.is_Changed())
			rma.saveEx();

		return "@OK@";
	}
}