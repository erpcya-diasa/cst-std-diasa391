package org.spin.process;

import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.sg.model.MXXLoadOrder;

public class CreateMovementInFromOut extends SvrProcess{

	int p_Record_ID = 0;
	MXXLoadOrder m_lOrder = null;
	private String p_DocAction = null;
	@Override
	protected void prepare() {
		
		p_Record_ID = getRecord_ID();
		m_lOrder = new MXXLoadOrder(getCtx(), p_Record_ID, get_TrxName());
		
		ProcessInfoParameter[] params = getParameter();
		
		for (ProcessInfoParameter para : params) {
			if (para.getParameterName().equals(MMovement.COLUMNNAME_DocAction))
				p_DocAction = para.getParameter() != null ? para.getParameter().toString() : null;
		}
	}

	@Override
	protected String doIt() throws Exception {
		String result = "";
		if (m_lOrder.getM_MovementPalletIn_ID()==0) {
			if (m_lOrder.getXX_LoadOrder_ID()!=0) {
				MMovement mOut = (MMovement) m_lOrder.getM_MovementPalletOut();
				if (mOut.getM_Movement_ID()!=0
						&& (mOut.getDocStatus().equals(MMovement.DOCSTATUS_Completed)
							|| mOut.getDocStatus().equals(MMovement.DOCSTATUS_Closed))) {
					MMovement mIn = new MMovement(getCtx(), 0, get_TrxName());
					MMovement.copyValues(mOut, mIn);
					mIn.setDocumentNo("");
					mIn.setProcessed(false);
					mIn.setDocStatus(MMovement.DOCSTATUS_Drafted);
					mIn.setDocAction(MMovement.DOCACTION_Complete);
					mIn.save();
					
					m_lOrder.setM_MovementPalletIn_ID(mIn.getM_Movement_ID());
					m_lOrder.save();
					
					for (MMovementLine mLineOut : mOut.getLines(true)) {
						MMovementLine mLineIn = new MMovementLine(mIn);
						mLineIn.setM_Product_ID(mLineOut.getM_Product_ID());
						mLineIn.setMovementQty(mLineOut.getMovementQty());
						mLineIn.setM_LocatorTo_ID(mLineOut.getM_Locator_ID());
						mLineIn.setM_Locator_ID(mLineOut.getM_LocatorTo_ID());
						mLineIn.setM_AttributeSetInstanceTo_ID(mLineOut.getM_AttributeSetInstance_ID());
						mLineIn.setM_AttributeSetInstance_ID(mLineOut.getM_AttributeSetInstanceTo_ID());
						mLineIn.save();
					}
					
					if (p_DocAction!=null) {
						mIn.processIt(p_DocAction);
						mIn.save();
					}
					
					
				}else
					result = "@Invalid@ @M_MovementPalletOut_ID@";
			}else
				result = "@NotFound@ @XX_LoadOrder_ID@";
		}else
			result = "@M_MovementPalletIn_ID@ @Generated@";
		return result;
	}

}
