package org.spin.process;

import org.compiere.model.MMovement;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.sg.model.MXXLoadOrder;

public class CompletedAnnulledMovement extends SvrProcess {

	private int p_M_Movement_ID = 0;
	private String p_DocAction = null;
	private MXXLoadOrder lOrder = null;
	private boolean isInput = false;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] params = getParameter();
		
		lOrder = new MXXLoadOrder(getCtx(), getRecord_ID(), get_TrxName());
		isInput = lOrder.getM_MovementPalletIn_ID()!=0;
		
		for (ProcessInfoParameter para : params) {
			if ((para.getParameterName().equals(MXXLoadOrder.COLUMNNAME_M_MovementPalletOut_ID) && !isInput)
					|| (para.getParameterName().equals(MXXLoadOrder.COLUMNNAME_M_MovementPalletIn_ID) && isInput))
				p_M_Movement_ID = para.getParameterAsInt();
			else if ((para.getParameterName().equals(MMovement.COLUMNNAME_DocAction + "Out") && !isInput)
						|| (para.getParameterName().equals(MMovement.COLUMNNAME_DocAction + "In") && isInput))
				p_DocAction = para.getParameter().toString();
		}
		
	}

	@Override
	protected String doIt() throws Exception {
		String result = "";
		if (p_M_Movement_ID!=0) {
			if (p_DocAction!=null
					&& !p_DocAction.isEmpty()) {
				
				MMovement mov = new MMovement(getCtx(), p_M_Movement_ID, get_TrxName());
				mov.setDocAction(p_DocAction);
				mov.processIt(p_DocAction);
				mov.save();
				result = "@M_Movement_ID@ = " + mov.getDocumentNo() + " -> " + mov.getDocStatus();
				
			}
			else 
				result = "@Invalid@ @DocAction@";
		}else
			result = "@Invalid@ @M_Movement_ID@";
		
		
		return result;
	}

}
