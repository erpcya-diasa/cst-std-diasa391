package org.spin.process;

import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.ProcessUtil;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MProcess;
import org.compiere.model.MRMA;
import org.compiere.model.MRMALine;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

public class GenerateReturnFromRMA extends SvrProcess {

	private int m_M_RMA_ID = 0;
	private String p_DocAction = null;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] pInfo = getParameter();
		
		for (ProcessInfoParameter param : pInfo) {
			if (param.getParameterName().equals("DocAction"))
				p_DocAction = param.getParameter().toString(); 
			
		}
		
		m_M_RMA_ID = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {
		MRMA rma = new MRMA(getCtx(), m_M_RMA_ID, get_TrxName());
		MDocType dt = null;
		MInOut ioReturn = new MInOut(getCtx(), 0, get_TrxName());
		MInOut ioSource = null;
		Timestamp dateDoc = null;
		MInOutLine iolSource = null;
		String result = "";
		
		if (rma.getM_RMA_ID()!=0) {
			
			if (!rma.getDocStatus().equals(MRMA.DOCSTATUS_Completed))
				throw new AdempiereException("@Invalid@ @DocStatus@");
			
			
			if (rma.getC_DocType_ID()!=0) 
				dt = (MDocType) rma.getC_DocType().getC_DocTypeShipment();
			else
				dt = new MDocType(getCtx(), MDocType.getDocType((rma.isSOTrx() ? MInOut.MOVEMENTTYPE_CustomerReturns : MInOut.MOVEMENTTYPE_VendorReturns)),get_TrxName());
			
			if (dt==null
					|| dt.getC_DocType_ID()==0)
				throw new AdempiereException("@Invalid@ @C_DocType_ID@");
			
			MInOut ioRef = new Query(getCtx(), MInOut.Table_Name, "M_RMA_ID=? AND DocStatus NOT IN ('VO','RE')", get_TrxName())
					.setParameters(rma.getM_RMA_ID())
					.first();
			if (ioRef!=null)
				throw new AdempiereException("@Created@ @M_InOut_ID@ " + ioRef.getDocumentNo());

			if (rma.get_Value("DateDoc")!=null) 
				dateDoc = (Timestamp)rma.get_Value("DateDoc");
			else
				dateDoc = Env.getContextAsDate(getCtx(), "#Date");
			
			if (rma.getInOut_ID()!=0)
				ioSource = (MInOut) rma.getInOut();
			
			MRMALine[] rmaLines = rma.getLines(true);
			
			if (rmaLines.length==0)
				throw new AdempiereException("@NotFound@ @M_RMALine_ID@");
			
			for (MRMALine rmaLine : rmaLines) {
				if (ioSource==null) {
					if (rmaLine.getM_InOutLine_ID()!=0) {
						iolSource = (MInOutLine)rmaLine.getM_InOutLine();
						ioSource =  (MInOut) iolSource.getM_InOut();
					}
				}
				
				if (ioReturn.getM_InOut_ID()==0) {
					iolSource = (MInOutLine)rmaLine.getM_InOutLine();
					MInOut.copyValues(ioSource, ioReturn);
					ioReturn.setMovementType((rma.isSOTrx() ? MInOut.MOVEMENTTYPE_CustomerReturns : MInOut.MOVEMENTTYPE_VendorReturns));
					ioReturn.setM_RMA_ID(rma.getM_RMA_ID());
					ioReturn.set_ValueOfColumn("M_RMAType_ID", rma.getM_RMAType_ID());
					ioReturn.setC_Order_ID(0);
					ioReturn.setMovementDate(dateDoc);
					ioReturn.setDateReceived(dateDoc);
					ioReturn.setDateAcct(dateDoc);
					ioReturn.setDocStatus(MInOut.DOCSTATUS_Drafted);
					ioReturn.setProcessed(false);
					ioReturn.setDocumentNo(null);
					ioReturn.setDescription(rma.getDescription());
					ioReturn.setDocAction(MInOut.DOCACTION_Complete);
					ioReturn.set_ValueOfColumn("IsGenerateReturn", rma.get_Value("IsGenerateReturn"));
					ioReturn.setPOReference(rma.get_ValueAsString("POReference"));
					ioReturn.setC_BPartner_Location_ID(rma.get_ValueAsInt("C_BPartner_Location_ID"));
					ioReturn.set_ValueOfColumn("TotalWeight", rma.get_Value("TotalWeight"));
					
					if (rma.get_ValueAsInt("M_Warehouse_ID")!=0) 
						ioReturn.setM_Warehouse_ID(rma.get_ValueAsInt("M_Warehouse_ID"));
					
					if (ioReturn.save()) {
						result = "@Created@ @M_InOut_ID@ " + ioReturn.getDocumentNo();
						rma.set_ValueOfColumn("M_InOut_ID", ioReturn.getM_InOut_ID());
						rma.save();
					}
				}
				if (ioSource==null) 
					throw new AdempiereException("@NotFound@ @M_InOut_ID@");
					
				
				if (ioReturn.getM_InOut_ID()!=0) {
					MInOutLine iolReturn = new MInOutLine(ioReturn);
					iolReturn.setM_Product_ID(rmaLine.getM_Product_ID(),true);
					iolReturn.setC_Charge_ID(rmaLine.getC_Charge_ID());
					iolReturn.setM_RMALine_ID(rmaLine.getM_RMALine_ID());
					iolReturn.set_ValueOfColumn("M_RMAType_ID", rmaLine.get_Value("M_RMAType_ID"));
					if (rmaLine.get_ValueAsInt("M_Locator_ID")!=0)
						iolReturn.setM_Locator_ID(rmaLine.get_ValueAsInt("M_Locator_ID"));
					
					iolReturn.setQty(rmaLine.getQty());
					
					if (iolSource!=null) {
						iolReturn.setC_UOM_ID(iolSource.getC_UOM_ID());
						iolReturn.setM_AttributeSetInstance_ID(iolSource.getM_AttributeSetInstance_ID());
						
						if (iolReturn.getM_Locator_ID()==0
								&& ioReturn.getM_Warehouse_ID()==ioSource.getM_Warehouse_ID())
								iolReturn.setM_Locator_ID(iolSource.getM_Locator_ID());
					}
					
					iolReturn.save();
				}
				
			}
			
			if (p_DocAction!=null) {
				ProcessInfo pi_InOut = new ProcessInfo (getProcessInfo().getTitle(), 109,MInOut.Table_ID,ioReturn.getM_InOut_ID());
				pi_InOut.setTransactionName(get_TrxName());
				MProcess pr = new MProcess(getCtx(), pi_InOut.getAD_Process_ID(), get_TrxName());
				pi_InOut.setAD_PInstance_ID(getAD_PInstance_ID());
				pi_InOut.setAD_User_ID(getAD_User_ID());
				pi_InOut.setAD_Client_ID(getAD_Client_ID());
				ProcessUtil.startWorkFlow(getCtx(), pi_InOut, pr.getAD_Workflow_ID());
			}
			
			
		}
		
		return result;
	}

}
