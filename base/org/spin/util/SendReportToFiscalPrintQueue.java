/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2015 E.R.P. Consultores y Asociados, C.A.               *
 * All Rights Reserved.                                                       *
 * Contributor(s): Yamel Senih www.erpya.com                                  *
 *****************************************************************************/
package org.spin.util;

import java.io.File;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.Env;
import org.compiere.util.Util;
import org.spin.model.MADAppRegistration;
import org.spin.util.support.AppSupportHandler;
import org.spin.util.support.IAppSupport;
import org.spin.util.support.mq.AbstractMessageQueue;
import org.spin.util.support.mq.PrinterMessage;

/**
 * 	@author Yamel Senih, ysenih@erpcya.com, ERPCyA http://www.erpcya.com
 * 		<a href="https://github.com/adempiere/adempiere/issues/1400">
 * 		@see FR [ 1400 ] Dynamic report export</a>
 */
public class SendReportToFiscalPrintQueue {
	
	/**
	 * Send file to Remote printer
	 * @author Yamel Senih, ysenih@erpya.com, ERPCyA http://www.erpya.com
	 * @param appRegisteredId
	 * @param file
	 * @throws Exception
	 * @return void
	 */
	public static void sendFile(int appRegisteredId, File file) {
		try {
			if(appRegisteredId <= 0) {
				throw new AdempiereException("@AD_AppRegistration_ID@ @NotFound@");
			}
			if(file == null
					|| !file.exists()) {
				throw new AdempiereException("@File@ @NotFound@");
			}
			if(file.isDirectory()) {
				throw new AdempiereException("@File@ @Invalid@");
			}
			MADAppRegistration registeredApplication = MADAppRegistration.getById(Env.getCtx(), appRegisteredId, null);
			IAppSupport supportedApplication = AppSupportHandler.getInstance().getAppSupport(registeredApplication);
			//	Exists a Application available for it?
			if(supportedApplication != null
					&& AbstractMessageQueue.class.isAssignableFrom(supportedApplication.getClass())) {
				AbstractMessageQueue messageQueue = (AbstractMessageQueue) supportedApplication;
				//	Send message
				PrinterMessage messageToPrint = new PrinterMessage(file);
				String channel = registeredApplication.getParameterValue("TargetChannel");
				if(Util.isEmpty(channel)) {
					throw new AdempiereException("@PrintChannel@ @NotFound@");
				}
				messageQueue.connect();
				messageQueue.publish(channel, messageToPrint);
				messageQueue.disconnect();
			}
		} catch (Exception e) {
			throw new AdempiereException(e);
		}
	}
}	//	AbstractBatchImport
