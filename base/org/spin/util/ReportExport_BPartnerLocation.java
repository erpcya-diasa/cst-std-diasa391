/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2015 E.R.P. Consultores y Asociados, C.A.               *
 * All Rights Reserved.                                                       *
 * Contributor(s): Carlos Parada www.erpcya.com                               *
 *****************************************************************************/
package org.spin.util;

import java.util.Properties;

import org.compiere.print.ReportEngine;

/**
 * @author Carlos Parada, cparada@erpcya.com
 * <li> FR [ 6 ] Add Support to Marker KML Export 
 * @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/6/soporte-para-generar-archivos-con
 */
public class ReportExport_BPartnerLocation  extends ReportExport{

	public ReportExport_BPartnerLocation(Properties ctx, ReportEngine reportEngine) {
		super(ctx, reportEngine);
		addExportFormat(new BPartnerLocationToXML(ctx, reportEngine));
		// TODO Auto-generated constructor stub
	}

}
