/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2015 E.R.P. Consultores y Asociados, C.A.               *
 * All Rights Reserved.                                                       *
 * Contributor(s): Carlos Parada www.erpcya.com                               *
 *****************************************************************************/
package org.spin.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.MSysConfig;
import org.compiere.print.MPrintFormat;
import org.compiere.print.MPrintFormatItem;
import org.compiere.print.PrintData;
import org.compiere.print.PrintDataElement;
import org.compiere.print.ReportEngine;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

/**
 * @author Carlos Parada, cparada@erpcya.com
 * <li> FR [ 6 ] Add Support to Marker KML Export 
 * @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/6/soporte-para-generar-archivos-con
 */
public class BPartnerLocationToXML extends ExportFormatXML{
	
	//Static Logger
	private static CLogger	log	= CLogger.getCLogger (BPartnerLocationToXML.class);
	private static String TAB ="\t";
	private static String HEADER = "<?xml version='1.0' encoding='UTF-8' ?>" +
									Env.NL +
									"<kml xmlns=\"http://www.opengis.net/kml/2.2\">" +
									Env.NL +
									TAB + "<Document>" +
									Env.NL ;
	private static String FOOTER =  TAB + TAB + "</Folder>" +
									Env.NL +
									TAB + "</Document>" +
									Env.NL +
									"</kml>";
	private String currentGrupName = "";
	
	private String defaultColor = MSysConfig.getValue("KML_DEFAULT_COLOR","ff00b371");
	
	private static String FOLDER_PREFFIX = "mm_folder_";
	
	private HashMap<Integer, String> currentgroupValue = new HashMap<Integer, String>();
	

	/**
	 * Constructor 
	 * @param ctx
	 * @param reportEngine
	 */
	public BPartnerLocationToXML(Properties ctx, ReportEngine reportEngine) {
		super(ctx, reportEngine);
	}
	
	/**
	 * Extension
	 */
	@Override
	public String getExtension() {
		return "kml";
	}
	
	/**
	 * Name
	 */
	@Override
	public String getName() {
		return Msg.translate(getLanguage(), "KML_NAME");
	}
	
	/**
	 * Export
	 */
	@Override
	public boolean exportToFile(File file) {
		if (getReportEngine() == null 
				&& getCtx() ==null)
			return false;
		else
			return createKML(convertFile(file), getReportEngine());
	}

	/**
	 * Create KML File
	 * @param writer
	 * @param engine
	 * @return
	 * @return boolean
	 */
	public boolean createKML(Writer writer,ReportEngine engine) {
		try{
			
			PrintData reportData = (engine!=null ? engine.getPrintData() : getPrintData());
			MPrintFormat format = (engine != null? engine.getPrintFormat(): getPrintFormat());
			int level = 2;
			int seq = 0;
			boolean dataFile = false; 
			currentGrupName = "";
			String groupName = "";
			String currentFolderColor = defaultColor;
			String currentMarkerColor = defaultColor;
			
			for (int i=0; i< reportData.getRowCount(); i++){
				reportData.setRowIndex(i);
				if (!reportData.isFunctionRow()) {
					String data = "";
					HashMap<Integer, String> newgroupValue = new HashMap<Integer, String>();
					List<ItemData> items = new ArrayList<ItemData>();
					
					for (int col = 0; col < format.getItemCount(); col++) {
						MPrintFormatItem item = format.getItem(col);
						int colID =0;
						String colTag = "";
						String colValue = "";
						String colName = "";
						int displayType = 0;
						if (!item.isPrinted() ||
								item.getPrintFormatType().equals(MPrintFormatItem.PRINTFORMATTYPE_PrintFormat)
									||item.getPrintFormatType().equals(MPrintFormatItem.PRINTFORMATTYPE_Line)
										||item.getPrintFormatType().equals(MPrintFormatItem.PRINTFORMATTYPE_Rectangle)
											|| item.getPrintFormatType().equals(MPrintFormatItem.PRINTFORMATTYPE_Image))
								continue;
						else{
							colID = item.getAD_PrintFormatItem_ID();
							colName = item.getName();
							colTag = item.getPrintName(getLanguage());
							colValue = "";
							Object obj = reportData.getNode(new Integer(item.getAD_Column_ID()));
							if (obj == null) {
								if(item.getPrintFormatType().equals(MPrintFormatItem.PRINTFORMATTYPE_Text)) {
									if(!Util.isEmpty(item.getPrintName())) {
										colValue = item.getPrintName() + (!Util.isEmpty(item.getPrintNameSuffix())? item.getPrintNameSuffix(): "");
										colTag = item.getName(); 
									}
								}
							}
							else if (obj instanceof PrintDataElement) {
								PrintDataElement pde = (PrintDataElement)obj;
								if (pde.getDisplayType()==DisplayType.Date || pde.getDisplayType()==DisplayType.DateTime) {
									if (pde.getValue()!=null)
										colValue = String.valueOf(((Timestamp)pde.getValue()).getTime());
									else 
										colValue = "-1";
								}else
									colValue = pde.getValueDisplay(getLanguage());
								
								displayType = pde.getDisplayType();
							}
							
						}
						ItemData itemData = new ItemData(colID,colName, colTag, colValue, displayType);
						if (!itemData.isValidData())
							continue;
						
						if (itemData.isStatic()){
							if (itemData.getName().equals(ItemData.STATIC_FIELDS[ItemData.POS_FOLDERCOLOR]))
								currentFolderColor = itemData.getItemValue();
							if (itemData.getName().equals(ItemData.STATIC_FIELDS[ItemData.POS_MARKERCOLOR]))
								currentMarkerColor = itemData.getItemValue();
						}
								
						
						items.add(itemData);
						if (item.isGroupBy())
							newgroupValue.put(item.getAD_PrintFormatItem_ID(), colValue);
					}
					if (!dataFile)
						writer.write(HEADER);
					
					groupName = getGroupName(newgroupValue);
					
					if (!groupName.equals(currentGrupName)){
						if (!currentGrupName.equals("")){
							 level -= 1;
							 data += TAB + TAB + "</Folder>" + Env.NL;
						}
						currentGrupName = groupName;
						seq +=1;
						data+=getStyle(FOLDER_PREFFIX + currentGrupName ,seq, currentFolderColor, level);
						//Start Folder 
						data+=getFolder(currentGrupName, seq, level, items);
						level +=1;
					}
					
					//Set Style of Marker
					data+=getStyle("mm_marker_" + currentGrupName ,i, currentMarkerColor, level);
					
					data+=getItemsData("mm_marker_" + currentGrupName, i, level, items);
					
					writer.write(data);
					writer.write(Env.NL);
					dataFile = true;
				}
			}
			if (dataFile){
				writer.write(FOOTER);
			}
			
			writer.flush();
		}catch (Exception e){
			log.log(Level.WARNING, e.getMessage());
		}
		return true;
	}
	
	/**
	 * Get Style for Marker or Folder 
	 * @param name
	 * @param id
	 * @param color
	 * @param pos
	 * @return
	 * @return String
	 */
	private String getStyle(String name,int id,String color,int pos){
		String tabs= getTabs(pos);

		return tabs + "<Style id=\""+name.replace(" ", "_") + "_" + id+"\"> " +
				Env.NL +
				tabs + TAB + "<IconStyle>" +
				Env.NL +
				tabs + TAB + TAB + "<color>"+color+"</color>" +
				Env.NL +
				tabs + TAB + TAB + "<colorMode>normal</colorMode>" +
				Env.NL +
				tabs + TAB + TAB + "<scale>1</scale>" +
				Env.NL +
				tabs + TAB + "</IconStyle>" +
				Env.NL +
				tabs + TAB + "<LineStyle>" +
				Env.NL +
				tabs + TAB + TAB + "<color>"+color+"</color>" +
				Env.NL +
				tabs + TAB + "</LineStyle>" +
				Env.NL +
				tabs + TAB + "<PolyStyle>" +
				Env.NL+
				tabs + TAB + TAB + "<color>"+color+"</color>" +
				Env.NL +
				tabs + TAB + "</PolyStyle>" +
				Env.NL +
				tabs + "</Style>"+
				Env.NL;
	}
	
	/**
	 * Get Folder 
	 * @param name
	 * @param id
	 * @param pos
	 * @param items
	 * @return
	 * @return String
	 */
	private String getFolder(String name, int id ,int pos, List<ItemData> items){
		
		String tabs= getTabs(pos);
		String retValue = "";
		int itemsCount = items.size(); 
		
		retValue+= tabs + "<Folder id=\"" + id + "\"> " +
				Env.NL +
				tabs + TAB + "<styleUrl>#" + FOLDER_PREFFIX + name.replace(" ", "_") + "_" + id + "</styleUrl>" +
				Env.NL +
				tabs + TAB + "<name>"+ name + "</name>" +
				Env.NL +
				tabs + TAB + "<TimeStamp>" +
				Env.NL +
				tabs + TAB + TAB + "<when>" + Env.getContext(getCtx(), "#Date") + "</when>" +
				Env.NL +
				tabs + TAB + "</TimeStamp>" +
				Env.NL +
				tabs + TAB + "<ExtendedData>" +
				Env.NL +
				tabs + TAB + TAB + "<Data name=\"com_exlyo_mapmarker_piniconcode\">" +
				Env.NL +
				tabs + TAB + TAB + TAB + "<value>-1</value>" +
				Env.NL +
				tabs + TAB + TAB + "</Data>" +
				Env.NL +
				tabs + TAB + TAB + "<Data name=\"com_exlyo_mapmarker_customfields\">" +
				Env.NL +
				tabs + TAB + TAB + TAB + "<value>[" +
				Env.NL;
		
		for (ItemData itemData : items) {
			itemsCount -=1;
			String itemType = "";
			String itemValue = "";
			if (!itemData.isStatic()){
				if (itemData.isValidData()){
					itemType = "\"field_type\": \"" + itemData.getFieldType("Type") +"\",";
					itemValue= "\"" + itemData.getFieldType("Structure") + "\": " + itemData.getFieldType("Default");
				}else{
					log.warning("@Not@ @DisplayType@ Support");
					continue;
				}
			}
			else
				continue;
			
			retValue += tabs + TAB + TAB + TAB + TAB + "{" +
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB +"\"base_params\": {" +
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB + TAB +"\"id\": \"" + itemData.getItemID() + "\"," +
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB + TAB + itemType +
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB + TAB + "\"is_enabled\": true," +
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB + TAB + "\"name\": \"" + itemData.getItemLabel() + "\"" +
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB +"}," + 
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB +"\"value\": {" +
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB + TAB + itemValue +
						Env.NL +
						tabs + TAB + TAB + TAB + TAB + TAB +"}" + 
						Env.NL +
						tabs + TAB + TAB + TAB + TAB +"}" ;
			
			if (itemsCount!=0)
				retValue +="," + 
						Env.NL;
		}
		
		retValue +=Env.NL +
				tabs + TAB + TAB + TAB + "]" +
				Env.NL +
				tabs + TAB + TAB + TAB + "</value>" +
				Env.NL +
				tabs + TAB + TAB + "</Data>" +
				Env.NL +
				tabs + TAB + "</ExtendedData>" +
				Env.NL ;
			
		
		return retValue;
	}
	
	/**
	 * Get Markers 
	 * @param name
	 * @param id
	 * @param pos
	 * @param items
	 * @return
	 * @return String
	 */
	private String getItemsData(String name, int id,int pos, List<ItemData> items){
		String retValue = "";
		String varItems = "";
		String tabs= getTabs(pos);
		
		HashMap<String,String> staticItems = new HashMap<String, String>();
		
		for (ItemData itemData : items) {
			if (itemData.isStatic())
				staticItems.put(itemData.getName(), itemData.getItemValue());
			else{
				if (itemData.isValidData()){
					if (!varItems.equals(""))
						varItems+="," + Env.NL;	
					
					varItems+=tabs + TAB + TAB + TAB + TAB + "{" +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + "\"base_params\": {" +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + "\"id\": \"" + itemData.getItemID() + "\"," +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + "\"field_type\": \"" + itemData.getFieldType("Type") +"\"," +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + "\"is_enabled\": " + (itemData.isEnabled() ? "true" : "false") + "," +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + "\"name\": \"" + itemData.getItemLabel() + "\"" +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + "}," +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + "\"value\": {" +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + TAB + "\"" + itemData.getFieldType("Structure") + "\": " + itemData.getDelimiter() +  itemData.getItemValue() + itemData.getDelimiter() +
							Env.NL +
							tabs + TAB + TAB + TAB + TAB + "}" +
							Env.NL +
							tabs + TAB + TAB + TAB  + "}" 
							;
				}else{
					log.warning("@Not@ @DisplayType@ Support");
					continue;
				}
					
			}
				
		}
		
		retValue += tabs + "<Placemark id=\"" + id + "\">"+
					Env.NL +
					tabs + TAB + "<name>" + staticItems.get(ItemData.STATIC_FIELDS[ItemData.POS_NAME]) + "</name>"+
					Env.NL +
					tabs + TAB + "<description>" + staticItems.get(ItemData.STATIC_FIELDS[ItemData.POS_DESCRIPTION]) + "</description>"+
					Env.NL +
					tabs + TAB + "<phoneNumber>" + staticItems.get(ItemData.STATIC_FIELDS[ItemData.POS_PHONE]) + "</phoneNumber>"+
					Env.NL +
					tabs + TAB + "<Point>" +
					Env.NL +
					tabs + TAB + TAB + "<coordinates>" + staticItems.get(ItemData.STATIC_FIELDS[ItemData.POS_LONGITUDE]) + "," + staticItems.get(ItemData.STATIC_FIELDS[ItemData.POS_LATITUDE]) + "</coordinates> " +
					Env.NL +
					tabs + TAB + "</Point>" +
					Env.NL +
					tabs + TAB + "<TimeStamp>" +
					Env.NL +
					tabs + TAB + TAB + "<when>" + Env.getContext(getCtx(), "#Date") + "</when>" +
					Env.NL +
					tabs + TAB + "</TimeStamp>" +
					Env.NL +
					tabs + TAB + "<ExtendedData>" +
					Env.NL +
					tabs + TAB + TAB + "<Data name=\"com_exlyo_mapmarker_piniconcode\">" +
					Env.NL +
					tabs + TAB + TAB + TAB + "<value>-1</value>" +
					Env.NL +
					tabs + TAB + TAB + "</Data>" +
					Env.NL +
					tabs + TAB + TAB + "<Data name=\"com_exlyo_mapmarker_customfields\">" +
					Env.NL +
					tabs + TAB + TAB + TAB + "<value>[" +
					Env.NL +
					varItems +
					Env.NL +
					tabs + TAB + TAB + TAB + "]</value>" +
					Env.NL +
					tabs + TAB + TAB + "</Data>" +
					Env.NL +
					tabs + TAB + "</ExtendedData>" +
					Env.NL +
					tabs + TAB + "<styleUrl>#"+ name.replace(" ", "_") + "_" + id + "</styleUrl>" +
					Env.NL +
					tabs + "</Placemark>" ;
		
		return retValue;
	}
	
	/**
	 * Get Tabs 
	 * @param tabsNo
	 * @return
	 * @return String
	 */
	private String getTabs(int tabsNo){
		StringBuffer retValue = new StringBuffer();
		
		for (int i=0;i<tabsNo;i++)
			retValue.append(TAB);
		
		return retValue.toString();
	}
	
	/**
	 * Get Group Name 
	 * @param newGroup
	 * @return
	 * @return String
	 */
	private String getGroupName(HashMap<Integer, String> newGroup){
		String retValue = "";
		if (newGroup.size()==0)
			retValue= "No Group";
		else{
			for (Map.Entry<Integer, String> itemGroup: newGroup.entrySet()){
				currentgroupValue.put(itemGroup.getKey(), itemGroup.getValue());
				retValue+=(!retValue.equals("") ? "_" : "") + itemGroup.getValue();
			}
		}	
		return retValue;
	}
	
	public BufferedWriter convertFile(File file) {
		Writer fileWriter = null;
		try {
			fileWriter = new OutputStreamWriter(new FileOutputStream(file, false),"UTF-8");
		} catch (FileNotFoundException e) {
			log.log(Level.SEVERE, e.getLocalizedMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		//	Validate null
		if(fileWriter == null) { 
			return null;
		}
		return new BufferedWriter(fileWriter);
	}
}

/**
 * Item Data
 * 
 */
class ItemData{
	
	private int itemID=0;
	private String itemLabel;
	private String itemValue;
	private int displayType = 0;
	private boolean isStatic = false;
	private boolean isValidData = false;
	private boolean enabled = true;
	private String name;
	
	public static int POS_NAME = 0;
	public static int POS_DESCRIPTION = 1;
	public static int POS_PHONE = 2;
	public static int POS_LATITUDE = 3;
	public static int POS_LONGITUDE = 4;
	public static int POS_FOLDERCOLOR = 5;
	public static int POS_MARKERCOLOR = 6;
	
	public static String[] STATIC_FIELDS = new String[]{"Name", "Description", "Phone", "Latitude", "Longitude", "FolderColor", "MarkerColor"};
												;
	/**
	 * Constructor
	 * @param itemID
	 * @param name
	 * @param itemLabel
	 * @param itemValue
	 * @param displayType
	 */
	public ItemData(int itemID,String name,String itemLabel,String itemValue, int displayType) {
		this.itemID=itemID;
		this.itemLabel=(itemLabel!=null ? itemLabel.replace("&", "") : "");
		this.itemValue=(itemValue!=null ? itemValue.replace("&", "") : "");
		this.displayType=displayType;
		this.name = name;
		setStatic();
		setValidData();
	}
	
	/**
	 * Get Item ID 
	 * @return
	 * @return int
	 */
	public int getItemID() {
		return itemID;
	}
	
	/**
	 * Get Item Label 
	 * @return
	 * @return String
	 */
	public String getItemLabel() {
		return itemLabel;
	}
	
	/**
	 * Get Item Value
	 * @return
	 * @return String
	 */
	public String getItemValue() {
		return itemValue;
	}
	
	/**
	 * Get Display Type 
	 * @return
	 * @return int
	 */
	public int getDisplayType() {
		return displayType;
	}
	
	/**
	 * Check is Static 
	 * @return
	 * @return boolean
	 */
	public boolean isStatic() {
		return isStatic;
	}
	
	/**
	 * Set Static 
	 * @return void
	 */
	private void setStatic() {
		for (String columnName : STATIC_FIELDS) {
			if (columnName.equals(getName()))
				isStatic = true;
		}
	}
	
	/**
	 * get Field Type 
	 * @param type
	 * @return
	 * @return String
	 */
	public String getFieldType(String type){
		String retValue = null;
		 if (isBoolean()){
			 if (type.equals("Type"))
				 retValue = "Boolean";
			 if (type.equals("Structure"))
				 retValue = "checked_value";
			 if (type.equals("Default"))
				 retValue = "false";
		 }else if (isDate()){
			 if (type.equals("Type"))
				 retValue = "Date";
			 if (type.equals("Structure"))
				 retValue = "date_value";
			 if (type.equals("Default"))
				 retValue = "-1";
		 }else if (isText()){
			 if (type.equals("Type"))
				 retValue = "FreeText";
			 if (type.equals("Structure"))
				 retValue = "text_value";
			 if (type.equals("Default"))
				 retValue = "\"\"";
		 } 
		return retValue; 
	}
	
	/**
	 * Check Valid Data 
	 * @return
	 * @return boolean
	 */
	public boolean isValidData() {
		return isValidData;
	}
	
	/**
	 * set Valid Data
	 * @return void
	 */
	public void setValidData() {
		isValidData = isText() || isBoolean() || isDate();
	}
	
	/**
	 * Set Enabled
	 * @return
	 * @return boolean
	 */
	public boolean isEnabled() {
		return enabled;
	}
	
	/**
	 * Get Delimiter
	 * @return
	 * @return String
	 */
	public String getDelimiter(){
		return (isText() ? "\"" : "" );
	}
	
	/**
	 * Get Name
	 * @return
	 * @return String
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Check is Date
	 * @return
	 * @return boolean
	 */
	private boolean isDate(){
	    return (getDisplayType() == DisplayType.Date) 
	    			|| (getDisplayType() == DisplayType.DateTime);
	}
	
	/**
	 * Check is Text
	 * @return
	 * @return boolean
	 */
	private boolean isText(){
	    return !isDate() && !isBoolean();
	}
	
	/**
	 * Check is Boolean
	 * @return
	 * @return boolean
	 */
	private boolean isBoolean(){
	    return getDisplayType() == DisplayType.YesNo;
	}
	  
	
	@Override
	public String toString() {
		return "{ID: " + getItemID() 
				+"\nLabel: " + getItemLabel()
				+"\nValue: " + getItemValue()
				+"\nStatic: " + isStatic() 
				+ "}";
	}
	
	
}