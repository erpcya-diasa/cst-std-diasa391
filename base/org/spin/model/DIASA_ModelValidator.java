package org.spin.model;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.ProcessUtil;
import org.compiere.model.I_C_DocType;
import org.compiere.model.MAllocationHdr;
import org.compiere.model.MAllocationLine;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceBatch;
import org.compiere.model.MInvoiceBatchLine;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MLocator;
import org.compiere.model.MMovement;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPInstance;
import org.compiere.model.MPInstancePara;
import org.compiere.model.MProduct;
import org.compiere.model.MSequence;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTransaction;
import org.compiere.model.MWarehouse;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.DocumentEngine;
import org.compiere.process.ProcessInfo;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.sg.model.MXXLODocGenerated;
import org.sg.model.MXXLoadOrder;
import org.sg.model.MXXLoadOrderLine;

/**
 * DIASA Model Validator
 * @author <a href="mailto:dixon.22martinez@gmail.com">Dixon Martinez</a>
 * Created Class
 * <li> FR [ 2 ] Add Support to Generate Invoice on Complete Sales Order
 * @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/2/generar-factura-al-completar-orden-de
 * <li> FR [ 4 ] Add Support to Allocate Invoice Vendor or Customer Automatically
 * @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/4/soporte-para-asignar-autom-ticamente
 * <li> FR [ 5 ] Add Support to Batch Print Invoice
 * @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/5/soporte-para-agregar-lote-a-documento-por
 */
public class DIASA_ModelValidator implements org.compiere.model.ModelValidator {

	/**
	 * Constructor.
	 */
	public DIASA_ModelValidator() {
		super();
	} // ModelValidator

	/** Logger */
	private static CLogger log = CLogger.getCLogger(DIASA_ModelValidator.class);
	
	/** Hardcoded Invoice Create Process */
	private static int AD_Process_ID = 134;
	/** Client */
	private int m_AD_Client_ID = -1;
	
	/**Is managment pallets movements mandatory*/
	private boolean isPalletMovementMandatory = false;
	
	/**
	 * Initialize Validation
	 * 
	 * @param engine
	 *            validation engine
	 * @param client
	 *            client
	 */
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		// client = null for global validator
		if (client != null) {
			m_AD_Client_ID = client.getAD_Client_ID();
			log.info(client.toString());
		} else {
			log.info("Initializing global validator: " + this.toString());
		}
		//FR [ 2 ]
		engine.addDocValidate(MOrder.Table_Name, this);
		engine.addDocValidate(MInvoice.Table_Name, this);
		engine.addModelChange(MInvoiceBatch.Table_Name, this);
		
		engine.addModelChange(MXXLoadOrder.Table_Name, this);
		engine.addModelChange(MXXLODocGenerated.Table_Name, this);
		engine.addModelChange(MTransaction.Table_Name, this);
		engine.addDocValidate(MMovement.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {
		isPalletMovementMandatory = MSysConfig.getBooleanValue("ISMANDATORYPALLETSMOVEMENT", false);
		return m_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		log.info("AD_User_ID=" + AD_User_ID);
		return null;
	}

	/**
	 * Model Change of a monitored Table. Called after
	 * PO.beforeSave/PO.beforeDelete when you called addModelChange for the
	 * table
	 * 
	 * @param po
	 *            persistent object
	 * @param type
	 *            TYPE_
	 * @return error message or null
	 * @exception Exception
	 *                if the recipient wishes the change to be not accept.
	 */
	public String modelChange(PO po, int type) throws Exception {
		String result = null;
		if (type == CHANGETYPE_CHANGE){
			//FR [ 5 ]
			if (po.get_TableName().equals(MInvoiceBatch.Table_Name)){
				MInvoiceBatch iBatch = (MInvoiceBatch) po;
				if (iBatch.is_ValueChanged(MInvoiceBatch.COLUMNNAME_Processed) 
						&& iBatch.isProcessed()){
					String DocumentNo = getNextInvoiceBatchDocNo(iBatch);
					if (DocumentNo != null){
						iBatch.setDocumentNo(DocumentNo);
						//iBatch.save();
					}
					
				}
			}
		}
		
		if (type == TYPE_AFTER_CHANGE) {
			if (po.get_TableName().equals(MXXLoadOrder.Table_Name)){
				MXXLoadOrder lOrder = (MXXLoadOrder) po;
				if (lOrder.is_ValueChanged(MXXLoadOrder.COLUMNNAME_XX_Annulled) 
						&& lOrder.getXX_Annulled().equals("Y")
							&& !lOrder.isXXIsBulk()){
					MMovement mOut =  (MMovement)lOrder.getM_MovementPalletOut();
					if (mOut.getM_Movement_ID()!=0 &&
							(!mOut.getDocStatus().equals(MMovement.DOCSTATUS_Voided)
									|| !mOut.getDocStatus().equals(MMovement.DOCSTATUS_Reversed))) {
						mOut.setDocAction(MMovement.DOCACTION_Void);
						mOut.processIt(MMovement.DOCACTION_Void);
						mOut.save();
					}
					
					MMovement mIn =  (MMovement)lOrder.getM_MovementPalletIn();
					if (mIn.getM_Movement_ID()!=0 &&
							(!mIn.getDocStatus().equals(MMovement.DOCSTATUS_Voided)
									|| !mIn.getDocStatus().equals(MMovement.DOCSTATUS_Reversed))) {
						mIn.setDocAction(MMovement.DOCACTION_Void);
						mIn.processIt(MMovement.DOCACTION_Void);
						mIn.save();
					}
					
				}
			}
		}
		
		if (type == TYPE_AFTER_NEW) {
			
			if (isPalletMovementMandatory) {
				if (po.get_TableName().equals(MXXLoadOrder.Table_Name)) {
					MXXLoadOrder lOrder = (MXXLoadOrder) po;
					if (!lOrder.isXXIsBulk())
						result = generateInventoryMovement(lOrder);
					
				}else if (po.get_TableName().equals(MXXLODocGenerated.Table_Name)) {
					MXXLODocGenerated docGen = (MXXLODocGenerated) po;
					MXXLoadOrderLine ocLine = new MXXLoadOrderLine(docGen.getCtx(), docGen.getXX_LoadOrderLine_ID(), docGen.get_TrxName());
					MXXLoadOrder oc = new MXXLoadOrder(ocLine.getCtx(), ocLine.getXX_LoadOrder_ID() , ocLine.get_TrxName()); 
					if (!oc.isXXIsBulk())
						result = completeInventoryMovement(oc);
				}
			}
			
			if (po.get_TableName().equals(MTransaction.Table_Name)) {
				MTransaction trx = (MTransaction) po;
				if (trx.get_ID()>0) {
					int docType_ID = trx.getDocumentLine().getC_DocType_ID();
					if (docType_ID>0) {
						MDocType docType = MDocType.get(trx.getCtx(), docType_ID);
						if (docType.get_ValueAsBoolean("IsValidateQuantity")) {
							checkQtyAvailable(trx.getM_Product_ID(), trx.getM_Locator_ID(),
									trx.getM_AttributeSetInstance_ID(), trx.getMovementQty(), trx.get_TrxName());
						}
					}
				}
			}
		}
		return result;
	} // modelChange


	private String completeInventoryMovement(MXXLoadOrder oc) {
		MMovement mOut = (MMovement) oc.getM_MovementPalletOut();
		String result = null;
		if (mOut.getM_Movement_ID()!=0) {
			if (mOut.getLines(true).length>0) {
				if (!mOut.getDocStatus().equals(MMovement.DOCSTATUS_Completed)
						&& !mOut.getDocStatus().equals(MMovement.DOCSTATUS_Closed)) {
					mOut.setDocAction(MMovement.DOCACTION_Complete);
					mOut.processIt(MMovement.DOCACTION_Complete);
					mOut.save();
				}
			}else
				result = "@NotFound@ @Line@ @M_MovementPalletOut_ID@";
		}else
			result = "@NotFound@ @M_MovementPalletOut_ID@";
		
		return result;
	}

	@Override
	public String docValidate(PO po, int timing) {
		//FR [ 2 ]
		//Generating Invoiced
		if (timing == TIMING_AFTER_COMPLETE){
			if (po.get_TableName().equals(MOrder.Table_Name)){
				MOrder order = (MOrder)	po;
				MDocType docType = (MDocType) order.getC_DocTypeTarget();
				if (docType.isSOTrx()){
					Trx trx = Trx.get(order.get_TrxName(), false);
					if (docType.get_ValueAsBoolean("GenerateInvoice") && 
							docType.getDocSubTypeSO().equals(MDocType.DOCSUBTYPESO_StandardOrder)){
						
						order.setDocStatus(MOrder.DOCSTATUS_Completed);
						order.save();
						
						MPInstance instance = new MPInstance(Env.getCtx(), AD_Process_ID, 0);
						if (!instance.save())
							return Msg.getMsg(Env.getCtx(), "ProcessNoInstance");
						
						ProcessInfo pi = new ProcessInfo ("", AD_Process_ID);
						pi.setAD_PInstance_ID (instance.getAD_PInstance_ID());
	
						//	Add Parameters
						MPInstancePara para = new MPInstancePara(instance, 10);
	
						para = new MPInstancePara(instance, 10);
						para.setParameter("DocAction", MOrder.DOCACTION_Complete);
						
						if (!para.save())
							return  "No Selection Parameter added";  //  not translated
						
						para = new MPInstancePara(instance, 20);
						para.setParameter("C_Order_ID", order.getC_Order_ID());
						
						if (!para.save())
							return  "No Selection Parameter added";  //  not translated
					
						ProcessUtil.startJavaProcess(order.getCtx(), pi, trx, false);
						if(pi.isError()) {
							//	Add Rollback for transaction
							trx.rollback();
							return pi.getSummary();
						}
						
					}
				}
			}
			//FR [ 4 ]
			if(po.get_TableName().equals(MInvoice.Table_Name)) {
				MInvoice m_Current_Invoice = (MInvoice) po;
				MDocType docType = (MDocType) m_Current_Invoice.getC_DocTypeTarget();
				//	Check Current Invoice if is paid return
				//	Check Current Invoice if is reversal return 
				if(docType.get_ValueAsBoolean("IsAutoAllocation")
						&& !m_Current_Invoice.isPaid()
						&& m_Current_Invoice.getReversal_ID() ==  0 )
					return invoiceAllocation(m_Current_Invoice);
			}
			
		}
		if(timing == TIMING_BEFORE_PREPARE){
			//FR [ 2 ]
			//Validating inventory 
			if (po.get_TableName().equals(MOrder.Table_Name)){
				MOrder order = (MOrder)	po;
				MDocType docType = (MDocType) order.getC_DocTypeTarget();
				if (docType.isSOTrx()){
					Trx trx = Trx.get(order.get_TrxName(), false);
					if ((docType.get_ValueAsBoolean("GenerateInvoice") && 
							docType.getDocSubTypeSO().equals(MDocType.DOCSUBTYPESO_StandardOrder))
								|| docType.getDocSubTypeSO().equals(MDocType.DOCSUBTYPESO_POSOrder)
							){
						MOrderLine[] oLines = order.getLines();
						
						for (MOrderLine line : oLines) {
							BigDecimal qty = Env.ZERO;
							if (line.getQtyOrdered().compareTo(Env.ZERO)==0)
								qty = line.getQtyEntered().subtract(line.getQtyInvoiced());
							else
								qty = line.getQtyOrdered().subtract(line.getQtyInvoiced());
							
							if (!isQtyAvailable(line.getLine(), line.getM_Product_ID(), 0, 
													line.getM_Warehouse_ID(), line.getM_AttributeSetInstance_ID(), 
														qty, trx.getTrxName(),false)){
								line.setQtyLostSales(line.getQtyOrdered());
								line.setQty(Env.ZERO);
								line.save(trx.getTrxName());
							}
							
						}
					}
				}
			}
		}
		
		if (timing == TIMING_AFTER_REVERSECORRECT
				|| timing == TIMING_AFTER_VOID) {
			if (po.get_TableName().equals(MMovement.Table_Name)) {
				MMovement mov = (MMovement)po;
				
				MXXLoadOrder lOrder = new Query(mov.getCtx(), MXXLoadOrder.Table_Name, "M_MovementPalletOut_ID = ? ", mov.get_TrxName())
										.setParameters(mov.getM_Movement_ID())
										.first();
				if (lOrder!=null
						&& lOrder.getXX_Annulled().equals("N")) {
					
					if (lOrder.getM_MovementPalletIn_ID()!=0) {
						MMovement mIn = (MMovement) lOrder.getM_MovementPalletIn(); 
						return "@M_MovementPalletIn_ID@ -> " + mIn.getDocumentNo() ;
					}
					
					MMovement nMov = new MMovement(mov.getCtx(), 0, mov.get_TrxName());
					MMovement.copyValues(mov, nMov);
					nMov.setDocumentNo("");
					nMov.setProcessed(false);
					nMov.setDocStatus(MMovement.DOCSTATUS_Drafted);
					nMov.setDocAction(MMovement.DOCACTION_Complete);
					nMov.save();
					lOrder.setM_MovementPalletOut_ID(nMov.getM_Movement_ID());
					lOrder.save();
					//lOrder
				}
				
				lOrder = new Query(mov.getCtx(), MXXLoadOrder.Table_Name, "M_MovementPalletIn_ID = ? ", mov.get_TrxName())
						.setParameters(mov.getM_Movement_ID())
						.first();
				
				if (lOrder!=null) {
					lOrder.setM_MovementPalletIn_ID(0);
					lOrder.save();
				}
			}
		}
		return null;
	}	

	/**
	 * Verify Existence Product in the Locator 
	 * @author <a href="mailto:jlct.master@gmail.com">Jorge Colmenarez</a> 2014-11-20, 09:24:20
	 * @author <a href="mailto:ysenih@erpcya.com">Yamel Senih</a> 2015-01-29, 20:00:00
	 * <li> * Show Line No, Product, Locator and Available
	 * @param p_LineNo
	 * @param p_M_Product_ID
	 * @param p_M_Locator_ID
	 * @param p_M_AttributeSetInstance_ID
	 * @param Qty
	 */
	private void checkQtyAvailable(int p_M_Product_ID, int p_M_Locator_ID, int p_M_AttributeSetInstance_ID, BigDecimal qty, String trx) {
		MProduct product = MProduct.get(Env.getCtx(), p_M_Product_ID);
		MLocator locator = MLocator.get(Env.getCtx(), p_M_Locator_ID);
		
		//	
		if(product.isStocked()){			
			ArrayList<Object> params = new ArrayList<Object>();
			StringBuffer sql = new StringBuffer("SELECT COALESCE(SUM(s.QtyOnHand),0)")
									.append(" FROM M_Storage s")
									.append(" WHERE s.M_Product_ID=?");
			params.add(product.getM_Product_ID());
			// Warehouse level
			if (locator.getM_Locator_ID() == 0) {
				sql.append(" AND EXISTS (SELECT 1 FROM M_Locator l WHERE s.M_Locator_ID=l.M_Locator_ID AND l.M_Warehouse_ID=?)");
				params.add(0);
			}
			// Locator level
			else {
				sql.append(" AND s.M_Locator_ID=?");
				params.add(locator.getM_Locator_ID());
			}
			// With ASI
			if (p_M_AttributeSetInstance_ID != 0) {
				sql.append(" AND s.M_AttributeSetInstance_ID=?");
				params.add(p_M_AttributeSetInstance_ID);
			}
			//
			BigDecimal available = DB.getSQLValueBD(trx, sql.toString(), params);
			
			
			if (available == null)
				available = Env.ZERO;
			
			/*if (available.signum() == 0)
				throw new AdempiereException("@NoQtyAvailable@ ["  
						+ " @M_Product_ID@ " + product.getValue() + " - " + product.getName() 
						+ " @M_Locator_ID@ " + locator.getValue() + " @QtyAvailable@ " + available.toString() + "]");*/
			if (available.compareTo(Env.ZERO) < 0) {
				throw new AdempiereException("@InsufficientQtyAvailable@ ["  
						+ " @M_Product_ID@ " + product.getValue() + " - " + product.getName() 
						+ " @M_Locator_ID@ " + locator.getValue() + " @QtyAvailable@ " + available.toString() + "]");
			}
		}
	}
	
	/**
	 * FR [ 2 ]
	 * Verify Existence Product in the Locator 
	 * @author <a href="mailto:jlct.master@gmail.com">Jorge Colmenarez</a> 2014-11-20, 09:24:20
	 * @author <a href="mailto:ysenih@erpcya.com">Yamel Senih</a> 2015-01-29, 20:00:00
	 * <li> * Show Line No, Product, Locator and Available
	 * @param p_LineNo
	 * @param p_M_Product_ID
	 * @param p_M_Locator_ID
	 * @param p_M_AttributeSetInstance_ID
	 * @param Qty
	 */
	private boolean isQtyAvailable(int p_LineNo, int p_M_Product_ID, int p_M_Locator_ID, int p_M_Warehouse_ID, 
			int p_M_AttributeSetInstance_ID, BigDecimal qty, String trx, boolean reserved) {
		MLocator locator = MLocator.get(Env.getCtx(), p_M_Locator_ID);
		MWarehouse warehouse = MWarehouse.get(Env.getCtx(), p_M_Warehouse_ID);
		MProduct product = MProduct.get(Env.getCtx(), p_M_Product_ID); 
		//	
		if(product != null &&
				product.isStocked()) {
			
			if (qty.compareTo(Env.ZERO)==0)
				return true;
			
			//	UOM
			ArrayList<Object> params = new ArrayList<Object>();
			StringBuffer sql = new StringBuffer("SELECT COALESCE(SUM(s.QtyOnHand " + (reserved ? "- s.QtyReserved" : "") + "),0)")
									.append(" FROM M_Storage s")
									.append(" WHERE s.M_Product_ID=?");
			params.add(product.getM_Product_ID());
			// Warehouse level
			if (locator.getM_Locator_ID() == 0) {
				sql.append(" AND EXISTS (SELECT 1 FROM M_Locator l WHERE s.M_Locator_ID=l.M_Locator_ID AND l.M_Warehouse_ID=?)");
				params.add(warehouse.getM_Warehouse_ID());
			}
			// Locator level
			else {
				sql.append(" AND s.M_Locator_ID=?");
				params.add(locator.getM_Locator_ID());
			}
			// With ASI
			if (p_M_AttributeSetInstance_ID != 0) {
				sql.append(" AND s.M_AttributeSetInstance_ID=?");
				params.add(p_M_AttributeSetInstance_ID);
			}
			//
			BigDecimal available = DB.getSQLValueBD(trx, sql.toString(), params);
			if (available == null)
				available = Env.ZERO;
			//	Pattern
			if (available.signum() <= 0)
				return false;
			else if (available.compareTo(qty) < 0) 
				return false;
			
		}
		
		return true;
	}
	
	/**
	 * FR [ 4 ]
	 * @param m_Current_Invoice
	 * @return
	 * @return String
	 */
	private String invoiceAllocation(MInvoice m_Current_Invoice){
		//		Instance Lines
		List<MInvoiceLine> iLines = new Query(m_Current_Invoice.getCtx(), 
												MInvoiceLine.Table_Name, 
												"C_Invoice_ID = ?", 
												m_Current_Invoice.get_TrxName())
												.setParameters(m_Current_Invoice.getC_Invoice_ID())
												.list();
		
		if (iLines.size()> 0 ){
			HashMap<Integer, BigDecimal> invoicesAffected = new HashMap<Integer, BigDecimal>();
			BigDecimal multiplier = Env.ZERO;
			BigDecimal multiplierAffected = Env.ZERO;
			BigDecimal openAmt = Env.ZERO; 
			BigDecimal grandAmount = Env.ZERO;
			MAllocationHdr alloc = null;
			int invoiceAffected_ID = m_Current_Invoice.get_ValueAsInt("XX_DocAffected");
			
			for (MInvoiceLine mInvoiceLine : iLines) {
				int invoiceLineAffected_ID = mInvoiceLine.get_ValueAsInt("XX_DocAffected");
				
				if (invoiceLineAffected_ID == 0)
					invoiceLineAffected_ID = invoiceAffected_ID;
				
				if (invoiceLineAffected_ID != 0){
					mInvoiceLine.set_ValueOfColumn("XX_DocAffected", invoiceLineAffected_ID);
					mInvoiceLine.save();
				}else 
					continue;
				
				//Don't Allocated When Invoice is Paid
				MInvoice invoiceAffected = new MInvoice(m_Current_Invoice.getCtx(), invoiceLineAffected_ID, m_Current_Invoice.get_TrxName());
				if (invoiceAffected.isPaid())
					continue;
				
				BigDecimal LineTotalAmt = invoicesAffected.get(invoiceLineAffected_ID);
				
				if (LineTotalAmt==null)
					LineTotalAmt = mInvoiceLine.getLineTotalAmt();
				else
					LineTotalAmt = LineTotalAmt.add(mInvoiceLine.getLineTotalAmt());
				invoicesAffected.put(invoiceLineAffected_ID, LineTotalAmt);
			}
			
			for (Map.Entry<Integer, BigDecimal> invoices: invoicesAffected.entrySet()){
				int DocAffected_ID = invoices.getKey();
				BigDecimal LineTotalAmt = invoices.getValue();

				MInvoice m_InvoiceAffected = MInvoice.get(m_Current_Invoice.getCtx(), DocAffected_ID);
				//	Set Multiplier depending of document type
				multiplier = setMultiplier(m_Current_Invoice.getC_DocType());
				multiplierAffected = setMultiplier(m_InvoiceAffected.getC_DocType());

				if(multiplier.compareTo(multiplierAffected) == 0) {
					multiplier = multiplier.multiply(Env.ONE.negate());
					multiplierAffected = multiplierAffected.multiply(Env.ONE.negate());
				}
				//	get open amount of document
				openAmt = openAmount(DocAffected_ID);
			
				BigDecimal amt = LineTotalAmt;
				BigDecimal newOpenAmt = Env.ZERO;
				// BR [ 140 ]
				if(m_Current_Invoice.isCreditMemo())
					newOpenAmt = (openAmt.subtract(amt)).multiply(multiplierAffected.multiply(new BigDecimal(openAmt.signum())));
				else 
					newOpenAmt = (openAmt.add(amt)).multiply(multiplierAffected.multiply(new BigDecimal(openAmt.signum())));

				grandAmount = grandAmount.add(amt);
				//	Add allocation 
				alloc = addAllocation(amt, openAmt, newOpenAmt, m_Current_Invoice, DocAffected_ID,alloc, multiplierAffected);
			}
			
			//	Complete Allocation
			completeAllocation(alloc, m_Current_Invoice, multiplierAffected, grandAmount);
		}
		
		return "";
	}

	/**
	 * FR [ 4 ]
	 * Set Multiplier
	 * @author <a href="mailto:dixon.22martinez@gmail.com">Dixon Martinez</a> 12/12/2014, 11:35:58
	 * @param c_DocType
	 * @return void
	 */
	private BigDecimal setMultiplier(I_C_DocType c_DocType) {
		MDocType docType = (MDocType) c_DocType;
		String docBaseType = docType.getDocBaseType();
		return (docBaseType.substring(2).equals("C")? Env.ONE.negate(): Env.ONE)
			.multiply((docBaseType.substring(1,2).equals("P")? Env.ONE.negate(): Env.ONE));
		
	}
	
	/**
	 * FR [ 4 ]
	 * Open Amount
	 * @author <a href="mailto:dixon.22martinez@gmail.com">Dixon Martinez</a> 12/12/2014, 11:27:54
	 * Close Connection
	 * @contributor <a href="mailto:carlosaparadam@gmail.com">Carlos Parada</a> 06/02/2015, 14:30:13
	 * @param p_C_Invoice_ID
	 * @return
	 * @return BigDecimal
	 */
	private BigDecimal openAmount(int p_C_Invoice_ID) {
		//	
		BigDecimal openAmt = Env.ZERO;
		CallableStatement cs = null;
		try {
			cs = DB.prepareCall("{call invoiceopen(?, 0, ?)}");
			cs.setInt(1, p_C_Invoice_ID);
			cs.registerOutParameter(2, java.sql.Types.NUMERIC);
			cs.execute();
			openAmt = cs.getBigDecimal(2);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			DB.close(cs);
		}
		return openAmt;
	}
	
	/**
	 * FR [ 4 ]
	 * Add Document Allocation
	 * @author <a href="mailto:dixon.22martinez@gmail.com">Dixon Martinez</a> 10/12/2014, 17:23:45
	 * @param p_C_BPartner_ID
	 * @param amt
	 * @param openAmt
	 * @param newOpenAmt
	 * @param m_Invoice
	 * @param p_C_Invoice_ID
	 * @return void
	 */
	private MAllocationHdr addAllocation(BigDecimal amt,
			BigDecimal openAmt, BigDecimal newOpenAmt, MInvoice m_Invoice, int p_C_Invoice_ID,MAllocationHdr alloc, BigDecimal multiplierAffected) {
		if(alloc == null ){
			alloc = new MAllocationHdr(Env.getCtx(), true,	//	manual
					//Env.getContextAsDate(m_Invoice.getCtx(), "#Date")
					//BR[ 139 ]
					m_Invoice.getDateAcct()
					, m_Invoice.getC_Currency_ID(), Env.getContext(Env.getCtx(), "#AD_User_Name"), m_Invoice.get_TrxName());
			alloc.setAD_Org_ID(m_Invoice.getAD_Org_ID());
			alloc.saveEx();
		}
		//	
		MAllocationLine aLine = new MAllocationLine (alloc, amt.multiply(multiplierAffected), 
				Env.ZERO, Env.ZERO, newOpenAmt);
		aLine.setDocInfo(m_Invoice.getC_BPartner_ID(), 0, p_C_Invoice_ID);
		aLine.saveEx();
		
		return alloc;
	}

	/**
	 * FR [ 4 ]
	 * Complete Allocation
	 * @author <a href="mailto:dixon.22martinez@gmail.com">Dixon Martinez</a> 10/12/2014, 17:23:23
	 * @return void
	 */
	private void completeAllocation(MAllocationHdr m_Current_Alloc, MInvoice m_Current_Invoice, BigDecimal multiplier, BigDecimal grandAmount){
		if(m_Current_Alloc != null){
			if(m_Current_Alloc.getDocStatus().equals(DocumentEngine.STATUS_Drafted)){
				log.fine("Amt Total Allocation=" + m_Current_Invoice.getGrandTotal());
				//	get open amount of document
				openAmount(m_Current_Invoice.get_ID());
				//	Set Multiplier depending of document type
				multiplier = setMultiplier(m_Current_Invoice.getC_DocType());
				
				BigDecimal amt = m_Current_Invoice.getGrandTotal();
				BigDecimal newOpenAmt = grandAmount.subtract(amt);
				
				MAllocationLine aLine = new MAllocationLine (m_Current_Alloc, grandAmount.multiply(multiplier), 
						Env.ZERO, Env.ZERO, newOpenAmt);
				aLine.setDocInfo(m_Current_Invoice.getC_BPartner_ID(), 0, m_Current_Invoice.getC_Invoice_ID());
				aLine.saveEx();
				//	
				if(m_Current_Alloc.getDocStatus().equals(DocumentEngine.STATUS_Drafted)){
					log.fine("Current Allocation = " + m_Current_Alloc.getDocumentNo());
					//	
					m_Current_Alloc.setDocAction(DocumentEngine.ACTION_Complete);
					m_Current_Alloc.processIt(DocumentEngine.ACTION_Complete);
					m_Current_Alloc.saveEx();			
				}	
			}
			m_Current_Alloc = null;
			
		}
	}
	
	/**
	 * FR [ 5 ] 
	 * @param iBatch
	 * @return
	 * @return String
	 */
	private String getNextInvoiceBatchDocNo(MInvoiceBatch iBatch){

		MInvoiceBatchLine iBatchLine = new Query(iBatch.getCtx(), 
												MInvoiceBatchLine.Table_Name, 
												"C_InvoiceBatch_ID = ? ", iBatch.get_TrxName())
											.setParameters(iBatch.getC_InvoiceBatch_ID())
											.first();
		
		if (iBatchLine != null){
			MInvoice invoice = (MInvoice)iBatchLine.getC_Invoice();
			MDocType dt = (MDocType) invoice.getC_DocType();
			MSequence seq = null;
			int BatchSeq_ID = dt.get_ValueAsInt("BatchSeq_ID");
			if (BatchSeq_ID==0)
				return null;
			else
				seq = new MSequence(iBatch.getCtx(), BatchSeq_ID , iBatch.get_TrxName());
			

			String DocumentNo = "";
			DocumentNo = seq.getPrefix() + seq.getNextID() + seq.getSuffix();
			seq.save();
			
			return DocumentNo;
		}
		
		return null;
	}
	
	private String generateInventoryMovement(MXXLoadOrder lOrder){
		MMovement iMov = lOrder.createInventoryMovement(null);
		return null;
		
	}
}
