package org.spin.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.adempiere.model.ImportValidator;
import org.adempiere.process.ImportProcess;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MClient;
import org.compiere.model.MDiscountSchema;
import org.compiere.model.MInvoiceSchedule;
import org.compiere.model.MLocation;
import org.compiere.model.MOrg;
import org.compiere.model.MPaymentTerm;
import org.compiere.model.MPriceList;
import org.compiere.model.MSalesRegion;
import org.compiere.model.MUser;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.model.X_I_BPartner;
import org.compiere.process.ImportBPartner;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.mb.model.X_XX_MB_Day;
import org.mb.model.X_XX_MB_Frequency;
import org.mb.model.X_XX_MB_PlanningVisit;

public class Import_ModelValidator implements ImportValidator, ModelValidator{

	/**	Logger							*/
	protected CLogger			log = CLogger.getCLogger (getClass());
	@Override
	public void validate(ImportProcess process, Object importModel, Object targetModel, int timing) {
		PO importPO = null;
		PO targetPO = null;
		if (importModel!=null)
			importPO= (PO) importModel;
		if (targetModel!=null)
			targetPO= (PO) targetModel;
		
		if (importPO !=null
				&& targetPO!=null) {
			if (timing == ImportValidator.TIMING_AFTER_IMPORT) {
				if (X_I_BPartner.Table_Name.equals(importPO.get_TableName()) 
						&& MBPartner.Table_Name.equals(targetPO.get_TableName()) ) {
					MBPartner bPartner = (MBPartner) targetPO;
					X_I_BPartner ibPartner = (X_I_BPartner) importPO;
					//Basic Data
					bPartner.set_ValueOfColumn("IsProspect", ibPartner.get_Value("IsProspect"));
					bPartner.set_ValueOfColumn("Rating", ibPartner.get_Value("Rating"));
					bPartner.set_ValueOfColumn("IsTaxExempt", ibPartner.get_Value("IsTaxExempt"));
					bPartner.setAcqusitionCost(ibPartner.get_Value("AcqusitionCost")!=null ? (BigDecimal) ibPartner.get_Value("AcqusitionCost") : Env.ZERO);
					bPartner.setSalesVolume(ibPartner.get_Value("SalesVolume")!=null ? ibPartner.get_ValueAsInt("SalesVolume") : 0);
					bPartner.set_ValueOfColumn("SOCreditStatus", ibPartner.get_Value("SOCreditStatus"));
					bPartner.set_ValueOfColumn("FirstSale",ibPartner.get_Value("FirstSale"));
					bPartner.set_ValueOfColumn("XX_TipoPersona", ibPartner.get_Value("XX_TipoPersona"));
					bPartner.set_ValueOfColumn("XX_APC_TBP", ibPartner.get_Value("XX_APC_TBP"));
					bPartner.set_ValueOfColumn("DvID", ibPartner.get_Value("DvID"));
					bPartner.set_ValueOfColumn("XX_BP_SubGroup", ibPartner.get_Value("XX_BP_SubGroup"));
					bPartner.set_ValueOfColumn("IsPop", ibPartner.get_Value("IsPop"));
					bPartner.set_ValueOfColumn("HightVolumen", ibPartner.get_Value("HightVolumen"));
					bPartner.set_ValueOfColumn("Scope", ibPartner.get_Value("Scope"));
					bPartner.set_ValueOfColumn("PollProductCategory1", ibPartner.get_Value("PollProductCategory1"));
					bPartner.set_ValueOfColumn("PollProductCategory2", ibPartner.get_Value("PollProductCategory2"));
					bPartner.set_ValueOfColumn("PollProductCategory3", ibPartner.get_Value("PollProductCategory3"));
					bPartner.set_ValueOfColumn("ShareMarket", ibPartner.get_Value("ShareMarket"));
					bPartner.set_ValueOfColumn("QtyProvider", ibPartner.get_ValueAsInt("QtyProvider"));
					bPartner.set_ValueOfColumn("IsPrePaid", ibPartner.get_Value("IsPrePaid"));
					bPartner.set_ValueOfColumn("NumberEmployees", ibPartner.get_ValueAsInt("NumberEmployees"));
					bPartner.set_ValueOfColumn("A1_CodigoTarjeta", ibPartner.get_Value("A1_CodigoTarjeta"));
					
					MBPartner parentBPartner = new Query(ibPartner.getCtx(), MBPartner.Table_Name, "Value = ?", ibPartner.get_TrxName())
											.setParameters(ibPartner.get_ValueAsString("BPValue"))
											.first();
					if (parentBPartner!=null) 
						bPartner.setBPartner_Parent_ID(parentBPartner.getC_BPartner_ID());
					
					MUser salesRep = new Query(ibPartner.getCtx(), MUser.Table_Name, "Name = ?", ibPartner.get_TrxName())
									.setParameters(ibPartner.get_ValueAsString("SalesRep_Name"))
									.first();
					if (salesRep!=null) 
						bPartner.setSalesRep_ID(salesRep.getAD_User_ID());
					
					PO taxGroup = new Query(ibPartner.getCtx(), "C_TaxGroup", "Value=?", ibPartner.get_TrxName())
								.setParameters(ibPartner.get_ValueAsString("TaxGroupValue"))
								.first();
					if (taxGroup!=null) 
						bPartner.set_ValueOfColumn("C_TaxGroup_ID", taxGroup.get_ValueAsInt("C_TaxGroup_ID"));
					
					MOrg orgBP = new Query(ibPartner.getCtx(), MOrg.Table_Name, "Value=?", ibPartner.get_TrxName())
							.setParameters(ibPartner.get_ValueAsString("OrgValue"))
							.first();
					if (orgBP!=null) 
						bPartner.set_ValueOfColumn("AD_Org_ID", orgBP.get_ValueAsInt("AD_Org_ID"));
					
					//Comun Data
					bPartner.set_ValueOfColumn("InvoiceRule", ibPartner.get_Value("InvoiceRule"));
					bPartner.set_ValueOfColumn("DeliveryRule", ibPartner.get_Value("DeliveryRule"));
					bPartner.set_ValueOfColumn("DeliveryViaRule", ibPartner.get_Value("DeliveryViaRule"));
					bPartner.setFlatDiscount(ibPartner.get_Value("FlatDiscount")!=null ? (BigDecimal) ibPartner.get_Value("FlatDiscount") : Env.ZERO);
					bPartner.set_ValueOfColumn("PaymentRule", ibPartner.get_Value("PaymentRule"));
					bPartner.set_ValueOfColumn("IsDiscountPrinted", ibPartner.get_Value("IsDiscountPrinted"));
					bPartner.set_ValueOfColumn("PollProduct1", ibPartner.get_Value("PollProduct1"));
					bPartner.set_ValueOfColumn("XX_First_Name", ibPartner.get_Value("XX_First_Name"));
					bPartner.set_ValueOfColumn("XX_SurName", ibPartner.get_Value("XX_SurName"));
					bPartner.set_ValueOfColumn("XX_Last_Name", ibPartner.get_Value("XX_Last_Name"));
					bPartner.set_ValueOfColumn("XX_Mother_Last_Name", ibPartner.get_Value("XX_Mother_Last_Name"));
					
					MInvoiceSchedule invoiceSchedule = new Query(ibPartner.getCtx(), MInvoiceSchedule.Table_Name, "Name=?", ibPartner.get_TrxName())
														.setParameters(ibPartner.get_ValueAsString("InvoiceScheduleName"))
														.first();
					if (invoiceSchedule!=null) 
						bPartner.setC_InvoiceSchedule_ID(invoiceSchedule.getC_InvoiceSchedule_ID());
					
					MPriceList priceList = new Query(ibPartner.getCtx(), MPriceList.Table_Name, "Name=?", ibPartner.get_TrxName())
													.setParameters(ibPartner.get_ValueAsString("PriceListName"))
													.first();
					if (priceList!=null) 
						bPartner.setM_PriceList_ID(priceList.getM_PriceList_ID());
					
					MDiscountSchema discountSchema = new Query(ibPartner.getCtx(), MDiscountSchema.Table_Name, "Name=?", ibPartner.get_TrxName())
													.setParameters(ibPartner.get_ValueAsString("DiscountSchemaName"))
													.first();
					if (discountSchema!=null) 
						bPartner.setM_DiscountSchema_ID(discountSchema.getM_DiscountSchema_ID());
					
					MPaymentTerm paymentTerm = new Query(ibPartner.getCtx(), MPaymentTerm.Table_Name, "Value=?", ibPartner.get_TrxName())
							.setParameters(ibPartner.get_ValueAsString("PaymentTermValue"))
							.first();
					if (paymentTerm!=null) 
						bPartner.setC_PaymentTerm_ID(paymentTerm.getC_PaymentTerm_ID());
										
					bPartner.save();
				}else if (X_I_BPartner.Table_Name.equals(importPO.get_TableName()) 
						&& MUser.Table_Name.equals(targetPO.get_TableName()) ) {
					
					MUser userContact = (MUser) targetPO;
					X_I_BPartner ibPartner = (X_I_BPartner) importPO;
					
					//Contact User
					userContact.setNotificationType(ibPartner.get_ValueAsString("NotificationType"));
					userContact.save();
				}else if (X_I_BPartner.Table_Name.equals(importPO.get_TableName()) 
						&& MBPartnerLocation.Table_Name.equals(targetPO.get_TableName()) ) {
					
					MBPartnerLocation bpLocation = (MBPartnerLocation) targetPO;
					X_I_BPartner ibPartner = (X_I_BPartner) importPO;
					
					//BPartner Location
					bpLocation.set_ValueOfColumn("Name", ibPartner.get_Value("LocationName"));
					MLocation location = (MLocation) bpLocation.getC_Location();
					if (location!=null
							&& location.getC_Location_ID() > 0) {
						location.set_ValueOfColumn("Address3", ibPartner.get_Value("Address3"));
						location.set_ValueOfColumn("Address4", ibPartner.get_Value("Address4"));
						location.save();
					}
					
					bpLocation.set_ValueOfColumn("ISDN", ibPartner.get_Value("ISDN"));
					bpLocation.set_ValueOfColumn("IsShipTo", ibPartner.get_Value("IsShipTo"));
					bpLocation.set_ValueOfColumn("IsBillTo", ibPartner.get_Value("IsBillTo"));
					bpLocation.set_ValueOfColumn("IsPayFrom", ibPartner.get_Value("IsPayFrom"));
					bpLocation.set_ValueOfColumn("IsRemitTo", ibPartner.get_Value("IsRemitTo"));
					
					bpLocation.set_ValueOfColumn("InSideLocation", ibPartner.get_Value("InSideLocation"));
					bpLocation.set_ValueOfColumn("XX_VisitFrequency", ibPartner.get_Value("XX_VisitFrequency"));
					bpLocation.set_ValueOfColumn("XX_VisitMonday", ibPartner.get_Value("XX_VisitMonday"));
					bpLocation.set_ValueOfColumn("XX_VisitTuesday", ibPartner.get_Value("XX_VisitTuesday"));
					bpLocation.set_ValueOfColumn("XX_VisitWednesday", ibPartner.get_Value("XX_VisitWednesday"));
					bpLocation.set_ValueOfColumn("XX_VisitThursDay", ibPartner.get_Value("XX_VisitThursDay"));
					bpLocation.set_ValueOfColumn("XX_VisitFriday", ibPartner.get_Value("XX_VisitFriday"));
					bpLocation.set_ValueOfColumn("XX_VisitSaturday", ibPartner.get_Value("XX_VisitSaturday"));
					bpLocation.set_ValueOfColumn("XX_VisitSunday", ibPartner.get_Value("XX_VisitSunday"));
					bpLocation.set_ValueOfColumn("Latitude", ibPartner.get_Value("Latitude"));
					bpLocation.set_ValueOfColumn("Longitude", ibPartner.get_Value("Longitude"));
					
					MSalesRegion salesRegion = new Query(ibPartner.getCtx(), MSalesRegion.Table_Name, "Value=?", ibPartner.get_TrxName())
												.setParameters(ibPartner.get_ValueAsString("SalesRegionValue"))
												.first();
					if (salesRegion!=null) 
						bpLocation.setC_SalesRegion_ID(salesRegion.getC_SalesRegion_ID());

					bpLocation.save();
					
					//Planning Visit
					MSalesRegion pvSalesRegion = new Query(ibPartner.getCtx(), MSalesRegion.Table_Name, "Value=?", ibPartner.get_TrxName())
							.setParameters(ibPartner.get_ValueAsString("PlanningVisitSalesRegionValue"))
							.first();
					if (pvSalesRegion!=null
							&& pvSalesRegion.getC_SalesRegion_ID()>0) { 
						
						MBPartnerLocation pvShipLocation = new Query(ibPartner.getCtx(), MBPartnerLocation.Table_Name, "Name=?", ibPartner.get_TrxName())
								.setParameters(ibPartner.get_ValueAsString("PlaningVisitShipAddressName"))
								.first();
						if (pvShipLocation!=null
								&& pvShipLocation.getC_BPartner_Location_ID()>0) {
							
							MBPartnerLocation pvBillLocation = new Query(ibPartner.getCtx(), MBPartnerLocation.Table_Name, "Name=?", ibPartner.get_TrxName())
									.setParameters(ibPartner.get_ValueAsString("PlaningVisitBillAddressName"))
									.first();
							if (pvBillLocation!=null
									&& pvBillLocation.getC_BPartner_Location_ID()>0) {
								
									X_XX_MB_Frequency mbFrequency = new Query(ibPartner.getCtx(), X_XX_MB_Frequency.Table_Name, "Name=?", ibPartner.get_TrxName())
											.setParameters(ibPartner.get_ValueAsString("FrequencyName"))
											.first();
									if (mbFrequency!=null
											&& mbFrequency.getXX_MB_Frequency_ID()>0) {
										
										PO mbDay = new Query(ibPartner.getCtx(), X_XX_MB_Day.Table_Name, "Name=?", ibPartner.get_TrxName())
												.setParameters(ibPartner.get_ValueAsString("DayName"))
												.first();
										if (mbDay!=null
												&& mbDay.get_ID()>0) { 
										X_XX_MB_PlanningVisit pVisit = new Query(ibPartner.getCtx(), X_XX_MB_PlanningVisit.Table_Name, "Name = ? AND IsActive = 'Y'", ibPartner.get_TrxName())
												.setParameters(ibPartner.get_ValueAsString("PlanningVisitName"))
												.first();
										if (pVisit!=null
												&& pVisit.getXX_MB_PlanningVisit_ID() > 0) { //Update Planning Visit
											pVisit.set_ValueOfColumn("Name",ibPartner.get_Value("PlanningVisitName"));
											pVisit.set_ValueOfColumn("SeqNo",ibPartner.get_Value("SeqNo"));
											pVisit.set_ValueOfColumn("IsValid",ibPartner.get_Value("IsValid"));
											pVisit.set_ValueOfColumn("Description",ibPartner.get_Value("Description"));
											pVisit.set_ValueOfColumn("ValidFrom",ibPartner.get_Value("ValidFrom"));
											pVisit.setC_SalesRegion_ID(pvSalesRegion.getC_SalesRegion_ID());
											pVisit.setXX_MB_Day_ID(mbDay.get_ID());
											pVisit.setXX_MB_Frequency_ID(mbFrequency.getXX_MB_Frequency_ID());
											pVisit.setC_BPartner_Location_ID(pvShipLocation.getC_BPartner_Location_ID());
											pVisit.setBill_Location_ID(pvBillLocation.getC_BPartner_Location_ID());
											pVisit.setC_BPartner_ID(bpLocation.getC_BPartner_ID());
											pVisit.save();
											
										}else {//Insert Planning Visit
											pVisit = new X_XX_MB_PlanningVisit(ibPartner.getCtx(), 0, ibPartner.get_TrxName());
											pVisit.set_ValueOfColumn("Name",ibPartner.get_Value("PlanningVisitName"));
											pVisit.set_ValueOfColumn("SeqNo",ibPartner.get_Value("SeqNo"));
											pVisit.set_ValueOfColumn("IsValid",ibPartner.get_Value("IsValid"));
											pVisit.set_ValueOfColumn("Description",ibPartner.get_Value("Description"));
											pVisit.set_ValueOfColumn("ValidFrom",ibPartner.get_Value("ValidFrom"));
											pVisit.setC_SalesRegion_ID(pvSalesRegion.getC_SalesRegion_ID());
											pVisit.setXX_MB_Day_ID(mbDay.get_ID());
											pVisit.setXX_MB_Frequency_ID(mbFrequency.getXX_MB_Frequency_ID());
											pVisit.setC_BPartner_Location_ID(pvShipLocation.getC_BPartner_Location_ID());
											pVisit.setBill_Location_ID(pvBillLocation.getC_BPartner_Location_ID());
											pVisit.setC_BPartner_ID(bpLocation.getC_BPartner_ID());
											pVisit.save();
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		if (process instanceof ImportBPartner
				&& timing == ImportValidator.TIMING_BEFORE_VALIDATE) {
			ImportBPartner importProcess = (ImportBPartner) process;
			StringBuffer sql = new StringBuffer ("UPDATE I_BPartner i "
					+ "SET AD_Org_ID=(SELECT AD_Org_ID FROM AD_Org org "
					+ " WHERE i.OrgValue=org.Value AND org.AD_Client_ID=i.AD_Client_ID) "
					+ "WHERE I_IsImported<>'Y'").append(importProcess.getWhereClause());
			int no = DB.executeUpdateEx(sql.toString(), importProcess.get_TrxName());
			log.fine("Set Org=" + no);
			
		}
	}

	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		engine.addImportValidate(X_I_BPartner.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {
		return 0;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {
		return null;
	}

	@Override
	public String docValidate(PO po, int timing) {
		return null;
	}

}
