/**************************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                               *
 * This program is free software; you can redistribute it and/or modify it    		  *
 * under the terms version 2 or later of the GNU General Public License as published  *
 * by the Free Software Foundation. This program is distributed in the hope           *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied         *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                   *
 * See the GNU General Public License for more details.                               *
 * You should have received a copy of the GNU General Public License along            *
 * with this program; if not, printLine to the Free Software Foundation, Inc.,        *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                             *
 * For the text or an alternative of this public license, you may reach us            *
 * Copyright (C) 2012-2018 E.R.P. Consultores y Asociados, S.A. All Rights Reserved.  *
 * Contributor: Carlos Parada cparada@erpya.com                                       *
 * See: www.erpya.com                                                                 *
 *************************************************************************************/

package org.spin.model;

import java.util.Properties;

import org.adempiere.controller.SmallViewController;
import org.compiere.model.GridField;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.util.CLogger;
import org.eevolution.grid.BrowserCallOutEngine;

public class CalloutBrowseRMA extends BrowserCallOutEngine{
	
	static CLogger log = CLogger.getCLogger(CalloutBrowseRMA.class);

	/**
	 * Set Invoice Parameters
	 * @param ctx
	 * @param WindowNo
	 * @param viewController
	 * @param field
	 * @param value
	 * @param oldValue
	 * @return
	 */
	public String invoice(Properties ctx,  int WindowNo,SmallViewController  viewController, GridField field, Object value, Object oldValue)
	{
		if (field!=null
				&& field.getColumnName()!=null
					&& field.getColumnName().equals("C_Invoice_ID")) {
			
			int C_Invoice_ID = (Integer)field.getValue();
			
			MInvoice invoice = MInvoice.get(ctx, C_Invoice_ID);
			if (invoice.get_ID()>0) {
				viewController.setValue("C_BPartner_ID", invoice.getC_BPartner_ID());
				viewController.setValue("C_BPartner_Location_ID", invoice.getC_BPartner_Location_ID());
				viewController.setValue("DateInvoiced", invoice.getDateInvoiced());
				viewController.setValueTo("DateInvoiced", invoice.getDateInvoiced());
				viewController.setValue("IsPaid", invoice.isPaid() ? "Y" : "N");
			}
		}
		
		return "";
	}

	/**
	 * Set InOut Parameters
	 * @param ctx
	 * @param WindowNo
	 * @param viewController
	 * @param field
	 * @param value
	 * @param oldValue
	 * @return
	 */
	public String inOut(Properties ctx,  int WindowNo,SmallViewController  viewController, GridField field, Object value, Object oldValue)
	{
		if (field!=null
				&& field.getColumnName()!=null
					&& field.getColumnName().equals("M_InOut_ID")) {
			
			int M_InOut_ID = (Integer)field.getValue();
			
			MInOut inOut = new MInOut(ctx, M_InOut_ID, null);
			if (inOut.get_ID()>0) {
				viewController.setValue("C_BPartner_ID", inOut.getC_BPartner_ID());
				viewController.setValue("C_BPartner_Location_ID", inOut.getC_BPartner_Location_ID());
				viewController.setValue("MovementDate", inOut.getMovementDate());
				viewController.setValueTo("MovementDate", inOut.getMovementDate());
			}
		}

		return "";
	}
	
	
}
