package org.spin.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.compiere.model.I_M_InventoryLine;
import org.compiere.model.I_M_RMA;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MInventoryLine;
import org.compiere.model.MRMA;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.eevolution.model.I_PP_Order;
import org.eevolution.model.MPPOrder;

/**
 * DIASA Model Validator
 * @author <a href="mailto:ysenih@erpya.com">Yamel Senih</a>
 */
public class DIASA391Validator implements org.compiere.model.ModelValidator {

	/**
	 * Constructor.
	 */
	public DIASA391Validator() {
		super();
	} // ModelValidator

	/** Logger */
	private static CLogger log = CLogger.getCLogger(DIASA391Validator.class);
	
	/** Client */
	private int clientId = -1;
	
	/**
	 * Initialize Validation
	 * 
	 * @param engine
	 *            validation engine
	 * @param client
	 *            client
	 */
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		// client = null for global validator
		if (client != null) {
			clientId = client.getAD_Client_ID();
			log.info(client.toString());
		} else {
			log.info("Initializing global validator: " + this.toString());
		}
		//	
		engine.addModelChange(I_M_InventoryLine.Table_Name, this);
		engine.addModelChange(I_M_RMA.Table_Name, this);
		engine.addModelChange(I_PP_Order.Table_Name, this);
	}

	@Override
	public int getAD_Client_ID() {
		return clientId;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		log.info("AD_User_ID=" + AD_User_ID);
		return null;
	}

	/**
	 * Model Change of a monitored Table. Called after
	 * PO.beforeSave/PO.beforeDelete when you called addModelChange for the
	 * table
	 * 
	 * @param po
	 *            persistent object
	 * @param type
	 *            TYPE_
	 * @return error message or null
	 * @exception Exception
	 *                if the recipient wishes the change to be not accept.
	 */
	public String modelChange(PO po, int type) throws Exception {
		String result = null;
		if (type == TYPE_BEFORE_CHANGE
				|| type == TYPE_BEFORE_NEW) {
			if (po.get_TableName().equals(I_M_InventoryLine.Table_Name)) {
				MInventoryLine inventoryLine = (MInventoryLine) po;
				boolean inventoryLineChange = false;
				if(inventoryLine.is_ValueChanged(I_M_InventoryLine.COLUMNNAME_QtyInternalUse)) {
					BigDecimal qtyInternalUse = inventoryLine.getQtyInternalUse();
					if(qtyInternalUse != null
							&& qtyInternalUse.compareTo(Env.ZERO)!=0) {
						inventoryLine.set_ValueOfColumn("XXQtyInternalUse", qtyInternalUse.negate());
						inventoryLineChange = true;
					}
				}
				if(inventoryLine.is_ValueChanged("XXQtyInternalUse") && !inventoryLineChange) {
					if(inventoryLine.get_Value("XXQtyInternalUse") != null
							&& ((BigDecimal)inventoryLine.get_Value("XXQtyInternalUse")).compareTo(Env.ZERO) != 0) {
						BigDecimal reverseQtyInternalUse = (BigDecimal) inventoryLine.get_Value("XXQtyInternalUse");
						inventoryLine.setQtyInternalUse(reverseQtyInternalUse.negate());
						inventoryLineChange = true;
					}
				}
				
			} else if (po.get_TableName().equals(I_M_RMA.Table_Name)) {
				MRMA rma = (MRMA) po;
				//	Set from document type
				if(rma.is_ValueChanged(I_M_RMA.COLUMNNAME_C_DocType_ID)
						|| rma.is_new()) {
					MDocType documentType = MDocType.get(rma.getCtx(), rma.getC_DocType_ID());
					rma.set_ValueOfColumn("IsGenerateReturn", documentType.get_ValueAsBoolean("IsGenerateReturn") ? "Y" : "N");
				}
			} else if (po.get_TableName().equals(I_PP_Order.Table_Name)) {
				if(po.is_ValueChanged("WeightRegistered")) {
					MPPOrder recordWeight = (MPPOrder) po;
					MDocType documentType = MDocType.get(recordWeight.getCtx(), recordWeight.get_ValueAsInt("C_DocTypeTarget_ID"));
					BigDecimal weight = (BigDecimal) recordWeight.get_Value("WeightRegistered");
				    BigDecimal grossWeight = (BigDecimal) (recordWeight.get_Value("Weight2") != null? recordWeight.get_Value("Weight2"): Env.ZERO); 
				    BigDecimal tareWeight = (BigDecimal) (recordWeight.get_Value("Weight") != null? recordWeight.get_Value("Weight"): Env.ZERO);
				    //	Valid Weight
				    if(weight == null) {
				    	weight = Env.ZERO;
				    }
				    if(grossWeight == null) {
				    	grossWeight = Env.ZERO;
				    }
				    if(tareWeight == null) {
				    	tareWeight = Env.ZERO;
				    }
				    //	
				    Timestamp today = new Timestamp(System.currentTimeMillis());
				    //	Validate Sales Order Transaction
				    if(documentType.isSOTrx()){
				    	if(tareWeight.intValue() == 0){
				    		recordWeight.set_ValueOfColumn("Weight", weight);
				    		recordWeight.set_ValueOfColumn("DateTo", today);
				    	}else{
				    		recordWeight.set_ValueOfColumn("Weight2", weight);
				    		recordWeight.set_ValueOfColumn("DateFrom", today);
				    	}
				    } else{
				    	if(grossWeight.intValue() == 0){
				    		recordWeight.set_ValueOfColumn("Weight2", weight);
				    		recordWeight.set_ValueOfColumn("DateFrom", today);
				    	} else{
				    		recordWeight.set_ValueOfColumn("Weight", weight);
				    		recordWeight.set_ValueOfColumn("DateTo", today);
				    	}
				    }
				    //	
				    grossWeight = (BigDecimal) (recordWeight.get_Value("Weight2") != null? recordWeight.get_Value("Weight2"): Env.ZERO); 
				    tareWeight = (BigDecimal) (recordWeight.get_Value("Weight") != null? recordWeight.get_Value("Weight"): Env.ZERO);
				    //	Set Net Weight
				    recordWeight.set_ValueOfColumn("Weight3", grossWeight.subtract(tareWeight));
				}
			}
		}
		return result;
	} // modelChange

	@Override
	public String docValidate(PO po, int timing) {
		if (timing == TIMING_AFTER_COMPLETE){
			if (po.get_TableName().equals(I_M_InventoryLine.Table_Name)){
				
			}	
		}
		return null;
	}
}
