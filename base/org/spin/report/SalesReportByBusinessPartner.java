/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2015 E.R.P. Consultores y Asociados, C.A.               *
 * All Rights Reserved.                                                       *
 * Contributor(s): Carlos Parada www.erpcya.com                               *
 *****************************************************************************/
package org.spin.report;

import java.sql.Timestamp;
import java.util.ArrayList;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;


/**
 * @author Carlos Parada, cparada@erpcya.com
 * <li> FR [ 6 ] Add Support Report to Export to Marker KML Export 
 * @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/6/soporte-para-generar-archivos-con
 */
public class SalesReportByBusinessPartner extends SvrProcess{

	private StringBuffer sql = new StringBuffer();
	private ArrayList<Object> parameters = new ArrayList<Object>();
	//Parameters
	private int p_AD_Org_ID 			= 0;
	private int p_C_BP_Group_ID 		= 0;
	private String p_XX_BP_SubGroup 	= "";
	private int p_C_BPartner_ID 		= 0;
	private int p_C_CashBPartner_ID 	= 0;
	private int p_SalesRep_ID			= 0;
	private int p_M_Product_Category_Parent_ID = 0;
	private int p_M_Product_Category_ID = 0;
	private int p_M_Product_ID			= 0;
	private Timestamp p_DateInvoicedFrom= null;
	private Timestamp p_DateInvoicedTo  = null;
	private String p_IsSOTrx			= "";
	private int p_C_PaymentTerm_ID		= 0;
	private int p_C_Activity_ID			= 0;
	private int p_C_DocType_ID			= 0;
	private String p_XX_PrintFiscalDocument = "";
	private String p_DocBaseType		= "";
	private int p_XX_LoadOrder_ID		= 0;
	private boolean p_GroupByLoadOrder	= false;
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		ProcessInfoParameter[] params = getParameter();
		
		for (ProcessInfoParameter para : params) {
			if (para.getParameter()==null
					&& para.getParameter()==null)
				continue;
			
			if (para.getParameterName().equals("AD_Org_ID"))
				p_AD_Org_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("C_BP_Group_ID"))
				p_C_BP_Group_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("XX_BP_SubGroup"))
				p_XX_BP_SubGroup = para.getParameter().toString();
			else if (para.getParameterName().equals("C_BPartner_ID"))
				p_C_BPartner_ID = para.getParameterAsInt();			
			else if (para.getParameterName().equals("C_CashBPartner_ID"))
				p_C_CashBPartner_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("SalesRep_ID"))
				p_SalesRep_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("M_Product_Category_Parent_ID"))
				p_M_Product_Category_Parent_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("M_Product_Category_ID"))
				p_M_Product_Category_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("M_Product_ID"))
				p_M_Product_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("DateInvoiced")){
				if (para.getParameter()!=null)
					p_DateInvoicedFrom = (Timestamp)para.getParameter();
				if (para.getParameter_To()!=null)
					p_DateInvoicedTo = (Timestamp)para.getParameter_To();
			}
			else if (para.getParameterName().equals("IsSOTrx"))
				p_IsSOTrx = para.getParameter().toString();
			else if (para.getParameterName().equals("C_PaymentTerm_ID"))
				p_C_PaymentTerm_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("C_Activity_ID"))
				p_C_Activity_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("C_DocType_ID"))
				p_C_DocType_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("XX_PrintFiscalDocument"))
				p_XX_PrintFiscalDocument = para.getParameter().toString();
			else if (para.getParameterName().equals("DocBaseType"))
				p_DocBaseType = para.getParameter().toString();
			else if (para.getParameterName().equals("XX_LoadOrder_ID"))
				p_XX_LoadOrder_ID = para.getParameterAsInt();
			else if (para.getParameterName().equals("GroupByLoadOrder"))
				p_GroupByLoadOrder = para.getParameterAsBoolean();
		}
		
		sql.append("INSERT INTO T_BPLocation "
				+ "(AD_Client_ID, "
				+ "AD_Org_ID, "
				+ "AD_PInstance_ID, "
				+ "C_BPartner_Location_ID, "
				+ "GrandTotal, "
				+ "QtyInvoiced, "
				+ "Weight, "
				+ "DateInvoiced "
				+ (p_GroupByLoadOrder || p_XX_LoadOrder_ID !=0 ? ", XX_LoadOrder_ID " : "")
				+ (p_GroupByLoadOrder ? ", C_Invoice_ID" : ""));
		sql.append(") SELECT "
				+ "bp.AD_Client_ID, "
				+ "i.AD_Org_ID, "
				+ getAD_PInstance_ID() + " AD_PInstance_ID,"
				+ "bp.C_BPartner_Location_ID, "
				+ "SUM(il.LineNetAmt) GrandTotal, "
				+ "SUM(il.QtyInvoiced) QtyInvoiced, "
				+ "SUM(il.QtyInvoiced * COALESCE(p.Weight,0)) Weight, "
				+ "MAX(i.DateInvoiced) DateInvoiced "
				+ (p_GroupByLoadOrder ? ", i.XX_LoadOrder_ID " : (p_XX_LoadOrder_ID!=0 ? ", " + p_XX_LoadOrder_ID + " AS XX_LoadOrder_ID " : ""))
				+ (p_GroupByLoadOrder ? ", i.C_Invoice_ID" : "")
				+ "FROM "
				+ "RV_BPartnerSalesRegion bp "
				+ "INNER JOIN C_Invoice i ON (bp.C_BPartner_ID = i.C_BPartner_ID AND bp.C_BPartner_Location_ID = i.C_BPartner_Location_ID) "
				+ "INNER JOIN C_InvoiceLine il ON (i.C_Invoice_ID = il.C_Invoice_ID) "
				+ "LEFT JOIN C_DocType dt ON (i.C_DocType_ID = dt.C_DocType_ID) "
				+ "LEFT JOIN M_Product p ON (p.M_Product_ID = il.M_Product_ID) "
				+ "LEFT JOIN M_Product_Category pc ON (p.M_Product_Category_ID = pc.M_Product_Category_ID) "
				+ "LEFT JOIN M_Product_PO mpo ON (p.M_Product_ID = mpo.M_Product_ID AND mpo.IsActive = 'Y' AND mpo.IsCurrentVendor = 'Y') ");
		
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		
		int recordAffected = 0;
		sql.append("WHERE i.DocStatus IN ('CL','CO') AND bp.AD_Client_ID = ? ");
		parameters.add(getAD_Client_ID());
		
		if (p_AD_Org_ID != 0){
			sql.append("AND i.AD_Org_ID = ? ");
			parameters.add(p_AD_Org_ID);
		}
		if (p_C_BP_Group_ID != 0){
			sql.append("AND bp.C_BP_Group_ID = ? ");
			parameters.add(p_C_BP_Group_ID);
		}
		if (p_XX_BP_SubGroup != ""){
			sql.append("AND bp.XX_BP_SubGroup = ? ");
			parameters.add(p_XX_BP_SubGroup);
		}
		
		if (p_C_BPartner_ID != 0){
			sql.append("AND bp.C_BPartner_ID = ? ");
			parameters.add(p_C_BPartner_ID);
		}
		
		if (p_C_CashBPartner_ID != 0){
			sql.append("AND mpo.C_BPartner_ID = ? ");
			parameters.add(p_C_CashBPartner_ID);
		}
		
		if (p_SalesRep_ID != 0){
			sql.append("AND i.SalesRep_ID = ? ");
			parameters.add(p_SalesRep_ID);
		}
		
		if (p_M_Product_Category_Parent_ID != 0){
			sql.append("AND pc.M_Product_Category_Parent_ID = ? ");
			parameters.add(p_M_Product_Category_Parent_ID);
		}
		
		if (p_M_Product_Category_ID != 0){
			sql.append("AND p.M_Product_Category_ID = ? ");
			parameters.add(p_M_Product_Category_ID);
		}
		
		if (p_M_Product_ID != 0){
			sql.append("AND p.M_Product_ID = ? ");
			parameters.add(p_M_Product_ID);
		}
		
		if (p_DateInvoicedFrom != null){
			sql.append("AND i.DateInvoiced >= ? ");
			parameters.add(p_DateInvoicedFrom);
		}
		
		if (p_DateInvoicedTo != null){
			sql.append("AND i.DateInvoiced <= ? ");
			parameters.add(p_DateInvoicedTo);
		}
		
		if (p_IsSOTrx != ""){
			sql.append("AND i.IsSOTrx = ? ");
			parameters.add(p_IsSOTrx);
		}

		if (p_C_PaymentTerm_ID != 0){
			sql.append("AND i.C_PaymentTerm_ID = ? ");
			parameters.add(p_C_PaymentTerm_ID);
		}
		
		if (p_C_Activity_ID != 0){
			sql.append("AND i.C_Activity_ID = ? ");
			parameters.add(p_C_Activity_ID);
		}
		
		if (p_C_DocType_ID != 0){
			sql.append("AND i.C_DocType_ID = ? ");
			parameters.add(p_C_DocType_ID);
		}
		
		if (p_XX_PrintFiscalDocument != ""){
			sql.append("AND i.XX_PrintFiscalDocument = ? ");
			parameters.add(p_XX_PrintFiscalDocument);
		}
		
		if (p_DocBaseType != ""){
			sql.append("AND dt.DocBaseType = ? ");
			parameters.add(p_DocBaseType);
		}
		
		if (p_XX_LoadOrder_ID != 0){
			sql.append("AND i.XX_LoadOrder_ID = ? ");
			parameters.add(p_XX_LoadOrder_ID);
		}

		sql.append("GROUP BY " 
				+ "bp.AD_Client_ID, "
				+ "i.AD_Org_ID, "
				+ "bp.C_BPartner_Location_ID"
				+ (p_GroupByLoadOrder ? ", i.XX_LoadOrder_ID" : "")
				+ (p_GroupByLoadOrder ? ", i.C_Invoice_ID" : ""));
		
		
		recordAffected = DB.executeUpdate(sql.toString(), parameters.toArray() , false , get_TrxName());
		
		return "@Updated@ " + recordAffected;
	}

}
