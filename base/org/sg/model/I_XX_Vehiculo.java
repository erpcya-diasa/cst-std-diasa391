/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.sg.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for XX_Vehiculo
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_XX_Vehiculo 
{

    /** TableName=XX_Vehiculo */
    public static final String Table_Name = "XX_Vehiculo";

    /** AD_Table_ID=1000005 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name Capacity */
    public static final String COLUMNNAME_Capacity = "Capacity";

	/** Set Capacity	  */
	public void setCapacity (BigDecimal Capacity);

	/** Get Capacity	  */
	public BigDecimal getCapacity();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name M_Shipper_ID */
    public static final String COLUMNNAME_M_Shipper_ID = "M_Shipper_ID";

	/** Set Shipper.
	  * Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID);

	/** Get Shipper.
	  * Method or manner of product delivery
	  */
	public int getM_Shipper_ID();

	public I_M_Shipper getM_Shipper() throws RuntimeException;

    /** Column name Nombre */
    public static final String COLUMNNAME_Nombre = "Nombre";

	/** Set Nombre	  */
	public void setNombre (String Nombre);

	/** Get Nombre	  */
	public String getNombre();

    /** Column name PerformanceFuelF */
    public static final String COLUMNNAME_PerformanceFuelF = "PerformanceFuelF";

	/** Set PerformanceFuelF	  */
	public void setPerformanceFuelF (BigDecimal PerformanceFuelF);

	/** Get PerformanceFuelF	  */
	public BigDecimal getPerformanceFuelF();

    /** Column name PerformanceFuelI */
    public static final String COLUMNNAME_PerformanceFuelI = "PerformanceFuelI";

	/** Set PerformanceFuelI	  */
	public void setPerformanceFuelI (BigDecimal PerformanceFuelI);

	/** Get PerformanceFuelI	  */
	public BigDecimal getPerformanceFuelI();

    /** Column name Placa */
    public static final String COLUMNNAME_Placa = "Placa";

	/** Set Placa	  */
	public void setPlaca (String Placa);

	/** Get Placa	  */
	public String getPlaca();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name XX_DistUOM_ID */
    public static final String COLUMNNAME_XX_DistUOM_ID = "XX_DistUOM_ID";

	/** Set XX_DistUOM_ID	  */
	public void setXX_DistUOM_ID (int XX_DistUOM_ID);

	/** Get XX_DistUOM_ID	  */
	public int getXX_DistUOM_ID();

	public I_C_UOM getXX_DistUOM() throws RuntimeException;

    /** Column name XX_Vehiculo_ID */
    public static final String COLUMNNAME_XX_Vehiculo_ID = "XX_Vehiculo_ID";

	/** Set Car	  */
	public void setXX_Vehiculo_ID (int XX_Vehiculo_ID);

	/** Get Car	  */
	public int getXX_Vehiculo_ID();

    /** Column name XX_VolUOM_ID */
    public static final String COLUMNNAME_XX_VolUOM_ID = "XX_VolUOM_ID";

	/** Set XX_VolUOM_ID	  */
	public void setXX_VolUOM_ID (int XX_VolUOM_ID);

	/** Get XX_VolUOM_ID	  */
	public int getXX_VolUOM_ID();

	public I_C_UOM getXX_VolUOM() throws RuntimeException;
}
