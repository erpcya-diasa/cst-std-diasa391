/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for XX_QO_PP_Order
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_QO_PP_Order extends PO implements I_XX_QO_PP_Order, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20110708L;

    /** Standard Constructor */
    public X_XX_QO_PP_Order (Properties ctx, int XX_QO_PP_Order_ID, String trxName)
    {
      super (ctx, XX_QO_PP_Order_ID, trxName);
      /** if (XX_QO_PP_Order_ID == 0)
        {
			setDocumentQuality_ID (0);
			setPP_Order_ID (0);
			setXX_QO_PP_Order_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_QO_PP_Order (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 6 - System - Client 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_QO_PP_Order[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.eevolution.model.I_PP_Order getDocumentQuality() throws RuntimeException
    {
		return (org.eevolution.model.I_PP_Order)MTable.get(getCtx(), org.eevolution.model.I_PP_Order.Table_Name)
			.getPO(getDocumentQuality_ID(), get_TrxName());	}

	/** Set Referencia de Orden de Calidad.
		@param DocumentQuality_ID Referencia de Orden de Calidad	  */
	public void setDocumentQuality_ID (int DocumentQuality_ID)
	{
		if (DocumentQuality_ID < 1) 
			set_Value (COLUMNNAME_DocumentQuality_ID, null);
		else 
			set_Value (COLUMNNAME_DocumentQuality_ID, Integer.valueOf(DocumentQuality_ID));
	}

	/** Get Referencia de Orden de Calidad.
		@return Referencia de Orden de Calidad	  */
	public int getDocumentQuality_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_DocumentQuality_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Manufacturing Order.
		@param PP_Order_ID 
		Manufacturing Order
	  */
	public void setPP_Order_ID (int PP_Order_ID)
	{
		if (PP_Order_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_PP_Order_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_PP_Order_ID, Integer.valueOf(PP_Order_ID));
	}

	/** Get Manufacturing Order.
		@return Manufacturing Order
	  */
	public int getPP_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_PP_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Ordenes de Calidad.
		@param XX_QO_PP_Order_ID Ordenes de Calidad	  */
	public void setXX_QO_PP_Order_ID (int XX_QO_PP_Order_ID)
	{
		if (XX_QO_PP_Order_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_QO_PP_Order_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_QO_PP_Order_ID, Integer.valueOf(XX_QO_PP_Order_ID));
	}

	/** Get Ordenes de Calidad.
		@return Ordenes de Calidad	  */
	public int getXX_QO_PP_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_QO_PP_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}