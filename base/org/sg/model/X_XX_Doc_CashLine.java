/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for XX_Doc_CashLine
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_Doc_CashLine extends PO implements I_XX_Doc_CashLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20110708L;

    /** Standard Constructor */
    public X_XX_Doc_CashLine (Properties ctx, int XX_Doc_CashLine_ID, String trxName)
    {
      super (ctx, XX_Doc_CashLine_ID, trxName);
      /** if (XX_Doc_CashLine_ID == 0)
        {
			setC_Cash_ID (0);
			setXX_Doc_CashLine_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_Doc_CashLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_Doc_CashLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_Bank getC_Bank() throws RuntimeException
    {
		return (I_C_Bank)MTable.get(getCtx(), I_C_Bank.Table_Name)
			.getPO(getC_Bank_ID(), get_TrxName());	}

	/** Set Bank.
		@param C_Bank_ID 
		Bank
	  */
	public void setC_Bank_ID (int C_Bank_ID)
	{
		if (C_Bank_ID < 1) 
			set_Value (COLUMNNAME_C_Bank_ID, null);
		else 
			set_Value (COLUMNNAME_C_Bank_ID, Integer.valueOf(C_Bank_ID));
	}

	/** Get Bank.
		@return Bank
	  */
	public int getC_Bank_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Bank_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Cash getC_Cash() throws RuntimeException
    {
		return (I_C_Cash)MTable.get(getCtx(), I_C_Cash.Table_Name)
			.getPO(getC_Cash_ID(), get_TrxName());	}

	/** Set Cash Journal.
		@param C_Cash_ID 
		Cash Journal
	  */
	public void setC_Cash_ID (int C_Cash_ID)
	{
		if (C_Cash_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Cash_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Cash_ID, Integer.valueOf(C_Cash_ID));
	}

	/** Get Cash Journal.
		@return Cash Journal
	  */
	public int getC_Cash_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Cash_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Monto.
		@param Monto Monto	  */
	public void setMonto (BigDecimal Monto)
	{
		set_Value (COLUMNNAME_Monto, Monto);
	}

	/** Get Monto.
		@return Monto	  */
	public BigDecimal getMonto () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Monto);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Nº de Referencia.
		@param Nro_Referencia Nº de Referencia	  */
	public void setNro_Referencia (String Nro_Referencia)
	{
		set_Value (COLUMNNAME_Nro_Referencia, Nro_Referencia);
	}

	/** Get Nº de Referencia.
		@return Nº de Referencia	  */
	public String getNro_Referencia () 
	{
		return (String)get_Value(COLUMNNAME_Nro_Referencia);
	}

	/** TipoPago AD_Reference_ID=1000018 */
	public static final int TIPOPAGO_AD_Reference_ID=1000018;
	/** Cheque = CH */
	public static final String TIPOPAGO_Cheque = "CH";
	/** Efectivo = EF */
	public static final String TIPOPAGO_Efectivo = "EF";
	/** Nota de Credito = NC */
	public static final String TIPOPAGO_NotaDeCredito = "NC";
	/** Nota de Debito = ND */
	public static final String TIPOPAGO_NotaDeDebito = "ND";
	/** Set Tipo de Pago.
		@param TipoPago Tipo de Pago	  */
	public void setTipoPago (String TipoPago)
	{

		set_Value (COLUMNNAME_TipoPago, TipoPago);
	}

	/** Get Tipo de Pago.
		@return Tipo de Pago	  */
	public String getTipoPago () 
	{
		return (String)get_Value(COLUMNNAME_TipoPago);
	}

	/** Set Formas de Pago.
		@param XX_Doc_CashLine_ID Formas de Pago	  */
	public void setXX_Doc_CashLine_ID (int XX_Doc_CashLine_ID)
	{
		if (XX_Doc_CashLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_Doc_CashLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_Doc_CashLine_ID, Integer.valueOf(XX_Doc_CashLine_ID));
	}

	/** Get Formas de Pago.
		@return Formas de Pago	  */
	public int getXX_Doc_CashLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Doc_CashLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}