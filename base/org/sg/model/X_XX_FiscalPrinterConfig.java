/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for XX_FiscalPrinterConfig
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_FiscalPrinterConfig extends PO implements I_XX_FiscalPrinterConfig, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20121127L;

    /** Standard Constructor */
    public X_XX_FiscalPrinterConfig (Properties ctx, int XX_FiscalPrinterConfig_ID, String trxName)
    {
      super (ctx, XX_FiscalPrinterConfig_ID, trxName);
      /** if (XX_FiscalPrinterConfig_ID == 0)
        {
			setName (null);
			setPrinterId (null);
			setPuertoRS (null);
			setXX_FiscalPrinterConfig_ID (0);
			setXX_FSeparator (0);
			setXX_OS_CMD (null);
// sh
			setXX_PathSpooler (null);
        } */
    }

    /** Load Constructor */
    public X_XX_FiscalPrinterConfig (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_FiscalPrinterConfig[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Printer Id.
		@param PrinterId Printer Id	  */
	public void setPrinterId (String PrinterId)
	{
		set_Value (COLUMNNAME_PrinterId, PrinterId);
	}

	/** Get Printer Id.
		@return Printer Id	  */
	public String getPrinterId () 
	{
		return (String)get_Value(COLUMNNAME_PrinterId);
	}

	/** Set Puerto RS-232.
		@param PuertoRS Puerto RS-232	  */
	public void setPuertoRS (String PuertoRS)
	{
		set_Value (COLUMNNAME_PuertoRS, PuertoRS);
	}

	/** Get Puerto RS-232.
		@return Puerto RS-232	  */
	public String getPuertoRS () 
	{
		return (String)get_Value(COLUMNNAME_PuertoRS);
	}

	/** Set Fiscal Printer Config.
		@param XX_FiscalPrinterConfig_ID Fiscal Printer Config	  */
	public void setXX_FiscalPrinterConfig_ID (int XX_FiscalPrinterConfig_ID)
	{
		if (XX_FiscalPrinterConfig_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_FiscalPrinterConfig_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_FiscalPrinterConfig_ID, Integer.valueOf(XX_FiscalPrinterConfig_ID));
	}

	/** Get Fiscal Printer Config.
		@return Fiscal Printer Config	  */
	public int getXX_FiscalPrinterConfig_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_FiscalPrinterConfig_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set File Separator.
		@param XX_FSeparator File Separator	  */
	public void setXX_FSeparator (int XX_FSeparator)
	{
		set_Value (COLUMNNAME_XX_FSeparator, Integer.valueOf(XX_FSeparator));
	}

	/** Get File Separator.
		@return File Separator	  */
	public int getXX_FSeparator () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_FSeparator);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Last Z Date.
		@param XX_LastZDate Last Z Date	  */
	public void setXX_LastZDate (Timestamp XX_LastZDate)
	{
		set_Value (COLUMNNAME_XX_LastZDate, XX_LastZDate);
	}

	/** Get Last Z Date.
		@return Last Z Date	  */
	public Timestamp getXX_LastZDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_XX_LastZDate);
	}

	/** XX_OS_CMD AD_Reference_ID=1000056 */
	public static final int XX_OS_CMD_AD_Reference_ID=1000056;
	/** Shell Linux = sh */
	public static final String XX_OS_CMD_ShellLinux = "sh";
	/** Shell Windows =   */
	public static final String XX_OS_CMD_ShellWindows = " ";
	/** Set O.S Command.
		@param XX_OS_CMD O.S Command	  */
	public void setXX_OS_CMD (String XX_OS_CMD)
	{

		set_Value (COLUMNNAME_XX_OS_CMD, XX_OS_CMD);
	}

	/** Get O.S Command.
		@return O.S Command	  */
	public String getXX_OS_CMD () 
	{
		return (String)get_Value(COLUMNNAME_XX_OS_CMD);
	}

	/** Set Path Log.
		@param XX_PathLog Path Log	  */
	public void setXX_PathLog (String XX_PathLog)
	{
		set_Value (COLUMNNAME_XX_PathLog, XX_PathLog);
	}

	/** Get Path Log.
		@return Path Log	  */
	public String getXX_PathLog () 
	{
		return (String)get_Value(COLUMNNAME_XX_PathLog);
	}

	/** Set Path Spooler.
		@param XX_PathSpooler Path Spooler	  */
	public void setXX_PathSpooler (String XX_PathSpooler)
	{
		set_Value (COLUMNNAME_XX_PathSpooler, XX_PathSpooler);
	}

	/** Get Path Spooler.
		@return Path Spooler	  */
	public String getXX_PathSpooler () 
	{
		return (String)get_Value(COLUMNNAME_XX_PathSpooler);
	}

	/** Set Path Tmp Files.
		@param XX_PathTmp Path Tmp Files	  */
	public void setXX_PathTmp (String XX_PathTmp)
	{
		set_Value (COLUMNNAME_XX_PathTmp, XX_PathTmp);
	}

	/** Get Path Tmp Files.
		@return Path Tmp Files	  */
	public String getXX_PathTmp () 
	{
		return (String)get_Value(COLUMNNAME_XX_PathTmp);
	}

	/** Set Speed.
		@param XX_Speed Speed	  */
	public void setXX_Speed (int XX_Speed)
	{
		set_Value (COLUMNNAME_XX_Speed, Integer.valueOf(XX_Speed));
	}

	/** Get Speed.
		@return Speed	  */
	public int getXX_Speed () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Speed);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}