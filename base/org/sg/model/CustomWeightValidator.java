/*************************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                              *
 * This program is free software; you can redistribute it and/or modify it           *
 * under the terms version 2 of the GNU General Public License as published          *
 * by the Free Software Foundation. This program is distributed in the hope          *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied        *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 * See the GNU General Public License for more details.                              *
 * You should have received a copy of the GNU General Public License along           *
 * with this program; if not, write to the Free Software Foundation, Inc.,           *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                            *
 * For the text or an alternative of this public license, you may reach us           *
 * Copyright (C) 2012-2013 E.R.P. Consultores y Asociados, S.A. All Rights Reserved. *
 * Contributor(s): Yamel Senih www.erpconsultoresyasociados.com                      *
 *************************************************************************************/
package org.sg.model;

import org.compiere.model.I_C_Invoice;
import org.compiere.model.MClient;
import org.compiere.model.MClientInfo;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.model.MSysConfig;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.eevolution.model.MPPOrder;

/**
 * @author Yamel Senih
 *
 */
public class CustomWeightValidator implements ModelValidator {

	/**
	 * *** Constructor de la Clase ***
	 * @author Yamel Senih 02/04/2013, 14:35:00
	 */
	public CustomWeightValidator() {
		super();
	}

	/** Logger */
	private static CLogger log = CLogger.getCLogger(CustomWeightValidator.class);
	/** Client */
	private int m_AD_Client_ID = -1;
	/**	Orgganization */
	private int m_AD_Org_ID = -1;
	
	/**
	 * Initialize Validation
	 * 
	 * @param engine
	 *            validation engine
	 * @param client
	 *            client
	 */
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		// client = null for global validator
		if (client != null) {
			m_AD_Client_ID = client.getAD_Client_ID();
			log.info(client.toString());
		} else {
			log.info("Initializing global validator: " + this.toString());
		}
		// We want to be informed when PP_Order is created/changed
		engine.addModelChange(MPPOrder.Table_Name, this);
		//	InOut Validator
		engine.addDocValidate(MInvoice.Table_Name, this);
		engine.addDocValidate(MInOut.Table_Name, this);
		
	}

	@Override
	public int getAD_Client_ID() {
		return m_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		log.info("AD_User_ID=" + AD_User_ID);
		m_AD_Org_ID = AD_Org_ID;
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {
		if (po.get_TableName().equals("PP_Order") && type == TYPE_BEFORE_NEW)
		{
			MPPOrder order = (MPPOrder)po;
			log.info(order.toString());
			//	Yamel Senih 2013-03-26 17:39 Add Default UOM
			int m_C_UOM_ID = MSysConfig.getIntValue("UOM_SCALE_ID", 0, m_AD_Client_ID, m_AD_Org_ID);
			if(m_C_UOM_ID == 0)
				m_C_UOM_ID = MClientInfo.get(Env.getCtx(), m_AD_Client_ID).getC_UOM_Weight_ID();
			if(m_C_UOM_ID != 0)
				order.setC_UOM_ID(m_C_UOM_ID);
			log.fine("C_UOM_ID="+m_C_UOM_ID);
			//	End Yamel Senih

		}
		return null;
	}

	@Override
	public String docValidate(PO po, int timing) {
		log.info(po.get_TableName() + " Timing: "+timing);
		
		//	Yamel Senih 07/12/2012 12:09 PA
		//	Yamel Senih Add Model Validator 2013-04-04 12:02 VE
		//	Valid if Printed Fiscal Document before reverse
		//	If Printed Fiscal Document
		//	Ignore all after Complete events
		if (timing == TIMING_BEFORE_REVERSECORRECT) {
			if (po.get_TableName().equals(MInOut.Table_Name))
			{
				MInOut ino = (MInOut)po;
				//	
				int m_Reference_ID = 0;
				String column = null;
				
				//	
				if(ino.getMovementType().equals(MInOut.MOVEMENTTYPE_CustomerShipment)){
					m_Reference_ID = ino.getC_Order_ID();
					//	
					column = I_C_Invoice.COLUMNNAME_C_Order_ID;
				} else if(ino.getMovementType().equals(MInOut.MOVEMENTTYPE_CustomerReturns)) {
					m_Reference_ID = ino.getM_RMA_ID();
					//	
					column = I_C_Invoice.COLUMNNAME_M_RMA_ID;
				}
				//	
				log.info(column + "=" + m_Reference_ID);
				//	Verify if exist Document
				if(m_Reference_ID != 0){
					MInvoice inv = (MInvoice) new Query(Env.getCtx(), MInvoice.Table_Name, 
							column+"=? " +
							"AND XX_PrintFiscalDocument='Y'", po.get_TrxName())
						.setParameters(m_Reference_ID).<MInvoice>first();
					//	
					if(inv != null)
						return "@DocumentNo@ " + inv.getDocumentNo() + " @SentToFiscalPrinter@ ";
				}
				log.info(po.toString());
			} else if(po.get_TableName().equals(MInvoice.Table_Name)){
				MInvoice inv = (MInvoice)po;
				if (inv.get_ValueAsBoolean("XX_PrintFiscalDocument")){
					return "@DocumentNo@ " + inv.getDocumentNo() + " @SentToFiscalPrinter@ ";
				}
			}
		}
		return null;
	}

}
