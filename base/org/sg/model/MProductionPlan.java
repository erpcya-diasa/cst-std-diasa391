/**
 * @finalidad 
 * @author Yamel Senih
 * @date 10/11/2011
 */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.X_M_ProductionPlan;

/**
 * @author Yamel Senih 10/11/2011, 17:16:48
 *
 */
public class MProductionPlan extends X_M_ProductionPlan{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MProductionPlan(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	public MProductionPlan(Properties ctx, int M_Production_ID, String trxName) {
		super(ctx, M_Production_ID, trxName);
		// TODO Auto-generated constructor stub
	}

}
