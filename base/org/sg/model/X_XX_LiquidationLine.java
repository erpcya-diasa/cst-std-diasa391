/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for XX_LiquidationLine
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_LiquidationLine extends PO implements I_XX_LiquidationLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20110731L;

    /** Standard Constructor */
    public X_XX_LiquidationLine (Properties ctx, int XX_LiquidationLine_ID, String trxName)
    {
      super (ctx, XX_LiquidationLine_ID, trxName);
      /** if (XX_LiquidationLine_ID == 0)
        {
			setAmt (Env.ZERO);
			setHumedad (Env.ZERO);
			setHumedadN (Env.ZERO);
			setHumedadW (Env.ZERO);
			setImpurezaN (Env.ZERO);
			setImpurezas (Env.ZERO);
			setImpurezaW (Env.ZERO);
			setLiqAmt (Env.ZERO);
			setPrice (Env.ZERO);
			setPriceN (Env.ZERO);
			setWeightPay (Env.ZERO);
			setXX_Boleta_ID (0);
			setXX_LiquidationLine_ID (0);
			setXX_NetWeight (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_XX_LiquidationLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_LiquidationLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Amount.
		@param Amt 
		Amount
	  */
	public void setAmt (BigDecimal Amt)
	{
		set_Value (COLUMNNAME_Amt, Amt);
	}

	/** Get Amount.
		@return Amount
	  */
	public BigDecimal getAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Amt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Humedad.
		@param Humedad Humedad	  */
	public void setHumedad (BigDecimal Humedad)
	{
		set_Value (COLUMNNAME_Humedad, Humedad);
	}

	/** Get Humedad.
		@return Humedad	  */
	public BigDecimal getHumedad () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Humedad);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Humedad Nueva.
		@param HumedadN Humedad Nueva	  */
	public void setHumedadN (BigDecimal HumedadN)
	{
		set_Value (COLUMNNAME_HumedadN, HumedadN);
	}

	/** Get Humedad Nueva.
		@return Humedad Nueva	  */
	public BigDecimal getHumedadN () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HumedadN);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Peso de Humedad.
		@param HumedadW Peso de Humedad	  */
	public void setHumedadW (BigDecimal HumedadW)
	{
		set_Value (COLUMNNAME_HumedadW, HumedadW);
	}

	/** Get Peso de Humedad.
		@return Peso de Humedad	  */
	public BigDecimal getHumedadW () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_HumedadW);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Impureza Nueva.
		@param ImpurezaN Impureza Nueva	  */
	public void setImpurezaN (BigDecimal ImpurezaN)
	{
		set_Value (COLUMNNAME_ImpurezaN, ImpurezaN);
	}

	/** Get Impureza Nueva.
		@return Impureza Nueva	  */
	public BigDecimal getImpurezaN () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ImpurezaN);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Impurezas.
		@param Impurezas Impurezas	  */
	public void setImpurezas (BigDecimal Impurezas)
	{
		set_Value (COLUMNNAME_Impurezas, Impurezas);
	}

	/** Get Impurezas.
		@return Impurezas	  */
	public BigDecimal getImpurezas () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Impurezas);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Peso de Impureza.
		@param ImpurezaW Peso de Impureza	  */
	public void setImpurezaW (BigDecimal ImpurezaW)
	{
		set_Value (COLUMNNAME_ImpurezaW, ImpurezaW);
	}

	/** Get Peso de Impureza.
		@return Peso de Impureza	  */
	public BigDecimal getImpurezaW () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ImpurezaW);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Liquidation Amount.
		@param LiqAmt Liquidation Amount	  */
	public void setLiqAmt (BigDecimal LiqAmt)
	{
		set_Value (COLUMNNAME_LiqAmt, LiqAmt);
	}

	/** Get Liquidation Amount.
		@return Liquidation Amount	  */
	public BigDecimal getLiqAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LiqAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Price.
		@param Price 
		Price
	  */
	public void setPrice (BigDecimal Price)
	{
		set_Value (COLUMNNAME_Price, Price);
	}

	/** Get Price.
		@return Price
	  */
	public BigDecimal getPrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Price);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Price New.
		@param PriceN Price New	  */
	public void setPriceN (BigDecimal PriceN)
	{
		set_Value (COLUMNNAME_PriceN, PriceN);
	}

	/** Get Price New.
		@return Price New	  */
	public BigDecimal getPriceN () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceN);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Weight Payment.
		@param WeightPay Weight Payment	  */
	public void setWeightPay (BigDecimal WeightPay)
	{
		set_Value (COLUMNNAME_WeightPay, WeightPay);
	}

	/** Get Weight Payment.
		@return Weight Payment	  */
	public BigDecimal getWeightPay () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_WeightPay);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.eevolution.model.I_PP_Order getXX_Boleta() throws RuntimeException
    {
		return (org.eevolution.model.I_PP_Order)MTable.get(getCtx(), org.eevolution.model.I_PP_Order.Table_Name)
			.getPO(getXX_Boleta_ID(), get_TrxName());	}

	/** Set Boleta de Romana.
		@param XX_Boleta_ID Boleta de Romana	  */
	public void setXX_Boleta_ID (int XX_Boleta_ID)
	{
		if (XX_Boleta_ID < 1) 
			set_Value (COLUMNNAME_XX_Boleta_ID, null);
		else 
			set_Value (COLUMNNAME_XX_Boleta_ID, Integer.valueOf(XX_Boleta_ID));
	}

	/** Get Boleta de Romana.
		@return Boleta de Romana	  */
	public int getXX_Boleta_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Boleta_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_XX_Liquidation getXX_Liquidation() throws RuntimeException
    {
		return (I_XX_Liquidation)MTable.get(getCtx(), I_XX_Liquidation.Table_Name)
			.getPO(getXX_Liquidation_ID(), get_TrxName());	}

	/** Set Liquidation.
		@param XX_Liquidation_ID Liquidation	  */
	public void setXX_Liquidation_ID (int XX_Liquidation_ID)
	{
		if (XX_Liquidation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_Liquidation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_Liquidation_ID, Integer.valueOf(XX_Liquidation_ID));
	}

	/** Get Liquidation.
		@return Liquidation	  */
	public int getXX_Liquidation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Liquidation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Liquidation Line.
		@param XX_LiquidationLine_ID Liquidation Line	  */
	public void setXX_LiquidationLine_ID (int XX_LiquidationLine_ID)
	{
		if (XX_LiquidationLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_LiquidationLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_LiquidationLine_ID, Integer.valueOf(XX_LiquidationLine_ID));
	}

	/** Get Liquidation Line.
		@return Liquidation Line	  */
	public int getXX_LiquidationLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_LiquidationLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Net Weight.
		@param XX_NetWeight Net Weight	  */
	public void setXX_NetWeight (BigDecimal XX_NetWeight)
	{
		set_Value (COLUMNNAME_XX_NetWeight, XX_NetWeight);
	}

	/** Get Net Weight.
		@return Net Weight	  */
	public BigDecimal getXX_NetWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_XX_NetWeight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}