/**
 * @finalidad 
 * @author Yamel Senih
 * @date 08/03/2012
 */
package org.sg.model;

import java.math.BigDecimal;

import org.compiere.util.Env;

/**
 * @author Yamel Senih 08/03/2012, 23:46:54
 *
 */
public class BufferTableSelect {
	/**
	 * 
	 * *** Constructor de la Clase ***
	 * @author Yamel Senih 08/03/2012, 23:47:19
	 */
	public BufferTableSelect(int m_Record_ID, BigDecimal qty, Integer seqNo){
		this.m_Record_ID = m_Record_ID;
		this.qty = qty;
		this.seqNo = seqNo;
	}
	
	/**
	 * Establece el valor del ID
	 * @author Yamel Senih 08/03/2012, 23:51:42
	 * @param m_Record_ID
	 * @return void
	 */
	public void setRecord_ID(int m_Record_ID){
		this.m_Record_ID = m_Record_ID;
	}
	
	/**
	 * Obtiene el valor del ID
	 * @author Yamel Senih 08/03/2012, 23:52:17
	 * @return
	 * @return int
	 */
	public int getRecord_ID(){
		return this.m_Record_ID;
	}
	
	/**
	 * Establece el valor de la cantidad
	 * @author Yamel Senih 08/03/2012, 23:53:07
	 * @param qty
	 * @return void
	 */
	public void setQty(BigDecimal qty){
		this.qty = qty;
	}
	
	/**
	 * Obtiene el valor de la Cantidad
	 * @author Yamel Senih 08/03/2012, 23:53:37
	 * @return
	 * @return BigDecimal
	 */
	public BigDecimal getQty(){
		return this.qty;
	}
	
	/**
	 * Establece el valor de la secuencia
	 * @author Yamel Senih 08/03/2012, 23:54:20
	 * @param seqNo
	 * @return void
	 */
	public void setSeqNo(Integer seqNo){
		this.seqNo = seqNo;
	}
	
	/**
	 * Obtiene el valor de la Secuencia
	 * @author Yamel Senih 08/03/2012, 23:55:03
	 * @return
	 * @return Integer
	 */
	public Integer getSeqNo(){
		return this.seqNo;
	}
	
	public String toString(){
		return "m_Record_ID = " + m_Record_ID 
				+ " qty = " + qty 
				+ "seqNo = " + seqNo;
	}
	
	/**	Record ID	*/
	private int m_Record_ID = 0;
	/**	Quantity	*/
	private BigDecimal qty = Env.ZERO;
	/**	Sequence	*/
	private Integer seqNo = 0;
}
