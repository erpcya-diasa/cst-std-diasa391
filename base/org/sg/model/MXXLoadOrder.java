/**
 * 
 */
package org.sg.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.model.MDocType;
import org.compiere.model.MMovement;

/**
 * @author Yamel Senih 27/06/2011, 02:57
 *
 */
public class MXXLoadOrder extends X_XX_LoadOrder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8637350886012568428L;
	
	public MXXLoadOrder(Properties ctx, int XX_LoadOrder_ID, String trxName) {
		super(ctx, XX_LoadOrder_ID, trxName);
		// TODO Auto-generated constructor stub
	}

	public MXXLoadOrder(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MMovement createInventoryMovement(Timestamp p_Date) {
		if (getXX_LoadOrder_ID()!=0) {
			MDocType dTypeOrder = (MDocType) getC_DocTypeOrder();
			MDocType dTypeMov   = null;
			
			if (dTypeOrder.get_ValueAsInt("C_DocTypeMovementPallet_ID")!=0)
				dTypeMov = new MDocType(getCtx(), dTypeOrder.get_ValueAsInt("C_DocTypeMovementPallet_ID"), get_TrxName());
			else
				dTypeMov = new MDocType(getCtx(), MDocType.getDocType(MDocType.DOCBASETYPE_MaterialMovement), get_TrxName());
			
			MMovement inventory = new MMovement(getCtx(), 0, get_TrxName());
			inventory.setMovementDate(p_Date!= null ? p_Date : getDateDoc());
			inventory.setC_DocType_ID(dTypeMov.getC_DocType_ID());
			inventory.saveEx();
			
			setM_MovementPalletOut_ID(inventory.getM_Movement_ID());
			save();
			return inventory;
		}
		
		return null;
	}

}
