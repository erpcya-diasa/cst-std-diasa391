/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_Conductor
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_Conductor extends PO implements I_XX_Conductor, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20110708L;

    /** Standard Constructor */
    public X_XX_Conductor (Properties ctx, int XX_Conductor_ID, String trxName)
    {
      super (ctx, XX_Conductor_ID, trxName);
      /** if (XX_Conductor_ID == 0)
        {
			setCedula (null);
			setNombre (null);
			setXX_Conductor_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_Conductor (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_Conductor[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Cédula.
		@param Cedula Cédula	  */
	public void setCedula (String Cedula)
	{
		set_Value (COLUMNNAME_Cedula, Cedula);
	}

	/** Get Cédula.
		@return Cédula	  */
	public String getCedula () 
	{
		return (String)get_Value(COLUMNNAME_Cedula);
	}

	public I_M_Shipper getM_Shipper() throws RuntimeException
    {
		return (I_M_Shipper)MTable.get(getCtx(), I_M_Shipper.Table_Name)
			.getPO(getM_Shipper_ID(), get_TrxName());	}

	/** Set Shipper.
		@param M_Shipper_ID 
		Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID)
	{
		if (M_Shipper_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Shipper_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Shipper_ID, Integer.valueOf(M_Shipper_ID));
	}

	/** Get Shipper.
		@return Method or manner of product delivery
	  */
	public int getM_Shipper_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Shipper_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Nombre.
		@param Nombre Nombre	  */
	public void setNombre (String Nombre)
	{
		set_Value (COLUMNNAME_Nombre, Nombre);
	}

	/** Get Nombre.
		@return Nombre	  */
	public String getNombre () 
	{
		return (String)get_Value(COLUMNNAME_Nombre);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getNombre());
    }

	/** Set Conductor.
		@param XX_Conductor_ID Conductor	  */
	public void setXX_Conductor_ID (int XX_Conductor_ID)
	{
		if (XX_Conductor_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_Conductor_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_Conductor_ID, Integer.valueOf(XX_Conductor_ID));
	}

	/** Get Conductor.
		@return Conductor	  */
	public int getXX_Conductor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Conductor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}