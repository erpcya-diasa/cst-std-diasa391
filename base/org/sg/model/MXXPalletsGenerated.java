/**
 * 
 */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * 
 * @author Yamel Senih 03/04/2012, 14:54:46
 *
 */
public class MXXPalletsGenerated extends X_XX_PalletsGenerated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7194108480610070579L;
	
	public MXXPalletsGenerated(Properties ctx, int XX_PalletsGenerated_ID, String trxName) {
		super(ctx, XX_PalletsGenerated_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MXXPalletsGenerated(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
}
