/**
 * 
 */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;

/**
 * @author Yamel Senih 06/05/2012, 14:35:00
 *
 */
public class MXXDocFiscalPrinterNo extends X_XX_DocFiscalPrinterNo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7194108480610070579L;
	
	public MXXDocFiscalPrinterNo(Properties ctx, int XX_DocFiscalPrinterNo_ID, String trxName) {
		super(ctx, XX_DocFiscalPrinterNo_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MXXDocFiscalPrinterNo(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Obtiene la Proxima Secuencia
	 * @author Yamel Senih 06/04/2012, 14:36:50
	 * @param ctx
	 * @param X_XX_DocFiscalPrinterNo_ID
	 * @param trxName
	 * @return
	 * @return String
	 * @throws Exception 
	 */
	public String nextSequence() throws Exception{
		String seqNo = null;
		String prefix = null;
		String suffix = null;
		int nextSeq = 0;
		int length = 0;
		//
		prefix = getPrefix();
		suffix = getSuffix();
		nextSeq = getCurrentNext();
		length = getXX_Length();
		//	
		StringBuffer seq = new StringBuffer();
		//	Prefix
		if(prefix != null)
			seq.append(prefix);
		
		//seq.append(nextSeq);
		
		String next = String.valueOf(nextSeq);
		if(next != null){
			String zero = "";
			if(length > next.length()){
				for(int i = 0; i < length - next.length(); i++){
					zero += "0"; 
				}
			} 
			seq.append(zero + next);
			//else
				//throw new AdempiereException("@XX_DocFiscalPrinterNo_ID@ @XX_Length@");
		} else
			throw new AdempiereException("@CurrentNext@ Null");
		//	Suffix
		if(suffix != null)
			seq.append(suffix);
		seqNo = seq.toString();
		setCurrentNext(nextSeq + 1);
		return seqNo;
	}
}
