/**
 * @finalidad 
 * @author Yamel Senih
 * @date 10/11/2011
 */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.X_M_ProductionLine;

/**
 * @author Yamel Senih 10/11/2011, 17:20:46
 *
 */
public class MProductionLine extends X_M_ProductionLine{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MProductionLine(Properties ctx, int M_ProductionLine_ID, String trxName) {
		super(ctx, M_ProductionLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MProductionLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
}
