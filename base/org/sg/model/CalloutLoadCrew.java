package org.sg.model;


import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

public class CalloutLoadCrew extends CalloutEngine

{

	public String shipDate (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		if (isCalloutActive())
			return "";
		Integer m_XX_LoadOrder_ID = (Integer)mTab.getValue("XX_LoadOrder_ID");
		if (m_XX_LoadOrder_ID != null && m_XX_LoadOrder_ID.intValue() != 0) {
			//MXXLoadOrder Nombre de la Clase
			//m_loadorder Nombre del Objeto
			//New Instancia de Clase MXXLoadOrder Control + Espaci (Cobinación de Tablas) Selecciono el Tipo Integer y Variables de Constructor de Clase 
			//Luego coloca la variable m_ es la que tiene el ID
			
			MXXLoadOrder m_loadorder = new MXXLoadOrder(ctx, m_XX_LoadOrder_ID, null);
			//Clase primera letra mayuscula y Objeto Primera letra Mayuscula
			Timestamp shipDate = m_loadorder.getShipDate();
			//"ShipDate"= Nombre de Base de Datos, shipDate = Mi Variable
			mTab.setValue("ShipDate", shipDate);
		}
		return "";
	}
	
	public String qtyDays (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//value es igual al valor de la columna que tiene el callout si es 
		//mField trae la informacion de Metadatos el nombre de la columna en la base de datos y sus propiedades
		if (isCalloutActive() || value == null)
			return "";
		Timestamp m_ShipDate = (Timestamp)mTab.getValue("ShipDate");
		
		//value es el valor en el momento de la columna que tiene el call out
		Timestamp m_FinishedDate = (Timestamp)value;
		
		Calendar fechaAnterior=Calendar.getInstance();
	    fechaAnterior.setTime(m_ShipDate);
	    
	    Calendar fechaActual = Calendar.getInstance();
	    fechaActual.setTime(m_FinishedDate);
	    
	    
	    long diferMills = fechaActual.getTimeInMillis()-fechaAnterior.getTimeInMillis();
	    long diferDias = diferMills/(1000*60*60*24);
	    //DateFinish Nombre de Base de Datos
	    
	    //if (diferDias == 0) {
	    	diferDias = diferDias + 1;
	    //}
	    
	    mTab.setValue("QtyDays", diferDias);
	    return "";
	}
	
	
	
}
