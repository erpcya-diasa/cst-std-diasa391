package org.sg.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;
import org.compiere.util.DB;
import org.eevolution.model.MPPOrder;


public class CalloutPP_Order extends CalloutEngine {
	
	/**
	 * @author Yamel Senih 29/04/2011, 9:59
	 * Obtiene el La instancia de conjuntos de atributos de la orden de calidad
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String getM_SetInstance (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){

		Integer m_DocumentQuality_ID = (Integer)value;
		if(m_DocumentQuality_ID == null 
				|| m_DocumentQuality_ID.intValue() == 0)
		return "";
		
		MPPOrder order = new MPPOrder(ctx, m_DocumentQuality_ID.intValue(), null);
		mTab.setValue("M_AttributeSetInstance_ID", order.getM_AttributeSetInstance_ID());
		mTab.setValue("M_Product_ID", order.getM_Product_ID());
		mTab.setValue("C_BPartnerRelation_ID", order.get_ValueAsInt("C_BPartnerRelation_ID"));
		mTab.setValue("XX_Vehiculo_ID", order.get_ValueAsInt("XX_Vehiculo_ID"));
		mTab.setValue("M_Warehouse_ID", order.getM_Warehouse_ID());
		mTab.setValue("AD_Workflow_ID", order.getAD_Workflow_ID());
		mTab.setValue("PP_Product_BOM_ID", order.getPP_Product_BOM_ID());
		return "";
	}	//	getM_SetInstance
	
	/**
	 * @author Yamel Senih 09/06/2011, 13:30
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String bPartner (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		Integer m_C_BPartner_ID = (Integer)value;
		if(m_C_BPartner_ID == null 
				|| m_C_BPartner_ID.intValue() == 0)
		return "";
		MBPartner socio = new MBPartner(ctx, m_C_BPartner_ID.intValue(), null);
		mTab.setValue("IsInclude", socio.get_Value("IsInclude"));
		return "";
	}
	
	/**
	 * @author Yamel Senih 09/06/2011, 13:33
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String shippingGuide (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		Integer m_M_ShippingGuide_ID = (Integer)value;
		if(m_M_ShippingGuide_ID == null 
				|| m_M_ShippingGuide_ID.intValue() == 0)
		return "";
		
		PreparedStatement pstmt = null;
		ResultSet res = null;
		String sql = new String("SELECT oc.XX_Vehiculo_ID, oc.C_BPartner_ID " +
				"FROM M_ShippingGuide oc WHERE oc.M_ShippingGuide_ID = ?");
		pstmt = DB.prepareStatement(sql, null);
		try {
			pstmt.setInt(1, m_M_ShippingGuide_ID.intValue());
			
			int m_C_BPartner_ID = 0;
			int m_XX_Vehiculo_ID = 0;
			
			res = pstmt.executeQuery();
			if(res != null){
				if(res.next()){
					m_XX_Vehiculo_ID = res.getInt(1);
					m_C_BPartner_ID = res.getInt(2);
				}
			}
			
			mTab.setValue("XX_Vehiculo_ID", m_XX_Vehiculo_ID);
			mTab.setValue("C_BPartnerRelation_ID", m_C_BPartner_ID);
		} catch (SQLException e) {
			DB.close(res, pstmt);
			throw new AdempiereException(); 
		} 
		
		return "";
	}
	
	
}
