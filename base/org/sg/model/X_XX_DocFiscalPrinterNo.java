/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for XX_DocFiscalPrinterNo
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_DocFiscalPrinterNo extends PO implements I_XX_DocFiscalPrinterNo, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120412L;

    /** Standard Constructor */
    public X_XX_DocFiscalPrinterNo (Properties ctx, int XX_DocFiscalPrinterNo_ID, String trxName)
    {
      super (ctx, XX_DocFiscalPrinterNo_ID, trxName);
      /** if (XX_DocFiscalPrinterNo_ID == 0)
        {
			setCurrentNext (0);
			setName (null);
			setXX_DocBaseType_Fiscal (null);
			setXX_DocFiscalPrinterNo_ID (0);
			setXX_Length (0);
// 1
        } */
    }

    /** Load Constructor */
    public X_XX_DocFiscalPrinterNo (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_DocFiscalPrinterNo[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Current Next.
		@param CurrentNext 
		The next number to be used
	  */
	public void setCurrentNext (int CurrentNext)
	{
		set_Value (COLUMNNAME_CurrentNext, Integer.valueOf(CurrentNext));
	}

	/** Get Current Next.
		@return The next number to be used
	  */
	public int getCurrentNext () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_CurrentNext);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set Prefix.
		@param Prefix 
		Prefix before the sequence number
	  */
	public void setPrefix (String Prefix)
	{
		set_Value (COLUMNNAME_Prefix, Prefix);
	}

	/** Get Prefix.
		@return Prefix before the sequence number
	  */
	public String getPrefix () 
	{
		return (String)get_Value(COLUMNNAME_Prefix);
	}

	/** Set Suffix.
		@param Suffix 
		Suffix after the number
	  */
	public void setSuffix (String Suffix)
	{
		set_Value (COLUMNNAME_Suffix, Suffix);
	}

	/** Get Suffix.
		@return Suffix after the number
	  */
	public String getSuffix () 
	{
		return (String)get_Value(COLUMNNAME_Suffix);
	}

	/** XX_DocBaseType_Fiscal AD_Reference_ID=1000057 */
	public static final int XX_DOCBASETYPE_FISCAL_AD_Reference_ID=1000057;
	/** Invoice = A */
	public static final String XX_DOCBASETYPE_FISCAL_Invoice = "A";
	/** Debit Memo = B */
	public static final String XX_DOCBASETYPE_FISCAL_DebitMemo = "B";
	/** Credit Memo = D */
	public static final String XX_DOCBASETYPE_FISCAL_CreditMemo = "D";
	/** Set Document Base Type Fiscal.
		@param XX_DocBaseType_Fiscal Document Base Type Fiscal	  */
	public void setXX_DocBaseType_Fiscal (String XX_DocBaseType_Fiscal)
	{

		set_Value (COLUMNNAME_XX_DocBaseType_Fiscal, XX_DocBaseType_Fiscal);
	}

	/** Get Document Base Type Fiscal.
		@return Document Base Type Fiscal	  */
	public String getXX_DocBaseType_Fiscal () 
	{
		return (String)get_Value(COLUMNNAME_XX_DocBaseType_Fiscal);
	}

	/** Set Document Fiscal Printer Number.
		@param XX_DocFiscalPrinterNo_ID Document Fiscal Printer Number	  */
	public void setXX_DocFiscalPrinterNo_ID (int XX_DocFiscalPrinterNo_ID)
	{
		if (XX_DocFiscalPrinterNo_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_DocFiscalPrinterNo_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_DocFiscalPrinterNo_ID, Integer.valueOf(XX_DocFiscalPrinterNo_ID));
	}

	/** Get Document Fiscal Printer Number.
		@return Document Fiscal Printer Number	  */
	public int getXX_DocFiscalPrinterNo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_DocFiscalPrinterNo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Length.
		@param XX_Length Length	  */
	public void setXX_Length (int XX_Length)
	{
		set_Value (COLUMNNAME_XX_Length, Integer.valueOf(XX_Length));
	}

	/** Get Length.
		@return Length	  */
	public int getXX_Length () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Length);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}