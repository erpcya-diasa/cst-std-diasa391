/**
 * 
 */
package org.sg.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.util.DB;

/**
 * 
 * @author Yamel Senih 03/04/2012, 14:53:19
 *
 */
public class MXXLODocGenerated extends X_XX_LODocGenerated{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7194108480610070579L;
	
	public MXXLODocGenerated(Properties ctx, int XX_LODocGenerated_ID, String trxName) {
		super(ctx, XX_LODocGenerated_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MXXLODocGenerated(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}	
	
	/**
	 * Verifica si existe una linea generada que hace referecia a una entrega o factura y de no existir la crea
	 * @author Yamel Senih 05/04/2012, 12:02:05
	 * @param ctx
	 * @param XX_LoadOrderLine_ID
	 * @param trxName
	 * @param IsInOut
	 * @return
	 * @throws SQLException
	 * @return MXXLODocGenerated
	 */
	public static MXXLODocGenerated getDocGenInOutInvoice(Properties ctx, int XX_LoadOrderLine_ID, 
			String trxName, boolean IsInOut) throws SQLException{
		StringBuffer sql = new StringBuffer("SELECT ldog.XX_LODocGenerated_ID " +
				"FROM XX_LODocGenerated ldog " +
					"WHERE ldog.XX_LoadOrderLine_ID = " + XX_LoadOrderLine_ID + " ");
		if(IsInOut)
			sql.append("AND ldog.M_InOut_ID IS NULL");
		else
			sql.append("AND ldog.C_Invoice_ID IS NULL");
		int m_XX_LODocGenerated_ID = 0;
		MXXLODocGenerated m_loDocGen = null;
		ResultSet rs;
		PreparedStatement pstmt = null;
		
		pstmt = DB.prepareStatement (sql.toString(), trxName);
		rs = pstmt.executeQuery();
		
		if(rs.next ()){
			m_XX_LODocGenerated_ID = rs.getInt("XX_LODocGenerated_ID");
			m_loDocGen = new MXXLODocGenerated (ctx, m_XX_LODocGenerated_ID, trxName);
		} else {
			m_loDocGen = new MXXLODocGenerated (ctx, 0, trxName);
			m_loDocGen.setXX_LoadOrderLine_ID(XX_LoadOrderLine_ID);
		}

		if (pstmt != null)
			pstmt.close ();
		pstmt = null;
		
		return m_loDocGen;
	}
	
	@Override
	protected boolean afterSave(boolean newRecord, boolean success) {
		// TODO Auto-generated method stub
		
		MXXLoadOrderLine loLine = new MXXLoadOrderLine(getCtx(), getXX_LoadOrderLine_ID(), get_TrxName());

		if (getC_Invoice_ID()!=0){
			MInvoice inv = (MInvoice) getC_Invoice();
			inv.set_ValueOfColumn("XX_LoadOrder_ID", loLine.getXX_LoadOrder_ID());
			inv.save(get_TrxName());
		}
		
		if (getM_InOut_ID()!=0){
			MInOut io = (MInOut) getM_InOut();
			io.set_ValueOfColumn("XX_LoadOrder_ID", loLine.getXX_LoadOrder_ID());
			io.save(get_TrxName());
		}
				
		return super.afterSave(newRecord, success);
	}
}
