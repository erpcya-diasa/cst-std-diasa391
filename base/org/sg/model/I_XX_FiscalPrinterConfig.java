/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.sg.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for XX_FiscalPrinterConfig
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_XX_FiscalPrinterConfig 
{

    /** TableName=XX_FiscalPrinterConfig */
    public static final String Table_Name = "XX_FiscalPrinterConfig";

    /** AD_Table_ID=1000064 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Name */
    public static final String COLUMNNAME_Name = "Name";

	/** Set Name.
	  * Alphanumeric identifier of the entity
	  */
	public void setName (String Name);

	/** Get Name.
	  * Alphanumeric identifier of the entity
	  */
	public String getName();

    /** Column name PrinterId */
    public static final String COLUMNNAME_PrinterId = "PrinterId";

	/** Set Printer Id	  */
	public void setPrinterId (String PrinterId);

	/** Get Printer Id	  */
	public String getPrinterId();

    /** Column name PuertoRS */
    public static final String COLUMNNAME_PuertoRS = "PuertoRS";

	/** Set Puerto RS-232	  */
	public void setPuertoRS (String PuertoRS);

	/** Get Puerto RS-232	  */
	public String getPuertoRS();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name XX_FiscalPrinterConfig_ID */
    public static final String COLUMNNAME_XX_FiscalPrinterConfig_ID = "XX_FiscalPrinterConfig_ID";

	/** Set Fiscal Printer Config	  */
	public void setXX_FiscalPrinterConfig_ID (int XX_FiscalPrinterConfig_ID);

	/** Get Fiscal Printer Config	  */
	public int getXX_FiscalPrinterConfig_ID();

    /** Column name XX_FSeparator */
    public static final String COLUMNNAME_XX_FSeparator = "XX_FSeparator";

	/** Set File Separator	  */
	public void setXX_FSeparator (int XX_FSeparator);

	/** Get File Separator	  */
	public int getXX_FSeparator();

    /** Column name XX_LastZDate */
    public static final String COLUMNNAME_XX_LastZDate = "XX_LastZDate";

	/** Set Last Z Date	  */
	public void setXX_LastZDate (Timestamp XX_LastZDate);

	/** Get Last Z Date	  */
	public Timestamp getXX_LastZDate();

    /** Column name XX_OS_CMD */
    public static final String COLUMNNAME_XX_OS_CMD = "XX_OS_CMD";

	/** Set O.S Command	  */
	public void setXX_OS_CMD (String XX_OS_CMD);

	/** Get O.S Command	  */
	public String getXX_OS_CMD();

    /** Column name XX_PathLog */
    public static final String COLUMNNAME_XX_PathLog = "XX_PathLog";

	/** Set Path Log	  */
	public void setXX_PathLog (String XX_PathLog);

	/** Get Path Log	  */
	public String getXX_PathLog();

    /** Column name XX_PathSpooler */
    public static final String COLUMNNAME_XX_PathSpooler = "XX_PathSpooler";

	/** Set Path Spooler	  */
	public void setXX_PathSpooler (String XX_PathSpooler);

	/** Get Path Spooler	  */
	public String getXX_PathSpooler();

    /** Column name XX_PathTmp */
    public static final String COLUMNNAME_XX_PathTmp = "XX_PathTmp";

	/** Set Path Tmp Files	  */
	public void setXX_PathTmp (String XX_PathTmp);

	/** Get Path Tmp Files	  */
	public String getXX_PathTmp();

    /** Column name XX_Speed */
    public static final String COLUMNNAME_XX_Speed = "XX_Speed";

	/** Set Speed	  */
	public void setXX_Speed (int XX_Speed);

	/** Get Speed	  */
	public int getXX_Speed();
}
