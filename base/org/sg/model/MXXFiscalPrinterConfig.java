/**
 * 
 */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * 
 * @author Yamel Senih 05/04/2012, 16:32:53
 *
 */
public class MXXFiscalPrinterConfig extends X_XX_FiscalPrinterConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7194108480610070579L;
	
	public MXXFiscalPrinterConfig(Properties ctx, int XX_FiscalPrinterConfig_ID, String trxName) {
		super(ctx, XX_FiscalPrinterConfig_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MXXFiscalPrinterConfig(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
}
