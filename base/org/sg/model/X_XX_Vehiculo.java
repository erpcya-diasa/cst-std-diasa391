/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_Vehiculo
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_Vehiculo extends PO implements I_XX_Vehiculo, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120611L;

    /** Standard Constructor */
    public X_XX_Vehiculo (Properties ctx, int XX_Vehiculo_ID, String trxName)
    {
      super (ctx, XX_Vehiculo_ID, trxName);
      /** if (XX_Vehiculo_ID == 0)
        {
			setNombre (null);
			setPlaca (null);
			setXX_Vehiculo_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_Vehiculo (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_Vehiculo[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_UOM getC_UOM() throws RuntimeException
    {
		return (I_C_UOM)MTable.get(getCtx(), I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Capacity.
		@param Capacity Capacity	  */
	public void setCapacity (BigDecimal Capacity)
	{
		set_Value (COLUMNNAME_Capacity, Capacity);
	}

	/** Get Capacity.
		@return Capacity	  */
	public BigDecimal getCapacity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Capacity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_Shipper getM_Shipper() throws RuntimeException
    {
		return (I_M_Shipper)MTable.get(getCtx(), I_M_Shipper.Table_Name)
			.getPO(getM_Shipper_ID(), get_TrxName());	}

	/** Set Shipper.
		@param M_Shipper_ID 
		Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID)
	{
		if (M_Shipper_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Shipper_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Shipper_ID, Integer.valueOf(M_Shipper_ID));
	}

	/** Get Shipper.
		@return Method or manner of product delivery
	  */
	public int getM_Shipper_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Shipper_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Nombre.
		@param Nombre Nombre	  */
	public void setNombre (String Nombre)
	{
		set_Value (COLUMNNAME_Nombre, Nombre);
	}

	/** Get Nombre.
		@return Nombre	  */
	public String getNombre () 
	{
		return (String)get_Value(COLUMNNAME_Nombre);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getNombre());
    }

	/** Set PerformanceFuelF.
		@param PerformanceFuelF PerformanceFuelF	  */
	public void setPerformanceFuelF (BigDecimal PerformanceFuelF)
	{
		set_Value (COLUMNNAME_PerformanceFuelF, PerformanceFuelF);
	}

	/** Get PerformanceFuelF.
		@return PerformanceFuelF	  */
	public BigDecimal getPerformanceFuelF () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PerformanceFuelF);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set PerformanceFuelI.
		@param PerformanceFuelI PerformanceFuelI	  */
	public void setPerformanceFuelI (BigDecimal PerformanceFuelI)
	{
		set_Value (COLUMNNAME_PerformanceFuelI, PerformanceFuelI);
	}

	/** Get PerformanceFuelI.
		@return PerformanceFuelI	  */
	public BigDecimal getPerformanceFuelI () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PerformanceFuelI);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Placa.
		@param Placa Placa	  */
	public void setPlaca (String Placa)
	{
		set_Value (COLUMNNAME_Placa, Placa);
	}

	/** Get Placa.
		@return Placa	  */
	public String getPlaca () 
	{
		return (String)get_Value(COLUMNNAME_Placa);
	}

	public I_C_UOM getXX_DistUOM() throws RuntimeException
    {
		return (I_C_UOM)MTable.get(getCtx(), I_C_UOM.Table_Name)
			.getPO(getXX_DistUOM_ID(), get_TrxName());	}

	/** Set XX_DistUOM_ID.
		@param XX_DistUOM_ID XX_DistUOM_ID	  */
	public void setXX_DistUOM_ID (int XX_DistUOM_ID)
	{
		if (XX_DistUOM_ID < 1) 
			set_Value (COLUMNNAME_XX_DistUOM_ID, null);
		else 
			set_Value (COLUMNNAME_XX_DistUOM_ID, Integer.valueOf(XX_DistUOM_ID));
	}

	/** Get XX_DistUOM_ID.
		@return XX_DistUOM_ID	  */
	public int getXX_DistUOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_DistUOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Car.
		@param XX_Vehiculo_ID Car	  */
	public void setXX_Vehiculo_ID (int XX_Vehiculo_ID)
	{
		if (XX_Vehiculo_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_Vehiculo_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_Vehiculo_ID, Integer.valueOf(XX_Vehiculo_ID));
	}

	/** Get Car.
		@return Car	  */
	public int getXX_Vehiculo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Vehiculo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_UOM getXX_VolUOM() throws RuntimeException
    {
		return (I_C_UOM)MTable.get(getCtx(), I_C_UOM.Table_Name)
			.getPO(getXX_VolUOM_ID(), get_TrxName());	}

	/** Set XX_VolUOM_ID.
		@param XX_VolUOM_ID XX_VolUOM_ID	  */
	public void setXX_VolUOM_ID (int XX_VolUOM_ID)
	{
		if (XX_VolUOM_ID < 1) 
			set_Value (COLUMNNAME_XX_VolUOM_ID, null);
		else 
			set_Value (COLUMNNAME_XX_VolUOM_ID, Integer.valueOf(XX_VolUOM_ID));
	}

	/** Get XX_VolUOM_ID.
		@return XX_VolUOM_ID	  */
	public int getXX_VolUOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_VolUOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}