/**
 * 
 */
package org.sg.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Properties;

import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;
import org.compiere.model.MDocType;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.util.Env;
import org.eevolution.model.MPPOrder;


public class CallOut_XX_WeightRegister extends CalloutEngine{
	
	/**
	 * Establece los IDs de Conductor, Vehiculo y Transportista a partir de una Orden de Carga
	 * @author Yamel Senih 28/02/2012, 00:29:12
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String select_XX_LoadOrder(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//	groovy:Call_Pes_ShippingGuide
		Integer m_XX_LoadOrder_ID = (Integer)value;
		if (m_XX_LoadOrder_ID == null || m_XX_LoadOrder_ID.intValue() == 0)
			return "";
		
		//	code
		
		MXXLoadOrder m_LoadOrder = new MXXLoadOrder(ctx, m_XX_LoadOrder_ID, null);
		
		mTab.setValue("XX_Vehiculo_ID", m_LoadOrder.getXX_Vehiculo_ID());
		mTab.setValue("XX_Conductor_ID", m_LoadOrder.getXX_Conductor_ID());
		mTab.setValue("M_Shipper_ID", m_LoadOrder.getM_Shipper_ID());
		mTab.setValue("M_Warehouse_ID", 0);
		return "";
	}
	
	/**
	 * Establece los cambios a partir de la orden de calidad
	 * @author Yamel Senih 28/02/2012, 00:29:12
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String select_XX_QualityOrder(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//	@script:groovy:Call_Pes_QO
		Integer m_XX_QualityOrder_ID = (Integer)value;
		if (m_XX_QualityOrder_ID == null || m_XX_QualityOrder_ID.intValue() == 0)
			return "";
		
		//	code

	    MPPOrder order = new MPPOrder(ctx, m_XX_QualityOrder_ID.intValue(), null);
	    mTab.setValue("M_AttributeSetInstance_ID", order.getM_AttributeSetInstance_ID());
	    mTab.setValue("M_Product_ID", order.getM_Product_ID());
	    mTab.setValue("M_Shipper_ID", order.get_ValueAsInt("M_Shipper_ID"));
	    mTab.setValue("XX_Conductor_ID", order.get_ValueAsInt("XX_Conductor_ID"));
	    mTab.setValue("XX_Vehiculo_ID", order.get_ValueAsInt("XX_Vehiculo_ID"));
	    mTab.setValue("M_Warehouse_ID", order.getM_Warehouse_ID());
	    mTab.setValue("AD_Workflow_ID", order.getAD_Workflow_ID());
	    mTab.setValue("XX_Ticket_Vigilancia_ID", order.get_ValueAsInt("XX_Ticket_Vigilancia_ID"));
	    mTab.setValue("C_BPartner_ID", order.get_ValueAsInt("C_BPartner_ID"));
	    mTab.setValue("IsInclude", order.get_Value("IsInclude"));
	    mTab.setValue("C_UOM_ID", order.get_ValueAsInt("C_UOM_ID"));
	    
	    return "";	    
	}

	/**
	 * Establece los la fecha de [pesada automaticamente al establecer el peso tara y bruto
	 * @author Yamel Senih 28/02/2012, 00:29:12
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String select_Weight(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//	@script:groovy:Call_Pes_Peso_Fecha
		//	code
		
		if(mTab.getValue("C_DocTypeTarget_ID") != null){    
			BigDecimal pesoTara =  (BigDecimal) (mTab.getValue("Weight") != null? mTab.getValue("Weight"): Env.ZERO);
		    BigDecimal pesoBruto = (BigDecimal) (mTab.getValue("Weight2") != null? mTab.getValue("Weight2"): Env.ZERO);
		    BigDecimal pesoNeto = pesoBruto.subtract(pesoTara);
		    mTab.setValue("Weight3",pesoNeto);

		    Integer p_C_DocType_ID = (Integer) mTab.getValue("C_DocTypeTarget_ID");
		    MDocType doc = new MDocType(ctx, p_C_DocType_ID.intValue(), null);
		  
		    Timestamp today = new Timestamp(System.currentTimeMillis());  
		  
		    if(doc.get_Value("XX_Tipo_Q_Pesada").equals("PT")){
		    	if(mField.getColumnName().equals("Weight")){
		    		mTab.setValue("DateFrom", today);
		    	}else{
		    		mTab.setValue("DateTo", today);
		    	}
		    } else{
		    	if(mField.getColumnName().equals("Weight")){
		    		mTab.setValue("DateTo", today);
		    	}else{
		    		mTab.setValue("DateFrom", today);
		    	}
		    }    
		}
		return "";
	}
	
	/**
	 * Establece los valores de si esta incluido el flete a partir de los datos del Socio de Negocio
	 * @author Yamel Senih 28/02/2012, 00:29:12
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String select_BPartner(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//	@script:groovy:Call_Pes_BPartner
		Integer m_C_BPartner_ID = (Integer)value;
		if (m_C_BPartner_ID == null || m_C_BPartner_ID.intValue() == 0)
			return "";
		
		//	code
		
		MBPartner socio = new MBPartner(ctx, m_C_BPartner_ID.intValue(), null);
		mTab.setValue("IsInclude", socio.get_Value("IsInclude"));
		
		return "";
	}
	
	/**
	 * Establece los valores de Socio de Negocio y Producto a partir de la linea de la Orden de Compra
	 * @author Yamel Senih 28/02/2012, 00:29:12
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String select_POLine(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//	@script:groovy:Call_Lin_PO
		Integer m_C_OrderLine_ID = (Integer)value;
		if (m_C_OrderLine_ID == null || m_C_OrderLine_ID.intValue() == 0)
			return "";
		
		//	code
		
		MOrderLine lOrder = new MOrderLine(ctx, m_C_OrderLine_ID.intValue(), null);
	    MOrder order = new MOrder(ctx, lOrder.getC_Order_ID(), null);
	    mTab.setValue("M_Product_ID", lOrder.getM_Product_ID());
	    mTab.setValue("C_BPartner_ID", order.getC_BPartner_ID());
		
		return "";
	}
	
	/**
	 * Establece los IDs de Conductor, Vehiculo, Transportista y Socio de Negocio a partir de una Orden de Carga
	 * @author Yamel Senih 28/02/2012, 00:29:12
	 * @param ctx
	 * @param WindowNo
	 * @param mTab
	 * @param mField
	 * @param value
	 * @return
	 */
	public String select_XX_Ticket_Vigilancia(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value){
		//	@script:groovy:Call_Calidad_TV
		Integer m_XX_Ticket_Vigilancia_ID = (Integer)value;
		if (m_XX_Ticket_Vigilancia_ID == null || m_XX_Ticket_Vigilancia_ID.intValue() == 0)
			return "";
		
		//	code
		
	    MXXTicketVigilancia tVigilancia = new MXXTicketVigilancia(ctx, m_XX_Ticket_Vigilancia_ID.intValue(), null);
	    mTab.setValue("C_BPartner_ID", tVigilancia.getC_BPartner_ID());
	    mTab.setValue("M_Shipper_ID", tVigilancia.get_ValueAsInt("M_Shipper_ID"));
	    mTab.setValue("XX_Conductor_ID", tVigilancia.get_ValueAsInt("XX_Conductor_ID"));
	    mTab.setValue("XX_Vehiculo_ID", tVigilancia.get_ValueAsInt("XX_Vehiculo_ID"));
	    Integer m_C_BPartner_ID =  tVigilancia.getC_BPartner_ID();
	    MBPartner socio = new MBPartner(ctx, m_C_BPartner_ID.intValue(), null);
	    mTab.setValue("IsInclude", socio.get_Value("IsInclude"));
		
		return "";
	}
	
	
	
}
