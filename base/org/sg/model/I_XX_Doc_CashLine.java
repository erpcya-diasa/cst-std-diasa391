/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.sg.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for XX_Doc_CashLine
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_XX_Doc_CashLine 
{

    /** TableName=XX_Doc_CashLine */
    public static final String Table_Name = "XX_Doc_CashLine";

    /** AD_Table_ID=1000018 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_Bank_ID */
    public static final String COLUMNNAME_C_Bank_ID = "C_Bank_ID";

	/** Set Bank.
	  * Bank
	  */
	public void setC_Bank_ID (int C_Bank_ID);

	/** Get Bank.
	  * Bank
	  */
	public int getC_Bank_ID();

	public I_C_Bank getC_Bank() throws RuntimeException;

    /** Column name C_Cash_ID */
    public static final String COLUMNNAME_C_Cash_ID = "C_Cash_ID";

	/** Set Cash Journal.
	  * Cash Journal
	  */
	public void setC_Cash_ID (int C_Cash_ID);

	/** Get Cash Journal.
	  * Cash Journal
	  */
	public int getC_Cash_ID();

	public I_C_Cash getC_Cash() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Monto */
    public static final String COLUMNNAME_Monto = "Monto";

	/** Set Monto	  */
	public void setMonto (BigDecimal Monto);

	/** Get Monto	  */
	public BigDecimal getMonto();

    /** Column name Nro_Referencia */
    public static final String COLUMNNAME_Nro_Referencia = "Nro_Referencia";

	/** Set Nº de Referencia	  */
	public void setNro_Referencia (String Nro_Referencia);

	/** Get Nº de Referencia	  */
	public String getNro_Referencia();

    /** Column name TipoPago */
    public static final String COLUMNNAME_TipoPago = "TipoPago";

	/** Set Tipo de Pago	  */
	public void setTipoPago (String TipoPago);

	/** Get Tipo de Pago	  */
	public String getTipoPago();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name XX_Doc_CashLine_ID */
    public static final String COLUMNNAME_XX_Doc_CashLine_ID = "XX_Doc_CashLine_ID";

	/** Set Formas de Pago	  */
	public void setXX_Doc_CashLine_ID (int XX_Doc_CashLine_ID);

	/** Get Formas de Pago	  */
	public int getXX_Doc_CashLine_ID();
}
