/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for XX_PalletsGenerated
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_PalletsGenerated extends PO implements I_XX_PalletsGenerated, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120406L;

    /** Standard Constructor */
    public X_XX_PalletsGenerated (Properties ctx, int XX_PalletsGenerated_ID, String trxName)
    {
      super (ctx, XX_PalletsGenerated_ID, trxName);
      /** if (XX_PalletsGenerated_ID == 0)
        {
			setM_Product_ID (0);
			setQty (Env.ZERO);
			setSeqNo (Env.ZERO);
			setXX_LoadOrder_ID (0);
			setXX_PalletsGenerated_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_PalletsGenerated (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_PalletsGenerated[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_UOM getC_UOM() throws RuntimeException
    {
		return (I_C_UOM)MTable.get(getCtx(), I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_UOM getC_UOM_To() throws RuntimeException
    {
		return (I_C_UOM)MTable.get(getCtx(), I_C_UOM.Table_Name)
			.getPO(getC_UOM_To_ID(), get_TrxName());	}

	/** Set UoM To.
		@param C_UOM_To_ID 
		Target or destination Unit of Measure
	  */
	public void setC_UOM_To_ID (int C_UOM_To_ID)
	{
		if (C_UOM_To_ID < 1) 
			set_Value (COLUMNNAME_C_UOM_To_ID, null);
		else 
			set_Value (COLUMNNAME_C_UOM_To_ID, Integer.valueOf(C_UOM_To_ID));
	}

	/** Get UoM To.
		@return Target or destination Unit of Measure
	  */
	public int getC_UOM_To_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_To_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Product_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quantity.
		@param Qty 
		Quantity
	  */
	public void setQty (BigDecimal Qty)
	{
		set_Value (COLUMNNAME_Qty, Qty);
	}

	/** Get Quantity.
		@return Quantity
	  */
	public BigDecimal getQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Qty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Sequence.
		@param SeqNo 
		Method of ordering records; lowest number comes first
	  */
	public void setSeqNo (BigDecimal SeqNo)
	{
		set_Value (COLUMNNAME_SeqNo, SeqNo);
	}

	/** Get Sequence.
		@return Method of ordering records; lowest number comes first
	  */
	public BigDecimal getSeqNo () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SeqNo);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Confirmed.
		@param XX_Confirmed Confirmed	  */
	public void setXX_Confirmed (boolean XX_Confirmed)
	{
		set_Value (COLUMNNAME_XX_Confirmed, Boolean.valueOf(XX_Confirmed));
	}

	/** Get Confirmed.
		@return Confirmed	  */
	public boolean isXX_Confirmed () 
	{
		Object oo = get_Value(COLUMNNAME_XX_Confirmed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Load Order.
		@param XX_LoadOrder_ID Load Order	  */
	public void setXX_LoadOrder_ID (int XX_LoadOrder_ID)
	{
		if (XX_LoadOrder_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_LoadOrder_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_LoadOrder_ID, Integer.valueOf(XX_LoadOrder_ID));
	}

	/** Get Load Order.
		@return Load Order	  */
	public int getXX_LoadOrder_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_LoadOrder_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Pallet Code.
		@param XX_PalletCode Pallet Code	  */
	public void setXX_PalletCode (String XX_PalletCode)
	{
		set_Value (COLUMNNAME_XX_PalletCode, XX_PalletCode);
	}

	/** Get Pallet Code.
		@return Pallet Code	  */
	public String getXX_PalletCode () 
	{
		return (String)get_Value(COLUMNNAME_XX_PalletCode);
	}

	/** Set Pallets Generated.
		@param XX_PalletsGenerated_ID Pallets Generated	  */
	public void setXX_PalletsGenerated_ID (int XX_PalletsGenerated_ID)
	{
		if (XX_PalletsGenerated_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_PalletsGenerated_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_PalletsGenerated_ID, Integer.valueOf(XX_PalletsGenerated_ID));
	}

	/** Get Pallets Generated.
		@return Pallets Generated	  */
	public int getXX_PalletsGenerated_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_PalletsGenerated_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}