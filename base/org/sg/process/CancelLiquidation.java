package org.sg.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.apps.ADialog;
import org.compiere.model.MInvoice;
import org.compiere.model.MPaySelection;
import org.compiere.model.MPayment;
import org.compiere.process.DocAction;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Msg;
import org.sg.model.MXXLiquidation;

public class CancelLiquidation extends SvrProcess {

	/**	Liquidation			*/
	private int m_XX_Liquidation_ID = 0;
	/**	Payment				*/
	private int m_C_Payment_ID = 0;
	/**	Invoice				*/
	private int m_C_Invoice_ID = 0;
	/**	Credit Memo			*/
	private int m_XX_CreditMemo_ID = 0;
	/**	Debit Memo			*/
	private int m_XX_DebitMemo_ID = 0;
	/**	Model  Invoice		*/
	private MInvoice invoice;
	/**	Model  Credit Memo	*/
	private MInvoice creditMemo;
	/**	Model  Debit Memo	*/
	private MInvoice debitMemo;
	/**	Model Payment		*/
	private MPayment pay;
	
	@Override
	protected void prepare() {
		m_XX_Liquidation_ID = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {
		MXXLiquidation liq = new MXXLiquidation(getCtx(), m_XX_Liquidation_ID, get_TrxName());
		
		String sql = new String("SELECT csp.C_Payment_ID " +
				"FROM C_PaySelection sp " +
				"INNER JOIN C_PaySelectionCheck csp ON(csp.C_PaySelection_ID = sp.C_PaySelection_ID) " +
				"WHERE sp.C_PaySelection_ID = ?");
		log.fine("consultaRealizados SQL = " + sql);
		
		PreparedStatement pstmt = null;
		try {
			pstmt = DB.prepareStatement (sql, get_TrxName());
			pstmt.setInt(1, liq.getC_PaySelection_ID());
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()){
				m_C_Payment_ID = rs.getInt("C_Payment_ID");
			}
		} catch (SQLException e) {
			throw new AdempiereException(Msg.translate(getCtx(), "SGBDError"));
		}
		
		m_C_Invoice_ID = liq.getC_Invoice_ID();
		m_XX_CreditMemo_ID = liq.getXX_CreditMemo_ID();
		m_XX_DebitMemo_ID = liq.getXX_DebitMemo_ID();
		String msg = "";
		
		
		if(liq.getC_PaySelection_ID() != 0){
			MPaySelection sp = new MPaySelection(getCtx(), liq.getC_PaySelection_ID(), get_TrxName());
			sp.setIsActive(false);
			sp.setIsApproved(false);
			if(!sp.save()){
				throw new AdempiereException("@C_PaySelection_ID@");
			}
		}
		//	save Payment
		if(m_C_Payment_ID != 0){
			pay = new MPayment(getCtx(), m_C_Payment_ID, get_TrxName());
			if(DocAction.STATUS_Completed.equals(pay.getDocStatus())){
				pay.reverseCorrectIt();
				if(!pay.save()){
					msg = Msg.translate(getCtx(), "SGErrorPayment");
					throw new AdempiereException(msg);
				}
			} else if(DocAction.STATUS_Closed.equals(pay.getDocStatus())){
				msg = Msg.translate(getCtx(), "SGPaymentClosed");
				ADialog.error(0, null, Msg.translate(getCtx(), "C_Payment_ID"), msg);
				return msg;
			}
		}
		
		//	save Invoice
		if(m_C_Invoice_ID != 0){
			invoice = new MInvoice(getCtx(), m_C_Invoice_ID, get_TrxName());
			if(DocAction.STATUS_Completed.equals(invoice.getDocStatus())){
				invoice.reverseCorrectIt();
				if(!invoice.save()){
					msg = Msg.translate(getCtx(), "SGErrorInvoice");
					throw new AdempiereException(msg);
				}
			} else if(DocAction.STATUS_Closed.equals(invoice.getDocStatus())){
				msg = Msg.translate(getCtx(), "SGInvoiceClosed");
				ADialog.error(0, null, Msg.translate(getCtx(), "C_Invoice_ID"), msg);
				return msg;
			}
		}
		
		//	save Credit Memo
		if(m_XX_CreditMemo_ID != 0){
			creditMemo = new MInvoice(getCtx(), m_XX_CreditMemo_ID, get_TrxName());
			if(DocAction.STATUS_Completed.equals(creditMemo.getDocStatus())){
				creditMemo.reverseCorrectIt();
				if(!creditMemo.save()){
					msg = Msg.translate(getCtx(), "SGErrorCreditMemo");
					throw new AdempiereException(msg);
				}
			} else if(DocAction.STATUS_Closed.equals(creditMemo.getDocStatus())){
				msg = Msg.translate(getCtx(), "SGCreditMemoClosed");
				ADialog.error(0, null, Msg.translate(getCtx(), "XX_CreditMemo_ID"), msg);
				return msg;
			}
		}
		
		//	save Debit Memo
			if(m_XX_DebitMemo_ID != 0){
				debitMemo = new MInvoice(getCtx(), m_XX_DebitMemo_ID, get_TrxName());
				if(DocAction.STATUS_Completed.equals(debitMemo.getDocStatus())){
					debitMemo.reverseCorrectIt();
					if(!debitMemo.save()){
						msg = Msg.translate(getCtx(), "SGErrorDebitMemo");
						throw new AdempiereException(msg);
					}
				} else if(DocAction.STATUS_Closed.equals(debitMemo.getDocStatus())){
					msg = Msg.translate(getCtx(), "SGDebitMemoClosed");
					ADialog.error(0, null, Msg.translate(getCtx(), "XX_DebitMemo_ID"), msg);
					return msg;
				}
			}
		
		//	save Liquidation
		liq.setXX_Cancel_Liquidation("Y");
		if(!liq.save()){
			msg = Msg.translate(getCtx(), "SGErrorLiquidaton");
			throw new AdempiereException(msg);
		}
		return "OK";
	}

}
