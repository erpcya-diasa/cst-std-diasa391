package org.sg.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MShipper;
import org.compiere.model.MUOMConversion;
import org.compiere.model.Tax;
import org.compiere.model.X_C_Order;
import org.compiere.model.X_M_InOut;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.eevolution.model.MPPOrder;

/**
 * @author Yamel Senih
 *
 */
public class WeightOrderPOCreate extends SvrProcess {

	/** Warehouse			*/
	private int			p_M_Warehouse_ID = 0;
	/**	Locator				*/
	private int			p_M_Locator_ID = 0;
	/**	Date From			*/
	private Timestamp 	p_DateOrdered_From;
	/**	Date To				*/
	private Timestamp 	p_DateOrdered_To;
	/**	Doc Date From		*/
	private Timestamp	p_DateDoc;
	/**	Doc Date From		*/
	private Timestamp	dateDoc;
	/** BPartner			*/
	private int 		p_C_BPartner_ID = 0;
	
	private int 		p_C_DocType_ID = 0;
	
	/**	UOM To				*/
	private int 		p_C_UOM_To_ID = 0;
	
	private int			m_created = 0;
	
	private boolean		p_ConsolidateDocument = false;
	
	/**	C_DocTypeTarget_ID	*/
	private int			p_C_DocTypeTarget_ID = 0;
	
	/** Order				*/
	private MOrder		m_order = null;
	
	/**	Orden Conductor		*/
	private MOrder		m_orderConduc = null;
	
	/** Quality Order		*/
	private MPPOrder	qOrder = null;

	/**	Producto Tipo Servicio				*/
	private int			p_M_Product_ID = 0;
	
	/** Tipo de Documento de Recepción*/
	private int			p_C_DocTypeShip_ID = 0;
	
	/** Tipo de Documento de Transportista*/
	private int 		p_C_DocTypeCarrier_ID = 0;
	
	/**	Created				*/
	private int 		m_created_InOut = 0;
	/** Received			*/
	
	private MInOut 		recepcion = null;
	
	/**	Ordenes ya generadas*/
	private int 		m_Ordenes = 0;
	
	/**	Ordenes a transportista ya generadas*/
	private int 		m_Transportista = 0;
	
	/**	Recepciones ya generadas*/
	private int 		m_Recepciones = 0;
	
	/**	Actividad				*/
	private int 		p_C_Activity_ID = 0;
	
	/**	Recepciones de Transportista Generadas*/
	private int m_RecepcionesT = 0;
	
	/**	Indicador de Estado del Proceso*/
	private int 		contProc = 0;
	
	@Override
	protected void prepare() {

		for (ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			//fjviejo e-evolution petrocasa end
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para.getParameterAsInt();
			else if (name.equals("M_Locator_ID"))
				p_M_Locator_ID = para.getParameterAsInt();
			else if (name.equals("DateDoc"))
				p_DateDoc = (Timestamp)para.getParameter();
			else if (name.equals("C_DocType_ID"))
				p_C_DocType_ID = para.getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para.getParameterAsInt();
			else if (name.equals("ConsolidateDocument"))
				p_ConsolidateDocument = "Y".equals(para.getParameter());
			else if (name.equals("C_DocTypeTarget_ID"))
				p_C_DocTypeTarget_ID = para.getParameterAsInt();
			else if (name.equals("C_DocTypeCarrier_ID"))
				p_C_DocTypeCarrier_ID = para.getParameterAsInt();
			else if (name.equals("C_DocTypeShip_ID"))
				p_C_DocTypeShip_ID = para.getParameterAsInt();
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para.getParameterAsInt();
			else if (name.equals("C_UOM_To_ID"))
				p_C_UOM_To_ID = para.getParameterAsInt();
			else if (name.equals("C_Activity_ID"))
				p_C_Activity_ID = para.getParameterAsInt();
			else if (name.equals("DateOrdered")){
				p_DateOrdered_From = (Timestamp)para.getParameter();
				p_DateOrdered_To = (Timestamp)para.getParameter_To();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare

	@Override
	protected String doIt() throws Exception {
		String sql = null;
		sql = "SELECT * FROM PP_Order o "
			+ "WHERE DocStatus IN('CO', 'CL') "
			+ "AND XX_TipoPesada = 'PM' "
			+ "AND AD_Client_ID=?";
		if (p_C_BPartner_ID != 0)
			sql += " AND C_BPartner_ID=?";
		if (p_DateOrdered_From != null)
			sql+=" AND TRUNC(DateOrdered,'DD') >= ?";
		if (p_DateOrdered_To != null)
			sql+=" AND TRUNC(DateOrdered,'DD') <= ?";
		if (p_C_DocType_ID != 0)
			sql += " AND C_DocType_ID=?";
		
		log.info("doIt SQL = " + sql.toString());
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, get_TrxName());
			int index = 1;
			pstmt.setInt(index++, getAD_Client_ID());
			if (p_C_BPartner_ID != 0)
				pstmt.setInt(index++, p_C_BPartner_ID);
			if (p_DateOrdered_From != null)
				pstmt.setTimestamp(index++, p_DateOrdered_From);
			if (p_DateOrdered_To != null)
				pstmt.setTimestamp(index++, p_DateOrdered_To);
			if (p_C_DocType_ID != 0)
				pstmt.setInt(index++, p_C_DocType_ID);
		}catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		
		return generarOrdenes(pstmt);
	}	//	doIt
	
	/*
	 *	generarOrdenes
	 */
	private String generarOrdenes(PreparedStatement pstmt){
		try{
			ResultSet rs = pstmt.executeQuery();
			if(p_ConsolidateDocument){
				consolidado(rs);
			}else{
				noConsolidado(rs);
			}		
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		} catch (Exception e){
			log.log(Level.SEVERE, "", e);
		}		
		return "@Created@ = " + m_created;
	}	//	generarOrdenes
	
	/*
	 * consolidado
	 */
	private String consolidado(ResultSet rs) throws SQLException{
		if(rs.next ()){
			qOrder = new MPPOrder (getCtx(), rs, get_TrxName());
			crearOrden();
			do{
				qOrder = new MPPOrder (getCtx(), rs, get_TrxName());
				crearLineas();
				//closeQOrder();
			}while (rs.next ());
			completeOrder();
		}
		return "@Created@ = " + m_created;
	}	//	consolidado
	
	/*
	 * consolidado
	 */
	private String noConsolidado(ResultSet rs) throws SQLException{
		
		while (rs.next ()){System.err.println("Hola " + rs.getString("DocumentNo"));
			qOrder = new MPPOrder (getCtx(), rs, get_TrxName());
			consultaRealizados();
			if(MUOMConversion.getProductRateTo(getCtx(), qOrder.getM_Product_ID(), p_C_UOM_To_ID) == null)
				throw new AdempiereException(Msg.translate(getCtx(), "SGCUOMNotF"));
			if(p_DateDoc == null){
				dateDoc = qOrder.getDateOrdered();
			} else {
				dateDoc = p_DateDoc;
			}
			
			if(m_Ordenes == 0){
				crearOrden();
				crearLineas();
				completeOrder();
				//m_Ordenes = m_order.getC_Order_ID();
				//System.out.println("m_Ordenes " + m_Ordenes);
				//procOrdenGenInPro();
				//closeQOrder();
			}
			
			if(m_Recepciones == 0){System.err.println("m_Recepciones == 0");
				if(m_Ordenes != 0){System.err.println("m_Ordenes != 0");
				MOrder m_orderPro = new MOrder(Env.getCtx(), m_Ordenes, get_TrxName());
					if(DocAction.STATUS_Completed.equals(m_orderPro.getDocStatus())){System.err.println("DocAction.STATUS_Completed.equals(m_order.getDocStatus())");
						procOrdenGenIn(m_orderPro);
						closeQOrder();
					}
				}
			}
			
			if(m_Transportista == 0){
				if(!qOrder.get_ValueAsBoolean("IsInclude")){
					crearOrdenTransportista();
					crearLineasConductor();
					//closeQOrder();
				}
			}
			
			if(m_RecepcionesT == 0){System.err.println("m_RecepcionesT == 0");
				if(!qOrder.get_ValueAsBoolean("IsInclude") && m_Transportista != 0){System.err.println("!qOrder.get_ValueAsBoolean(IsInclude) && m_Transportista != 0");
					MOrder orderConduc = new MOrder(Env.getCtx(), m_Transportista, null);System.err.println("m_Transportista " + m_Transportista);
					if(DocAction.STATUS_Completed.equals(orderConduc.getDocStatus())){System.err.println("DocAction.STATUS_Completed.equals(m_orderConduc.getDocStatus())");
						procOrdenGenIn(orderConduc);
						closeQOrder();
					}
				}
			}System.err.println("Hola-- " + rs.getString("DocumentNo"));
		}
		return "@Created@ = " + m_created;
	}	//	consolidado
	
	
	private void consultaRealizados(){
		String sql = new String("SELECT " +
				"(SELECT lo.C_Order_ID " +
				"FROM PP_Order pp " +
				"INNER JOIN C_OrderLine lo ON(lo.PP_Order_ID = pp.PP_Order_ID AND lo.M_Product_ID = pp.M_Product_ID) " +
				"WHERE pp.PP_Order_ID = ?) ordenes, " +
				"(SELECT lo.C_Order_ID " +
				"FROM PP_Order pp " +
				"INNER JOIN C_OrderLine lo ON(lo.PP_Order_ID = pp.PP_Order_ID) " +
				"WHERE pp.PP_Order_ID = ? " +
				"AND lo.M_Product_ID = ?) transportista, " +
				"(SELECT count(*) " +
				"FROM PP_Order pp " +
				"INNER JOIN C_OrderLine lo ON(lo.PP_Order_ID = pp.PP_Order_ID AND lo.M_Product_ID = pp.M_Product_ID) " +
				"INNER JOIN M_InOutLine lr ON(lr.C_OrderLine_ID = lo.C_OrderLine_ID) " +
				"WHERE pp.PP_Order_ID = ?) recepciones, " +
				"(SELECT count(*) " +
				"FROM PP_Order pp " +
				"INNER JOIN C_OrderLine lo ON(lo.PP_Order_ID = pp.PP_Order_ID) " +
				"INNER JOIN M_InOutLine lr ON(lr.C_OrderLine_ID = lo.C_OrderLine_ID) " +
				"WHERE pp.PP_Order_ID = ? " +
				"AND lo.M_Product_ID = ?) recepcionesT");
		log.fine("consultaRealizados SQL = " + sql);
		System.err.println("SQL " + sql);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			int cont = 1;
			pstmt = DB.prepareStatement (sql, get_TrxName());
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			pstmt.setInt(cont++, p_M_Product_ID);
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			pstmt.setInt(cont++, p_M_Product_ID);
			rs = pstmt.executeQuery();
			if(rs.next()){
				m_Ordenes = rs.getInt("ordenes");
				m_Transportista = rs.getInt("transportista");
				m_Recepciones = rs.getInt("recepciones");
				m_RecepcionesT= rs.getInt("recepcionesT");
			}
		} catch (SQLException e) {
			throw new AdempiereException(Msg.translate(getCtx(), "SGBDError") + ":" + e.getMessage());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
	}
	
	/*
	 *	crearOrden
	 */
	private void crearOrden(){
		MBPartner m_bpartner = MBPartner.get(getCtx(), qOrder.get_ValueAsInt("C_BPartner_ID"));
		m_order = new MOrder(getCtx(), 0, get_TrxName());
		m_order.setDatePromised(dateDoc);
		m_order.setDateAcct(dateDoc);
		m_order.setDateOrdered(dateDoc);
		m_order.setC_DocTypeTarget_ID(p_C_DocTypeTarget_ID);
		m_order.setBPartner(m_bpartner);
		m_order.setM_PriceList_ID(m_bpartner.getPO_PriceList_ID());
		m_order.setC_PaymentTerm_ID(m_bpartner.getPO_PaymentTerm_ID());
		m_order.setDateAcct(dateDoc);
		m_order.setC_Activity_ID(p_C_Activity_ID);
		m_order.setIsSOTrx(false);
		if(p_M_Warehouse_ID != 0){
			m_order.setM_Warehouse_ID(p_M_Warehouse_ID);
		} else {
			m_order.setM_Warehouse_ID(qOrder.getM_Warehouse_ID());
		}
		m_order.setPriorityRule(qOrder.getPriorityRule());
			
	}	//	crearOrden
	
	/*
	 * 	crearLineas
	 */
	private void crearLineas(){
		
		MProduct product = qOrder.getM_Product();
		
		BigDecimal pAcond = calcAcond();
		
		BigDecimal rConversion = MUOMConversion.getProductRateTo(getCtx(), 
				qOrder.getM_Product_ID(), p_C_UOM_To_ID);
		
		BigDecimal pAcondConv = pAcond.multiply(rConversion);
		
		if(m_order.save()){			
			MOrderLine m_orderLine = new MOrderLine(m_order);
			m_orderLine.setProduct(product);
			int C_Tax_ID = Tax.get (getCtx(), product.getM_Product_ID(), 0, dateDoc, dateDoc,
					m_order.getAD_Org_ID(), m_order.getM_Warehouse_ID(), m_order.getBill_Location_ID(), m_order.getC_BPartner_Location_ID(),
					"Y".equals(m_order.isSOTrx()));
			m_orderLine.setOrder(m_order);
			m_orderLine.setC_Order_ID(m_order.get_ID());
			
			m_orderLine.setQtyOrdered(pAcondConv);
			m_orderLine.setQtyEntered(pAcondConv);
			m_orderLine.setQty(pAcondConv);
			m_orderLine.setDateOrdered(qOrder.getDateOrdered());
			m_orderLine.setDatePromised(qOrder.getDatePromised());
			m_orderLine.setDiscount(Env.ZERO);
			m_orderLine.setPrice();
			m_orderLine.setC_UOM_ID(p_C_UOM_To_ID);
			m_orderLine.setC_Currency_ID(m_order.getC_Currency_ID());
			m_orderLine.setM_Warehouse_ID(m_order.getM_Warehouse_ID());
			m_orderLine.setC_Tax_ID(C_Tax_ID);
			m_orderLine.set_ValueOfColumn("PP_Order_ID", qOrder.getPP_Order_ID());
			m_orderLine.setM_AttributeSetInstance_ID(qOrder.getM_AttributeSetInstance_ID());
			m_orderLine.setC_Activity_ID(p_C_Activity_ID);
			m_orderLine.setDiscount(Env.ZERO);
			
			m_order.setDescription(qOrder.getDocumentNo());
			
			if(m_orderLine.save()){
				qOrder.setC_OrderLine_ID(m_orderLine.getC_OrderLine_ID());
				qOrder.saveEx();
			} else{
				throw new AdempiereException("@SaveError@ @C_OrderLine_ID@");
			}
		} else{
			throw new AdempiereException("@SaveError@ @C_Order_ID@");
		}
	}	//	crearLineas

	
	/**
	 * Calculo de Peso Acondicionado
	 * @return BigDecimal
	 */
	private BigDecimal calcAcond(){
		BigDecimal humedad = Env.ZERO;
		BigDecimal impureza = Env.ZERO;
		BigDecimal pAcond = Env.ZERO;

		String sql = new String("SELECT round(XX_Fn_getHumedad(br.Weight3, br.Humedad), 2) p_Humedad, " +
				"round(XX_Fn_getImpureza(br.Weight3, br.Humedad, br.Impurezas), 2) p_Impureza, " +
				"round(XX_Fn_getPAcond(br.Weight3, br.Humedad, br.Impurezas), 2) p_Pagar " +
				"FROM XX_RV_Boleta_Romana br " +
				"WHERE br.PP_Order_ID = ?");

		log.fine("calcAcond SQL = " + sql);
		
		BigDecimal pNeto = (BigDecimal)qOrder.get_Value("Weight3");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			pstmt.setInt(1, qOrder.getPP_Order_ID());
			rs = pstmt.executeQuery();
			if(rs.next()){
				humedad = rs.getBigDecimal("p_Humedad");
				impureza = rs.getBigDecimal("p_Impureza");
				pAcond = rs.getBigDecimal("p_Pagar");
			} else {
				new AdempiereException("@M_AttributeSetInstance_ID@ Invalid");
			}
		}catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		log.info("Impureza = " + impureza);
		log.info("Humedad = " + humedad);
		log.info("Peso Neto = " + pNeto);
		log.info("Peso Acondicionado = " + pAcond);
		
		return pAcond;
	}	//	calcAcond
	
	
	/*
	 *	crearOrden Conductor
	 */
	private void crearOrdenTransportista(){
		int m_M_Shipper_ID = qOrder.get_ValueAsInt("M_Shipper_ID");
		MShipper m_shipper = new MShipper(getCtx(), m_M_Shipper_ID, get_TrxName());
		MBPartner m_bpartner = MBPartner.get(getCtx(), m_shipper.getC_BPartner_ID());
		
		/*
		 * Hora 10:18 am
		 * Ésto se debe cambiar para antes de las 11:18 am
		 * */
		
		m_orderConduc = new MOrder(getCtx(), 0, get_TrxName());
		m_orderConduc.setDatePromised(dateDoc);
		m_orderConduc.setDateAcct(dateDoc);
		m_orderConduc.setDateOrdered(dateDoc);
		m_orderConduc.setC_DocTypeTarget_ID(p_C_DocTypeCarrier_ID);
		m_orderConduc.setBPartner(m_bpartner);
		m_orderConduc.setM_PriceList_ID(m_bpartner.getPO_PriceList_ID());
		m_orderConduc.setC_PaymentTerm_ID(m_bpartner.getPO_PaymentTerm_ID());
		m_orderConduc.setDateAcct(dateDoc);
		m_orderConduc.setIsSOTrx(false);
		m_orderConduc.setM_Warehouse_ID(Env.getContextAsInt(Env.getCtx(), "#M_Warehouse_ID"));
		m_orderConduc.setC_Activity_ID(p_C_Activity_ID);
		m_orderConduc.setPriorityRule(qOrder.getPriorityRule());
			
	}	//	crearOrden
	
	/*
	 * 	crearLineas Transportista
	 */
	private void crearLineasConductor(){
				
		if(m_orderConduc.save()){
			BigDecimal rConversion = MUOMConversion.getProductRateTo(getCtx(), 
					p_M_Product_ID, p_C_UOM_To_ID);
			int C_Tax_ID = Tax.get (getCtx(), p_M_Product_ID, 0, dateDoc, dateDoc,
					m_orderConduc.getAD_Org_ID(), m_orderConduc.getM_Warehouse_ID(), 
					m_orderConduc.getBill_Location_ID(), m_orderConduc.getC_BPartner_Location_ID(),
					"Y".equals(m_orderConduc.isSOTrx()));
			BigDecimal pNeto = (BigDecimal)qOrder.get_Value("Weight3");
			MOrderLine m_orderLine = new MOrderLine(m_orderConduc);
			m_orderLine.setM_Product_ID(p_M_Product_ID);
			m_orderLine.setC_Tax_ID(C_Tax_ID);
			m_orderLine.setQtyEntered(pNeto.multiply(rConversion));
			m_orderLine.setQtyOrdered(pNeto.multiply(rConversion));
			m_orderLine.setQty(pNeto.multiply(rConversion));
			m_orderLine.setDiscount(Env.ZERO);
			m_orderLine.setPrice();
			m_orderLine.setDateOrdered(qOrder.getDateOrdered());
			m_orderLine.setDatePromised(qOrder.getDatePromised());
			m_orderLine.set_ValueOfColumn("PP_Order_ID", qOrder.getPP_Order_ID());
			m_orderLine.setC_Activity_ID(p_C_Activity_ID);
			m_orderLine.setDiscount(Env.ZERO);
			
			m_orderConduc.setDescription(qOrder.getDocumentNo());
			
			if(m_orderLine.save()){
				addLog(m_orderConduc.getC_Order_ID(), m_orderConduc.getDateOrdered(), null, m_orderConduc.getDocumentNo());
				m_created++;
				contProc++;
			} else{
				throw new AdempiereException("@SaveError@ @C_OrderLine_ID@");
			}
		} else{
			throw new AdempiereException("@SaveError@ @C_Order_ID@");
		}
	}	//	crearLineas
	
	
	
	
	/*
	 * 	Complete Order
	 */
	private void completeOrder()
	{
		if (m_order != null)
		{
			//	Fails if there is a confirmation
			if (!m_order.processIt(DocAction.ACTION_Complete))
				log.warning("Failed: " + m_order);
			m_order.setDocStatus(X_C_Order.DOCACTION_Complete);
			m_order.setDocAction(X_C_Order.DOCACTION_Close);
			m_order.setProcessed(true);
			m_order.save();
			contProc++;
			addLog(m_order.getC_Order_ID(), m_order.getDateOrdered(), null, m_order.getDocumentNo());
			m_created++;			
		}
	}	//	completeOrder
	
	/*
	 * Close Quality Order
	 */
	private void closeQOrder(){
		if(qOrder != null){
			qOrder.setDocStatus(X_C_Order.DOCACTION_Close);
			qOrder.setDocAction(X_C_Order.DOCACTION_None);
			qOrder.setProcessed(true);
			qOrder.save();
		}
	}	//	closeQOrder

	
	private void procOrdenGenInPro(){
		/*
		 * Se crean las lineas a partir de las lineas de la orden
		 */
		MOrderLine lineasOrden[] = m_order.getLines();
		for(MOrderLine lineaOrden : lineasOrden){System.out.println("For " + lineaOrden);
			/*
			 * Crea el encabezado de la Recepcion a partir de la orden
			 */
			MInOut recep = new MInOut(m_order, p_C_DocTypeShip_ID, m_order.getDateOrdered());
			
			MBPartner m_bpartner = MBPartner.get(getCtx(), m_order.getC_BPartner_ID());
			
			MPPOrder ppOrder = new MPPOrder(getCtx(), lineaOrden.get_ValueAsInt("PP_Order_ID"), get_TrxName());
			
			recep.setAD_Org_ID(lineaOrden.getAD_Org_ID());
			recep.setDateAcct(dateDoc);
			recep.setDateOrdered(dateDoc);
			recep.setDateReceived(dateDoc);
			recep.setM_Warehouse_ID(lineaOrden.getM_Warehouse_ID());
			recep.setC_Activity_ID(p_C_Activity_ID);
			recep.setBPartner(m_bpartner);
			recep.setSalesRep_ID(m_bpartner.getSalesRep_ID());		
			recep.setIsSOTrx(false);
			recep.setC_Order_ID(m_order.getC_Order_ID());
			/*
			 * Se crea la linea de la recepción a partir de la linea de la Orden
			 */
			
			System.out.println("Recepcion " + recep);
			
			if(recep.save()){System.out.println("recepcion.save() " + recep);
				MInOutLine lineaRecepcion = new MInOutLine(getCtx(), 0, get_TrxName());
				lineaRecepcion.setM_InOut_ID(recep.getM_InOut_ID());
				lineaRecepcion.setAD_Org_ID(m_order.getAD_Org_ID());
				lineaRecepcion.setOrderLine(lineaOrden, 0, Env.ZERO);
				lineaRecepcion.setQtyEntered(lineaOrden.getQtyEntered());
				lineaRecepcion.setMovementQty(lineaOrden.getQtyOrdered());
				lineaRecepcion.setC_Activity_ID(p_C_Activity_ID);
				lineaRecepcion.setQty(lineaOrden.getQtyEntered());
				lineaRecepcion.setQtyEntered(lineaOrden.getQtyEntered());
				lineaRecepcion.setC_UOM_ID(p_C_UOM_To_ID);
				lineaRecepcion.setAD_Org_ID(lineaOrden.getAD_Org_ID());
				
				int m_M_Locator_ID = ppOrder.get_ValueAsInt("M_Locator_ID"); 
				if(m_M_Locator_ID != 0){
					lineaRecepcion.setM_Locator_ID(m_M_Locator_ID);
				}
				if(lineaRecepcion.save()){
					completeReceived(recep);
				} else {
					throw new AdempiereException("@SaveError@ @M_InOutLine_ID@");
				}
			} else {
				throw new AdempiereException("@SaveError@ @M_InOut_ID@");
			}
		}
		
	}
	
	
	/**
	 * Este es un proceso tonto, no debo pasar de 40 min haciendolo
	 * Comienzo: 9:07
	 * Final: 9:40
	 */
	
	/**
	 * Crea las Recepciones una a una a partir de una Orden de Compra
	 * @return
	 */
	private void procOrdenGenIn(MOrder order){
		/*
		 * Se crean las lineas a partir de las lineas de la orden
		 */
		MOrderLine lineasOrden[] = order.getLines();
		for(MOrderLine lineaOrden : lineasOrden){System.out.println("For " + lineaOrden);
			/*
			 * Crea el encabezado de la Recepcion a partir de la orden
			 */
			MInOut recep = new MInOut(order, p_C_DocTypeShip_ID, order.getDateOrdered());
			
			MBPartner m_bpartner = MBPartner.get(getCtx(), order.getC_BPartner_ID());
			
			MPPOrder ppOrder = new MPPOrder(getCtx(), lineaOrden.get_ValueAsInt("PP_Order_ID"), get_TrxName());
			
			recep.setAD_Org_ID(lineaOrden.getAD_Org_ID());
			recep.setDateAcct(dateDoc);
			recep.setDateOrdered(dateDoc);
			recep.setDateReceived(dateDoc);
			recep.setM_Warehouse_ID(lineaOrden.getM_Warehouse_ID());
			recep.setC_Activity_ID(p_C_Activity_ID);
			recep.setBPartner(m_bpartner);
			recep.setSalesRep_ID(m_bpartner.getSalesRep_ID());		
			recep.setIsSOTrx(false);
			recep.setC_Order_ID(order.getC_Order_ID());
			/*
			 * Se crea la linea de la recepción a partir de la linea de la Orden
			 */
			
			System.out.println("Recepcion " + recep);
			
			if(recep.save()){System.out.println("recepcion.save() " + recep);
				MInOutLine lineaRecepcion = new MInOutLine(getCtx(), 0, get_TrxName());
				lineaRecepcion.setM_InOut_ID(recep.getM_InOut_ID());
				lineaRecepcion.setAD_Org_ID(order.getAD_Org_ID());
				lineaRecepcion.setOrderLine(lineaOrden, 0, Env.ZERO);
				lineaRecepcion.setQtyEntered(lineaOrden.getQtyEntered());
				lineaRecepcion.setMovementQty(lineaOrden.getQtyOrdered());
				lineaRecepcion.setC_Activity_ID(p_C_Activity_ID);
				lineaRecepcion.setQty(lineaOrden.getQtyEntered());
				lineaRecepcion.setQtyEntered(lineaOrden.getQtyEntered());
				lineaRecepcion.setC_UOM_ID(p_C_UOM_To_ID);
				lineaRecepcion.setAD_Org_ID(lineaOrden.getAD_Org_ID());
				
				int m_M_Locator_ID = ppOrder.get_ValueAsInt("M_Locator_ID"); 
				if(m_M_Locator_ID != 0){
					lineaRecepcion.setM_Locator_ID(m_M_Locator_ID);
				}
				if(lineaRecepcion.save()){
					completeReceived(recep);
				} else {
					throw new AdempiereException("@SaveError@ @M_InOutLine_ID@");
				}
			} else {
				throw new AdempiereException("@SaveError@ @M_InOut_ID@");
			}
		}
		
	}
	
	private void completeReceived(MInOut recep){
		/*
		 * Completa la Recepción de Material
		 */
		recep.setDocAction(X_M_InOut.DOCACTION_Complete);
		if (recep != null){
			recep.completeIt();
			recep.setDocStatus(X_M_InOut.DOCSTATUS_Completed);
			recep.save();
			//
			addLog(recep.getM_InOut_ID(), recep.getDateAcct(), null, recep.getDocumentNo());
			m_created_InOut++;
		}
	}	
}