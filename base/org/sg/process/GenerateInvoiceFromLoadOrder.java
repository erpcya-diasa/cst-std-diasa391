/**
 * 
 */
package org.sg.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.exceptions.DBException;
import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MUOM;
import org.compiere.model.MUOMConversion;
import org.compiere.model.X_C_Invoice;
import org.compiere.model.X_C_Order;
import org.compiere.model.X_M_InOut;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.eevolution.model.MPPOrder;
import org.sg.model.MXXLODocGenerated;
import org.sg.model.MXXLoadOrder;
import org.sg.model.MXXLoadOrderLine;

/**
 * @author info-analista2
 *
 */
public class GenerateInvoiceFromLoadOrder extends SvrProcess {
	/**	Load Order ID		*/
	private int 			p_XX_LoadOrder_ID = 0;
	/**	Sales Order			*/
	private int				m_C_Order_ID = 0;
	/**	Load Order			*/
	private MXXLoadOrder 	m_LoadOrder = null;
	/**	Created				*/
	private int 			m_created = 0;
	/** Received			*/
	private MInvoice 		m_Invoice = null;
	
	/**	Weight Register		*/
	private MPPOrder		m_WeightRegister = null;
	
	/**	Date Inveoice		*/
	private Timestamp		p_DateInvoiced;
	
	private String 			trxName = null;
	private Trx 			trx = null;
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("XX_LoadOrder_ID"))
				p_XX_LoadOrder_ID = para.getParameterAsInt();
			else if (name.equals("DateInvoiced"))
				p_DateInvoiced = (Timestamp)para.getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		trxName = Trx.createTrxName("GI");
		trx = Trx.get(trxName, true);
	}
	
	/**
	 * Validación del Proceso de ADempiere
	 * 
	 * 	EXISTS(SELECT lo.DocumentNo, lov.C_Order_ID FROM XX_LoadOrder lo
		INNER JOIN XX_LoadOrderLine lol ON(lol.XX_LoadOrder_ID = lo.XX_LoadOrder_ID)
		INNER JOIN C_OrderLine lov ON(lov.C_OrderLine_ID = lol.C_OrderLine_ID)
		WHERE lo.XX_Annulled = 'N' AND lo.XXIsInternalLoad = 'N'
		AND lo.XX_LoadOrder_ID = XX_LoadOrder.XX_LoadOrder_ID
		AND NOT EXISTS(SELECT 1 FROM XX_LODocGenerated lodg
			INNER JOIN C_Invoice fc ON(fc.C_Invoice_ID = lodg.C_Invoice_ID)
			WHERE lodg.XX_LoadOrderLine_ID = lol.XX_LoadOrderLine_ID
			AND fc.DocStatus IN('CO', 'CL'))
		AND (EXISTS(SELECT * FROM PP_Order pp 
				WHERE lo.XX_LoadOrder_ID = pp.XX_LoadOrder_ID 
				AND ((lo.XXIsBulk = 'N' AND pp.DocStatus IN('DR', 'IP', 'IN', 'VO', 'RE', 'CO', 'CL')) 
					OR (lo.XXIsBulk = 'Y' AND pp.DocStatus IN('CO', 'CL'))))
					OR lo.XXIsWeightRegister = 'N')
		GROUP BY lo.DocumentNo, lov.C_Order_ID)
	 */

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		String out = null;
		String sql = new String("SELECT lov.C_Order_ID FROM XX_LoadOrder lo " +
				"INNER JOIN XX_LoadOrderLine lol ON(lol.XX_LoadOrder_ID = lo.XX_LoadOrder_ID) " +
				"INNER JOIN C_OrderLine lov ON(lov.C_OrderLine_ID = lol.C_OrderLine_ID) " +
				"WHERE lo.XX_Annulled = 'N' AND lo.XXIsInternalLoad = 'N' " +
				"AND lo.XX_LoadOrder_ID = ? " + 
				"AND NOT EXISTS(SELECT 1 FROM XX_LODocGenerated lodg " + 
				"	INNER JOIN C_Invoice fc ON(fc.C_Invoice_ID = lodg.C_Invoice_ID) " + 
				//"	LEFT JOIN PP_Order pp ON(pp.XX_LoadOrder_ID = lo.XX_LoadOrder_ID) " +
				"	WHERE lodg.XX_LoadOrderLine_ID = lol.XX_LoadOrderLine_ID " + 
				"	AND fc.DocStatus IN('CO', 'CL')) " +
				"AND (EXISTS(SELECT * FROM PP_Order pp " +
				"		WHERE lo.XX_LoadOrder_ID = pp.XX_LoadOrder_ID " +
				"		AND ((lo.XXIsBulk = 'N' AND pp.DocStatus IN('DR', 'IP', 'IN', 'VO', 'RE', 'CO', 'CL')) " +
				"			OR (lo.XXIsBulk = 'Y' AND pp.DocStatus IN('CO', 'CL')))) " +
				"			OR lo.XXIsWeightRegister = 'N') " + 
				"GROUP BY lov.C_Order_ID");
		
		m_LoadOrder = new MXXLoadOrder(getCtx(), p_XX_LoadOrder_ID, trxName);
		
		String sqlInOut = "SELECT DocumentNO "
				+ "FROM XX_LoadOrderLine lol "
				+ "INNER JOIN XX_LODocGenerated lodg ON ( lol.XX_LoadOrderLine_ID =lodg.XX_LoadOrderLine_ID) "
				+ "INNER JOIN M_InOut io ON (io.M_InOut_ID = lodg.M_InOut_ID) "
				+ "WHERE lol.XX_LoadOrder_ID= ? AND io.DocStatus = 'IN'";
		
		int index = 1;
		PreparedStatement pstmt = null;
		//	Dixon Martinez 
		//	Validate Shipment 
		String documentNO = DB.getSQLValueString(get_TrxName(), sqlInOut, m_LoadOrder.getXX_LoadOrder_ID());
		if(documentNO != null
				&& documentNO.length() > 0)
			throw new AdempiereException("@SGNotCompletedDoc@ \n @M_InOut_ID@: " + documentNO );
		
		//	End Dixon Martinez
		try	{
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			
			if (p_XX_LoadOrder_ID != 0)
				pstmt.setInt(index++, p_XX_LoadOrder_ID);
			
			out = generarEntregas(pstmt);
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			log.log(Level.SEVERE, sql.toString(), e);
			out = e.getMessage();
		}		
		return out;
	}
	
	/**
	 * @author Yamel Senih, 10/05/2011 11:14
	 * Genera las Entregas de materiales
	 * @param pstmt
	 * @return
	 * @throws SQLException 
	 */
	private String generarEntregas(PreparedStatement pstmt) throws Exception{
		ResultSet rs;
		rs = pstmt.executeQuery ();
		findWeightRegister();
		while (rs.next ()){
			m_C_Order_ID = rs.getInt("C_Order_ID");
			MOrder orden = new MOrder (getCtx(), m_C_Order_ID, trxName);
			if(X_C_Order.DOCSTATUS_Completed.equals(orden.getDocStatus())){
				procOrden(orden);
			}
		}

		if (pstmt != null)
			pstmt.close ();
		pstmt = null;
		return "@Created@ = " + m_created;
	}
	
	/**
	 * Crea las Entregas una a una a partir de una Orden de Venta
	 * @return
	 */
	private void procOrden(MOrder orden) throws Exception{
		
		/*
		 * Crea el encabezado de la Factura a partir de la orden de Venta
		 */
		m_Invoice = new MInvoice(orden, 0, orden.getDateOrdered());
		
		MBPartner m_bpartner = MBPartner.get(getCtx(), orden.getC_BPartner_ID());
		
		m_Invoice.setAD_Org_ID(orden.getAD_Org_ID());
		m_Invoice.setM_PriceList_ID(orden.getM_PriceList_ID());
		m_Invoice.setC_Activity_ID(orden.getC_Activity_ID());
		m_Invoice.setDateInvoiced(p_DateInvoiced);
		m_Invoice.setBPartner(m_bpartner);
		m_Invoice.setSalesRep_ID(orden.getSalesRep_ID());		
		m_Invoice.setDateAcct(p_DateInvoiced);
		//	Correccion de Regla de Pago en la factura
		m_Invoice.setPaymentRule(orden.getPaymentRule());
		//	Corrección de Termino de Pago en Fact
		if(orden.getC_Payment_ID() != 0)
			m_Invoice.setC_PaymentTerm_ID(orden.getC_Payment_ID());
		
		int m_Bill_Loacation_ID = (orden.getBill_Location_ID() != 0
				? orden.getBill_Location_ID(): orden.getC_BPartner_Location_ID());
		m_Invoice.setC_BPartner_Location_ID(m_Bill_Loacation_ID);
		//	Price List
		m_Invoice.setM_PriceList_ID(orden.getM_PriceList_ID());
		
		m_Invoice.setIsSOTrx(true);
		
		//	Reference Load Order
		m_Invoice.set_ValueOfColumn("XX_LoadOrder_ID", p_XX_LoadOrder_ID);
		
		if(m_Invoice.save()){
			StringBuffer sql = new StringBuffer("SELECT loc.* FROM XX_LoadOrder oc " +
					"INNER JOIN XX_LoadOrderLine loc ON(loc.XX_LoadOrder_ID = oc.XX_LoadOrder_ID) " +
					"INNER JOIN C_OrderLine lov ON(lov.C_OrderLine_ID = loc.C_OrderLine_ID) " +
					"WHERE oc.Processed = 'N' " +
					"AND lov.C_Order_ID = ? " +
					"AND loc.XX_LoadOrder_ID = ?");
					
			PreparedStatement pstmt = null;
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			pstmt.setInt(1, orden.getC_Order_ID());
			pstmt.setInt(2, p_XX_LoadOrder_ID);
			ResultSet rs;
			rs = pstmt.executeQuery ();
			while (rs.next ()){
				MXXLoadOrderLine loCarga = new MXXLoadOrderLine(getCtx(), rs, trxName);
				MOrderLine loVenta = new MOrderLine(getCtx(), loCarga.getC_OrderLine_ID(), trxName);
				MInvoiceLine m_InvoiceLine = new MInvoiceLine(m_Invoice);
				
				BigDecimal toInvoice = loCarga.getQty();
				//	Is Bulk
				if(m_WeightRegister != null && m_LoadOrder.isXXIsBulk()){
					BigDecimal netWeight = (BigDecimal) m_WeightRegister.get_Value("Weight3");
					int m_C_UOM_ID = m_WeightRegister.getC_UOM_ID();
					int m_C_UOM_To_ID = loCarga.getM_Product().getC_UOM_ID();
					//	Correcciones de Error en la Conversion Usando el Cache
					//	Correccion de error cuando la unidad de medida es la misma
					//MUOMConversion.getRate(m_C_UOM_ID, m_C_UOM_To_ID);
					BigDecimal rate = MUOMConversion.convert(m_C_UOM_ID, m_C_UOM_To_ID, new BigDecimal(1), false);//getRate(getCtx(), m_C_UOM_ID, m_C_UOM_To_ID);
					BigDecimal qtyWeightRegister = Env.ZERO;
					if(rate != null){
						qtyWeightRegister = netWeight.multiply(rate);
						if(toInvoice.compareTo(qtyWeightRegister) >= 0){
							toInvoice = qtyWeightRegister;
						} else {
							throw new AdempiereException(Msg.translate(getCtx(), "SGQtyWRegisterHi") + " " 
									+ Msg.translate(getCtx(), "QtyOrdered") + " " 
									+ toInvoice.doubleValue() + " " 
									+ Msg.translate(getCtx(), "SGQtyWeightRegister") + " " 
									+ qtyWeightRegister.doubleValue()
									);
						}
					} else {
						MUOM m_uom = new MUOM(getCtx(), m_C_UOM_ID, trxName);
						MUOM m_uom_to = new MUOM(getCtx(), m_C_UOM_To_ID, trxName);
						throw new AdempiereException(Msg.translate(getCtx(), "SGNotConversion") + " " 
								+ Msg.translate(getCtx(), "of") + " "
								+ m_uom.getName() + " " 
								+ Msg.translate(getCtx(), "to") + " " 
								+ m_uom_to.getName()
								);
					}
				}
				
				BigDecimal qtyEntered = toInvoice;
				
				//	Correct UOM for QtyEntered
				if (loVenta.getQtyEntered().compareTo(loVenta.getQtyOrdered()) != 0)
					qtyEntered = toInvoice
						.multiply(loVenta.getQtyEntered())
						.divide(loVenta.getQtyOrdered(), 12, BigDecimal.ROUND_HALF_UP);
				
				/*line.setOrderLine(orderLine);
				line.setQtyInvoiced(qtyInvoiced);
				line.setQtyEntered(qtyEntered);
				*/
				//m_InvoiceLine.setC_Invoice_ID(m_Invoice.getC_Invoice_ID());
				//m_InvoiceLine.setAD_Org_ID(orden.getAD_Org_ID());
				//m_InvoiceLine.setLine(m_line + loVenta.getLine());
				m_InvoiceLine.setOrderLine(loVenta);
				//m_InvoiceLine.setM_Product_ID(loVenta.getM_Product_ID(), loVenta.getC_UOM_ID());
				
				m_InvoiceLine.setC_Activity_ID(loVenta.getC_Activity_ID());
				//m_InvoiceLine.setQtyEntered(loCarga.getQty());
				//inOutLine.setMovementQty(loCarga.getQty());
				m_InvoiceLine.setQtyInvoiced(toInvoice);
				
				m_InvoiceLine.setQtyEntered(qtyEntered);
				//m_InvoiceLine.setQty(loCarga.getQty());
				//m_InvoiceLine.setQtyEntered(loCarga.getQty());
				//m_InvoiceLine.setQtyInvoiced(loCarga.getQty());
				
				//m_InvoiceLine.setPrice(loVenta.getPriceEntered());
				//m_InvoiceLine.setPriceActual(loVenta.getPriceActual());
				//m_InvoiceLine.setPriceEntered(loVenta.getPriceEntered());
				//m_InvoiceLine.setPriceList(loVenta.getPriceList());
				
				if(!m_InvoiceLine.save())
					throw new AdempiereException("@SaveError@ @C_InvoiceLine_ID@");
				//	Reference Invoice
				/*loCarga.setC_Invoice_ID(m_Invoice.getC_Invoice_ID());
				if(!loCarga.save())
					throw new AdempiereException("@SaveError@ @XX_LoadOrderLine_ID@");*/
				//	Documents Generated
				MXXLODocGenerated m_DocGen = MXXLODocGenerated.getDocGenInOutInvoice(getCtx(), 
						loCarga.getXX_LoadOrderLine_ID(), trxName, false);
				//m_DocGen.setXX_LoadOrderLine_ID(loCarga.getXX_LoadOrderLine_ID());
				m_DocGen.setC_Invoice_ID(m_Invoice.getC_Invoice_ID());
				m_DocGen.setC_InvoiceLine_ID(m_InvoiceLine.getC_InvoiceLine_ID());
				m_DocGen.saveEx();
			}	
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
			m_LoadOrder.setIsInvoiced(true);
			m_LoadOrder.saveEx();
			completeInvoice();
		} else {
			throw new AdempiereException("@SaveError@ @C_Invoice_ID@");
		}
	}
	
	private void completeInvoice(){
		/*
		 * Completa la Entrega de Material
		 */
		m_Invoice.setDocAction(X_M_InOut.DOCACTION_Complete);
		if (m_Invoice != null){
			//m_Invoice.completeIt();
			m_Invoice.processIt(X_C_Invoice.DOCACTION_Complete);
			//m_Invoice.setDocStatus(X_M_InOut.DOCSTATUS_Completed);
			
			m_Invoice.saveEx();
				//throw new AdempiereException("@SaveError@ @C_Invoice_ID@");
				//trx.commit();
			//else
				//trx.rollback();
			//
			addLog(m_Invoice.getC_Invoice_ID(), m_Invoice.getDateAcct(), null, m_Invoice.getDocumentNo());
			m_created++;
		}
	}

	/**
	 * Verifica que exista una Pesada completada relacionada a la Orden de Carga
	 * @author Yamel Senih 19/03/2012, 05:44:06
	 * @throws Exception
	 * @return void
	 */
	private void findWeightRegister() throws Exception{
		String sql = new String("SELECT pp.* " +
				"FROM PP_Order pp " +
				"WHERE pp.XX_LoadOrder_ID = ? " +
				"AND pp.DocStatus IN('CO', 'CL')");
		PreparedStatement pstmt = null;
		//try	{
			pstmt = DB.prepareStatement (sql, trxName);
			int index = 1;
			if (p_XX_LoadOrder_ID != 0)
				pstmt.setInt(index++, p_XX_LoadOrder_ID);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()){
				m_WeightRegister = new MPPOrder(getCtx(), rs, trxName);
			}
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		/*}catch (Exception e) {
			pstmt = null;
			log.log(Level.SEVERE, sql.toString(), e);
		}	*/	
	}
	
	/*
import java.util.logging.Level;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;
import test.GenerateInOutFromLoadOrder;
  String mens = "OK";
  String nProcess = "entregas";
  ProcessInfoParameter pi1 = new ProcessInfoParameter("M_ShippingGuide_ID", 1000025,"","","");
  ProcessInfo pi = new ProcessInfo("", 0,0,0);
  pi.setParameter(new ProcessInfoParameter[] {pi1});
  MProcess pr = new Query(A_Ctx, MProcess.Table_Name, "value=?", A_TrxName).setParameters(new Object[]{nProcess}).first();
  if (pr != null) {
    GenerateInOutFromLoadOrder process = new GenerateInOutFromLoadOrder();
    MPInstance mpi = new MPInstance(A_Ctx, 0, null);
    mpi.setAD_Process_ID(pr.get_ID());
    mpi.setRecord_ID(0);
    mpi.save();
    pi.setAD_PInstance_ID(mpi.get_ID());
    boolean res = process.startProcess(A_Ctx, pi, A_Trx);
    if(!res){
      mens = Msg.getMsg(A_Ctx, "SGBadResult");
    }
    mens = "OK";    
  } else {
    mens =  Msg.getMsg(Env.getCtx(), "SGNoProcess");
  }
result = mens;
	 * */
	
}
