package org.sg.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;

import org.compiere.model.X_AD_TreeNode;
import org.compiere.model.X_XX_P_Time_Dimension;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;



public class M_XX_P_GeneratePeriods {

	public M_XX_P_GeneratePeriods(int id) throws SQLException {
		// TODO Auto-generated constructor stub
		m_ctx = Env.getCtx();
		trxName = Trx.createTrxName("XXGeneratePeriods");
		trx = Trx.get(trxName, true);
		m_timed = new X_XX_P_Time_Dimension(m_ctx, id, trxName);
		m_AD_Tree_ID = getDefaultAD_Tree();
		m_year=m_timed.getDateFrom().getYear();
		m_year_s= m_timed.getValue();
		generatePeriods(m_timed.getTimeCategory());
	}
	
	private boolean generatePeriods(String TimeCategory)
	{
		if (TimeCategory.equals("Y"))
		{
			if (createSemesters(m_timed))
				trx.commit();
			else
				trx.rollback();
		}
		else if (TimeCategory.equals("S"))
		{
			if (createQuarters((m_timed.getDateFrom().getMonth()==1?1:2),m_timed))
				trx.commit();
			else
				trx.rollback();
		}
		return true;
	}
	
	private boolean createSemesters(X_XX_P_Time_Dimension res_td)
	{

		X_XX_P_Time_Dimension td;
		
		for (int i=0;i<2;i++)
		{
			try
			{
				td = new X_XX_P_Time_Dimension(m_ctx, 0, trxName);
				td.setValue(i+1+" " + Msg.translate(m_ctx, "Semester"));
				td.setName(i+1+ " "+Msg.translate(m_ctx, "SemesterOf")+" "+m_year_s);
				td.setDescription(i+1+" "+Msg.translate(m_ctx, "SemesterOf")+" "+m_year_s);
				td.setTimeCategory("S");
				td.setDateFrom(new Timestamp(m_year,(i*6),getDate(true, m_year,(i*5)), 0, 0, 0, 0));
				td.setDateTo(new Timestamp(m_year,((i+1)*6)-1,getDate(false, m_year,((i+1)*6)-1), 0, 0, 0, 0));
				td.setIsActive(true);
				td.setIsFestive(false);
				td.setIsSummary(true);
				td.saveEx(trxName);
				if(!createNode(td.getXX_P_Time_Dimension_ID(), res_td.getXX_P_Time_Dimension_ID(), i*10))
					return false;
				if(!createQuarters(i+1, td))
					return false;
					
			}
			catch (Exception e) {
				return false;
			}
			

		}
	
		return true;
	}
	private boolean createQuarters(int sem,X_XX_P_Time_Dimension res_td)
	{
		X_XX_P_Time_Dimension td;
		int l_trim=0;
		for (int i=1;i<3;i++)
		{
			try
			{
				l_trim=i+((sem*2)-2);
				td = new X_XX_P_Time_Dimension(m_ctx, 0, trxName);
				td.setValue(l_trim+Msg.translate(m_ctx,"Trimester"));
				td.setName(l_trim+" " +Msg.translate(m_ctx,"TrimesterOf")+ " " +m_year_s);
				td.setDescription(l_trim+" "+Msg.translate(m_ctx,"TrimesterOf")+" "+m_year_s);
				td.setTimeCategory("Q");
				td.setDateFrom(new Timestamp(m_year,l_trim*3-3,getDate(true, m_year,(l_trim*3-3)), 0, 0, 0, 0));
				
				td.setDateTo(new Timestamp(m_year,l_trim*3-1,getDate(false, m_year,l_trim*3-1), 0, 0, 0, 0));
				
				td.setIsActive(true);
				td.setIsFestive(false);
				td.setIsSummary(true);
				td.saveEx(trxName);
				if(!createNode(td.getXX_P_Time_Dimension_ID(), res_td.getXX_P_Time_Dimension_ID(), i*10))
					return false;
				if(!createMonths(l_trim,td))
					return false;
			}
			catch (Exception e) {
				return false;
			}
		}
		return true;
	}
	private boolean createMonths(int quart,X_XX_P_Time_Dimension res_td)
	{
		X_XX_P_Time_Dimension td;
		int l_month=0;
		Timestamp date = res_td.getDateFrom();
		for (int i=1;i<4;i++)
		{
			try
			{
				l_month=i+((quart*3)-3);
				td = new X_XX_P_Time_Dimension(m_ctx, 0, trxName);
				td.setValue(Msg.translate(m_ctx,"Month")+" "+l_month+ " "+Msg.translate(m_ctx,"Of")+" " + m_year_s);
				td.setName(Msg.translate(m_ctx,"Month")+" "+l_month+ " "+Msg.translate(m_ctx,"Of")+" " + m_year_s);
				td.setDescription(Msg.translate(m_ctx,"Month")+" "+l_month+ " "+Msg.translate(m_ctx,"Of")+" " + m_year_s);
				td.setTimeCategory("M");
				td.setDateFrom(new Timestamp(m_year,l_month-1,getDate(true, m_year,(l_month-1)), 0, 0, 0, 0));
				td.setDateTo(new Timestamp(m_year,l_month-1,getDate(false, m_year,l_month-1), 0, 0, 0, 0));
				td.setIsActive(true);
				td.setIsFestive(false);
				td.setIsSummary(true);
				td.saveEx(trxName);
				if(!createNode(td.getXX_P_Time_Dimension_ID(), res_td.getXX_P_Time_Dimension_ID(), i*10))
					return false;
				
				if(!createDays(l_month,td))
					return false;
				
			}
			catch (Exception e) {
				return false;
			}
		}
		return true;
	}
	
	private boolean createDays(int month,X_XX_P_Time_Dimension res_td)
	{
		X_XX_P_Time_Dimension td;
		X_XX_P_Time_Dimension tdw;
		int l_month=0;
		int l_week=-1;
		int l_week_id =-1;
		Timestamp date = res_td.getDateFrom();
		Timestamp dateini = res_td.getDateFrom();
		Timestamp date_to = new Timestamp(res_td.getDateTo().getYear(), res_td.getDateTo().getMonth(), res_td.getDateTo().getDate()+1, 0, 0, 0, 0);
		//getDateWeek(boolean isFirst,int year,int month,int day)
		while (date.before(date_to))
		{
			try
			{
					
				if (l_week != getDateNumber(date.getYear(),date.getMonth(),date.getDate(),Calendar.WEEK_OF_YEAR))
				{
					l_week = getDateNumber(date.getYear(),date.getMonth(),date.getDate(),Calendar.WEEK_OF_YEAR);
					l_week_id = createWeek(l_week, res_td);
					dateini= date;
					if (l_week_id==-1)
						return false;
				}
				td = new X_XX_P_Time_Dimension(m_ctx, 0, trxName);
				tdw = new X_XX_P_Time_Dimension(m_ctx, l_week_id, trxName);
				
				td.setValue(((Integer)date.getDate()).toString());
				td.setName(((Integer)date.getDate()).toString());
				td.setDescription(((Integer)date.getDate()).toString());
				td.setTimeCategory("D");
				td.setDateFrom(date);
				td.setDateTo(date);
				td.setIsActive(true);
				td.setIsFestive(false);
				td.setIsSummary(false);
				td.saveEx(trxName);
				tdw.setDateFrom(dateini);
				tdw.setDateTo(date);
				tdw.saveEx(trxName);
				if(!createNode(td.getXX_P_Time_Dimension_ID(), l_week_id,date.getDate()))
					return false;
			}
			catch (Exception e) {
				return false;
			}
			
			date = new Timestamp(date.getYear(), date.getMonth(), date.getDate()+1, 0,0, 0, 0);
		}
		return true;
	} 
	
	private int createHalfMonth(int week,X_XX_P_Time_Dimension res_td)
	{
		X_XX_P_Time_Dimension td;
		int l_month=res_td.getDateFrom().getMonth()+1;
		int l_day =res_td.getDateFrom().getDate();
		try
		{
			
			td = new X_XX_P_Time_Dimension(m_ctx, 0, trxName);
			td.setValue(Msg.translate(m_ctx, "HalfMonth")+" "+ week + " "+ m_year_s);
			td.setName(Msg.translate(m_ctx, "HalfMonth")+" "+ week + " "+ m_year_s);
			td.setDescription(Msg.translate(m_ctx, "HalfMonth")+" "+ week + " "+ m_year_s);
			td.setTimeCategory("H");
			td.setDateFrom(new Timestamp(m_year,l_month-1,getDateWeek(true, m_year,l_month,l_day), 0, 0, 0, 0));
			td.setDateTo(new Timestamp(m_year,l_month-1,getDateWeek(false, m_year,l_month,l_day), 0, 0, 0, 0));
			td.setIsActive(true);
			td.setIsFestive(false);
			td.setIsSummary(true);
			td.saveEx(trxName);
			if(!createNode(td.getXX_P_Time_Dimension_ID(), res_td.getXX_P_Time_Dimension_ID(), week))
				return -1;
		}
		catch (Exception e) {
			return -1;
		}
		
		return td.getXX_P_Time_Dimension_ID();
	}
	
	private int createWeek(int week,X_XX_P_Time_Dimension res_td)
	{
		X_XX_P_Time_Dimension td;
		int l_month=res_td.getDateFrom().getMonth()+1;
		int l_day =res_td.getDateFrom().getDate();
		try
		{
			
			td = new X_XX_P_Time_Dimension(m_ctx, 0, trxName);
			td.setValue(Msg.translate(m_ctx, "Week")+" "+ week + " "+ m_year_s);
			td.setName(Msg.translate(m_ctx, "Week")+" "+ week + " "+ m_year_s);
			td.setDescription(Msg.translate(m_ctx, "Week")+" "+ week + " "+ m_year_s);
			td.setTimeCategory("W");
			td.setDateFrom(new Timestamp(m_year,l_month-1,getDateWeek(true, m_year,l_month,l_day), 0, 0, 0, 0));
			td.setDateTo(new Timestamp(m_year,l_month-1,getDateWeek(false, m_year,l_month,l_day), 0, 0, 0, 0));
			td.setIsActive(true);
			td.setIsFestive(false);
			td.setIsSummary(true);
			td.saveEx(trxName);
			if(!createNode(td.getXX_P_Time_Dimension_ID(), res_td.getXX_P_Time_Dimension_ID(), week))
				return -1;
		}
		catch (Exception e) {
			return -1;
		}
		
		return td.getXX_P_Time_Dimension_ID();
	}
	private boolean createNode(int nodeID,int parentID,int seq)
	{
		try
		{
			X_AD_TreeNode tn = new X_AD_TreeNode(m_ctx, 0, trxName);
			tn.setAD_Tree_ID(m_AD_Tree_ID);
			tn.setNode_ID(nodeID);
			tn.setParent_ID(parentID);
			tn.setSeqNo(seq);
			tn.saveEx(trxName);	
		}
		catch (Exception e) {
			return false;
		}
		
		return true;
	}
	
	private Integer getDefaultAD_Tree() throws SQLException
	{
		PreparedStatement ps;
		ResultSet rs;
		StringBuffer sql = new StringBuffer();
		Integer lnReturn =0;
		
		sql.append("Select * from AD_Tree Where treetype='TD'");
		ps = DB.prepareStatement(sql.toString());
		rs = ps.executeQuery();
		if (rs.next())
		{
			lnReturn=rs.getInt("AD_Tree_ID");
			rs.close();
			ps.close();
			return lnReturn;
		}
		
		return lnReturn;
	}
	private int getDate(boolean isFirst,int year,int month)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, 1);
		if (isFirst)
			return cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		else
			return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			
	}
	
	private String getNameDate(int year,int month,int display,int style)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, 1);
	
		Locale local =new Locale(Env.getAD_Language(Env.getCtx()));
		
		return cal.getDisplayName(display, style, local);
	}
	private int getDateWeek(boolean isFirst,int year,int month,int day)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		System.out.println(cal.getActualMinimum(Calendar.DAY_OF_WEEK));
		System.out.println(cal.getActualMaximum(Calendar.DAY_OF_WEEK));
		
		if (isFirst)
			return cal.getActualMinimum(Calendar.DAY_OF_WEEK);
		else
			return cal.getActualMaximum(Calendar.DAY_OF_WEEK);
			
	}
	private int getDateNumber(int year,int month,int day,int display)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);

		return cal.get(display);
			
	}
	private X_XX_P_Time_Dimension m_timed;
	private String trxName = null;
	private Integer m_year;
	private String m_year_s;
	private Trx trx = null;
	Properties m_ctx;
	private Integer m_AD_Tree_ID;
	
}
