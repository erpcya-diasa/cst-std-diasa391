/**
 * @finalidad 
 * @author Yamel Senih
 * @date 04/04/2012
 */
package org.sg.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MMovementLine;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.sg.model.MXXLODocGenerated;
import org.sg.model.MXXLoadOrder;
import org.sg.model.MXXLoadOrderLine;

/**
 * @author Yamel Senih 04/04/2012, 18:36:35
 *
 */
public class InsertDocGeneratedFromLoadOrder extends SvrProcess {

	/**	Load Order ID		*/
	private int 			p_XX_LoadOrder_ID = 0;
	/**	Load Order			*/
	private MXXLoadOrder 	m_LoadOrder = null;
	/**	Load Order Line		*/
	private MXXLoadOrderLine m_LoadOrderLine = null;
	/**	Created				*/
	private int 			m_created_Movement = 0;
	
	private String 			message;
	
	private String 			trxName = null;
	private Trx 			trx = null;
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("XX_LoadOrder_ID"))
				p_XX_LoadOrder_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		trxName = Trx.createTrxName("LODG");
		trx = Trx.get(trxName, true);
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		ResultSet rs = null;		
		StringBuffer sql = new StringBuffer("SELECT lol.* " +
				"FROM XX_LoadOrder lo " + 
				"INNER JOIN XX_LoadOrderLine lol ON(lol.XX_LoadOrder_ID = lo.XX_LoadOrder_ID) " + 
				"WHERE lo.XX_Annulled = 'N' " +
				"AND (lol.M_InOut_ID IS NOT NULL OR lol.C_Invoice_ID IS NOT NULL OR lol.M_MovementLine_ID IS NOT NULL) " + 
				"AND lo.XX_LoadOrder_ID = ? " +
				"AND NOT EXISTS(SELECT 1 " +
				"FROM XX_LODocGenerated dg " +
				"WHERE dg.XX_LoadOrderLine_ID = lol.XX_LoadOrderLine_ID) ");
		//System.err.println(sql);
		log.info(sql.toString());
		
		m_LoadOrder = new MXXLoadOrder(getCtx(), p_XX_LoadOrder_ID, trxName);
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			//	Param
			int index = 1;
			if (p_XX_LoadOrder_ID != 0)
				pstmt.setInt(index++, p_XX_LoadOrder_ID);
			
			rs = pstmt.executeQuery();
			
			insertDocGenerated(rs);
			
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			message = Msg.translate(getCtx(), "SaveError") + ":" + e.getMessage();
			log.log(Level.SEVERE, message);
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		return message;
	}
	
	/**
	 * Inserta las Referencias de los documentos generados en la tabla XX_LODocGenerated
	 * @author Yamel Senih 04/04/2012, 18:46:17
	 * @param rs
	 * @throws SQLException
	 * @return void
	 */
	private void insertDocGenerated(ResultSet rs) throws SQLException{
		while (rs.next()) {
			MXXLoadOrderLine m_loLoadOrderLine = new MXXLoadOrderLine(getCtx(), rs, trxName);
			MXXLODocGenerated m_docGen = new MXXLODocGenerated(getCtx(), 0, trxName);
			m_docGen.setXX_LoadOrderLine_ID(m_loLoadOrderLine.getXX_LoadOrderLine_ID());
			if(m_LoadOrder.isXXIsInternalLoad()){
				MMovementLine lm = new MMovementLine(getCtx(), m_loLoadOrderLine.getM_MovementLine_ID(), trxName);
				m_docGen.setM_Movement_ID(lm.getM_Movement_ID());
				m_docGen.setM_MovementLine_ID(lm.getM_MovementLine_ID());
			} else {
				m_docGen.setC_Invoice_ID(m_loLoadOrderLine.getC_Invoice_ID());
				m_docGen.setM_InOut_ID(m_loLoadOrderLine.getM_InOut_ID());
			}
			m_docGen.saveEx();
			m_created_Movement ++;
		}
		message = new String ("@Created@ = " + m_created_Movement);	
	}

}
