/**
 * @finalidad 
 * @author Yamel Senih
 * @date 04/05/2012
 */
package org.sg.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.model.MUser;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;
import org.fp.util.HandlerSpooler;
import org.sg.model.MXXFiscalPrinterConfig;

/**
 * @author Yamel Senih 04/05/2012, 17:11:09
 *
 */
public class PrintFiscalReport extends SvrProcess {

	private String 			message;
	/**	User Login			*/
	private int				p_AD_User_ID = 0;
	/**	Report Type			*/
	private String			p_ReportType = null;
	/**	Date From			*/
	private Timestamp		p_Date_From = null;
	/**	Date To				*/
	private Timestamp		p_Date_To = null;
	/**	(Z) From			*/
	private int				p_Z_From = 0;
	/**	(Z) To				*/
	private int				p_Z_To = 0;
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("ReportType"))
				p_ReportType = (String)para.getParameter();
			else if (name.equals("DateDoc")){
				p_Date_From = (Timestamp)para.getParameter();
				p_Date_To = (Timestamp)para.getParameter_To();
			} else if (name.equals("Z_Report")){
				p_Z_From = para.getParameterAsInt();
				p_Z_To = para.getParameter_ToAsInt();
			} else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		p_AD_User_ID = getAD_User_ID();;
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		try {
			printDocument();
		} catch (Exception e) {
			message = e.getMessage();
		}
		return message;
	}

	/**
	 * Imprime el Documento Fiscal
	 * @author Yamel Senih 06/04/2012, 14:31:21
	 * @throws Exception
	 * @return void
	 */
	private void printDocument() throws Exception {
		MUser user = new MUser(getCtx(), p_AD_User_ID, null);
		int m_XX_FiscalPrinterConfig_ID = user.get_ValueAsInt("XX_FiscalPrinterConfig_ID");
		if(m_XX_FiscalPrinterConfig_ID != 0){
			MXXFiscalPrinterConfig m_fpConfig = new MXXFiscalPrinterConfig(getCtx(), m_XX_FiscalPrinterConfig_ID, null);
			HandlerSpooler hsp = new HandlerSpooler("Report", m_fpConfig);
			//	Print Report
			if(p_ReportType.equals("X"))							//	Reporte (X).
				hsp.printXReport();
			else if(p_ReportType.equals("Z")){						//	Cierre (X).
				hsp.printZReport();
				//	Actualizacion de Fecha Reporte Z
				m_fpConfig.setXX_LastZDate(new Timestamp(System.currentTimeMillis()));
				m_fpConfig.saveEx();
			} else if(p_Date_From != null && p_Date_To != null){	//	Reporte de Cierres en un Rango de fecha.
				hsp.printFiscalMemReport(p_Date_From, p_Date_To, p_ReportType);
			} else if(p_Z_From != 0 && p_Z_To != 0){				//	Reporte de Cierres en un Rango de (Z)s.
				hsp.printFiscalMemReport(p_Z_From, p_Z_To, p_ReportType);
			} else
				throw new Exception(Msg.translate(getCtx(), "@Error@ @Param@"));
			//	Close Spooler
			hsp.printStackCmd();
			message = "OK";			
		} else
			throw new Exception(Msg.translate(getCtx(), "SGNotConfFiscalPrinter"));	//	Impresora Fiscal No Configurada					
	}
	
}
