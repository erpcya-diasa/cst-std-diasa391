/**
 * 
 */
package org.sg.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MShipper;
import org.compiere.model.MUOMConversion;
import org.compiere.model.Tax;
import org.compiere.model.X_C_Order;
import org.compiere.model.X_M_InOut;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.eevolution.model.MPPOrder;

/**
 * @author Yamel Senih
 *
 */
public class QualityOrderPOCreateShipper extends SvrProcess {

	/** Warehouse			*/
	private int			p_M_Warehouse_ID = 0;

	/**	Doc Date From		*/
	private Timestamp	p_DateDoc;
	/**	Doc Date From		*/
	private Timestamp	dateDoc;
	/** BPartner			*/
	private int 		p_C_BPartner_ID = 0;
	
	private int 		p_C_DocType_ID = 0;
	
	/**	UOM					*/
	private int			p_C_UOM_ID = 0;
	
	/**	UOM To				*/
	private int 		p_C_UOM_To_ID = 0;
	
	private int			m_created_Order = 0;
	
	private boolean		p_ConsolidateDocument = false;
	
	/** Order				*/
	private MOrder		m_orderShipper = null;
	
	/** Quality Order		*/
	private MPPOrder	qOrder = null;

	/**	Producto Tipo Servicio				*/
	private int			p_M_Product_ID = 0;

	/** Tipo de Documento de Transportista*/
	private int 		p_C_DocTypeTarget_ID = 0;
	
	/**	Created				*/
	private int 		m_created_InOut = 0;
	/** Received			*/
	
	private MInOut		recepcion = null;
	
	/**	Ordenes ya generadas*/
	private int 		m_Ordenes = 0;
	
	/**	Business Partner Shipper			*/
	private MBPartner m_bpartner = null;
	
	/**	Ordenes a transportista ya generadas*/
	private int 		m_C_Order_ID = 0;
	
	/**	Actividad				*/
	private int 		p_C_Activity_ID = 0;
	
	/**	Errores					*/
	private int 		m_Errors = 0;
	
	/**	Summary				*/
	private StringBuffer summary = new StringBuffer();
	
	private String 		message = null;
	
	@Override
	protected void prepare() {

		for (ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			//fjviejo e-evolution petrocasa end
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para.getParameterAsInt();
			else if (name.equals("M_Locator_ID"))
				para.getParameterAsInt();
			else if (name.equals("DateDoc"))
				p_DateDoc = (Timestamp)para.getParameter();
			else if (name.equals("C_DocType_ID"))
				p_C_DocType_ID = para.getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para.getParameterAsInt();
			else if (name.equals("ConsolidateDocument"))
				p_ConsolidateDocument = "Y".equals(para.getParameter());
			else if (name.equals("C_DocTypeTarget_ID"))
				p_C_DocTypeTarget_ID = para.getParameterAsInt();
			else if (name.equals("C_UOM_ID"))
				p_C_UOM_ID = para.getParameterAsInt();
			else if (name.equals("C_UOM_To_ID"))
				p_C_UOM_To_ID = para.getParameterAsInt();
			else if (name.equals("C_Activity_ID"))
				p_C_Activity_ID = para.getParameterAsInt();
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare

	@Override
	protected String doIt() throws Exception {
		String sql = null;
		sql = "SELECT * FROM PP_Order o "
			+ "WHERE DocStatus IN('CO', 'CL') "
			+ "AND XX_TipoPesada = 'PM' "
			+ "AND XX_Estatus_Calidad NOT IN('R') "
			+ "AND AD_Client_ID=? "
			+ "AND EXISTS(SELECT pc.* "
			+ "FROM C_Period p "
			+ "INNER JOIN C_PeriodControl pc ON(pc.C_Period_ID = p.C_Period_ID) "
			+ "WHERE pc.DocBaseType IN('MQO', 'MMS', 'POO') "
			+ "AND pc.PeriodStatus = 'O' "
			+ "AND p.StartDate <= DateOrdered AND p.EndDate >= DateOrdered) ";
		if (p_C_BPartner_ID != 0)
			sql += " AND C_BPartner_ID=?";
		if (p_C_DocType_ID != 0)
			sql += " AND C_DocType_ID=?";
		
		sql+= " Order BY PP_Order_ID ";
		log.info(sql.toString());
		
		System.out.println(sql);
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, get_TrxName());
			int index = 1;
			pstmt.setInt(index++, getAD_Client_ID());
			if (p_C_BPartner_ID != 0)
				pstmt.setInt(index++, p_C_BPartner_ID);
			if (p_C_DocType_ID != 0)
				pstmt.setInt(index++, p_C_DocType_ID);
		}catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		
		return generarOrdenes(pstmt);
	}	//	doIt
	
	/*
	 *	generarOrdenes
	 */
	private String generarOrdenes(PreparedStatement pstmt){
		try{
			ResultSet rs = pstmt.executeQuery();
			if(p_ConsolidateDocument){
				consolidado(rs);
			}else{
				noConsolidado(rs);
			}		
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		} catch (Exception e){
			m_Errors ++;
			log.log(Level.SEVERE, "", e);
		}		
		
		summary.append("@C_Order_ID@ = ");
		summary.append(m_created_Order);
		summary.append(" @M_InOut_ID@ = ");
		summary.append(m_created_InOut);
		summary.append(" Errors = ");
		summary.append(m_Errors);

		addLog(summary.toString());
		
		if(message != null)
			return message;
		return summary.toString();
	}	//	generarOrdenes
	
	/*
	 * consolidado
	 */
	private String consolidado(ResultSet rs) throws SQLException{
		if(rs.next ()){
			qOrder = new MPPOrder (getCtx(), rs, get_TrxName());
			crearOrden();
			do{
				qOrder = new MPPOrder (getCtx(), rs, get_TrxName());
				crearLineas();
				closeQOrder();
			}while (rs.next ());
			//completeOrder();
		}
		return "";
	}	//	consolidado
	
	private void consultaRealizados(){
		String sql = new String("SELECT " +
				"(SELECT count(*) " +
				"FROM PP_Order pp INNER JOIN C_OrderLine lo ON(lo.PP_Order_ID = pp.PP_Order_ID AND lo.M_Product_ID <> pp.M_Product_ID) " +
				"WHERE pp.PP_Order_ID = ? " +
				"AND pp.IsInclude = 'N') ordenes, " +
				"(SELECT MAX(lo.C_Order_ID) " +
				"FROM PP_Order pp " +
				"INNER JOIN C_OrderLine lo ON(lo.PP_Order_ID = pp.PP_Order_ID AND lo.M_Product_ID <> pp.M_Product_ID) " +
				"INNER JOIN C_Order o ON(o.C_Order_ID = lo.C_Order_ID) " +
				"LEFT JOIN M_InOutLine iol ON(lo.C_OrderLine_ID = iol.C_OrderLine_ID) " +
				"WHERE iol.M_InOutLine_ID IS NULL " +
				"AND pp.IsInclude = 'N' " +
				"AND o.DocStatus IN('CO', 'CL') " +
				"AND pp.PP_Order_ID = ?) C_Order_ID");
		
		log.fine("consultaRealizados SQL = " + sql);
		
		//System.err.println("SQL " + sql);
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			int cont = 1;
			pstmt = DB.prepareStatement (sql, get_TrxName());
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			rs = pstmt.executeQuery();
			if(rs.next()){
				m_Ordenes = rs.getInt("ordenes");
			
				m_C_Order_ID = rs.getInt("C_Order_ID");
				//System.err.println("m_Ordenes " + m_Ordenes + " m_C_Order_ID " + m_C_Order_ID);
			}
		} catch (SQLException e) {
			m_Errors ++;
			throw new AdempiereException(Msg.translate(getCtx(), "SGBDError") + ":" + e.getMessage());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
	}
	
	
	/*
	 * consolidado
	 */
	private void noConsolidado(ResultSet rs) throws SQLException{
		int m_M_Shipper_ID = 0;
		MShipper m_shipper = null;
		while (rs.next ()){
			System.out.println("Pesada " + rs.getString("DocumentNo"));
			qOrder = new MPPOrder (getCtx(), rs, get_TrxName());
			consultaRealizados();
			if(p_C_UOM_To_ID == 0){
				m_Errors ++;
				throw new AdempiereException(Msg.translate(getCtx(), "SGCUOMNotF"));
			}
			if(p_DateDoc == null){
				dateDoc = qOrder.getDateOrdered();
			} else {
				dateDoc = p_DateDoc;
			}
			
			if(m_Ordenes == 0){
				if(!qOrder.get_ValueAsBoolean("IsInclude")){ //System.out.println("Order");
					m_M_Shipper_ID = qOrder.get_ValueAsInt("M_Shipper_ID");
					m_shipper = new MShipper(getCtx(), m_M_Shipper_ID, get_TrxName());
					m_bpartner = MBPartner.get(getCtx(), m_shipper.getC_BPartner_ID());
					if(MBPartnerLocation.getForBPartner(getCtx(), m_bpartner.getC_BPartner_ID(), get_TrxName()) == null 
							|| MBPartnerLocation.getForBPartner(getCtx(), m_bpartner.getC_BPartner_ID(), get_TrxName()).length == 0){
						message = Msg.translate(getCtx(), "C_BPartner_ID") + ": " + 
								m_bpartner.getValue() + " - " + m_bpartner.getName() +
								" No tiene Dirección";
						break;
					} else {
						crearOrden();
						crearLineas();
						procOrdenGenInPro();
					}
				}
			} /*else if(m_C_Order_ID != 0){
				if(!qOrder.get_ValueAsBoolean("IsInclude")){ //System.out.println("Recep");
					m_orderCarrier = new MOrder(Env.getCtx(), m_C_Order_ID, get_TrxName());
					procOrdenGenInPro();
				}
			}*/
		}
	}	//	consolidado
	
	/*
	 *	crearOrden
	 */
	private void crearOrden(){

			m_orderShipper = new MOrder(getCtx(), 0, get_TrxName());
			m_orderShipper.setAD_Org_ID(qOrder.getAD_Org_ID());
			m_orderShipper.setDatePromised(dateDoc);
			m_orderShipper.setDateAcct(dateDoc);
			m_orderShipper.setDateOrdered(dateDoc);
			m_orderShipper.setC_DocTypeTarget_ID(p_C_DocTypeTarget_ID);
			m_orderShipper.setBPartner(m_bpartner);
			m_orderShipper.setM_PriceList_ID(m_bpartner.getPO_PriceList_ID());
			m_orderShipper.setC_PaymentTerm_ID(m_bpartner.getPO_PaymentTerm_ID());
			m_orderShipper.setDateAcct(dateDoc);
			m_orderShipper.setC_Activity_ID(p_C_Activity_ID);
			m_orderShipper.setIsSOTrx(false);
		
			m_orderShipper.setDescription(qOrder.getDocumentNo());
		
			if(p_M_Warehouse_ID != 0){
				m_orderShipper.setM_Warehouse_ID(p_M_Warehouse_ID);
			} else {
				m_orderShipper.setM_Warehouse_ID(qOrder.getM_Warehouse_ID());
			}
			m_orderShipper.setPriorityRule(qOrder.getPriorityRule());
			
	}	//	crearOrden
	
	
	/*
	 * 	crearLineas
	 */
	private void crearLineas(){
		
		if(m_orderShipper.save()){	System.out.println("Order Save " + m_orderShipper);		
			
			MProduct m_Product = new MProduct(getCtx(), p_M_Product_ID, get_TrxName());
			
			/*	Conversion	*/
			BigDecimal rConversion = MUOMConversion.getRate(getCtx(), p_C_UOM_ID, p_C_UOM_To_ID);
			//System.err.println(rConversion);
			int C_Tax_ID = Tax.get (getCtx(), p_M_Product_ID, 0, dateDoc, dateDoc,
					m_orderShipper.getAD_Org_ID(), m_orderShipper.getM_Warehouse_ID(), 
				m_orderShipper.getBill_Location_ID(), m_orderShipper.getC_BPartner_Location_ID(),
				"Y".equals(m_orderShipper.isSOTrx()));
			
			BigDecimal pNeto = (BigDecimal)qOrder.get_Value("Weight3");
			MOrderLine m_orderLine = new MOrderLine(m_orderShipper);
			
			m_orderLine.setProduct(m_Product);
			m_orderLine.setC_UOM_ID(m_Product.getC_UOM_ID());
			
			m_orderLine.setC_Tax_ID(C_Tax_ID);

			m_orderLine.setQty(pNeto.multiply(rConversion));
			m_orderLine.setQtyOrdered(pNeto.multiply(rConversion));
			
			m_orderLine.setDiscount(Env.ZERO);
			m_orderLine.setPrice();
			m_orderLine.setAD_Org_ID(m_orderShipper.getAD_Org_ID());
			m_orderLine.setDateOrdered(qOrder.getDateOrdered());
			m_orderLine.setDatePromised(qOrder.getDatePromised());
			m_orderLine.set_ValueOfColumn("PP_Order_ID", qOrder.getPP_Order_ID());
			m_orderLine.setC_Activity_ID(p_C_Activity_ID);
			m_orderLine.setDiscount(Env.ZERO);
			
			if(m_orderLine.save()){
				completeOrder();
				//addLog(m_orderShipper.getC_Order_ID(), m_orderShipper.getDateAcct(), null, m_orderShipper.getDocumentNo());
				//m_created_Order++;
			} else {
				m_Errors ++;
				throw new AdempiereException("@SaveError@ @C_OrderLine_ID@");
			}
		} else{
			m_Errors ++;
			throw new AdempiereException("@SaveError@ @C_Order_ID@");
		}
	}	//	crearLineas
	
	
	/*
	 * 	Complete Order
	 */
	private void completeOrder()
	{
		if (m_orderShipper != null)
		{
			//	Fails if there is a confirmation
			if (!m_orderShipper.processIt(DocAction.ACTION_Complete))
				log.warning("Failed: " + m_orderShipper);
			m_orderShipper.setDocStatus(X_C_Order.DOCSTATUS_Completed);
			m_orderShipper.setDocAction(X_C_Order.DOCACTION_Close);
			m_orderShipper.setProcessed(true);
			m_orderShipper.save();
			//
			addLog(m_orderShipper.getC_Order_ID(), m_orderShipper.getDateOrdered(), null, m_orderShipper.getDocumentNo());
			m_created_Order++;
		}
	}	//	completeOrder
	
	private void procOrdenGenInPro(){
		/*
		 * Se crean las lineas a partir de las lineas de la orden
		 */
		MOrderLine lineasOrden[] = m_orderShipper.getLines();
		
		if(lineasOrden != null && lineasOrden.length > 0){
			MDocType docType = new MDocType(getCtx(), p_C_DocTypeTarget_ID, get_TrxName());
		
			//	Se obtiene el documento destino de Recepciones
			int m_C_DocTypeShip_ID =docType.getC_DocTypeShipment_ID();
		
			//	Se Obtienen las lineas de las Ordenes
			MOrderLine lineaOrden = lineasOrden[0];
			System.out.println("Get " + lineaOrden);
			/*
			 * Crea el encabezado de la Recepcion a partir de la orden
			 */
			recepcion = new MInOut(m_orderShipper, m_C_DocTypeShip_ID, m_orderShipper.getDateOrdered());
			
			MBPartner m_bpartner = MBPartner.get(getCtx(), m_orderShipper.getC_BPartner_ID());
			
			MPPOrder ppOrder = new MPPOrder(getCtx(), lineaOrden.get_ValueAsInt("PP_Order_ID"), get_TrxName());
			
			recepcion.setAD_Org_ID(lineaOrden.getAD_Org_ID());
			recepcion.setDateAcct(dateDoc);
			recepcion.setDateOrdered(dateDoc);
			recepcion.setDateReceived(dateDoc);
			recepcion.setM_Warehouse_ID(lineaOrden.getM_Warehouse_ID());
			recepcion.setC_Activity_ID(p_C_Activity_ID);
			recepcion.setBPartner(m_bpartner);
			recepcion.setSalesRep_ID(m_orderShipper.getSalesRep_ID());		
			recepcion.setIsSOTrx(false);
			recepcion.setC_Order_ID(m_orderShipper.getC_Order_ID());
			/*
			 * Se crea la linea de la recepción a partir de la linea de la Orden
			 */
			
			System.out.println("Recepcion " + recepcion);
			
			if(recepcion.save()){
				MInOutLine lineaRecepcion = new MInOutLine(getCtx(), 0, get_TrxName());
				lineaRecepcion.setM_InOut_ID(recepcion.getM_InOut_ID());
				lineaRecepcion.setAD_Org_ID(m_orderShipper.getAD_Org_ID());
				lineaRecepcion.setOrderLine(lineaOrden, 0, Env.ZERO);
			
				lineaRecepcion.setQty(lineaOrden.getQtyOrdered());
				lineaRecepcion.setQtyEntered(lineaOrden.getQtyEntered());
			
				lineaRecepcion.setC_Activity_ID(p_C_Activity_ID);
				
				lineaRecepcion.setC_UOM_ID(lineaOrden.getC_UOM_ID());
			
				lineaRecepcion.setAD_Org_ID(lineaOrden.getAD_Org_ID());
				
				int m_M_Locator_ID = ppOrder.get_ValueAsInt("M_Locator_ID"); 
				if(m_M_Locator_ID != 0){
					lineaRecepcion.setM_Locator_ID(m_M_Locator_ID);
				}
				if(lineaRecepcion.save()){
					completeReceived();
				} else {
					m_Errors ++;
					throw new AdempiereException("@SaveError@ @M_InOutLine_ID@");
				}
			} else {
				m_Errors ++;
				throw new AdempiereException("@SaveError@ @M_InOut_ID@");
			}
		} else {
			log.warning("@C_OrderLine_ID@ Not Found");
		}
	}
	
	/*
	 * Close Quality Order
	 */
	private void closeQOrder(){
		if(qOrder != null){
			qOrder.setDocStatus(X_C_Order.DOCSTATUS_Closed);
			qOrder.setDocAction(X_C_Order.DOCACTION_None);
			//qOrder.setProcessed(true);
			qOrder.save();
		}
	}	//	closeQOrder
	
	private void completeReceived(){
		/*
		 * Completa la Recepción de Material
		 */
		if (recepcion != null){
			recepcion.completeIt();
			recepcion.setDocStatus(X_M_InOut.DOCSTATUS_Completed);
			recepcion.setDocAction(X_M_InOut.DOCACTION_Close);
			recepcion.setProcessed(true);
			recepcion.save();
			//
			addLog(recepcion.getM_InOut_ID(), recepcion.getDateAcct(), null, recepcion.getDocumentNo());
			m_created_InOut++;
		}
	}
}