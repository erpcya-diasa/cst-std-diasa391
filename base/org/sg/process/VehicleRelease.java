/**
 * @finalidad 
 * @author Yamel Senih
 * @date 24/10/2011
 */
package org.sg.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.sg.model.MXXLoadOrder;

/**
 * 		 
 * @author Yamel Senih 21/03/2012, 18:41:09
 *
 */
public class VehicleRelease extends SvrProcess {

	/**	Load Order ID		*/
	private int 			p_XX_LoadOrder_ID = 0;
	/**	Date Finish			*/
	private Timestamp		p_DateFinish = null;
	/**	Description			*/
	private String 			p_VehicleDescription = null;
	/**	Message				*/
	private String 			message;
	
	private String 			trxName = null;
	private Trx 			trx = null;
	
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("XX_LoadOrder_ID"))
				p_XX_LoadOrder_ID = para.getParameterAsInt();
			else if(name.equals("DateFinish"))
				p_DateFinish = (Timestamp)para.getParameter();
			else if (name.equals("VehicleDescription"))
				p_VehicleDescription = (String) para.getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		trxName = Trx.createTrxName("DR");
		trx = Trx.get(trxName, true);
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() {
		try {
			if(p_XX_LoadOrder_ID != 0){
				MXXLoadOrder m_XX_LoadOrder = new MXXLoadOrder(getCtx(), p_XX_LoadOrder_ID, trxName);
				if((m_XX_LoadOrder.isInvoiced() 
						&& m_XX_LoadOrder.isDelivered())
						|| m_XX_LoadOrder.isXXIsMoved()){
					boolean release = m_XX_LoadOrder.isXXIsVehicleReleased();
					if(!release){
						m_XX_LoadOrder.setXXIsVehicleReleased(true);
						m_XX_LoadOrder.setDateFinish(p_DateFinish);
						if(m_XX_LoadOrder.getVehicleDescription() != null)
							m_XX_LoadOrder.setVehicleDescription(m_XX_LoadOrder.getVehicleDescription() + " [" + p_VehicleDescription + "]");
						else
							m_XX_LoadOrder.setVehicleDescription("[" + p_VehicleDescription + "]");
						m_XX_LoadOrder.saveEx();
						trx.commit();
						message = "OK";
					} else {
						message = Msg.translate(getCtx(), "SGVehicleReleased");
					}
				} else {
					message = Msg.translate(getCtx(), "SGLoadOrderIncomplete");
				}
			}
		} catch (Exception e) {
			message = Msg.translate(getCtx(), "SaveError") + ":" + e.getMessage();
			log.log(Level.SEVERE, message);
			trx.rollback();
		}
		
		return message;
	}
	
}
