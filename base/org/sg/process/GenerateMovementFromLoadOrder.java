/**
 * @finalidad 
 * @author Yamel Senih
 * @date 24/10/2011
 */
package org.sg.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.X_M_Movement;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.eevolution.model.MPPOrder;
import org.sg.model.MXXLODocGenerated;
import org.sg.model.MXXLoadOrder;
import org.sg.model.MXXLoadOrderLine;

/**
 * 
 * @author Yamel Senih 19/03/2012, 21:06:50
 *
 */			 
public class GenerateMovementFromLoadOrder extends SvrProcess {

	/**	Locator From		*/
	private int				m_M_LocatorFrom_ID = 0;
	/**	Locator				*/
	private int				m_M_LocatorTo_ID = 0;
	/**	Actividad				*/
	private int 			p_C_Activity_ID = 0;
	/**	Load Order ID		*/
	private int 			p_XX_LoadOrder_ID = 0;
	/**	Load Order			*/
	private MXXLoadOrder 	m_LoadOrder = null;
	/**	Load Order Line		*/
	private MXXLoadOrderLine m_LoadOrderLine = null;
	/** Movement			*/
	private MMovement		m_movement = null;
	/**	Weight Register		*/
	private MPPOrder		m_WeightRegister = null;
	/**	Created				*/
	private int 			m_created_Movement = 0;
	
	private String 			message;
	
	private String 			trxName = null;
	private Trx 			trx = null;
	
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("XX_LoadOrder_ID"))
				p_XX_LoadOrder_ID = para.getParameterAsInt();
			else if (name.equals("C_Activity_ID"))
				p_C_Activity_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		trxName = Trx.createTrxName("GMFLO");
		trx = Trx.get(trxName, true);
	}

	/**
	 * Proceso en ADempiere
	 * 
	 * 	EXISTS(SELECT lo.DocumentNo, lov.C_Order_ID FROM XX_LoadOrder lo
		INNER JOIN XX_LoadOrderLine lol ON(lol.XX_LoadOrder_ID = lo.XX_LoadOrder_ID)
		INNER JOIN C_OrderLine lov ON(lov.C_OrderLine_ID = lol.C_OrderLine_ID)
		WHERE lo.XX_Annulled = 'N' AND lo.XXIsInternalLoad = 'Y'
		AND lo.XX_LoadOrder_ID = XX_LoadOrder.XX_LoadOrder_ID
		AND NOT EXISTS(SELECT 1 FROM XX_LODocGenerated lodg
			INNER JOIN M_Movement mv ON(mv.M_Movement_ID = lodg.M_Movement_ID)
			WHERE lodg.XX_LoadOrderLine_ID = lol.XX_LoadOrderLine_ID
			AND mv.DocStatus IN('CO', 'CL', 'IP'))
		AND (EXISTS(SELECT * FROM PP_Order pp 
				WHERE lo.XX_LoadOrder_ID = pp.XX_LoadOrder_ID 
				AND pp.DocStatus IN('CO', 'CL')) 
				OR lo.XXIsWeightRegister = 'N')
		GROUP BY lo.DocumentNo, lov.C_Order_ID)
	 */
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() {
		ResultSet rs = null;		
		StringBuffer sql = new StringBuffer("SELECT lol.* FROM XX_LoadOrder lo " + 
				"INNER JOIN XX_LoadOrderLine lol ON(lol.XX_LoadOrder_ID = lo.XX_LoadOrder_ID) " + 
				//"INNER JOIN C_OrderLine lov ON(lov.C_OrderLine_ID = lol.C_OrderLine_ID) " + 
				"WHERE lo.XX_Annulled = 'N' AND lo.XXIsInternalLoad = 'Y' " + 
				"AND lo.XX_LoadOrder_ID = ? " + 
				"AND NOT EXISTS(SELECT 1 FROM XX_LODocGenerated lodg " + 
				"	INNER JOIN M_Movement mv ON(mv.M_Movement_ID = lodg.M_Movement_ID) " + 
				"	WHERE lodg.XX_LoadOrderLine_ID = lol.XX_LoadOrderLine_ID " + 
				"	AND mv.DocStatus IN('CO', 'CL', 'IP')) " + 
				"AND (EXISTS(SELECT * FROM PP_Order pp "  + 
				"		WHERE lo.XX_LoadOrder_ID = pp.XX_LoadOrder_ID " + 
				"		AND pp.DocStatus IN('CO', 'CL')) " +
				"		OR lo.XXIsWeightRegister = 'N') ");
		//System.err.println(sql);
		log.info(sql.toString());
		
		m_LoadOrder = new MXXLoadOrder(getCtx(), p_XX_LoadOrder_ID, trxName);
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			//	Param
			int index = 1;
			if (p_XX_LoadOrder_ID != 0)
				pstmt.setInt(index++, p_XX_LoadOrder_ID);
			
			rs = pstmt.executeQuery();
			
			crearMovimientos(rs);
			
		}catch (Exception e) {
			trx.rollback();
			message = Msg.translate(getCtx(), "SaveError") + ":" + e.getMessage();
			log.log(Level.SEVERE, message);
			throw new AdempiereException(e);
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return message;
	}
	
	/**
	 * Crea Movimientos de Inventario a partir de Orden de Carga
	 * @author Yamel Senih 20/03/2012, 09:54:46
	 * @param rs
	 * @throws SQLException
	 * @return void
	 */
	private void crearMovimientos(ResultSet rs) throws Exception{
		
		if (rs.next()){
			m_LoadOrderLine = new MXXLoadOrderLine(getCtx(), rs, trxName);
			
			if(m_LoadOrderLine != null){
				String DocStatusM = m_LoadOrderLine.getM_MovementLine().getM_Movement().getDocStatus();
				if(m_LoadOrderLine.getM_MovementLine_ID() == 0 
						|| X_M_Movement.DOCSTATUS_Voided.equals(DocStatusM) 
						|| X_M_Movement.DOCSTATUS_Reversed.equals(DocStatusM)){
					findWeightRegister();
					crearEncabezado();
					do{
						m_LoadOrderLine = new MXXLoadOrderLine(getCtx(), rs, trxName);
						crearLineas(rs);		
					} while(rs.next());
				}
				completeMovement();
			}
		}
		
		message = new String ("@Created@ = " + m_created_Movement);		
	}	//	crearMovimientos

	/**
	 * Crea el encabezado de los Movimientos
	 * @author Yamel Senih 24/10/2011, 12:12:20
	 * @return void
	 */
	private void crearEncabezado(){
		m_M_LocatorFrom_ID = m_LoadOrder.getM_Locator_ID();
		/*if(m_M_LocatorFrom_ID == 0){
			//m_Errors ++;
			message = Msg.translate(getCtx(), "XX_WeightRegister_ID") 
					+ " : " + m_WeightRegister.getDocumentNo() + " " + Msg.translate(getCtx(), "SGLocatorFromNot");
			throw new AdempiereException(message);
		}*/
		m_M_LocatorTo_ID = m_LoadOrder.getM_LocatorTo_ID();
		/*if(m_M_LocatorTo_ID == 0){
			//m_Errors ++;
			message = Msg.translate(getCtx(), "XX_WeightRegister_ID") 
					+ " : " + m_WeightRegister.getDocumentNo() + " " + Msg.translate(getCtx(), "SGLocatorToNot");
			throw new AdempiereException(message);
		}
		
		if(m_M_LocatorFrom_ID == m_M_LocatorTo_ID){
			//m_Errors++;
			message = Msg.translate(getCtx(), "XX_WeightRegister_ID") 
					+ " : " + m_WeightRegister.getDocumentNo() + " " + Msg.translate(getCtx(), "SGLocatorsWheight");
			throw new AdempiereException(message);
		}*/
		
		m_movement = new MMovement(getCtx(), 0, trxName);
		m_movement.setAD_Org_ID(m_LoadOrder.getAD_Org_ID());
		m_movement.setDateReceived(m_LoadOrder.getDateDoc());
		m_movement.setMovementDate(m_LoadOrder.getDateDoc());
		m_movement.setDescription(m_LoadOrder.getDocumentNo());
		/*	Activity					*/
		if(p_C_Activity_ID != 0)
			m_movement.setC_Activity_ID(p_C_Activity_ID);
		else {
			m_movement.setC_Activity_ID(m_LoadOrderLine.getC_OrderLine().getC_Order().getC_Activity_ID());
		}
		/*	ID de La Boleta de Romana	*/
		if(m_WeightRegister != null 
				&& m_LoadOrder.isXXIsWeightRegister()){
			m_movement.set_ValueOfColumn("XX_Boleta_ID", m_WeightRegister.getPP_Order_ID());
		}
		m_movement.set_ValueOfColumn("XX_LoadOrder_ID", m_LoadOrder.getXX_LoadOrder_ID());
		
		MDocType m_DocTypeOrder = new MDocType(getCtx(), m_LoadOrder.getC_DocTypeOrder_ID(), trxName);
		
		if(m_DocTypeOrder != null){
			int p_C_DocTypeMovement_ID = m_DocTypeOrder.get_ValueAsInt("C_DoctypeMovement_ID");
			System.out
					.println("GenerateMovementFromLoadOrder.crearEncabezado() " + p_C_DocTypeMovement_ID);
			if(p_C_DocTypeMovement_ID != 0) {
				m_movement.setC_DocType_ID(p_C_DocTypeMovement_ID);
			} else {
				message = Msg.translate(getCtx(), "SGNotDefDocTypeForMovement");
				//m_Errors++;
				throw new AdempiereException(message);
			}
		}
		m_movement.saveEx();
		
	}	//	crearEncabezado
	
	/**
	 * Crea las lineas del Movimiento a partir de la Orden de Carga
	 * @author Yamel Senih 20/03/2012, 09:45:22
	 * @param rs
	 * @return void
	 */
	private void crearLineas(ResultSet rs) throws Exception {

		MMovementLine m_movementLine = new MMovementLine(getCtx(), 0, trxName);
		
		m_movementLine.setAD_Org_ID(m_movement.getAD_Org_ID());
		m_movementLine.setM_Movement_ID(m_movement.getM_Movement_ID());
		
		/*	Product		*/
		m_movementLine.setM_Product_ID(m_LoadOrderLine.getM_Product_ID());
		/*	Atributos	*/
		//m_movementLine.setM_AttributeSetInstance_ID(m_WeightRegister.getM_AttributeSetInstance_ID());
		//m_movementLine.setM_AttributeSetInstanceTo_ID(m_WeightRegister.getM_AttributeSetInstance_ID());
		/*	Cantidad	*/
		m_movementLine.setMovementQty(m_LoadOrderLine.getQty());
		/*	Unidad de Medida	*/
		/*	Localización desde Hasta	*/
		m_movementLine.setM_Locator_ID(m_M_LocatorFrom_ID);
		m_movementLine.setM_LocatorTo_ID(m_M_LocatorTo_ID);
		m_movementLine.saveEx();
		//System.err.println(m_movement.getDocumentNo());
		//System.err.println();
		//m_LoadOrderLine.setM_MovementLine_ID(m_movementLine.getM_MovementLine_ID());
		//m_LoadOrderLine.saveEx();
		//closeOrder(m_LoadOrderLine.getC_OrderLine_ID());
		//	Documents Generated
		MXXLODocGenerated m_DocGen = new MXXLODocGenerated(getCtx(), 0, trxName);
		m_DocGen.setXX_LoadOrderLine_ID(m_LoadOrderLine.getXX_LoadOrderLine_ID());
		m_DocGen.setM_Movement_ID(m_movement.getM_Movement_ID());
		m_DocGen.setM_MovementLine_ID(m_movementLine.getM_MovementLine_ID());
		m_DocGen.saveEx();
	}
	
	/**
	 * Completa el Movimiento
	 * @author Yamel Senih 24/10/2011, 17:10:10
	 * @return void
	 */
	private void completeMovement() throws Exception {
		if (m_movement != null){
			//m_movement.completeIt();
			m_movement.processIt(X_M_Movement.DOCACTION_Complete);
			/*m_movement.setDocStatus(X_M_Movement.DOCSTATUS_Completed);
			m_movement.setDocAction(X_M_Movement.DOCACTION_Close);*/
			//m_movement.setProcessed(true);
			m_movement.saveEx();
			m_LoadOrder.setXXIsMoved(true);
			m_LoadOrder.saveEx();
			addLog(m_movement.getM_Movement_ID(), m_movement.getDateReceived(), null, m_movement.getDocumentNo());
			m_created_Movement++;
			trx.commit();
		}
	}

	/**
	 * Cierra las Ordenes de Venta que estan procesandose
	 * @author Yamel Senih 21/03/2012, 16:22:38
	 * @param m_C_OrderLine_ID
	 * @return void
	 */
	/*private void closeOrder(int m_C_OrderLine_ID) throws Exception {
		if (m_C_OrderLine_ID != 0){
			MOrderLine orderLine = new MOrderLine(getCtx(), m_C_OrderLine_ID, trxName);
			MOrder order = new MOrder(getCtx(), orderLine.getC_Order_ID(), trxName);
			if(DocAction.STATUS_Completed.equals(order.getDocStatus())){
				//m_movement.completeIt();
				order.processIt(X_M_Movement.DOCACTION_Complete);
				//m_movement.setProcessed(true);
				order.saveEx();
			}
		}
	}*/
	
	/**
	 * Verifica que exista una Pesada completada relacionada a la Orden de Carga
	 * @author Yamel Senih 19/03/2012, 05:44:06
	 * @throws Exception
	 * @return void
	 */
	private void findWeightRegister() throws Exception{
		String sql = new String("SELECT pp.* " +
				"FROM PP_Order pp " +
				"WHERE pp.XX_LoadOrder_ID = ? " +
				"AND pp.DocStatus IN('CO', 'CL')");
		PreparedStatement pstmt = null;
		pstmt = DB.prepareStatement (sql, trxName);
		int index = 1;
		if (p_XX_LoadOrder_ID != 0)
			pstmt.setInt(index++, p_XX_LoadOrder_ID);
		ResultSet rs = pstmt.executeQuery();
		if(rs.next()){
			m_WeightRegister = new MPPOrder(getCtx(), rs, trxName);
		}
		if (pstmt != null)
			pstmt.close ();
		pstmt = null;
	}
	
	
	
}
