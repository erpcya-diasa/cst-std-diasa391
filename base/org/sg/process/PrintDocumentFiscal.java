/**
 * @finalidad 
 * @author Yamel Senih
 * @date 04/05/2012
 */
package org.sg.process;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.compiere.model.I_C_BPartner_Location;
import org.compiere.model.I_C_Location;
import org.compiere.model.MBPartner;
import org.compiere.model.MCharge;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceBatch;
import org.compiere.model.MInvoiceBatchLine;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MProduct;
import org.compiere.model.MTax;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.fp.util.HandlerSpooler;
import org.sg.model.MXXDocFiscalPrinterNo;
import org.sg.model.MXXFiscalPrinterConfig;

/**
 * @author Yamel Senih 04/05/2012, 17:11:09
 * @author Carlos Parada, cparada@erpcya.com
 * 	<li> FR [ 3 ] Add support to print fiscal document in batch
 * 	@see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/3/proceso-para-imprimir-facturas-en-lote 
 *  <li> FR [ 5 ] Add Support to Batch Print Invoice
 *  @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/5/soporte-para-agregar-lote-a-documento-por
 */
public class PrintDocumentFiscal extends SvrProcess {

	private String 			message;
	
	//private String 		trxName = null;
	private Trx 			trx = null;
	
	private int			p_C_Invoice_ID = 0;
	private int			p_AD_User_ID = 0;
	//FR [ 3 ] 
	private int 		p_C_DocType_ID	= 0;
	private int 		p_C_DocTypeInvoice_ID	= 0;
	private int 		p_SalesRep_ID	= 0;
	private int			p_PrintNo		= 0;
	private Timestamp	p_DateFrom		= null;
	private Timestamp	p_DateTo		= null;
	private Timestamp	p_DateInvoiceFrom = null;
	private Timestamp	p_DateInvoiceTo	= null;
	
	
	//FR [ 5 ]
	private int			p_C_InvoiceBatch_ID = 0;
	private HashMap<Integer, MInvoiceBatch> m_InvoiceSalesRep = new HashMap<Integer, MInvoiceBatch>();
	private HashMap<Integer, Integer> m_InvoiceLineNo = new HashMap<Integer, Integer>();
	private int p_XX_LoadOrder_ID = 0;
	
	//private int			p_XX_LODocGenerated_ID = 0;
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("C_Invoice_ID"))
				p_C_Invoice_ID = para.getParameterAsInt();
			//FR [ 3 ] 
			else if (name.equals("C_DocType_ID"))
				p_C_DocType_ID = para.getParameterAsInt();
			else if (name.equals("C_DocTypeInvoice_ID"))
				p_C_DocTypeInvoice_ID = para.getParameterAsInt();
			else if (name.equals("SalesRep_ID"))
				p_SalesRep_ID = para.getParameterAsInt();
			//FR [ 5 ]
			else if (name.equals("C_InvoiceBatch_ID"))
				p_C_InvoiceBatch_ID = para.getParameterAsInt();
			else if (name.equals("DateOrdered")){
				if (para.getParameter()	!= null)
					p_DateFrom = (Timestamp)para.getParameter();

				if (para.getParameter_To()	!= null)
					p_DateTo = (Timestamp)para.getParameter_To();
			}
			else if (name.equals("DateInvoiced")){
				if (para.getParameter()	!= null)
					p_DateInvoiceFrom = (Timestamp)para.getParameter();

				if (para.getParameter_To()	!= null)
					p_DateInvoiceTo = (Timestamp)para.getParameter_To();
			}
			else if  (name.equals("PrintNo"))
				p_PrintNo = para.getParameterAsInt();
			else if (name.equals("XX_LoadOrder_ID"))
				p_XX_LoadOrder_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		if(p_C_Invoice_ID == 0){
			p_C_Invoice_ID = getRecord_ID();
		}
		p_AD_User_ID = getAD_User_ID();
		//trxName = Trx.createTrxName("PF");
		trx = Trx.get(get_TrxName(), false);
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		try {
			//FR [ 3 ] 
			if (p_C_Invoice_ID != 0)
				printDocument();
			else 
				printDocuments();
			trx.commit();
		} catch (Exception e) {
			message = e.getMessage();
			trx.rollback();
		}
		//FR [ 5 ]
		if (!m_InvoiceSalesRep.isEmpty() && p_C_InvoiceBatch_ID == 0){
			for (Map.Entry<Integer, MInvoiceBatch> iBatchs: m_InvoiceSalesRep.entrySet()){
				MInvoiceBatch iBatch = iBatchs.getValue();
				iBatch.setProcessed(true);
				iBatch.save(get_TrxName());
			}
		}
		return message;
	}

	/**
	 * Print Documents in Batch
	 * //FR [ 3 ] 
	 * @throws Exception
	 */
	private void printDocuments() throws Exception{
		String whereClause = "XX_PrintFiscalDocument='N' AND DocStatus IN ('CO', 'CL') ";
		List<Object> parameters = new ArrayList<Object>();
		//Invoice Document Type 
		if (p_C_DocTypeInvoice_ID!=0){
			whereClause +=" AND C_DocType_ID = ? ";
			parameters.add(p_C_DocTypeInvoice_ID);
		}
		
		//Order Document Type 
		if (p_C_DocType_ID!=0){
			whereClause +=" AND EXISTS (SELECT 1 FROM C_Order o WHERE o.C_DocType_ID = ? AND o.C_Order_ID = C_Invoice.C_Order_ID) ";
			parameters.add(p_C_DocType_ID);
		}
		//Sales Representative 
		if (p_SalesRep_ID!=0){
			whereClause += "AND SalesRep_ID = ? ";
			parameters.add(p_SalesRep_ID);
		}
		
		//Date Ordered From  
		if (p_DateFrom!=null){
			whereClause +="AND DateOrdered >= ? ";
			parameters.add(p_DateFrom);
		}
		
		//Date Ordered To  
		if (p_DateTo!=null){
			whereClause +="AND DateOrdered <= ? ";
			parameters.add(p_DateTo);
		}
		
		//Date Ordered From  
		if (p_DateInvoiceFrom!=null){
			whereClause +="AND DateInvoiced >= ? ";
			parameters.add(p_DateInvoiceFrom);
		}
		
		//Date Ordered To  
		if (p_DateInvoiceTo!=null){
			whereClause +="AND DateInvoiced <= ? ";
			parameters.add(p_DateInvoiceTo);
		}
		
		//Load Order
		if (p_XX_LoadOrder_ID != 0){
			whereClause += "AND XX_LoadOrder_ID = ? ";
			parameters.add(p_XX_LoadOrder_ID);
	    }
		
		List<MInvoice> invoices = new Query(getCtx(), MInvoice.Table_Name, whereClause, get_TrxName())
										.setParameters(parameters)
										.list();
		if (p_PrintNo == 0)
			p_PrintNo = invoices.size();
		
		for (MInvoice mInvoice : invoices) {
			if (p_PrintNo == 0)
				return;
			
			p_C_Invoice_ID = mInvoice.getC_Invoice_ID();
			printDocument();
			p_PrintNo = p_PrintNo -1;
		}
	}

	/**
	 * Imprime el Documento Fiscal
	 * @author Yamel Senih 06/04/2012, 14:31:21
	 * @throws Exception
	 * @return void
	 */
	private void printDocument() throws Exception {
		if(p_C_Invoice_ID != 0){
			MInvoice m_invoice = new MInvoice(getCtx(), p_C_Invoice_ID, get_TrxName());
			if(m_invoice.getDocStatus().equals(DocAction.STATUS_Completed)){
				if(!m_invoice.get_ValueAsBoolean("XX_PrintFiscalDocument")){
					MUser user = new MUser(getCtx(), p_AD_User_ID, get_TrxName());
					int m_XX_FiscalPrinterConfig_ID = user.get_ValueAsInt("XX_FiscalPrinterConfig_ID");
					if(m_XX_FiscalPrinterConfig_ID != 0){
						MDocType m_DocType = new MDocType(getCtx(), m_invoice.getC_DocType_ID(), get_TrxName());
						//	Fiscal Document
						int m_XX_DocFiscalPrinterNo_ID = m_DocType.get_ValueAsInt("XX_DocFiscalPrinterNo_ID");
						if(m_XX_DocFiscalPrinterNo_ID != 0){
							//	Load Fiscal No
							MXXDocFiscalPrinterNo docNoFiscal = new MXXDocFiscalPrinterNo(getCtx(), m_XX_DocFiscalPrinterNo_ID, get_TrxName());
							
							MXXFiscalPrinterConfig m_fpConfig = new MXXFiscalPrinterConfig(getCtx(), m_XX_FiscalPrinterConfig_ID, get_TrxName());
							HandlerSpooler hsp = new HandlerSpooler(m_invoice.getDocumentNo(), m_fpConfig);
							
							MBPartner cp = new MBPartner(getCtx(), m_invoice.getC_BPartner_ID(), get_TrxName());
							
							//	Document Affected
							int m_DA_ID = m_invoice.get_ValueAsInt("XX_DocAffected");
							
							 String nroVoucher = null;
							 String codPrinter = null;
							 String dateVoucher = null;
							 String timeVoucher = null;
							 
							if(m_DA_ID != 0){
								MInvoice m_XX_DocAffected = new MInvoice(getCtx(), m_DA_ID, get_TrxName());
								nroVoucher = m_XX_DocAffected.get_ValueAsString("XX_FiscalNo");
								codPrinter = m_XX_DocAffected.get_ValueAsString("PrinterId");
								SimpleDateFormat sdfDate = new SimpleDateFormat("yyMMdd");
								SimpleDateFormat sdfTime = new SimpleDateFormat("hhmmss");
								Timestamp dV = m_XX_DocAffected.getDateInvoiced();//_ValueAsString("PrinterId");
								dateVoucher = sdfDate.format(dV);
								timeVoucher = sdfTime.format(dV);
								if(nroVoucher == null || nroVoucher.equals(""))
									throw new Exception(Msg.translate(getCtx(), "SGNotDocAff"));	//	No existe Codigo del Documento Afectado
								if(codPrinter == null || codPrinter.equals(""))
									throw new Exception(Msg.translate(getCtx(), "SGNotPrinterIDDocAff"));	//	No existe Codigo de Impresora en el Documento Afectado
							}
							//	Next Sequence
							MXXDocFiscalPrinterNo pdn = new MXXDocFiscalPrinterNo(getCtx(), m_XX_DocFiscalPrinterNo_ID, get_TrxName());
							
							String salesOrder = m_invoice.getC_Order().getDocumentNo();
							String poReference = m_invoice.getC_Order().getPOReference();
							StringBuffer soRefNo = new StringBuffer();
							
							if(salesOrder == null)
								salesOrder = " ";
							
							soRefNo.append("*" + Msg.translate(getCtx(), "FP_SO") + ":" + salesOrder + "*");
							soRefNo.append(" ");
							
							if(poReference == null)
								poReference = " ";
							
							soRefNo.append("*" + Msg.translate(getCtx(), "FP_PORef") + ":" + poReference + "*");	
							
							int top = (soRefNo.toString().length() -1 >= 0? soRefNo.toString().length() -1: 0);
							
							if(top > 45)
								top = 45;
							
							hsp.printTextHeader(soRefNo.toString().substring(0, top));
								
							hsp.printTextHeader("*" + Msg.translate(getCtx(), "FP_RefAdempiere") + ":" + (m_invoice.getDocumentNo() != null? m_invoice.getDocumentNo(): "") + "*");
							
							hsp.printHeader(cp.getName(), cp.getValue(), nroVoucher, codPrinter, dateVoucher, timeVoucher, docNoFiscal.getXX_DocBaseType_Fiscal());
							
							//	get Partner Location
							I_C_BPartner_Location address = m_invoice.getC_BPartner_Location();
							//	get Location
							I_C_Location loc = address.getC_Location();
							//	Concat Location
							StringBuffer nameLoc = new StringBuffer();
							if(address.getName() != null){
								nameLoc.append(address.getName());
								nameLoc.append(", ");	
							}
							
							if(loc.getRegionName() != null){
								nameLoc.append(loc.getRegionName());
								nameLoc.append(", ");	
							}
							
							if(loc.getCity() != null){
								nameLoc.append(loc.getCity());
								nameLoc.append(", ");
							}
							
							
							StringBuffer nameLoc1 = new StringBuffer();
							if(loc.getAddress1() != null){
								nameLoc1.append(loc.getAddress1());
								nameLoc1.append(", ");	
							}
							
							if(loc.getAddress3() != null){
								nameLoc1.append(loc.getAddress2());
								nameLoc1.append(", ");
							}
							
							StringBuffer nameLoc2 = new StringBuffer();
							if(loc.getAddress3() != null){
								nameLoc2.append(loc.getAddress3());
								nameLoc2.append(", ");
							}
							
							if(loc.getAddress4() != null){
								nameLoc2.append(loc.getAddress4());
							}
							
							StringBuffer soPtSr = new StringBuffer();
							String paymentTerm = m_invoice.getC_PaymentTerm().getName();
							String salesRep = m_invoice.getSalesRep().getName();
							
							if(paymentTerm != null){
								soPtSr.append("*" + Msg.translate(getCtx(), "FP_PT") + ":" + paymentTerm + "*");
								soPtSr.append(" ");	
							}
							
							if(salesRep != null){
								soPtSr.append("*" + Msg.translate(getCtx(), "FP_SR") + ":" + salesRep + "*");
							}
							
							top = (soPtSr.toString().length() -1 >= 0? soPtSr.toString().length() -1: 0);
							
							if(top > 41)
								top = 41;
							hsp.printMessage(soPtSr.toString().substring(0, top) + " ");
							
							top = (nameLoc.toString().length() -1 >= 0? nameLoc.toString().length() -1: 0);
							
							if(top > 41)
								top = 41;
							hsp.printMessage(Msg.translate(getCtx(), "FP_Address") + ":" + nameLoc.toString().substring(0, top));
							
							top = (nameLoc1.toString().length() -1 >= 0? nameLoc1.toString().length() -1: 0);
							
							if(top > 41)
								top = 41;
							
							hsp.printMessage(nameLoc1.toString().substring(0, top) + " ");
							top = (nameLoc2.toString().length() -1 >= 0? nameLoc2.toString().length() -1: 0);
							
							if(top > 41)
								top = 41;
							
							hsp.printMessage(nameLoc2.toString().substring(0, top) + " ");
							
							MInvoiceLine [] lInvoices = m_invoice.getLines();
							for (MInvoiceLine m_InvoiceLine : lInvoices) {
								String nameItem = null;
								String valueItem = null;
								if(m_InvoiceLine.getM_Product_ID() != 0){
									MProduct product = new MProduct(getCtx(), m_InvoiceLine.getM_Product_ID(), get_TrxName());
									nameItem = product.getName();
									valueItem = product.getValue();	
								} else if(m_InvoiceLine.getC_Charge_ID() != 0){
									MCharge charge = new MCharge(getCtx(), m_InvoiceLine.getC_Charge_ID(), get_TrxName());
									nameItem = charge.getName();
									valueItem = charge.getName().substring(0, 3);
								} else
									continue;
									//throw new Exception(Msg.translate(getCtx(), "SGNotItem"));	//	No existe Item
								
								if(m_InvoiceLine.getQtyInvoiced().doubleValue() != 0
										&& m_InvoiceLine.getPriceEntered().doubleValue() != 0){
								
									MTax tax = new MTax(getCtx(), m_InvoiceLine.getC_Tax_ID(), get_TrxName());
									hsp.printLine(nameItem, 
											m_InvoiceLine.getQtyInvoiced().doubleValue(), 
											m_InvoiceLine.getPriceEntered().doubleValue(), 
											tax.getRate().doubleValue(), 
											null, 
											valueItem);
								}
							}
							
							String desc = new String();
							
							//	Print Description
							if(m_invoice.getDescription() != null
									&& m_invoice.getDescription().length() > 0){
								desc = m_invoice.getDescription();
							}
							
							top = (desc.length() -1 >= 0? desc.length() -1: 0);
							if(top > 45)
								top = 45;
							hsp.printTextTrailer(desc.substring(0, top).trim() + " ");
							
							hsp.printCommand("E");
							//	Close Spooler
							hsp.printStackCmd();
							//	Printer ID
							m_invoice.set_ValueOfColumn("PrinterId", m_fpConfig.getPrinterId());
							
							//	Next Sequence
							m_invoice.set_ValueOfColumn("XX_FiscalNo", pdn.nextSequence());
							m_invoice.set_ValueOfColumn("XX_PrintFiscalDocument", "Y");
							m_invoice.saveEx();
							pdn.saveEx();
							//FR [ 5 ]
							//Batch Document Invoice
							if (!isAddtoBatch(m_invoice)){
								int C_SalesRep_ID = m_invoice.getSalesRep_ID();
								MInvoiceBatch iBatch = null;
								if (p_C_InvoiceBatch_ID!=0)
									iBatch = new MInvoiceBatch(getCtx(), p_C_InvoiceBatch_ID, get_TrxName());
								else
									iBatch= m_InvoiceSalesRep.get(C_SalesRep_ID);
								
								if (iBatch == null){
									iBatch = new MInvoiceBatch(getCtx(), 0, get_TrxName());
									iBatch.setDateDoc(m_invoice.getDateInvoiced());
									iBatch.setIsSOTrx(m_invoice.isSOTrx());
									iBatch.setSalesRep_ID(C_SalesRep_ID);
									iBatch.setC_Currency_ID(m_invoice.getC_Currency_ID());
									m_InvoiceLineNo.put(C_SalesRep_ID, 10);
									m_InvoiceSalesRep.put(C_SalesRep_ID, iBatch);
								}
								//Set Batch Amounts
								iBatch.setControlAmt(iBatch.getControlAmt().add(m_invoice.getGrandTotal()));
								iBatch.setDocumentAmt(iBatch.getControlAmt());
							
								if (iBatch.save(get_TrxName())){
									int lineNo =  m_InvoiceLineNo.get(C_SalesRep_ID);
									MInvoiceBatchLine ilBatch = new MInvoiceBatchLine(getCtx(), 0, get_TrxName());
									ilBatch.setC_InvoiceBatch_ID(iBatch.getC_InvoiceBatch_ID());
									ilBatch.setLine(lineNo);
									ilBatch.setC_DocType_ID(m_invoice.getC_DocType_ID());
									ilBatch.setDocumentNo(m_invoice.getDocumentNo());
									ilBatch.setDescription(m_invoice.getDescription());
									ilBatch.setDateInvoiced(m_invoice.getDateInvoiced());
									ilBatch.setDateAcct(m_invoice.getDateAcct());
									ilBatch.setC_BPartner_ID(m_invoice.getC_BPartner_ID());
									ilBatch.setC_BPartner_Location_ID(m_invoice.getC_BPartner_Location_ID());
									ilBatch.setAD_User_ID(m_invoice.getAD_User_ID());
									ilBatch.setPriceEntered(m_invoice.getTotalLines());
									ilBatch.setLineNetAmt(m_invoice.getTotalLines());
									ilBatch.setLineTotalAmt(m_invoice.getGrandTotal());
									ilBatch.setTaxAmt(m_invoice.getGrandTotal().subtract(m_invoice.getTotalLines()));
									ilBatch.setC_Invoice_ID(m_invoice.getC_Invoice_ID());
									
									if (p_C_InvoiceBatch_ID!=0)
										ilBatch.setProcessed(true);
									
									if (ilBatch.save(get_TrxName()))
										m_InvoiceLineNo.put(C_SalesRep_ID, lineNo + 10);
									else
										throw new Exception(Msg.translate(getCtx(), "@SaveError@ @C_InvoiceBatchLine_ID@"));
									
								}else	
									throw new Exception(Msg.translate(getCtx(), "@SaveError@ @C_InvoiceBatch_ID@"));
							}
							message = "OK";
						} else
							throw new Exception(Msg.translate(getCtx(), "SGNotConfDocType"));	//	Secuencia Fiscal No Configurada en el Tipo de Documento
					} else
						throw new Exception(Msg.translate(getCtx(), "SGNotConfFiscalPrinter"));	//	Impresora Fiscal No Configurada
				} else
					throw new Exception(Msg.translate(getCtx(), "SGSentDoc"));	//	El Documento ya fue enviado a la Impresora Fiscal
			} else
				throw new Exception(Msg.translate(getCtx(), "SGNotCompletedDoc"));	//	Documento no Completado
		} else
			throw new Exception(Msg.translate(getCtx(), "SGDocNotFound"));	//	Documento no Encontrado
	}
	
	private boolean isAddtoBatch(MInvoice inv){
		return new Query(getCtx(), MInvoiceBatchLine.Table_Name, "C_Invoice_ID = ?", get_TrxName()).setParameters(inv.getC_Invoice_ID()).match();
	}
}
