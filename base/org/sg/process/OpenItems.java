/**
 * @finalidad 
 * @author Yamel Senih
 * @date 31/10/2011
 */
package org.sg.process;

import java.sql.PreparedStatement;
import java.sql.Timestamp;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Msg;

/**
 * @author Yamel Senih 31/10/2011, 15:02:45
 *
 */
public class OpenItems extends SvrProcess {

	/**	Organization			*/
	private int 	p_AD_Org_ID = 0;
	
	/**	Document Type Invoice	*/
	private int 	p_C_DocType_ID = 0;
	
	/**	Business Partner Group	*/
	private int		p_C_BP_Group_ID = 0;
	/**	Business Partner		*/
	private int 	p_C_BPartner_ID = 0;
	
	/**	Order					*/
	private int		p_C_Order_ID = 0;
	/**	Invoice					*/
	private int		p_C_Invoice_ID = 0;
	/**	Receipt					*/
	private int		p_M_InOut_ID = 0;

	/**	Activity				*/
	private int		p_C_Activity_ID = 0;
	/**	Date Invoiced 			*/
	private Timestamp	p_DateInvoiced = null;
	
	/**	Is Sales Transaction	*/
	private boolean		p_IsSOTrx = false;
	
	/**	Days Due				*/
	private int 		p_DaysDue = 0;
	
	/**	Document Status			*/
	private String		p_DocStatus = null;
	
	/**	Order Status			*/
	private String		p_StatusOrder = null;
	
	/**	Receipt Status			*/
	private String		p_StatusReceipt = null;
	
	/*
	 * Modificado por Yamel Senih 16/05/2012 18:35
	 * Se agrega el parametro regla de pago
	 */
	
	private String		p_PaymentRule = null;
	/**	Payment Term			*/
	private int				p_C_PaymentTerm_ID = 0;
	/**	Payment Term To			*/
	private int				p_C_PaymentTerm_To_ID = 0;	
	/**	Parameter Where Clause	*/
	private StringBuffer		m_parameterWhere = new StringBuffer();
	
	/**	Start Time				*/
	private long 				m_start = System.currentTimeMillis();
	
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("DateInvoiced"))
				p_DateInvoiced = (Timestamp)para.getParameter_To();
			else if (name.equals("AD_Org_ID"))
				p_AD_Org_ID = para.getParameterAsInt();
			else if (name.equals("C_DocType_ID"))
				p_C_DocType_ID = para.getParameterAsInt();
			else if (name.equals("C_Order_ID"))
				p_C_Order_ID = para.getParameterAsInt();
			else if (name.equals("C_Invoice_ID"))
				p_C_Invoice_ID = para.getParameterAsInt();
			else if (name.equals("M_InOut_ID"))
				p_M_InOut_ID = para.getParameterAsInt();
			else if (name.equals("C_BP_Group_ID"))
				p_C_BP_Group_ID = para.getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para.getParameterAsInt();
			else if (name.equals("IsSOTrx"))
				p_IsSOTrx = para.getParameterAsBoolean();
			else if (name.equals("DaysDue"))
				p_DaysDue = para.getParameterAsInt();
			else if (name.equals("StatusOrder"))
				p_StatusOrder = (String) para.getParameter();
			else if (name.equals("DocStatus"))
				p_DocStatus = (String) para.getParameter();
			else if (name.equals("StatusReceipt"))
				p_StatusReceipt = (String) para.getParameter();
			else if (name.equals("PaymentRule"))
				p_PaymentRule = (String) para.getParameter();
			else if (name.equals("C_Activity_ID"))
				p_C_Activity_ID = para.getParameterAsInt();
			else if (name.equals("C_PaymentTerm_ID")){
				p_C_PaymentTerm_ID = para.getParameterAsInt();
				p_C_PaymentTerm_To_ID = para.getParameter_ToAsInt();
			}
			/*else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);*/
		}
		
		//	Optional Org
		if (p_AD_Org_ID != 0)
			m_parameterWhere.append(" AND fac.AD_Org_ID = ? ");
		//	Document Type
		if(p_C_DocType_ID != 0)
			m_parameterWhere.append(" AND fac.C_DocType_ID = ? ");
		//	Order
		if(p_C_Order_ID != 0)
			m_parameterWhere.append(" AND ord.C_Order_ID = ? ");
		/*
		 * Se Agrega para Filtro por Regla de Pago
		 */
		//	Payment Rule
		if(p_PaymentRule != null)
			m_parameterWhere.append(" AND fac.PaymentRule = ? ");
		/*
		 * Se Agrega para Filtro por Actividad
		 */
		//	Actividad
		if(p_C_Activity_ID != 0)
			m_parameterWhere.append(" AND fac.C_Activity_ID = ? ");

		/*
		 * Se Agrega para Filtro por Termino de Pago
		 */
		//	Termino de Pago
		/*if(p_C_PaymentTerm_ID != 0)
			m_parameterWhere.append(" AND fac.C_PaymentTerm_ID = ? ");*/
		
		
		//	Invoice
		if(p_C_Invoice_ID != 0)
			m_parameterWhere.append(" AND fac.C_Invoice_ID = ? ");
		//	Receipt
		if(p_M_InOut_ID != 0)
			m_parameterWhere.append(" AND desp.M_InOut_ID = ? ");
		//	Business Partner Group
		if(p_C_BP_Group_ID != 0)
			m_parameterWhere.append(" AND cp.C_BP_Group_ID = ? ");
		//	Business Partner
		if(p_C_BPartner_ID != 0)
			m_parameterWhere.append(" AND cp.C_BPartner_ID = ? ");
		//	Status Order
		if(p_StatusOrder != null)
			m_parameterWhere.append(" AND ord.StatusOrder = ? ");
		//	Status Invoice
		/*if(p_DocStatus != null)
			m_parameterWhere.append(" AND fac.DocStatus = ? ");*/
		//	Status Receipt
		if(p_StatusReceipt != null)
			m_parameterWhere.append(" AND desp.StatusReceipt = ? ");
		
	}	//	prepare


	@Override
	protected String doIt(){
		
		StringBuffer sql = new StringBuffer();
		
		//	Insert
		sql.append("INSERT INTO T_XX_OpenItem(C_BP_Group_ID, " +
				"C_BPartner_ID, " +
				"Name, " +
				"C_Order_ID, " +
				"DocOrder, " +
				"DateOrdered, " +
				"OrderAmt, " +
				//	Parametro PaymentRule
				"PaymentRule, " +
				//	Parametro C_Activity_ID
				"C_Activity_ID, " +
				//	Parametro Termino de PAgo
				"C_PaymentTerm_ID, " +
				"C_DocType_ID, " +
				"C_Invoice_ID, " +
				"DocumentNo, " +
				"DateInvoiced, " +
				"DateAcct, " +
				"InvoiceAmt, " +
				"M_InOut_ID, " +
				"Receipt, " +
				"MovementDate, " +
				"NetDays, " +
				"DueDate, " +
				"DaysDue, " +
				"F_P, " +
				"PayAmt, " +
				"PayDate, " +
				"IsSOTrx, " +
				"AD_Client_ID, " +
				"AD_Org_ID, " +
				"AD_PInstance_ID) ");
		//	Business Par"InvoiceAmt," +tner
		sql.append("SELECT cp.C_BP_Group_ID, " +
				"cp.C_BPartner_ID," +
				"cp.Name, " +
				"ord.C_Order_ID, " +
				"ord.DocumentNo DocOrder, " +
				"ord.DateOrdered, " +
				"ord.GrandTotal OrderAmt, " +
				//	Parametro PaymentRule
				"fac.PaymentRule, " +
				//	Parametro C_Activity_ID
				"fac.C_Activity_ID, " +
				//	Parametro Termino de Pago
				"fac.C_PaymentTerm_ID, " +
				"fac.C_DocType_ID, " +
				"fac.C_Invoice_ID, " +
				"fac.DocumentNo, " +
				"fac.DateInvoiced, " +
				"fac.DateAcct, ");
		//	Invoice Amt
		sql.append("CASE " +
				"WHEN Doc.DocBaseType IN('ARC', 'APC') " +
				"	THEN fac.GrandTotal * -1 " +
				"	ELSE fac.GrandTotal " +
				"END InvoiceAmt,");
		//	Receipts
		/*sql.append("desp.M_InOut_ID, " +
				"desp.DocumentNo Receipt, " +
				"desp.MovementDate, " +*/
		sql.append("null, " +
				"null Receipt, " +
				"null, " +
				"cpa.NetDays, " +
				"fac.DateInvoiced + cpa.NetDays as DueDate," +
				"(? - (fac.DateInvoiced + cpa.NetDays)) DaysDue, ");
		//	Total Open
		sql.append("ROUND(" +
				"CASE " +
				"	WHEN Doc.DocBaseType IN('ARC', 'APC') " +
				"	THEN fac.GrandTotal * -1 " +
				"	ELSE fac.GrandTotal	" +
				"END " +
				" + " +
				"SUM(" +
				"	coalesce(" +
				"			CASE " +
				"				WHEN apa.DateAcct <= ? AND apa.DocStatus IN ('CO', 'CL') " +
				"				THEN lpa.Amount " +
				"				ELSE 0 " +
				"			END, 0)" +
				" + " +
				"	coalesce(" +
				"			CASE " +
				"				WHEN apa.DateAcct <= ? AND apa.DocStatus IN ('CO', 'CL') " +
				"				THEN lpa.DiscountAmt " +
				"				ELSE 0 " +
				"			END, 0)" +
				"	) * " +
				"	(CASE " +
				"		WHEN fac.IsSOTrx = 'Y' " +
				"		THEN -1 " +
				"		ELSE 1 " +
				"	END), " +
				"2) F_P,");
		//	Payment Amount
		sql.append("ROUND(" +
				"SUM(" +
				"	coalesce(" +
				"			CASE " +
				"				WHEN apa.DateAcct <= ? AND apa.DocStatus IN ('CO', 'CL') " +
				"				THEN lpa.Amount	" +
				"				ELSE 0 " +
				"			END, 0)" +
				" + " +
				"	coalesce(" +
				"			CASE " +
				"				WHEN apa.DateAcct <= ? AND apa.DocStatus IN ('CO', 'CL') " +
				"				THEN lpa.DiscountAmt " +
				"				ELSE 0 " +
				"			END, 0)" +
				"	), 2) PayAmt,");
		//	Payment Date
		sql.append("MAX(apa.DateAcct) PayDate, fac.IsSOTrx, fac.AD_Client_ID, ");
		//	Client Data
		sql.append("fac.AD_Org_ID, ");
		//	Process Instance
		sql.append("? ");
		//	Tables
		sql.append("FROM C_Invoice fac " +
				"INNER JOIN C_BPartner cp ON(cp.C_BPartner_ID = fac.C_BPartner_ID) " +
				"INNER JOIN C_BPartner_Location lcp ON(fac.C_BPartner_Location_ID = lcp.C_BPartner_Location_ID) " +
				"INNER JOIN C_Location loc ON (lcp.C_Location_ID = loc.C_Location_ID) " +
				"INNER JOIN M_PriceList lsp ON(lsp.M_PriceList_ID = fac.M_PriceList_ID) " +
				"INNER JOIN C_PaymentTerm cpa ON(cpa.C_PaymentTerm_ID = fac.C_PaymentTerm_ID) " +
				"INNER JOIN C_DocType doc ON(doc.C_DocType_ID = fac.C_DocType_ID) " +
				"LEFT JOIN C_AllocationLine lpa ON (lpa.C_Invoice_ID = fac.C_Invoice_ID) " +
				"LEFT JOIN C_AllocationHDR apa ON (apa.C_AllocationHDR_ID = lpa.C_AllocationHDR_ID) " +
				"LEFT JOIN C_Order ord ON(ord.C_Order_ID = fac.C_Order_ID) ");
				//"LEFT JOIN M_InOut desp ON(desp.C_Order_ID = ord.C_Order_ID) ");
		//	Where
		sql.append("WHERE  fac.DocStatus IN ('CO', 'CL') " +
				//"AND (apa.DocStatus IN ('CO', 'CL') OR apa.DocStatus IS NULL) " +
				"AND fac.IspayScheduleValid <> 'Y' " +
				"AND fac.DateInvoiced <= ? " +
				"AND fac.IsSOTrx = ? " +
				"AND fac.AD_Client_ID = " + getAD_Client_ID() + " ");
		//	Clause Where Add
		sql.append(m_parameterWhere);
		//	Group By
		sql.append("GROUP BY " +
				"cp.C_BP_Group_ID, " +
				"cp.C_BPartner_ID, " +
				"cp.Name, " +
				"ord.C_Order_ID, " +
				"ord.DocumentNo, " +
				"ord.DateOrdered, " +
				"ord.GrandTotal, " +
				"fac.C_Invoice_ID, " +
				//	Parametro Regla de Pago
				"fac.PaymentRule, " +
				//	Parametro C_Activity_ID
				"fac.C_Activity_ID, " +
				"fac.DocumentNo, " +
				"fac.DateInvoiced, " +
				"fac.DateAcct, " +
				"Doc.DocBaseType, " +
				"fac.GrandTotal, " +
				//"desp.M_InOut_ID, " +
				//"desp.DocumentNo, " +
				//"desp.MovementDate, " +
				"cpa.NetDays, " +
				"fac.AD_Client_ID, " +
				"fac.AD_Org_ID, " +
				"fac.C_DocType_ID," +
				"fac.IsSOTrx");
		
		log.fine("SQL = " + sql.toString());
		//System.err.println(sql);
		int i = 1;
		int noInserts = 0;
		try{
			PreparedStatement pstmt = DB.prepareStatement (sql.toString(), null);
			//	Date Invoiced
			pstmt.setTimestamp(i++, p_DateInvoiced);
			pstmt.setTimestamp(i++, p_DateInvoiced);
			pstmt.setTimestamp(i++, p_DateInvoiced);
			pstmt.setTimestamp(i++, p_DateInvoiced);
			pstmt.setTimestamp(i++, p_DateInvoiced);
			//	Process Instance
			pstmt.setInt(i++, getAD_PInstance_ID());
			
			pstmt.setTimestamp(i++, p_DateInvoiced);
					
			//	Is Sales Transaction
			pstmt.setString(i++, (p_IsSOTrx? "Y": "N"));
			
			//	Optional Org
			if (p_AD_Org_ID != 0)
				pstmt.setInt(i++, p_AD_Org_ID);
			//	Document Type
			if(p_C_DocType_ID != 0)
				pstmt.setInt(i++, p_C_DocType_ID);
			//	Order
			if(p_C_Order_ID != 0)
				pstmt.setInt(i++, p_C_Order_ID);
			/*
			 * Se agrega el parametro Regla de Pago
			 */
			//	Payment Rule
			if(p_PaymentRule != null)
				pstmt.setString(i++, p_PaymentRule);
			/*
			 * Se agrega el parametro Actividad
			 */
			//	Activity
			if(p_C_Activity_ID != 0)
				pstmt.setInt(i++, p_C_Activity_ID);
			/*
			 * Se agrega el parametro Termino de Pago
			 */
			/*
			if(p_C_PaymentTerm_ID != 0)
				pstmt.setInt(i++, p_C_PaymentTerm_ID);
			*/
			//	Invoice
			if(p_C_Invoice_ID != 0)
				pstmt.setInt(i++, p_C_Invoice_ID);
			//	Receipt
			if(p_M_InOut_ID != 0)
				pstmt.setInt(i++, p_M_InOut_ID);
			//	Business Partner Group
			if(p_C_BP_Group_ID != 0)
				pstmt.setInt(i++, p_C_BP_Group_ID);
			//	Business Partner
			if(p_C_BPartner_ID != 0)
				pstmt.setInt(i++, p_C_BPartner_ID);
			if(p_StatusOrder != null)
				pstmt.setString(i++, p_StatusOrder);
			//	Status Invoice
			/*if(p_DocStatus != null)
				pstmt.setString(i++, p_DocStatus);*/
			//	Status Receipt
			if(p_StatusReceipt != null)
				pstmt.setString(i++, p_StatusReceipt);
			
			noInserts = pstmt.executeUpdate();
			
			pstmt.close();

		} catch(Exception e){
			throw new AdempiereException(Msg.translate(getCtx(), "SGBDError") + ":" + e.getMessage());
		}
		
		log.fine((System.currentTimeMillis() - m_start) + " ms");
		
		return "@Created@ = " + noInserts;
	}	//	doIt

}
