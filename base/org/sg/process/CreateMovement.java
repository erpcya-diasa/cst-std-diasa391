/**
 * @finalidad 
 * @author Yamel Senih
 * @date 24/10/2011
 */
package org.sg.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MProduct;
import org.compiere.model.MUOMConversion;
import org.compiere.model.X_C_Order;
import org.compiere.model.X_M_Movement;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.eevolution.model.MPPOrder;
import org.eevolution.model.X_PP_Order;

/**
 * @author Yamel Senih 24/10/2011, 09:56:57
 *
 */
public class CreateMovement extends SvrProcess {

	/**	Locator From		*/
	private int			m_M_LocatorFrom_ID = 0;
	/**	Locator				*/
	private int			m_M_LocatorTo_ID = 0;
	/**	Doc Date From		*/
	private Timestamp	p_DateDoc;
	/**	Doc Date From		*/
	private Timestamp	dateDoc;
	/** BPartner			*/
	private int 		p_C_BPartner_ID = 0;
	
	/**	Tipo de Documento Pesada	*/
	private int 		p_C_DocType_ID = 0;
	
	/** Movement			*/
	private MMovement	m_movement = null;
	
	/** Quality Order		*/
	private MPPOrder	qOrder = null;
	
	/** Tipo de Documento de Movimiento*/
	private int			p_C_DocTypeMovement_ID = 0;
	
	/**	Created				*/
	private int 		m_created_Movement = 0;
	
	/**	Annulated			*/
	private int			m_annulated_Movement = 0;
	
	/**	Movimientos ya generados*/
	private int 		m_Mov = 0;
	
	/**	Errores					*/
	private int 		m_Errors = 0;
	
	/**	Actividad			*/
	private int 		p_C_Activity_ID = 0;
	
	/**	Pesada de Romana	*/
	private int			p_PP_Order_ID = 0;
	
	/**	UOM					*/
	private int			p_C_UOM_ID = 0;
	/**	UOM To				*/
	private int			p_C_UOM_To_ID = 0;
	
	/**	Movimiento registrado*/
	private int			m_M_Movement_ID = 0;
	
	/**	Summary				*/
	private StringBuffer summary = new StringBuffer();
	
	private String 		message;
	
	private String 		trxName = null;
	private Trx 		trx = null;
	
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("PP_Order_ID"))
				p_PP_Order_ID = para.getParameterAsInt();
			else if (name.equals("DateDoc"))
				p_DateDoc = (Timestamp)para.getParameter();
			else if (name.equals("C_DocType_ID"))
				p_C_DocType_ID = para.getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para.getParameterAsInt();
			else if (name.equals("C_DocTypeMovement_ID"))
				p_C_DocTypeMovement_ID = para.getParameterAsInt();
			else if (name.equals("C_Activity_ID"))
				p_C_Activity_ID = para.getParameterAsInt();
			else if (name.equals("C_UOM_ID"))
				p_C_UOM_ID = para.getParameterAsInt();
			else if (name.equals("C_UOM_To_ID"))
				p_C_UOM_To_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		trxName = Trx.createTrxName("GM");
		trx = Trx.get(trxName, true);
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt() throws Exception {
		ResultSet rs = null;
		
		String sql = "SELECT * FROM PP_Order o "
			+ "WHERE DocStatus IN('CO', 'CL', 'VO') "
			+ "AND XX_TipoPesada = 'PM' "
			+ "AND AD_Client_ID=? "
			+ "AND EXISTS(SELECT pc.* "
			+ "FROM C_Period p "
			+ "INNER JOIN C_PeriodControl pc ON(pc.C_Period_ID = p.C_Period_ID) "
			+ "WHERE pc.DocBaseType IN('MQO', 'MMM') "
			+ "AND pc.PeriodStatus = 'O' "
			+ "AND p.StartDate <= DateOrdered AND p.EndDate >= DateOrdered) ";
		if (p_PP_Order_ID != 0)
			sql += " AND PP_Order_ID=?";
		if (p_C_BPartner_ID != 0)
			sql += " AND C_BPartner_ID=?";
		if (p_C_DocType_ID != 0)
			sql += " AND C_DocType_ID=?";
		
		
		log.info(sql.toString());
		
		//System.err.println(sql + " " + " " + p_C_DocType_ID);
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, get_TrxName());
			int index = 1;
			
			pstmt.setInt(index++, getAD_Client_ID());
			
			if (p_PP_Order_ID != 0)
				pstmt.setInt(index++, p_PP_Order_ID);
			if (p_C_BPartner_ID != 0)
				pstmt.setInt(index++, p_C_BPartner_ID);
			if (p_C_DocType_ID != 0)
				pstmt.setInt(index++, p_C_DocType_ID);
			
			rs = pstmt.executeQuery();
			
			crearMovimientos(rs);
			
			trx.commit();
		}catch (Exception e)
		{
			trx.rollback();
			log.log(Level.SEVERE, sql, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		return message;
	}
	
	/**
	 * Consulta los Movimientos realizados
	 * @author Yamel Senih 24/10/2011, 20:00:00
	 * @param 
	 * @return
	 */
	private void consultaRealizados(){
		String sql = new String("SELECT (SELECT COUNT(*) " +
				"FROM M_Movement m " +
				"WHERE m.AD_Client_ID = " + getAD_Client_ID() +
				" AND m.XX_Boleta_ID = ?) Movements, " +
				"(SELECT max(m.M_Movement_ID) " +
				"FROM M_Movement m " +
				"WHERE m.AD_Client_ID = " + getAD_Client_ID() +
				" AND m.XX_Boleta_ID = ?) M_Movement_ID");
		
		log.fine("consultaRealizados SQL = " + sql);
		
		//System.err.println("SQL " + sql);
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			int cont = 1;
			pstmt = DB.prepareStatement (sql, get_TrxName());
			
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				m_Mov = rs.getInt("Movements");
				m_M_Movement_ID = rs.getInt("M_Movement_ID");
			}
		} catch (SQLException e) {
			m_Errors ++;
			throw new AdempiereException(Msg.translate(getCtx(), "SGBDError") + ":" + e.getMessage());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
	}
	
	/**
	 * Crea los Movimientos de Inventario
	 * @author Yamel Senih 24/10/2011, 10:15:57
	 * @param rs
	 * @return
	 * @throws SQLException
	 * @return String
	 */
	private void crearMovimientos(ResultSet rs) throws Exception {
		
		while (rs.next ()){
			System.out.println("Pesada " + rs.getString("DocumentNo"));
			qOrder = new MPPOrder (getCtx(), rs, trxName);
			
			//	Consulta Movimientos
			consultaRealizados();
			
			if(m_Mov == 0 
					&& X_PP_Order.DOCSTATUS_Completed.equals(qOrder.getDocStatus())){
				if(p_DateDoc == null){
					dateDoc = qOrder.getDateOrdered();
				} else {
					dateDoc = p_DateDoc;
				}
				crearEncabezado();
				crearLineas();
				completeMovement();
			} else if(m_Mov == 1
					&& m_M_Movement_ID != 0
					&& X_PP_Order.DOCSTATUS_Voided.equals(qOrder.getDocStatus())){

				m_movement = new MMovement(getCtx(), m_M_Movement_ID, trxName);
				m_movement.reverseCorrectIt();
				
				m_movement.saveEx();
				m_annulated_Movement ++;
				addLog(m_movement.getM_Movement_ID(), m_movement.getDateReceived(), null, m_movement.getDocumentNo());	
			}
			
		}
		
		summary.append("@Created@ = ");
		summary.append(m_created_Movement);
		summary.append(" @Voided@ = ");
		summary.append(m_annulated_Movement);
		summary.append(" Errors = ");
		summary.append(m_Errors);
		
		addLog(summary.toString());
		
		message =  summary.toString();
	}	//	crearMovimientos

	/**
	 * Crea el encabezado de los Movimientos
	 * @author Yamel Senih 24/10/2011, 12:12:20
	 * @return void
	 */
	private void crearEncabezado() throws Exception {
		m_M_LocatorFrom_ID = qOrder.get_ValueAsInt("XXM_LocatorFrom_ID");
		if(m_M_LocatorFrom_ID == 0){
			m_Errors ++;
			message = Msg.translate(getCtx(), "XX_Boleta_ID") 
					+ " : " + qOrder.getDocumentNo() + " " + Msg.translate(getCtx(), "SGLocatorFromNot");
			throw new AdempiereException(message);
		}
		m_M_LocatorTo_ID = qOrder.get_ValueAsInt("M_Locator_ID");
		if(m_M_LocatorTo_ID == 0){
			m_Errors ++;
			message = Msg.translate(getCtx(), "XX_Boleta_ID") 
					+ " : " + qOrder.getDocumentNo() + " " + Msg.translate(getCtx(), "SGLocatorToNot");
			throw new AdempiereException(message);
		}
		
		if(m_M_LocatorFrom_ID == m_M_LocatorTo_ID){
			m_Errors++;
			message = Msg.translate(getCtx(), "XX_Boleta_ID") 
					+ " : " + qOrder.getDocumentNo() + " " + Msg.translate(getCtx(), "SGLocatorsWheight");
			throw new AdempiereException(message);
		}
		
		m_movement = new MMovement(getCtx(), 0, trxName);
		m_movement.setAD_Org_ID(qOrder.getAD_Org_ID());
		m_movement.setDateReceived(dateDoc);
		m_movement.setMovementDate(dateDoc);
		m_movement.setDescription(qOrder.getDocumentNo());
		/*	ID de La Boleta de Romana	*/
		m_movement.set_ValueOfColumn("XX_Boleta_ID", qOrder.getPP_Order_ID());
		if(p_C_DocTypeMovement_ID != 0)
			m_movement.setC_DocType_ID(p_C_DocTypeMovement_ID);
		m_movement.setC_Activity_ID(p_C_Activity_ID);
		
	}	//	crearEncabezado
	
	/**
	 * Crea las Lineas de los Movimientos
	 * @author Yamel Senih 24/10/2011, 12:13:45
	 * @return void
	 */
	private void crearLineas() throws Exception {
		
		System.out.println("Movement " + m_movement);
		
		m_movement.saveEx();	
		MProduct product = qOrder.getM_Product();
		
		/*	Conversion	*/
		BigDecimal rConversion = MUOMConversion.convert(p_C_UOM_ID, p_C_UOM_To_ID, new BigDecimal(1), false);//	MUOMConversion.getRate(p_C_UOM_ID, p_C_UOM_To_ID);
		
		
		BigDecimal pNeto = (BigDecimal)qOrder.get_Value("Weight3");
		
		MMovementLine m_movementLine = new MMovementLine(getCtx(), 0, trxName);
		m_movementLine.setAD_Org_ID(m_movement.getAD_Org_ID());
		m_movementLine.setM_Movement_ID(m_movement.getM_Movement_ID());
		
		/*	Producto	*/
		m_movementLine.setM_Product_ID(product.getM_Product_ID());
		/*	Atributos	*/
		m_movementLine.setM_AttributeSetInstance_ID(qOrder.getM_AttributeSetInstance_ID());
		m_movementLine.setM_AttributeSetInstanceTo_ID(qOrder.getM_AttributeSetInstance_ID());
		/*	Cantidad	*/
		m_movementLine.setMovementQty(pNeto.multiply(rConversion));
		/*	Unidad de Medida	*/
		/*	Localización desde Hasta	*/
		m_movementLine.setM_Locator_ID(m_M_LocatorFrom_ID);
		m_movementLine.setM_LocatorTo_ID(m_M_LocatorTo_ID);
		
		m_movementLine.setDescription(qOrder.getDocumentNo());
		//	Close Order
		m_movementLine.saveEx();
		closeQOrder();
		qOrder.saveEx();
			
	}
	
	/**
	 * Completa el Movimiento
	 * @author Yamel Senih 24/10/2011, 17:10:10
	 * @return void
	 */
	private void completeMovement() throws Exception {
		if (m_movement != null){
			/*m_movement.completeIt();
			m_movement.setDocStatus(X_M_Movement.DOCSTATUS_Completed);
			m_movement.setDocAction(X_M_Movement.DOCACTION_Close);*/
			//m_movement.setProcessed(true);
			m_movement.processIt(X_M_Movement.DOCACTION_Complete);
			m_movement.saveEx();
			m_movement.saveEx();
			//
			addLog(m_movement.getM_Movement_ID(), m_movement.getDateReceived(), null, m_movement.getDocumentNo());
			m_created_Movement++;
		}
	}

	/*
	 * Close Quality Order
	 */
	private void closeQOrder() throws Exception {
		if(qOrder != null){
			qOrder.setDocStatus(X_C_Order.DOCSTATUS_Closed);
			qOrder.setDocAction(X_C_Order.DOCACTION_None);
			//qOrder.setProcessed(true);
			qOrder.saveEx();
		}
	}	//	closeQOrder
	
}
