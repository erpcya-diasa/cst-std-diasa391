package org.sg.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MDocType;
import org.compiere.model.MOrder;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.sg.model.MXXLoadOrder;

/**
 * @author Carlos Parada, cparada@erpcya.com
 * <li> FR [ 2 ] Add Support to Void Load order when sales order is POS
 * @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/2/generar-factura-al-completar-orden-de
 */
public class AnnulLoadOrder extends SvrProcess {

	/**	Load Order ID		*/
	private int 		m_XX_LoadOrder_ID = 0;
	/**	Load Order			*/
	private MXXLoadOrder loadOrd = null;
	
	private String 		trxName = null;
	
	private Trx 		trx = null;
	
	@Override
	protected void prepare() {
		m_XX_LoadOrder_ID = getRecord_ID();
		trxName = Trx.createTrxName("ALO");
		trx = Trx.get(trxName, true);
	}

	@Override
	protected String doIt() throws Exception {
		loadOrd = new MXXLoadOrder(getCtx(), m_XX_LoadOrder_ID, trxName);
		loadOrd.setXX_Annulled("Y");
		
		if(viewResultPeriod()){
			String resultQuery = viewResult();
			if(resultQuery != null && resultQuery.length() > 0){
				throw new AdempiereException(Msg.translate(Env.getCtx(), "SGErrorsProcess") 
						+ "\n" + resultQuery);
			} else {			
				if(!loadOrd.save()){
					throw new AdempiereException("@XX_LoadOrder_ID@");
				} else {
					trx.commit();
				}
			}	
		} else {
			throw new AdempiereException("@C_Period_ID@" 
					+ " " +Msg.translate(Env.getCtx(), "Closed"));
		}
		
		return "OK";
	}

	/**
	 * Consulta los Documentos Relacionados
	 * @return
	 */
	private String viewResult(){
		String sql = new String("SELECT 'XX_Boleta_ID' tp, wre.DocumentNo, wre.DocStatus, 1 SeqNo " +
				"FROM PP_Order wre " +
				"WHERE wre.DocStatus NOT IN('VO', 'RE') AND wre.XX_LoadOrder_ID = " +
				m_XX_LoadOrder_ID + 
				" UNION " +
				"SELECT 'M_InOut_ID' tp, shi.DocumentNo, shi.DocStatus, 2 SeqNo " +
				"FROM M_InOut shi " +
				"WHERE NOT EXISTS (SELECT 1 FROM C_Order o INNER JOIN C_DocType dt ON (o.C_DocType_ID = dt.C_DocType_ID) WHERE shi.C_Order_ID = o.C_Order_ID AND dt.DocSubTypeSO = '" + MOrder.DocSubTypeSO_POS + "') " +
									" AND shi.DocStatus NOT IN('VO', 'RE') AND shi.XX_LoadOrder_ID = " +
				m_XX_LoadOrder_ID + 
				" UNION " +
				"SELECT 'C_Invoice_ID' tp, inv.DocumentNo, inv.DocStatus, 3 SeqNo " +
				"FROM C_Invoice inv " +
				"INNER JOIN C_DocType dt ON (inv.C_DocType_ID = dt.C_DocType_ID) " +
				"WHERE NOT EXISTS (SELECT 1 FROM C_Order o INNER JOIN C_DocType dt ON (o.C_DocType_ID = dt.C_DocType_ID) WHERE inv.C_Order_ID = o.C_Order_ID AND dt.DocSubTypeSO = '" + MOrder.DocSubTypeSO_POS + "') " +
				" AND dt.DocBaseType = '" + MDocType.DOCBASETYPE_ARInvoice + "' AND inv.DocStatus NOT IN('VO', 'RE') AND inv.XX_LoadOrder_ID = " +
				m_XX_LoadOrder_ID + 
				" UNION " +
				"SELECT 'M_Movement_ID' tp, mov.DocumentNo, mov.DocStatus, 4 SeqNo " +
				"FROM M_Movement mov " +
				"WHERE mov.DocStatus NOT IN('VO', 'RE') AND mov.XX_LoadOrder_ID = " +
				m_XX_LoadOrder_ID + 
				" ORDER BY SeqNo");
		
		log.fine("viewResult SQL = " + sql);
		
		String m_Result = null;
		try {
			StringBuffer m_SB_Add = new StringBuffer();
			PreparedStatement pstmt = DB.prepareStatement(sql, trxName);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				m_SB_Add.append(Msg.translate(Env.getCtx(), rs.getString("tp")) + " = " + rs.getString("DocumentNo") + " ");
				m_SB_Add.append(Msg.translate(Env.getCtx(), "DocStatus") + " = " + rs.getString("DocStatus")  + "\n");
			}
			rs.close();
			pstmt.close();
			m_Result = m_SB_Add.toString();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, m_Result, e);
		} 
		return m_Result;
	}
	
	/**
	 * Verifica si el periodo esta abierto
	 * @return
	 */
	private boolean viewResultPeriod(){
		String sql = new String("SELECT p.* " +
				"FROM C_Period p " +
				"INNER JOIN C_PeriodControl pc ON(pc.C_Period_ID = p.C_Period_ID) " +
				"WHERE pc.DocBaseType = 'SOO' " +
				"AND pc.PeriodStatus = 'O' " +
				"AND pc.AD_Client_ID = ? " +
				"AND p.StartDate <= ? AND p.EndDate >= ?");
		
		log.fine("viewResultPeriod SQL = " + sql);
		try {
			PreparedStatement pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, getAD_Client_ID());
			pstmt.setTimestamp(2, loadOrd.getDateDoc());
			pstmt.setTimestamp(3, loadOrd.getDateDoc());
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()){
				return true;
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, null, e);
		} 
		return false;
	}
	
}
