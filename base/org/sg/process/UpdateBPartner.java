/**
 * @finalidad Actualizar el Socio de Negocio en el
 * Ticket de Vigilancia, Orden de Calidad, Pesada de 
 * meterial, Orden de Compra y Recepcion de material.
 * @author Yamel Senih
 * @date 18/11/2011
 */
package org.sg.process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInOut;
import org.compiere.model.MOrder;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.eevolution.model.MPPOrder;
import org.sg.model.MXXTicketVigilancia;

/**
 * @author Yamel Senih 18/11/2011, 17:39:30
 *
 */
public class UpdateBPartner extends SvrProcess {

	/**	Ticket de Vigilancia		*/
	private int		p_XX_Ticket_Vigilancia_ID = 0;
	
	/**	Business Partner			*/
	private int		p_C_BPartner_ID = 0;
	
	/**	Business Partner	Location*/
	private int		p_C_BPartner_Location_ID = 0;
	
	/**	Shipper						*/
	private int		p_M_Shipper_ID = 0;

	/**	Driver						*/
	private int		p_XX_Conductor_ID = 0;
	
	/**	Car					*/
	private int		p_XX_Vehiculo_ID = 0;
	
	/**	Document Quality			*/
	private int		m_DocumentQuality_ID = 0;
	
	/**	PPOrder						*/
	private int		m_PP_Order_ID = 0;
	
	/**	Order						*/
	private int		m_C_Order_ID = 0;
	
	/**	Receipt						*/
	private int		m_M_InOut_ID = 0;
	
	/**	Description					*/
	private String p_Description = null;
	
	/**	Summary						*/
	private StringBuffer summary = new StringBuffer();
	
	/**	Errores						*/
	private int 		m_Errors = 0;
	
	private int			m_Ticket_Updated = 0;
	private int			m_DocumentQ_Updated = 0;
	private int			m_PPOrder_Updated = 0;
	private int			m_Order_Updated = 0;
	private int			m_Receipt_Updated = 0;
	
	private String 		trxName = null;
	private Trx 		trx = null;
	
	/**	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("XX_Ticket_Vigilancia_ID"))
				p_XX_Ticket_Vigilancia_ID = para.getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para.getParameterAsInt();
			else if (name.equals("C_BPartner_Location_ID"))
				p_C_BPartner_Location_ID = para.getParameterAsInt();
			else if (name.equals("M_Shipper_ID"))
				p_M_Shipper_ID = para.getParameterAsInt();
			else if (name.equals("XX_Conductor_ID"))
				p_XX_Conductor_ID = para.getParameterAsInt();
			else if (name.equals("XX_Vehiculo_ID"))
				p_XX_Vehiculo_ID = para.getParameterAsInt();
			else if (name.equals("Description"))
				p_Description = (String) para.getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
			log.info(name + " = " + para.getParameter());
		}
		trxName = Trx.createTrxName("UpBP");
		trx = Trx.get(trxName, true);
	}

	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()0050769333443
	 */
	@Override
	protected String doIt() throws Exception {
		String msg = null;
		String sql = new String("SELECT tv.XX_Ticket_Vigilancia_ID, ac.PP_Order_ID DocumentQuality_ID, pm.PP_Order_ID, oc.C_Order_ID, re.M_InOut_ID " +
				"FROM XX_Ticket_Vigilancia tv " +
				"LEFT JOIN PP_Order ac ON(tv.XX_Ticket_Vigilancia_ID = ac.XX_Ticket_Vigilancia_ID) " +
				"LEFT JOIN PP_Order pm ON(ac.PP_Order_ID = pm.DocumentQuality_ID) " +
				"LEFT JOIN C_OrderLine loc ON(loc.PP_Order_ID = pm.PP_Order_ID) " +
				"LEFT JOIN C_Order oc ON(oc.C_Order_ID = loc.C_Order_ID) " +
				"LEFT JOIN M_InOutLine lre ON(lre.C_OrderLine_ID = loc.C_OrderLine_ID) " +
				"LEFT JOIN M_InOut re ON(re.M_InOut_ID = lre.M_InOut_ID) " +
				"WHERE pm.AD_Client_ID = ? " +
				"AND tv.XX_Ticket_Vigilancia_ID = ? " +
				"AND (pm.M_Product_ID = loc.M_Product_ID OR loc.M_Product_ID IS NULL) " +
				"ORDER BY tv.XX_Ticket_Vigilancia_ID");
		log.info(sql.toString());
			
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			int index = 1;
			pstmt.setInt(index++, getAD_Client_ID());
			pstmt.setInt(index++, p_XX_Ticket_Vigilancia_ID);
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next ()){
				//	System.out.println("Hola " + rs.getString("DocumentNo"));
				m_DocumentQuality_ID = rs.getInt("DocumentQuality_ID");
				log.info("DocumetQuality_ID = " + m_DocumentQuality_ID);
				m_PP_Order_ID = rs.getInt("PP_Order_ID");
				log.info("PP_Order_ID = " + m_PP_Order_ID);
				m_C_Order_ID = rs.getInt("C_Order_ID");
				log.info("C_Order_ID = " + m_C_Order_ID);
				m_M_InOut_ID = rs.getInt("M_InOut_ID");
				log.info("M_InOut_ID = " + m_M_InOut_ID);
					
				msg = verExiste();
				if(msg == null) {
					updateAll();
				} else {
					trx.rollback();
					break;
				}
				
			}
			if(m_Errors == 0)
				trx.commit();
		}catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
			throw new AdempiereException("SQL " + e.getMessage());
		}

		summary.append("@XX_Ticket_Vigilancia_ID@ = ");
		summary.append(m_Ticket_Updated);
		summary.append(" @DocumentQuality_ID@ = ");
		summary.append(m_DocumentQ_Updated);
		summary.append(" @XX_Boleta_ID@ = ");
		summary.append(m_PPOrder_Updated);
		summary.append(" @C_Order_ID@ = ");
		summary.append(m_Order_Updated);
		summary.append(" @M_InOut_ID@ = ");
		summary.append(m_Receipt_Updated);
		summary.append(" Errors = ");
		summary.append(m_Errors);
		
		addLog(summary.toString());
		
		if(msg == null)
			return  summary.toString();
		return  msg;
	}

	/**
	 * Veri
	 * @return
	 */
	private String verExiste(){
		
		int liquidation = 0;
		int invoice = 0;
		
		String sql = new String("SELECT " +
				"(SELECT count(ll.*) " +
				"FROM XX_LiquidationLine ll " +
				"WHERE ll.XX_Boleta_ID = ?) Liquidation, " +
				"(SELECT count(lf.*) " +
				"FROM C_Invoice f " +
				"INNER JOIN C_InvoiceLine lf ON(lf.C_Invoice_ID = f.C_Invoice_ID) " +
				"INNER JOIN M_InOutLine lr ON(lr.M_InOutLine_ID = lf.M_InOutLine_ID) " +
				"INNER JOIN M_InOut r ON(lr.M_InOut_ID = r.M_InOut_ID) " +
				"WHERE r.M_InOut_ID = ?) Invoice");
		
		log.info("VerExiste = " + sql.toString());
		System.err.println(m_PP_Order_ID + " - " + m_M_InOut_ID);
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			int index = 1;
			pstmt.setInt(index++, m_PP_Order_ID);
			
			pstmt.setInt(index++, m_M_InOut_ID);
			
			ResultSet rs = pstmt.executeQuery();
			if (rs.next ()){
				liquidation = rs.getInt("Liquidation");
				log.info("Liquidation = " + liquidation);
				invoice = rs.getInt("Invoice");
				log.info("Invoice = " + invoice);				
			}
		}catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
			throw new AdempiereException("SQL " + e.getMessage());
		}
		if(liquidation> 0)
			return Msg.translate(getCtx(), "SGLiquidated");
		else if(invoice > 0)
			return Msg.translate(getCtx(), "SGInvoiced");
		return null;
	}
	
	/**
	 * Actualiza todos los registros con los parametros del proceso
	 * @author Yamel Senih 18/11/2011, 20:01:57
	 * @return void
	 */
	private void updateAll(){
		MXXTicketVigilancia m_Ticket = new MXXTicketVigilancia(getCtx(), p_XX_Ticket_Vigilancia_ID, trxName);
		m_Ticket.setC_BPartner_ID(p_C_BPartner_ID);
		m_Ticket.setC_BPartner_Location_ID(p_C_BPartner_Location_ID);
		if(m_Ticket.getDescription() != null)
			m_Ticket.setDescription(m_Ticket.getDescription() + " [" + p_Description + "]");
		else
			m_Ticket.setDescription("[" + p_Description + "]");
		if(m_Ticket.save(trxName)){
			m_Ticket_Updated++;
			if(m_DocumentQuality_ID != 0){
				updateDocumentQuality();
			}
		} else {
			m_Errors++;
			trx.rollback();
			throw new AdempiereException("@XX_Ticket_Vigilancia_ID@");
		}
	}
	
	/**
	 * Actualiza el Análisis de Calidad
	 * @author Yamel Senih 18/11/2011, 20:26:32
	 * @return void
	 */
	private void updateDocumentQuality(){
		MPPOrder m_DocumentQ = new MPPOrder(getCtx(), m_DocumentQuality_ID, trxName);
		m_DocumentQ.set_ValueOfColumn("C_BPartner_ID", p_C_BPartner_ID);
		if(m_DocumentQ.getDescription() != null)
			m_DocumentQ.setDescription(m_DocumentQ.getDescription() + " [" + p_Description + "]");
		else
			m_DocumentQ.setDescription("[" + p_Description + "]");
		if(m_DocumentQ.save(trxName)){
			m_DocumentQ_Updated++;
			if(m_PP_Order_ID != 0){
				updatePPOrder();
			}
		} else {
			m_Errors++;
			trx.rollback();
			throw new AdempiereException("@DocumentQuality_ID@");
		}
	}
	
	/**
	 * Acualiza la Pesada de Material
	 * @author Yamel Senih 18/11/2011, 20:34:41
	 * @return void
	 */
	private void updatePPOrder(){
		MPPOrder m_Boleta = new MPPOrder(getCtx(), m_PP_Order_ID, trxName);
		m_Boleta.set_ValueOfColumn("C_BPartner_ID", p_C_BPartner_ID);
		if(m_Boleta.getDescription() != null)
			m_Boleta.setDescription(m_Boleta.getDescription() + " [" + p_Description + "]");
		else
			m_Boleta.setDescription("[" + p_Description + "]");
		if(m_Boleta.save(trxName)){
			m_PPOrder_Updated++;
			if(m_C_Order_ID != 0){
				updateOrder();
			}
		} else {
			m_Errors++;
			trx.rollback();
			throw new AdempiereException("@XX_Boleta_ID@");
		}
	}
	
	/**
	 * Actualiza la Orden de Compra
	 * @author Yamel Senih 18/11/2011, 20:46:57
	 * @return void
	 */
	private void updateOrder(){
		MOrder m_order = new MOrder(getCtx(), m_C_Order_ID, trxName);
		m_order.setC_BPartner_ID(p_C_BPartner_ID);
		m_order.setC_BPartner_Location_ID(p_C_BPartner_Location_ID);
		if(m_order.getDescription() != null)
			m_order.setDescription(m_order.getDescription() + " [" + p_Description + "]");
		else
			m_order.setDescription("[" + p_Description + "]");
		if(m_order.save(trxName)){
			m_Order_Updated++;
			if(m_M_InOut_ID != 0){
				updateReceipt();
			}
		} else {
			m_Errors++;
			trx.rollback();
			throw new AdempiereException("@C_Order_ID@");
		}
	}
	
	/**
	 * Actualiza la Recepcion de Material
	 * @author Yamel Senih 18/11/2011, 20:48:59
	 * @return void
	 */
	private void updateReceipt(){
		MInOut m_InOut = new MInOut(getCtx(), m_M_InOut_ID, trxName);
		m_InOut.setC_BPartner_ID(p_C_BPartner_ID);
		m_InOut.setC_BPartner_Location_ID(p_C_BPartner_Location_ID);
		if(m_InOut.getDescription() != null)
			m_InOut.setDescription(m_InOut.getDescription() + " [" + p_Description + "]");
		else
			m_InOut.setDescription("[" + p_Description + "]");
		if(m_InOut.save(trxName)){
			m_Receipt_Updated++;
		} else {
			m_Errors++;
			trx.rollback();
			throw new AdempiereException("@M_InOut_ID@");
		}
	}
	
}
