package org.sg.process;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.apache.ecs.xhtml.bdo;
import org.compiere.model.MQuery;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.report.MReport;
import org.compiere.report.MReportLine;
import org.compiere.report.MReportTree;
import org.compiere.util.DB;

/**
 *	Precio Ponderado
 *	
 *  @author Carlos Parada
 *
 **/
public class XXPrecio_Ponderado extends SvrProcess{
	private Integer nAd_Client_Id=0;
	private Integer nAd_Org_ID=0;
	private Integer nM_Product_ID=0;
	private Integer nM_Product_ID_to=0;
	private Integer nM_Locator_ID = 0;
	private Integer nM_Product_Category_ID = 0;
	private Date dFechaDesde;
	private Date dFechaHasta;
	
	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		
		ProcessInfoParameter[] lArrParametros = getParameter();
		int lnTotalParametros = lArrParametros.length;
		String lcParametro = new String("");
		for (int i=0;i<lnTotalParametros;i++)
		{
			
			lcParametro = lArrParametros[i].getParameterName();
			//ID de la instalacion
			if (lcParametro.toLowerCase().equals("ad_client_id"))
				nAd_Client_Id = ((BigDecimal)(lArrParametros[i].getParameter()==null?new BigDecimal(0):lArrParametros[i].getParameter())).intValue();
			//id de la Organizacion 
			if (lcParametro.toLowerCase().equals("ad_org_id"))
				nAd_Org_ID = ((BigDecimal)(lArrParametros[i].getParameter()==null?new BigDecimal(0):lArrParametros[i].getParameter())).intValue();
			
			//ids de los productos Seleccionados
			if (lcParametro.toLowerCase().equals("m_product_id"))
			{
				nM_Product_ID = ((BigDecimal)(lArrParametros[i].getParameter()==null?new BigDecimal(0):lArrParametros[i].getParameter())).intValue();
				nM_Product_ID_to=((BigDecimal)(lArrParametros[i].getParameter_To()==null?new BigDecimal(0):lArrParametros[i].getParameter_To())).intValue();
			}
			//Rango de Fechas
			if (lcParametro.toLowerCase().equals("dateinvoiced"))
			{
				dFechaDesde = (lArrParametros[i].getParameter()==null?null:new Date(((Timestamp)(lArrParametros[i].getParameter())).getTime()));
				dFechaHasta = (lArrParametros[i].getParameter_To()==null?null:new Date(((Timestamp)(lArrParametros[i].getParameter_To())).getTime()));
			}
			//id de el localizador 
			if (lcParametro.toLowerCase().equals("m_locator_id"))
				nM_Locator_ID = ((BigDecimal)(lArrParametros[i].getParameter()==null?new BigDecimal(0):lArrParametros[i].getParameter())).intValue();
			//id de la categoria de productos
			if (lcParametro.toLowerCase().equals("m_product_category"))
				nM_Product_Category_ID = ((BigDecimal)(lArrParametros[i].getParameter()==null?new BigDecimal(0):lArrParametros[i].getParameter())).intValue();
			
			
		}	
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer("");
		prepare();
		LLenarDatos();
		sb.append("Cliente:"+nAd_Client_Id+"- Organización:"+nAd_Org_ID);
		sb.append("Producto:"+nM_Product_ID+ " y "+ nM_Product_ID_to);
		sb.append("Desde "+dFechaDesde+ " Hasta "+ dFechaHasta);
		log.fine(sb.toString());
		return null;
	}
	protected void LLenarDatos(){
		try {
			PreparedStatement loPstmt = DB.prepareStatement(CrearSql().toString(), null);
			loPstmt.setDate(1, dFechaDesde);
			loPstmt.setDate(2, dFechaHasta);
			loPstmt.setDate(3, dFechaDesde);
			loPstmt.setDate(4, dFechaHasta);
			loPstmt.executeUpdate();
			loPstmt.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.log(Level.SEVERE,e.getMessage().toString());
			//e.printStackTrace();
		}
		
	}
	private StringBuffer CrearSql()
	{
		
		StringBuffer lcCmd = new StringBuffer();
		StringBuffer lcWhereSubConsulta= new StringBuffer();
		StringBuffer lcWhere= new StringBuffer();
		lcCmd.append("insert into T_Precio_Ponderado(ad_client_id,ad_org_id,ad_pinstance_id,c_invoice_id,dateinvoiced,m_product_id,priceactual,qtyinvoiced,totalqtyinvoiced,m_locator_id,m_product_category_id) " +
						" Select  ci.ad_client_id, " +
						"		ci.ad_org_id, " +
						"		"+getAD_PInstance_ID()+", " +
						"		ci.c_invoice_id, " +
						"		ci.dateinvoiced, " +
						"		mp.m_product_id, " +
						"		cil.priceactual, " +
						"		cil.qtyinvoiced, " +
						"		(Select Sum(cils.qtyinvoiced) " + 
						"			from c_invoice cis  " +
						"			inner join c_invoiceline cils on cis.c_invoice_id=cils.c_invoice_id "+
						"			inner join m_product mps on cils.m_product_id=mps.m_product_id "+
						"			Inner Join m_matchinv mmis on mmis.c_invoiceline_id = cils.c_invoiceline_id  "+
						"			Inner Join m_inoutline miols on miols.m_inoutline_id=mmis.m_inoutline_id  "+
						"			where  "+
						"				cis.ad_client_id=ci.ad_client_id  " +
						"				and cis.ad_org_id=ci.ad_org_id  " +
						"				and cis.isactive=ci.isactive  " +
						"				and cis.issotrx=ci.issotrx  " );
		
		lcWhereSubConsulta.append((nM_Product_ID!=0 && nM_Product_ID_to !=0?"			And (cils.m_product_id in ("+nM_Product_ID+","+nM_Product_ID_to+") )  ":""));
		lcWhereSubConsulta.append((nM_Product_ID!=0 && nM_Product_ID_to ==0?"			And (cils.m_product_id = "+nM_Product_ID+" )  ":""));
		lcWhereSubConsulta.append((nM_Product_ID==0 && nM_Product_ID_to !=0?"			And (cils.m_product_id = "+nM_Product_ID_to+" )  ":""));
		lcWhereSubConsulta.append((nM_Product_Category_ID!=0 ?"				mps.m_product_category_id="+nM_Product_Category_ID:""));
		lcWhereSubConsulta.append((nM_Locator_ID!=0 ?"				And miols.m_locator_id="+nM_Locator_ID:""));
		lcWhereSubConsulta.append("			And (cis.dateinvoiced between  ? and ? ) ");
		lcCmd.append(lcWhereSubConsulta.toString());
		
		lcCmd.append(	"		) as Totalamt,miol.m_locator_id,mp.m_product_category_id " +
						" From  " +
						" c_invoice ci " + 
						" Inner Join c_invoiceline cil On ci.c_invoice_id=cil.c_invoice_id " + 
						" Inner Join m_matchinv mmi on mmi.c_invoiceline_id = cil.c_invoiceline_id " +
						" Inner Join m_inoutline miol on miol.m_inoutline_id=mmi.m_inoutline_id " +
						" Inner Join m_product mp On mp.m_product_id = cil.m_product_id  " +
						" Where  " +
						" ci.ad_client_id= " + nAd_Client_Id +
						" And " +
						" ci.ad_org_id= "+ nAd_Org_ID + 
						" And  " +
						" ci.isactive='Y' and cil.isactive='Y' " + 
						" And  " +
						" ci.issotrx='N' " + 
						" And  " +
						" ci.docstatus in ('CO','CL') " );
		lcWhere.append((nM_Product_ID!=0 && nM_Product_ID_to !=0?" And (cil.m_product_id in ("+nM_Product_ID+","+nM_Product_ID_to+") )":""));
		lcWhere.append((nM_Product_ID!=0 && nM_Product_ID_to ==0?" And (cil.m_product_id = "+nM_Product_ID+" )":""));
		lcWhere.append((nM_Product_ID==0 && nM_Product_ID_to !=0?" And (cil.m_product_id = "+nM_Product_ID_to+" )":""));
		lcWhere.append((nM_Product_Category_ID!=0 ?" And mp.m_product_category_id="+nM_Product_Category_ID:""));
		lcWhere.append((nM_Locator_ID!=0 ?" And miol.m_locator_id="+nM_Locator_ID:""));
		lcWhere.append(" And (ci.dateinvoiced between  ? and ? ) ");
		lcCmd.append(lcWhere.toString());
		System.out.println(lcCmd);
			return lcCmd;
	}
	

}
