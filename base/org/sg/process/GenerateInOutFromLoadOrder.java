/**
 * 
 */
package org.sg.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MUOM;
import org.compiere.model.MUOMConversion;
import org.compiere.model.X_C_Order;
import org.compiere.model.X_M_InOut;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.eevolution.model.MPPOrder;
import org.sg.model.MXXLODocGenerated;
import org.sg.model.MXXLoadOrder;
import org.sg.model.MXXLoadOrderLine;

/**
 * @author info-analista2
 *
 */
public class GenerateInOutFromLoadOrder extends SvrProcess {
	/**	Load Order ID		*/
	private int 			p_XX_LoadOrder_ID = 0;
	/**	Sales Order			*/
	private int				m_C_Order_ID = 0;
	/**	Load Order			*/
	private MXXLoadOrder 	m_LoadOrder = null;
	/**	Created				*/
	private int 			m_created = 0;
	/** Received			*/
	private MInOut 			inOut = null;
	/**	Weight Register		*/
	private MPPOrder		m_WeightRegister = null;
	
	/**	Date Inveoice		*/
	private Timestamp		p_ShipDate;
	
	private String 			trxName = null;
	private Trx 			trx = null;
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("XX_LoadOrder_ID"))
				p_XX_LoadOrder_ID = para.getParameterAsInt();
			else if (name.equals("ShipDate"))
				p_ShipDate = (Timestamp)para.getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		trxName = Trx.createTrxName("GM");
		trx = Trx.get(trxName, true);
	}

	/**
	 * Validación en el Proceso de ADempiere
	 * 
	 * 	EXISTS(SELECT lo.DocumentNo, lov.C_Order_ID FROM XX_LoadOrder lo
		INNER JOIN XX_LoadOrderLine lol ON(lol.XX_LoadOrder_ID = lo.XX_LoadOrder_ID)
		INNER JOIN C_OrderLine lov ON(lov.C_OrderLine_ID = lol.C_OrderLine_ID)
		WHERE lo.XX_Annulled = 'N' AND lo.XXIsInternalLoad = 'N'
		AND lo.XX_LoadOrder_ID = XX_LoadOrder.XX_LoadOrder_ID
		AND NOT EXISTS(SELECT 1 FROM XX_LODocGenerated lodg
		INNER JOIN M_InOut mi ON(mi.M_InOut_ID = lodg.M_InOut_ID)
		WHERE lodg.XX_LoadOrderLine_ID = lol.XX_LoadOrderLine_ID
		AND mi.DocStatus IN('CO', 'CL'))
		AND (EXISTS(SELECT * FROM PP_Order pp 
				WHERE lo.XX_LoadOrder_ID = pp.XX_LoadOrder_ID 
				AND ((lo.XXIsBulk = 'N' AND pp.DocStatus IN('DR', 'IP', 'IN', 'VO', 'RE', 'CO', 'CL')) 
					OR (lo.XXIsBulk = 'Y' AND pp.DocStatus IN('CO', 'CL'))))
					OR lo.XXIsWeightRegister = 'N')
		GROUP BY lo.DocumentNo, lov.C_Order_ID)
	 */
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#doIt()
	 */
	@Override
	protected String doIt(){
		String out = null;
		String sql = new String("SELECT lov.C_Order_ID FROM XX_LoadOrder lo " +
				"INNER JOIN XX_LoadOrderLine lol ON(lol.XX_LoadOrder_ID = lo.XX_LoadOrder_ID) " +
				"INNER JOIN C_OrderLine lov ON(lov.C_OrderLine_ID = lol.C_OrderLine_ID) " +
				"WHERE lo.XX_Annulled = 'N' AND lo.XXIsInternalLoad = 'N' " +
				"AND lo.XX_LoadOrder_ID = ? " + 
				"AND NOT EXISTS(SELECT 1 FROM XX_LODocGenerated lodg " + 
				"	INNER JOIN M_InOut mi ON(mi.M_InOut_ID = lodg.M_InOut_ID) " + 
				//"	LEFT JOIN PP_Order pp ON(pp.XX_LoadOrder_ID = lo.XX_LoadOrder_ID) " +
				"	WHERE lodg.XX_LoadOrderLine_ID = lol.XX_LoadOrderLine_ID " + 
				"	AND mi.DocStatus IN('CO', 'CL')) " +
				"AND (EXISTS(SELECT * FROM PP_Order pp " +
				"		WHERE lo.XX_LoadOrder_ID = pp.XX_LoadOrder_ID " +
				"		AND ((lo.XXIsBulk = 'N' AND pp.DocStatus IN('DR', 'IP', 'IN', 'VO', 'RE', 'CO', 'CL')) " +
				"			OR (lo.XXIsBulk = 'Y' AND pp.DocStatus IN('CO', 'CL')))) " +
				"			OR lo.XXIsWeightRegister = 'N')" + 
				"GROUP BY lov.C_Order_ID");
		
		m_LoadOrder = new MXXLoadOrder(getCtx(), p_XX_LoadOrder_ID, trxName);
		
		PreparedStatement pstmt = null;
		try	{
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			int index = 1;
			if (p_XX_LoadOrder_ID != 0)
				pstmt.setInt(index++, p_XX_LoadOrder_ID);
			
			out = generarEntregas(pstmt);
			trx.commit();
		}catch (Exception e) {
			trx.rollback();
			log.log(Level.SEVERE, sql.toString(), e);
			out = e.getMessage();
		}
		
		return out;
	}
	
	/**
	 * @author Yamel Senih, 10/05/2011 11:14
	 * Genera las Entregas de materiales
	 * @param pstmt
	 * @return
	 * @throws SQLException 
	 */
	private String generarEntregas(PreparedStatement pstmt) throws Exception{
		ResultSet rs;
		rs = pstmt.executeQuery ();
		findWeightRegister();
		while (rs.next ()){
			m_C_Order_ID = rs.getInt("C_Order_ID");
			MOrder orden = new MOrder (getCtx(), m_C_Order_ID, trxName);
			if(X_C_Order.DOCSTATUS_Completed.equals(orden.getDocStatus())){
				procOrden(orden);
			}
		}

		if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		return "@Created@ = " + m_created;
	}
	
	/**
	 * Crea las Entregas una a una a partir de una Orden de Venta
	 * @return
	 */
	private void procOrden(MOrder orden) throws Exception {
		
		/*
		 * Crea el encabezado de la Entrega a partir de la orden de Venta
		 */
		inOut = new MInOut(orden, 0, orden.getDateOrdered());
		
		MBPartner m_bpartner = MBPartner.get(getCtx(), orden.getC_BPartner_ID());
		
		inOut.setAD_Org_ID(orden.getAD_Org_ID());
		inOut.setM_Warehouse_ID(orden.getM_Warehouse_ID());
		inOut.setC_Activity_ID(orden.getC_Activity_ID());
		inOut.setDateReceived(p_ShipDate);
		inOut.setMovementDate(p_ShipDate);
		inOut.setBPartner(m_bpartner);
		inOut.setSalesRep_ID(orden.getSalesRep_ID());		
		inOut.setDateAcct(p_ShipDate);
		inOut.setC_BPartner_Location_ID(orden.getC_BPartner_Location_ID());
		inOut.setIsSOTrx(true);
		
		//	Reference Load Order
		inOut.set_ValueOfColumn("XX_LoadOrder_ID", p_XX_LoadOrder_ID);
		
		int m_M_Locator_ID = m_LoadOrder.getM_Locator_ID();
		
		if(inOut.save()){
			StringBuffer sql = new StringBuffer("SELECT loc.* FROM XX_LoadOrder oc " +
					"INNER JOIN XX_LoadOrderLine loc ON(loc.XX_LoadOrder_ID = oc.XX_LoadOrder_ID) " +
					"INNER JOIN C_OrderLine lov ON(lov.C_OrderLine_ID = loc.C_OrderLine_ID) " +
					"WHERE oc.Processed = 'N' " +
					"AND lov.C_Order_ID = ? " +
					"AND loc.XX_LoadOrder_ID = ? ");
					
			PreparedStatement pstmt = null;
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			pstmt.setInt(1, orden.getC_Order_ID());
			pstmt.setInt(2, p_XX_LoadOrder_ID);
			ResultSet rs;
			rs = pstmt.executeQuery ();
			while (rs.next ()){
				MXXLoadOrderLine loCarga = new MXXLoadOrderLine(getCtx(), rs, trxName);
				MOrderLine loVenta = new MOrderLine(getCtx(), loCarga.getC_OrderLine_ID(), trxName);
				MInOutLine inOutLine = new MInOutLine(inOut);
				//inOutLine.setM_InOut_ID(entrega.getM_InOut_ID());
				//inOutLine.setAD_Org_ID(orden.getAD_Org_ID());
				//inOutLine.setC_UOM_ID(loVenta.getC_UOM_ID());
				inOutLine.setOrderLine(loVenta, 0, Env.ZERO);
				inOutLine.setC_Activity_ID(loVenta.getC_Activity_ID());
				//	Locator From Load Order
				inOutLine.setM_Locator_ID(m_M_Locator_ID);
				//inOutLine.setQtyEntered(loCarga.getQty());
				//inOutLine.setMovementQty(loCarga.getQty());
				//inOutLine.setQty(loCarga.getQty());
				
				BigDecimal toDeliver = loCarga.getQty();
				
				if(m_WeightRegister != null && m_LoadOrder.isXXIsBulk()){
					BigDecimal netWeight = (BigDecimal) m_WeightRegister.get_Value("Weight3");
					int m_C_UOM_ID = m_WeightRegister.getC_UOM_ID();
					int m_C_UOM_To_ID = loCarga.getM_Product().getC_UOM_ID();
					//	Correcciones de Error en la Conversion Usando el Cache
					//	Correccion de error cuando la unidad de medida es la misma
					//MUOMConversion.getRate(m_C_UOM_ID, m_C_UOM_To_ID);
					BigDecimal rate = MUOMConversion.convert(m_C_UOM_ID, m_C_UOM_To_ID, new BigDecimal(1), false);//Rate(getCtx(), m_C_UOM_ID, m_C_UOM_To_ID);
					BigDecimal qtyWeightRegister = Env.ZERO;
					if(rate != null){
						qtyWeightRegister = netWeight.multiply(rate);
						if(toDeliver.compareTo(qtyWeightRegister) >= 0){
							toDeliver = qtyWeightRegister;
						} else {
							throw new AdempiereException(Msg.translate(getCtx(), "SGQtyWRegisterHi") + " " 
									+ Msg.translate(getCtx(), "QtyOrdered") + " " 
									+ toDeliver.doubleValue() + " " 
									+ Msg.translate(getCtx(), "SGQtyWeightRegister") + " " 
									+ qtyWeightRegister.doubleValue()
									);
						}
					} else {
						MUOM m_uom = new MUOM(getCtx(), m_C_UOM_ID, trxName);
						MUOM m_uom_to = new MUOM(getCtx(), m_C_UOM_To_ID, trxName);
						throw new AdempiereException(Msg.translate(getCtx(), "SGNotConversion") + " " 
								+ Msg.translate(getCtx(), "of") + " "
								+ m_uom.getName() + " " 
								+ Msg.translate(getCtx(), "to") + " " 
								+ m_uom_to.getName()
								);
					}
				}
				
				BigDecimal qtyEntered = toDeliver;
				
				/*loVenta.getQtyOrdered()
						.subtract(loVenta.getQtyDelivered());*/
				
				inOutLine.setQty(toDeliver);	//	Correct UOM
				
				if (loVenta.getQtyEntered().compareTo(loVenta.getQtyOrdered()) != 0)
					qtyEntered = toDeliver
							.multiply(loVenta.getQtyEntered())
							.divide(loVenta.getQtyOrdered(), 12, BigDecimal.ROUND_HALF_UP);
				
				inOutLine.setQtyEntered(qtyEntered);
				
				if(!inOutLine.save())
					throw new AdempiereException("@SaveError@ @M_InOutLine_ID@");
				//	Reference InOut
				/*loCarga.setM_InOut_ID(inOut.getM_InOut_ID());
				if(!loCarga.save())
					throw new AdempiereException("@SaveError@ @XX_LoadOrderLine_ID@");*/
				//	Documents Generated
				MXXLODocGenerated m_DocGen = MXXLODocGenerated.getDocGenInOutInvoice(getCtx(), 
						loCarga.getXX_LoadOrderLine_ID(), trxName, true);
				//m_DocGen.setXX_LoadOrderLine_ID(loCarga.getXX_LoadOrderLine_ID());
				m_DocGen.setM_InOut_ID(inOut.getM_InOut_ID());
				m_DocGen.setM_InOutLine_ID(inOutLine.getM_InOutLine_ID());
				m_DocGen.saveEx();
			}	
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
			m_LoadOrder.setIsDelivered(true);
			if(m_LoadOrder.save()){
				completeReceived();
			} else {
				//trx.rollback();
				throw new AdempiereException("@SaveError@ @XX_LoadOrder_ID@");
			}
		} else {
			throw new AdempiereException("@SaveError@ @M_InOut_ID@");
		}
	}
	
	private void completeReceived(){
		/*
		 * Completa la Entrega de Material
		 */
		inOut.setDocAction(X_M_InOut.DOCACTION_Complete);
		if (inOut != null){
			//entrega.completeIt();
			inOut.processIt(X_M_InOut.DOCACTION_Complete);
			//entrega.setDocStatus(X_M_InOut.DOCSTATUS_Completed);
			
			/*if(!*/inOut.saveEx();/*)
				throw new AdempiereException("@SaveError@ @M_InOut_ID@");//trx.commit();*/
			//else
				//trx.rollback();
			//
			addLog(inOut.getM_InOut_ID(), inOut.getDateAcct(), null, inOut.getDocumentNo());
			m_created++;
		}
	}

	/**
	 * Verifica que exista una Pesada completada relacionada a la Orden de Carga
	 * @author Yamel Senih 19/03/2012, 03:51:27
	 * @return void
	 */
	private void findWeightRegister() throws Exception{
		String sql = new String("SELECT pp.* " +
				"FROM PP_Order pp " +
				"WHERE pp.XX_LoadOrder_ID = ? " +
				"AND pp.DocStatus IN('CO', 'CL')");
		PreparedStatement pstmt = null;
		//try	{
			pstmt = DB.prepareStatement (sql, trxName);
			int index = 1;
			if (p_XX_LoadOrder_ID != 0)
				pstmt.setInt(index++, p_XX_LoadOrder_ID);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()){
				m_WeightRegister = new MPPOrder(getCtx(), rs, trxName);
			}
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		/*}catch (Exception e) {
			pstmt = null;
			log.log(Level.SEVERE, sql.toString(), e);
		}	*/	
	}
	
	
	/*
import java.util.logging.Level;
import org.compiere.model.MPInstance;
import org.compiere.model.MProcess;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Msg;
import test.GenerateInOutFromLoadOrder;
  String mens = "OK";
  String nProcess = "entregas";
  ProcessInfoParameter pi1 = new ProcessInfoParameter("M_ShippingGuide_ID", 1000025,"","","");
  ProcessInfo pi = new ProcessInfo("", 0,0,0);
  pi.setParameter(new ProcessInfoParameter[] {pi1});
  MProcess pr = new Query(A_Ctx, MProcess.Table_Name, "value=?", A_TrxName).setParameters(new Object[]{nProcess}).first();
  if (pr != null) {
    GenerateInOutFromLoadOrder process = new GenerateInOutFromLoadOrder();
    MPInstance mpi = new MPInstance(A_Ctx, 0, null);
    mpi.setAD_Process_ID(pr.get_ID());
    mpi.setRecord_ID(0);
    mpi.save();
    pi.setAD_PInstance_ID(mpi.get_ID());
    boolean res = process.startProcess(A_Ctx, pi, A_Trx);
    if(!res){
      mens = Msg.getMsg(A_Ctx, "SGBadResult");
    }
    mens = "OK";    
  } else {
    mens =  Msg.getMsg(Env.getCtx(), "SGNoProcess");
  }
result = mens;
	 * */
	
}
