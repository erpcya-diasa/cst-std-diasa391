package org.sg.process;

import java.util.logging.Level;

import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceBatchLine;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Trx;
import org.sg.model.MXXDocFiscalPrinterNo;

/**
 * @author Carlos Parada, cparada@erpcya.com
 * <li> FR [ 5 ] Add Support to Batch Print Invoice
 * @see https://bitbucket.org/erpcya-diasa/custom-diasa/issues/5/soporte-para-agregar-lote-a-documento-por
 */
public class GenerateDocumentFromInvoice extends SvrProcess {

	/**	Document Type		*/
	private int documentTypeTargetId = 0;
	/**	From Invoice		*/
	private int p_C_Invoice_ID = 0;
	/**	Description			*/
	private String p_Description = null;
	
	private String 		trxName = null;
	private Trx 		trx = null;
	
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter()){
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("C_DocTypeTarget_ID"))
				documentTypeTargetId = para.getParameterAsInt();
			else if(name.equals("Description"))
				p_Description = (String)para.getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		p_C_Invoice_ID = getRecord_ID();
		trxName = Trx.createTrxName("GDFI");
		trx = Trx.get(trxName, true);
	}

	@Override
	protected String doIt() throws Exception {
		if(documentTypeTargetId == 0)
			throw new IllegalArgumentException("No Document Type");
			
		MInvoice invoiceFrom = new MInvoice(getCtx(), p_C_Invoice_ID, trxName);
		//	Copy
		MInvoice invoice_To = MInvoice.copyFrom(
				invoiceFrom, 
				invoiceFrom.getDateInvoiced(), 
				invoiceFrom.getDateAcct(), 
				documentTypeTargetId, 
				invoiceFrom.isSOTrx(), 
				false, 
				false,
				trxName, 
				false);
		//	Set Load Order
		if(invoiceFrom.get_ValueAsInt("XX_LoadOrder_ID") != 0)
			invoice_To.set_ValueOfColumn("XX_LoadOrder_ID", invoiceFrom.get_ValueAsInt("XX_LoadOrder_ID"));
		//	Set Description
		if(p_Description != null)
			invoice_To.addDescription(p_Description);
		
		invoice_To.set_ValueOfColumn("XX_FiscalNo", null);
		invoice_To.set_ValueOfColumn("PrinterId", null);
		invoice_To.set_ValueOfColumn("XX_PrintFiscalDocument", "N");
		
		MDocType docType = new MDocType(getCtx(), documentTypeTargetId, trxName);
		
		int m_XX_DocFiscalPrinterNo_ID = docType.get_ValueAsInt("XX_DocFiscalPrinterNo_ID");
		
		MXXDocFiscalPrinterNo m_MXXDocFiscalPrinterNo = new MXXDocFiscalPrinterNo(getCtx(), m_XX_DocFiscalPrinterNo_ID, trxName);
		
		if(m_MXXDocFiscalPrinterNo.getXX_DocBaseType_Fiscal() != null 
				&& !m_MXXDocFiscalPrinterNo.getXX_DocBaseType_Fiscal()
				.equals(MXXDocFiscalPrinterNo.XX_DOCBASETYPE_FISCAL_Invoice))
			invoice_To.set_ValueOfColumn("XX_DocAffected", p_C_Invoice_ID);
		else
			invoice_To.set_ValueOfColumn("XX_DocAffected", null);
		//	Save
		if(!invoice_To.save())
			throw new IllegalArgumentException(invoice_To.getProcessMsg());
		//FR [ 5 ]
		addBatch(invoiceFrom, invoice_To);
		trx.commit();
		return invoice_To.getDocumentNo();
	}
	
	/**
	 * FR [ 5 ]
	 * @param invoiceSource
	 * @param invoiceTarget
	 * @return void
	 */
	private void addBatch(MInvoice invoiceSource, MInvoice invoiceTarget){
		MInvoiceBatchLine iBatchLineSource = new Query(getCtx(), MInvoiceBatchLine.Table_Name, "C_Invoice_ID = ? ", get_TrxName())
										.setParameters(invoiceSource.getC_Invoice_ID())
										.first();
		if (iBatchLineSource!= null){
			MInvoiceBatchLine iBatchLineTarget = new MInvoiceBatchLine(getCtx(), 0, get_TrxName());
			MInvoiceBatchLine.copyValues(iBatchLineSource, iBatchLineTarget);
			iBatchLineTarget.setC_Invoice_ID(invoiceTarget.getC_Invoice_ID());
			iBatchLineTarget.save();
		}
	}

}
