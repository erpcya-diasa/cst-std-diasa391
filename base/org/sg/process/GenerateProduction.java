package org.sg.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MTransaction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.sg.model.MProduction;
import org.sg.model.MProductionLine;
import org.sg.model.MProductionPlan;

public class GenerateProduction extends SvrProcess{

	/**	PPOrder						*/
	private int m_PP_Order_ID = 0;
	/**	InOut						*/
	private int m_M_InOut_ID = 0;
	/**	Product						*/
	private int p_M_Product_ID = 0;
	/**	Product To					*/
	private int p_M_Product_To_ID = 0;
	/**	InOut Line					*/
	private int m_M_InOutLine_ID = 0;
	/**	Weight 						*/
	private BigDecimal pAcond = Env.ZERO;
	/**	Document Status Receipt		*/
	private String m_DocStatus = null;
	/**	Summary						*/
	private StringBuffer summary = new StringBuffer();
	/**	Errores					*/
	private int 		m_Errors = 0;
	/**	Created				*/
	private int 		m_created_Movement = 0;
	/**	Annulated			*/
	private int			m_annulated_Movement = 0;
	private String 		trxName = null;
	private Trx 		trx = null;
	
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			if (name.equals("M_Product_ID"))
				p_M_Product_ID = para.getParameterAsInt();
			else if (name.equals("M_Product_To_ID"))
				p_M_Product_To_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
		trxName = Trx.createTrxName("GP");
		trx = Trx.get(trxName, true);
	}

	@Override
	protected String doIt() {
		String sql = new String("SELECT br.PP_Order_ID, br.DocumentNo, lrec.M_InOutLine_ID, rec.M_InOut_ID, rec.DocStatus, " +
				"lrec.MovementQty, round(pAcond, 2) pAcond " +
				"FROM XX_RV_Boleta_Romana br " +
				"INNER JOIN C_DocType td ON(td.C_DocType_ID = br.C_DocType_ID) " +
				"INNER JOIN C_OrderLine loc ON(loc.PP_Order_ID = br.PP_Order_ID AND br.M_Product_ID = loc.M_Product_ID) " +
				"INNER JOIN M_InOutLine lrec ON(lrec.C_OrderLine_ID = loc.C_OrderLine_ID) " +
				"INNER JOIN M_InOut rec ON(rec.M_InOut_ID = lrec.M_InOut_ID) " +
				"WHERE td.XX_Tipo_Q_Pesada = 'MP' " +
				"AND br.DocStatus IN('CO','CL', 'VO') " +
				"AND rec.DocStatus IN('CO','CL', 'VO', 'RE') " +
				//"AND lrec.XXIsProduced = 'N' " +
				"AND NOT EXISTS(SELECT p.* FROM M_Production p WHERE p.M_InOutLine_ID = lrec.M_InOutLine_ID) " +
				"AND EXISTS(SELECT p.* FROM C_Period p " +
				"INNER JOIN C_PeriodControl pc ON(pc.C_Period_ID = p.C_Period_ID) " +
				"WHERE pc.DocBaseType = 'MQO' AND pc.PeriodStatus = 'O' " +
				"AND pc.AD_Client_ID = br.AD_Client_ID AND p.StartDate <= br.DateOrdered " +
				"AND p.EndDate >= br.DateOrdered) " +
				"AND br.AD_Client_ID = ? " +
				"AND br.M_Product_ID = ? ");
		log.info(sql.toString());

		System.err.println(sql);
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), trxName);
			int index = 1;
			pstmt.setInt(index++, getAD_Client_ID());
			pstmt.setInt(index++, p_M_Product_ID);
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next ()){
				System.out.println("Hola " + rs.getString("DocumentNo"));
				m_PP_Order_ID = rs.getInt("PP_Order_ID");
				m_M_InOut_ID = rs.getInt("M_InOut_ID");
				m_M_InOutLine_ID = rs.getInt("M_InOutLine_ID");
				m_DocStatus = rs.getString("DocStatus");
				pAcond = rs.getBigDecimal("pAcond");
				
				//	Create Production
				createProduction();
			}
			
		}catch (Exception e) {
			log.log(Level.SEVERE, sql, e);
			throw new AdempiereException("SQL " + e.getMessage());
		}
		
		summary.append("@Created@ = ");
		summary.append(m_created_Movement);
		summary.append(" @Voided@ = ");
		summary.append(m_annulated_Movement);
		summary.append(" Errors = ");
		summary.append(m_Errors);
		
		addLog(summary.toString());
		
		return  summary.toString();
	}

	/**
	 * Crea la producción a Partir de las recepciones
	 */
	private void createProduction(){

		//	InOut
		MInOut m_inOut = new MInOut(getCtx(), m_M_InOut_ID, trxName);
		//System.out.println("RMP " + m_inOut);
		MInOutLine m_inOutLine = new MInOutLine(getCtx(), m_M_InOutLine_ID, trxName);
		BigDecimal sign = new BigDecimal(m_inOutLine.getMovementQty().signum());

		//	Cambio de Signo
		pAcond = pAcond.multiply(sign);
		
		//	Calculo Peso Acondicionado
		if(pAcond == null)
			throw new AdempiereException("@WeightAcond@");
		//	Production
		MProduction m_production = new MProduction(getCtx(), 0, trxName);
		m_production.setMovementDate(m_inOut.getMovementDate());
		//m_production.setC_Activity_ID(m_inOut.getC_Activity_ID());
		//m_production.setDescription(m_inOut.getDescription());
		m_production.setName(Msg.translate(getCtx(), "M_InOut_ID") + " - " + m_inOut.getDocumentNo() + " - " + m_DocStatus);
		//	PP_Order
		m_production.set_ValueOfColumn("XX_Boleta_ID", m_PP_Order_ID);
		//	Line Receipt
		m_production.set_ValueOfColumn("M_InOutLine_ID", m_M_InOutLine_ID);
		
		m_production.setIsCreated(true);
		//	Production Save
		if(m_production.save()){//System.out.println("Saved " + m_production);
			MProductionPlan m_productionPlan = new MProductionPlan(getCtx(), 0, trxName);
			
			m_productionPlan.setM_Production_ID(m_production.getM_Production_ID());
			
			m_productionPlan.setM_Product_ID(p_M_Product_To_ID);
			m_productionPlan.setM_Locator_ID(m_inOutLine.getM_Locator_ID());
			m_productionPlan.setProductionQty(pAcond);
			
			m_productionPlan.setLine(10);
			//	Productio Plan
			if(m_productionPlan.save()){//System.out.println("Saved " + m_productionPlan);
				MProductionLine m_productionLine = new MProductionLine(getCtx(), 0, trxName);
				m_productionLine.setM_ProductionPlan_ID(m_productionPlan.getM_ProductionPlan_ID());
				
				m_productionLine.setM_Locator_ID(m_inOutLine.getM_Locator_ID());
				
				m_productionLine.setM_Product_ID(p_M_Product_To_ID);
				m_productionLine.setMovementQty(pAcond);
				m_productionLine.setLine(10);
				
				//	Production Line
				if(m_productionLine.save()){//System.out.println("Saved " + m_productionLine);
					MProductionLine m_productionLineHum = new MProductionLine(getCtx(), 0, trxName);
					m_productionLineHum.setM_ProductionPlan_ID(m_productionPlan.getM_ProductionPlan_ID());
					
					m_productionLineHum.setM_Locator_ID(m_inOutLine.getM_Locator_ID());
					
					m_productionLineHum.setM_Product_ID(m_inOutLine.getM_Product_ID());
					m_productionLineHum.setM_AttributeSetInstance_ID(m_inOutLine.getM_AttributeSetInstance_ID());
					m_productionLineHum.setMovementQty(pAcond.negate());
					m_productionLineHum.setLine(20);
					
					//	Line Production
					if(m_productionLineHum.save()){//System.out.println("Saved " + m_productionLineHum);
						
						MTransaction mtrx = new MTransaction (getCtx(), m_productionLine.getAD_Org_ID(), 
							MTransaction.MOVEMENTTYPE_ProductionPlus, m_productionLine.getM_Locator_ID(),
							m_productionLine.getM_Product_ID(), m_productionLine.getM_AttributeSetInstance_ID(), 
							pAcond, m_production.getMovementDate(), trxName);
						mtrx.setM_ProductionLine_ID(m_productionLine.getM_ProductionLine_ID());
						
						//	Save transaction
						if(mtrx.save()){
							m_productionLine.setProcessed(true);
							if(m_productionLine.save()){
								mtrx = new MTransaction (getCtx(), m_productionLineHum.getAD_Org_ID(), 
									MTransaction.MOVEMENTTYPE_Production_, m_productionLineHum.getM_Locator_ID(),
									m_productionLineHum.getM_Product_ID(), m_productionLineHum.getM_AttributeSetInstance_ID(), 
									pAcond.negate(), m_production.getMovementDate(), trxName);
								mtrx.setM_ProductionLine_ID(m_productionLineHum.getM_ProductionLine_ID());
								
								//	Save transaction
								if(mtrx.save()){
									m_productionLineHum.setProcessed(true);
									if(m_productionLineHum.save()){
										m_production.setProcessed(true);
										m_productionPlan.setProcessed(true);
										if(m_productionPlan.save()){
											if(m_production.save()){//System.out.println("Saved " + m_production);
												//m_inOutLine.set_ValueOfColumn("XXIsProduced", true);
												//if(m_inOutLine.save()){//System.out.println("Saved " + m_inOutLine);
													trx.commit();
													addLog(m_production.getM_Production_ID(), m_production.getMovementDate(), null, m_production.getName());
													if(sign.compareTo(Env.ZERO) < 0)
														m_annulated_Movement++;
													else
														m_created_Movement++;
												/*} else {
													m_Errors++;
													throw new AdempiereException("@M_InOut_ID@");
												}*/
											} else {
												m_Errors++;
												throw new AdempiereException("@M_Production_ID@");
											}
										} else {
											m_Errors++;
											throw new AdempiereException("@M_ProductionPlan_ID@");
										}
									} else {
										m_Errors++;
										throw new AdempiereException("@M_ProductionLine_ID@");
									}
								} else {
									m_Errors++;
									throw new AdempiereException("@M_Transaction_ID@");
								}
							} else {
								m_Errors++;
								throw new AdempiereException("@M_ProductionLine_ID@");
							}
						} else {
							m_Errors++;
							throw new AdempiereException("@M_Transaction_ID@");
						}
					} else {
						m_Errors++;
						throw new AdempiereException("@M_ProductionLine_ID@");
					} 
				} else {
					m_Errors++;
					throw new AdempiereException("@M_ProductionLine_ID@");
				}
			} else {
				m_Errors++;
				throw new AdempiereException("@M_ProductionPlan_ID@");
			}
		} else {
			m_Errors++;
			throw new AdempiereException("@M_Production_ID@");
		}
	}
	
}
